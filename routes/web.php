<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\BanksController;
use App\Http\Controllers\BankBranchesController;
use App\Http\Controllers\DepartmentsController;
use App\Http\Controllers\DesignationsController;
use App\Http\Controllers\QualificationsController;
use App\Http\Controllers\MotherTonguesController;
use App\Http\Controllers\GradesController;
use App\Http\Controllers\CountriesController;
use App\Http\Controllers\StatesController;
use App\Http\Controllers\DistrictsController;
use App\Http\Controllers\CitiesController;
use App\Http\Controllers\PincodesController;
use App\Http\Controllers\StockCategoriesController;
use App\Http\Controllers\TaxManagersController;
use App\Http\Controllers\StockRegistersController;
use App\Http\Controllers\BranchesController;
use App\Http\Controllers\AdvanceSalariesController;
use App\Http\Controllers\OverTimesController;
use App\Http\Controllers\DamagesController;
use App\Http\Controllers\FinesController;
use App\Http\Controllers\EmployeesController;
use App\Http\Controllers\SystemConfigurationsController;
use App\Http\Controllers\UsersController;
use App\Http\Controllers\MasterController;
use App\Http\Controllers\PdfController;
use App\Http\Controllers\SettingsController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\MasterCrudController;
use App\Http\Controllers\BranchTransfersController;
use App\Http\Controllers\AttendancesController;
use App\Http\Controllers\SalariesController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\CreditSalariesController;
use App\Http\Controllers\WalferController;
use App\Http\Controllers\BranchBillsController;
use App\Http\Controllers\CompanyBanksController;
use App\Http\Controllers\EmployeeLeaveHistoriesController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
     return view('welcome');
});


Auth::routes();

Route::get('/home', [HomeController::class, 'index'])->name('home');

Route::get('/report', [HomeController::class, 'report'])->name('report.index');

Route::get('/logout', [App\Http\Controllers\Auth\LoginController::class, 'logout'])->name('logout');


Route::group([
     'prefix' => 'banks',
], function () {
     Route::get('/', [BanksController::class, 'index'])
          ->name('banks.bank.index');
     Route::get('/create', [BanksController::class, 'create'])
          ->name('banks.bank.create');
     Route::get('/show/{bank}', [BanksController::class, 'show'])
          ->name('banks.bank.show')->where('id', '[0-9]+');
     Route::get('/{bank}/edit', [BanksController::class, 'edit'])
          ->name('banks.bank.edit')->where('id', '[0-9]+');
     Route::post('/', [BanksController::class, 'store'])
          ->name('banks.bank.store');
     Route::put('bank/{bank}', [BanksController::class, 'update'])
          ->name('banks.bank.update')->where('id', '[0-9]+');
     Route::delete('/bank/{bank}', [BanksController::class, 'destroy'])
          ->name('banks.bank.destroy')->where('id', '[0-9]+');
});


Route::group([
     'prefix' => 'bank_branches',
], function () {
     Route::get('/', [BankBranchesController::class, 'index'])
          ->name('bank_branches.bank_branch.index');
     Route::get('/create', [BankBranchesController::class, 'create'])
          ->name('bank_branches.bank_branch.create');
     Route::get('/show/{bankBranch}', [BankBranchesController::class, 'show'])
          ->name('bank_branches.bank_branch.show')->where('id', '[0-9]+');
     Route::get('/{bankBranch}/edit', [BankBranchesController::class, 'edit'])
          ->name('bank_branches.bank_branch.edit')->where('id', '[0-9]+');
     Route::post('/', [BankBranchesController::class, 'store'])
          ->name('bank_branches.bank_branch.store');
     Route::put('bank_branch/{bankBranch}', [BankBranchesController::class, 'update'])
          ->name('bank_branches.bank_branch.update')->where('id', '[0-9]+');
     Route::delete('/bank_branch/{bankBranch}', [BankBranchesController::class, 'destroy'])
          ->name('bank_branches.bank_branch.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'departments',
], function () {
     Route::get('/', [DepartmentsController::class, 'index'])
          ->name('departments.department.index');
     Route::get('/create', [DepartmentsController::class, 'create'])
          ->name('departments.department.create');
     Route::get('/show/{department}', [DepartmentsController::class, 'show'])
          ->name('departments.department.show')->where('id', '[0-9]+');
     Route::get('/{department}/edit', [DepartmentsController::class, 'edit'])
          ->name('departments.department.edit')->where('id', '[0-9]+');
     Route::post('/', [DepartmentsController::class, 'store'])
          ->name('departments.department.store');
     Route::put('department/{department}', [DepartmentsController::class, 'update'])
          ->name('departments.department.update')->where('id', '[0-9]+');
     Route::delete('/department/{department}', [DepartmentsController::class, 'destroy'])
          ->name('departments.department.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'designations',
], function () {
     Route::get('/', [DesignationsController::class, 'index'])
          ->name('designations.designation.index');
     Route::get('/create', [DesignationsController::class, 'create'])
          ->name('designations.designation.create');
     Route::get('/show/{designation}', [DesignationsController::class, 'show'])
          ->name('designations.designation.show')->where('id', '[0-9]+');
     Route::get('/{designation}/edit', [DesignationsController::class, 'edit'])
          ->name('designations.designation.edit')->where('id', '[0-9]+');
     Route::post('/', [DesignationsController::class, 'store'])
          ->name('designations.designation.store');
     Route::put('designation/{designation}', [DesignationsController::class, 'update'])
          ->name('designations.designation.update')->where('id', '[0-9]+');
     Route::delete('/designation/{designation}', [DesignationsController::class, 'destroy'])
          ->name('designations.designation.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'qualifications',
], function () {
     Route::get('/', [QualificationsController::class, 'index'])
          ->name('qualifications.qualification.index');
     Route::get('/create', [QualificationsController::class, 'create'])
          ->name('qualifications.qualification.create');
     Route::get('/show/{qualification}', [QualificationsController::class, 'show'])
          ->name('qualifications.qualification.show')->where('id', '[0-9]+');
     Route::get('/{qualification}/edit', [QualificationsController::class, 'edit'])
          ->name('qualifications.qualification.edit')->where('id', '[0-9]+');
     Route::post('/', [QualificationsController::class, 'store'])
          ->name('qualifications.qualification.store');
     Route::put('qualification/{qualification}', [QualificationsController::class, 'update'])
          ->name('qualifications.qualification.update')->where('id', '[0-9]+');
     Route::delete('/qualification/{qualification}', [QualificationsController::class, 'destroy'])
          ->name('qualifications.qualification.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'mother_tongues',
], function () {
     Route::get('/', [MotherTonguesController::class, 'index'])
          ->name('mother_tongues.mother_tongue.index');
     Route::get('/create', [MotherTonguesController::class, 'create'])
          ->name('mother_tongues.mother_tongue.create');
     Route::post('/', [MotherTonguesController::class, 'store'])
          ->name('mother_tongues.mother_tongue.store');
     Route::get('/show/{motherTongue}', [MotherTonguesController::class, 'show'])
          ->name('mother_tongues.mother_tongue.show')->where('id', '[0-9]+');
     Route::get('/{motherTongue}/edit', [MotherTonguesController::class, 'edit'])
          ->name('mother_tongues.mother_tongue.edit')->where('id', '[0-9]+');
     Route::put('mother_tongue/{motherTongue}', [MotherTonguesController::class, 'update'])
          ->name('mother_tongues.mother_tongue.update')->where('id', '[0-9]+');
     Route::delete('/mother_tongue/{motherTongue}', [MotherTonguesController::class, 'destroy'])
          ->name('mother_tongues.mother_tongue.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'grades',
], function () {
     Route::get('/', [GradesController::class, 'index'])
          ->name('grades.grade.index');
     Route::get('/create', [GradesController::class, 'create'])
          ->name('grades.grade.create');
     Route::get('/show/{grade}', [GradesController::class, 'show'])
          ->name('grades.grade.show')->where('id', '[0-9]+');
     Route::get('/{grade}/edit', [GradesController::class, 'edit'])
          ->name('grades.grade.edit')->where('id', '[0-9]+');
     Route::post('/', [GradesController::class, 'store'])
          ->name('grades.grade.store');
     Route::put('grade/{grade}', [GradesController::class, 'update'])
          ->name('grades.grade.update')->where('id', '[0-9]+');
     Route::delete('/grade/{grade}', [GradesController::class, 'destroy'])
          ->name('grades.grade.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'countries',
], function () {
     Route::get('/', [CountriesController::class, 'index'])
          ->name('countries.country.index');
     Route::get('/create', [CountriesController::class, 'create'])
          ->name('countries.country.create');
     Route::get('/show/{country}', [CountriesController::class, 'show'])
          ->name('countries.country.show')->where('id', '[0-9]+');
     Route::get('/{country}/edit', [CountriesController::class, 'edit'])
          ->name('countries.country.edit')->where('id', '[0-9]+');
     Route::post('/', [CountriesController::class, 'store'])
          ->name('countries.country.store');
     Route::put('country/{country}', [CountriesController::class, 'update'])
          ->name('countries.country.update')->where('id', '[0-9]+');
     Route::delete('/country/{country}', [CountriesController::class, 'destroy'])
          ->name('countries.country.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'states',
], function () {
     Route::get('/', [StatesController::class, 'index'])
          ->name('states.state.index');
     Route::get('/create', [StatesController::class, 'create'])
          ->name('states.state.create');
     Route::get('/show/{state}', [StatesController::class, 'show'])
          ->name('states.state.show')->where('id', '[0-9]+');
     Route::get('/{state}/edit', [StatesController::class, 'edit'])
          ->name('states.state.edit')->where('id', '[0-9]+');
     Route::post('/', [StatesController::class, 'store'])
          ->name('states.state.store');
     Route::put('state/{state}', [StatesController::class, 'update'])
          ->name('states.state.update')->where('id', '[0-9]+');
     Route::delete('/state/{state}', [StatesController::class, 'destroy'])
          ->name('states.state.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'districts',
], function () {
     Route::get('/', [DistrictsController::class, 'index'])
          ->name('districts.district.index');
     Route::get('/create', [DistrictsController::class, 'create'])
          ->name('districts.district.create');
     Route::get('/show/{district}', [DistrictsController::class, 'show'])
          ->name('districts.district.show')->where('id', '[0-9]+');
     Route::get('/{district}/edit', [DistrictsController::class, 'edit'])
          ->name('districts.district.edit')->where('id', '[0-9]+');
     Route::post('/', [DistrictsController::class, 'store'])
          ->name('districts.district.store');
     Route::put('district/{district}', [DistrictsController::class, 'update'])
          ->name('districts.district.update')->where('id', '[0-9]+');
     Route::delete('/district/{district}', [DistrictsController::class, 'destroy'])
          ->name('districts.district.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'cities',
], function () {
     Route::get('/', [CitiesController::class, 'index'])
          ->name('cities.city.index');
     Route::get('/create', [CitiesController::class, 'create'])
          ->name('cities.city.create');
     Route::get('/show/{city}', [CitiesController::class, 'show'])
          ->name('cities.city.show')->where('id', '[0-9]+');
     Route::get('/{city}/edit', [CitiesController::class, 'edit'])
          ->name('cities.city.edit')->where('id', '[0-9]+');
     Route::post('/', [CitiesController::class, 'store'])
          ->name('cities.city.store');
     Route::put('city/{city}', [CitiesController::class, 'update'])
          ->name('cities.city.update')->where('id', '[0-9]+');
     Route::delete('/city/{city}', [CitiesController::class, 'destroy'])
          ->name('cities.city.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'pincodes',
], function () {
     Route::get('/', [PincodesController::class, 'index'])
          ->name('pincodes.pincode.index');
     Route::get('/create', [PincodesController::class, 'create'])
          ->name('pincodes.pincode.create');
     Route::get('/show/{pincode}', [PincodesController::class, 'show'])
          ->name('pincodes.pincode.show')->where('id', '[0-9]+');
     Route::get('/{pincode}/edit', [PincodesController::class, 'edit'])
          ->name('pincodes.pincode.edit')->where('id', '[0-9]+');
     Route::post('/', [PincodesController::class, 'store'])
          ->name('pincodes.pincode.store');
     Route::put('pincode/{pincode}', [PincodesController::class, 'update'])
          ->name('pincodes.pincode.update')->where('id', '[0-9]+');
     Route::delete('/pincode/{pincode}', [PincodesController::class, 'destroy'])
          ->name('pincodes.pincode.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'stock_categories',
], function () {
     Route::get('/', [StockCategoriesController::class, 'index'])
          ->name('stock_categories.stock_category.index');
     Route::get('/create', [StockCategoriesController::class, 'create'])
          ->name('stock_categories.stock_category.create');
     Route::get('/show/{stockCategory}', [StockCategoriesController::class, 'show'])
          ->name('stock_categories.stock_category.show')->where('id', '[0-9]+');
     Route::get('/{stockCategory}/edit', [StockCategoriesController::class, 'edit'])
          ->name('stock_categories.stock_category.edit')->where('id', '[0-9]+');
     Route::post('/', [StockCategoriesController::class, 'store'])
          ->name('stock_categories.stock_category.store');
     Route::put('stock_category/{stockCategory}', [StockCategoriesController::class, 'update'])
          ->name('stock_categories.stock_category.update')->where('id', '[0-9]+');
     Route::delete('/stock_category/{stockCategory}', [StockCategoriesController::class, 'destroy'])
          ->name('stock_categories.stock_category.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'tax_managers',
], function () {
     Route::get('/', [TaxManagersController::class, 'index'])
          ->name('tax_managers.tax_manager.index');
     Route::get('/create', [TaxManagersController::class, 'create'])
          ->name('tax_managers.tax_manager.create');
     Route::get('/show/{taxManager}', [TaxManagersController::class, 'show'])
          ->name('tax_managers.tax_manager.show')->where('id', '[0-9]+');
     Route::get('/{taxManager}/edit', [TaxManagersController::class, 'edit'])
          ->name('tax_managers.tax_manager.edit')->where('id', '[0-9]+');
     Route::post('/', [TaxManagersController::class, 'store'])
          ->name('tax_managers.tax_manager.store');
     Route::put('tax_manager/{taxManager}', [TaxManagersController::class, 'update'])
          ->name('tax_managers.tax_manager.update')->where('id', '[0-9]+');
     Route::delete('/tax_manager/{taxManager}', [TaxManagersController::class, 'destroy'])
          ->name('tax_managers.tax_manager.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'stock_registers',
], function () {
     Route::get('/', [StockRegistersController::class, 'index'])
          ->name('stock_registers.stock_register.index');
     Route::get('/create', [StockRegistersController::class, 'create'])
          ->name('stock_registers.stock_register.create');
     Route::get('/show/{stockRegister}', [StockRegistersController::class, 'show'])
          ->name('stock_registers.stock_register.show')->where('id', '[0-9]+');
     Route::get('/{stockRegister}/edit', [StockRegistersController::class, 'edit'])
          ->name('stock_registers.stock_register.edit')->where('id', '[0-9]+');
     Route::post('/', [StockRegistersController::class, 'store'])
          ->name('stock_registers.stock_register.store');
     Route::put('stock_register/{stockRegister}', [StockRegistersController::class, 'update'])
          ->name('stock_registers.stock_register.update')->where('id', '[0-9]+');
     Route::delete('/stock_register/{stockRegister}', [StockRegistersController::class, 'destroy'])
          ->name('stock_registers.stock_register.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'branches',
], function () {
     Route::get('/', [BranchesController::class, 'index'])
          ->name('branches.branch.index');
     Route::get('/create', [BranchesController::class, 'create'])
          ->name('branches.branch.create');
     Route::get('/show/{branch}', [BranchesController::class, 'show'])
          ->name('branches.branch.show')->where('id', '[0-9]+');
     Route::get('/{branch}/edit', [BranchesController::class, 'edit'])
          ->name('branches.branch.edit')->where('id', '[0-9]+');
     Route::post('/', [BranchesController::class, 'store'])
          ->name('branches.branch.store');
     Route::put('branch/{branch}', [BranchesController::class, 'update'])
          ->name('branches.branch.update')->where('id', '[0-9]+');
     Route::delete('/branch/{branch}', [BranchesController::class, 'destroy'])
          ->name('branches.branch.destroy')->where('id', '[0-9]+');
});

Route::delete('branches/branchdesignation/{id}', [BranchesController::class, 'destroyBranchDesignation'])
     ->name('branches.branchdesignation.destroyBranchDesignation')->where('id', '[0-9]+');

Route::delete('branches/workorder/{id}', [BranchesController::class, 'destroyWorkOrder'])
     ->name('branches.workorder.destroyWorkOrder')->where('id', '[0-9]+');

Route::group([
     'prefix' => 'advance_salaries',
], function () {
     Route::get('/', [AdvanceSalariesController::class, 'index'])
          ->name('advance_salaries.advance_salary.index');
     Route::get('/create', [AdvanceSalariesController::class, 'create'])
          ->name('advance_salaries.advance_salary.create');
     Route::get('/show/{advanceSalary}', [AdvanceSalariesController::class, 'show'])
          ->name('advance_salaries.advance_salary.show')->where('id', '[0-9]+');
     Route::get('/{advanceSalary}/edit', [AdvanceSalariesController::class, 'edit'])
          ->name('advance_salaries.advance_salary.edit')->where('id', '[0-9]+');
     Route::post('/', [AdvanceSalariesController::class, 'store'])
          ->name('advance_salaries.advance_salary.store');
     Route::put('advance_salary/{advanceSalary}', [AdvanceSalariesController::class, 'update'])
          ->name('advance_salaries.advance_salary.update')->where('id', '[0-9]+');
     Route::delete('/advance_salary/{advanceSalary}', [AdvanceSalariesController::class, 'destroy'])
          ->name('advance_salaries.advance_salary.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'over_times',
], function () {
     Route::get('/', [OverTimesController::class, 'index'])
          ->name('over_times.over_time.index');
     Route::get('/create', [OverTimesController::class, 'create'])
          ->name('over_times.over_time.create');
     Route::get('/show/{overTime}', [OverTimesController::class, 'show'])
          ->name('over_times.over_time.show')->where('id', '[0-9]+');
     Route::get('/{overTime}/edit', [OverTimesController::class, 'edit'])
          ->name('over_times.over_time.edit')->where('id', '[0-9]+');
     Route::post('/', [OverTimesController::class, 'store'])
          ->name('over_times.over_time.store');
     Route::put('over_time/{overTime}', [OverTimesController::class, 'update'])
          ->name('over_times.over_time.update')->where('id', '[0-9]+');
     Route::delete('/over_time/{overTime}', [OverTimesController::class, 'destroy'])
          ->name('over_times.over_time.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'damages',
], function () {
     Route::get('/', [DamagesController::class, 'index'])
          ->name('damages.damage.index');
     Route::get('/create', [DamagesController::class, 'create'])
          ->name('damages.damage.create');
     Route::get('/show/{damage}', [DamagesController::class, 'show'])
          ->name('damages.damage.show')->where('id', '[0-9]+');
     Route::get('/{damage}/edit', [DamagesController::class, 'edit'])
          ->name('damages.damage.edit')->where('id', '[0-9]+');
     Route::post('/', [DamagesController::class, 'store'])
          ->name('damages.damage.store');
     Route::put('damage/{damage}', [DamagesController::class, 'update'])
          ->name('damages.damage.update')->where('id', '[0-9]+');
     Route::delete('/damage/{damage}', [DamagesController::class, 'destroy'])
          ->name('damages.damage.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'fines',
], function () {
     Route::get('/', [FinesController::class, 'index'])
          ->name('fines.fine.index');
     Route::get('/create', [FinesController::class, 'create'])
          ->name('fines.fine.create');
     Route::get('/show/{fine}', [FinesController::class, 'show'])
          ->name('fines.fine.show')->where('id', '[0-9]+');
     Route::get('/{fine}/edit', [FinesController::class, 'edit'])
          ->name('fines.fine.edit')->where('id', '[0-9]+');
     Route::post('/', [FinesController::class, 'store'])
          ->name('fines.fine.store');
     Route::put('fine/{fine}', [FinesController::class, 'update'])
          ->name('fines.fine.update')->where('id', '[0-9]+');
     Route::delete('/fine/{fine}', [FinesController::class, 'destroy'])
          ->name('fines.fine.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'employees',
], function () {
     Route::get('/', [EmployeesController::class, 'index'])
          ->name('employees.employee.index');
     Route::get('/create', [EmployeesController::class, 'create'])
          ->name('employees.employee.create');
     Route::get('/show/{employee}', [EmployeesController::class, 'show'])
          ->name('employees.employee.show')->where('id', '[0-9]+');
     Route::get('/{employee}/edit', [EmployeesController::class, 'edit'])
          ->name('employees.employee.edit')->where('id', '[0-9]+');
     Route::post('/', [EmployeesController::class, 'store'])
          ->name('employees.employee.store');
     Route::put('employee/{employee}', [EmployeesController::class, 'update'])
          ->name('employees.employee.update')->where('id', '[0-9]+');
     Route::delete('/employee/{employee}', [EmployeesController::class, 'destroy'])
          ->name('employees.employee.destroy')->where('id', '[0-9]+');

     Route::get('/{employee}/companylegal', [EmployeesController::class, 'companyLegal'])
          ->name('employees.employee.editcompanylegal')->where('id', '[0-9]+');
     Route::get('/{employee}/otherinfo', [EmployeesController::class, 'otherInfo'])
          ->name('employees.employee.editOther')->where('id', '[0-9]+');
     Route::get('/{employee}/document', [EmployeesController::class, 'Document'])
          ->name('employees.employee.editdocument')->where('id', '[0-9]+');

     Route::get('/leftemp', [EmployeesController::class, 'leftEmployee'])
          ->name('employees.employee.leftemp');

     Route::get('/{employee}/join', [EmployeesController::class, 'Join'])
          ->name('employees.employee.join');

     // PF Code Popup Modal 
     Route::get('/changepf/{id}', [EmployeesController::class, 'changePf'])->name('employees.employee.changepf');

     Route::get('/employee/{employee_id}/branch_transfer', [BranchTransfersController::class, 'create'])
          ->name('employees.branch_transfer.create')->where('id', '[0-9]+');
});


Route::group([
     'prefix' => 'masters',
], function () {
     Route::get('/', [MasterCrudController::class, 'index'])
          ->name('masters.master.index');
});

Route::group([
     'prefix' => 'system_configurations',
], function () {
     /* Route::get('/', [SystemConfigurationsController::class, 'index'])
         ->name('system_configurations.system_configuration.index');
    Route::get('/create', [SystemConfigurationsController::class, 'create'])
         ->name('system_configurations.system_configuration.create'); */
     /* Route::get('/show/{systemConfiguration}',[SystemConfigurationsController::class, 'show'])
         ->name('system_configurations.system_configuration.show')->where('id', '[0-9]+'); */
     Route::get('/{systemConfiguration}/edit', [SystemConfigurationsController::class, 'edit'])
          ->name('system_configurations.system_configuration.edit')->where('id', '[0-9]+');
     /* Route::post('/', [SystemConfigurationsController::class, 'store'])
         ->name('system_configurations.system_configuration.store'); */
     Route::put('system_configuration/{systemConfiguration}', [SystemConfigurationsController::class, 'update'])
          ->name('system_configurations.system_configuration.update')->where('id', '[0-9]+');
     /* Route::delete('/system_configuration/{systemConfiguration}',[SystemConfigurationsController::class, 'destroy'])
         ->name('system_configurations.system_configuration.destroy')->where('id', '[0-9]+'); */
});

Route::group([
     'prefix' => 'users',
], function () {
     Route::get('/', [UsersController::class, 'index'])
          ->name('users.user.index');
     Route::get('/create', [UsersController::class, 'create'])
          ->name('users.user.create');
     Route::get('/show/{user}', [UsersController::class, 'show'])
          ->name('users.user.show')->where('id', '[0-9]+');
     Route::get('/{user}/edit', [UsersController::class, 'edit'])
          ->name('users.user.edit')->where('id', '[0-9]+');
     Route::post('/', [UsersController::class, 'store'])
          ->name('users.user.store');
     Route::put('user/{user}', [UsersController::class, 'update'])
          ->name('users.user.update')->where('id', '[0-9]+');
     Route::delete('/user/{user}', [UsersController::class, 'destroy'])
          ->name('users.user.destroy')->where('id', '[0-9]+');
});

// Application Form bhavesh
Route::get('/appliction_form', [PdfController::class, 'applictionForm'])
     ->name('appliction_form');

Route::post('/appliction_form/{id}', [PdfController::class, 'printApplicationForm'])
     ->name('print_appliction_form');

// Goverment Form bhavesh
Route::get('/goverment_form', [PdfController::class, 'govermentForm'])
     ->name('goverment_form');

Route::post('/goverment_form/{id}', [PdfController::class, 'printGovermentForm'])
     ->name('print_goverment_form');

//Icard pdf
Route::get('/icard', [PdfController::class, 'icard'])
     ->name('icard');
Route::post('/icard_pdf/{id}', [PdfController::class, 'icardPdf'])
     ->name('icard_pdf');

// Advance Salary Register
Route::get('/advance_salary_register', [PdfController::class, 'advanceSalaryRegister'])
     ->name('advance_salary_register');

Route::post('/advance_salary_register', [PdfController::class, 'printAdvanceSalaryRegister'])
     ->name('print_advance_salary_register');

Route::get('/get_advance_salary_branches', [MasterController::class, 'getAdvanceSalaryBranches'])
     ->name('get_advance_salary_branches');

// Over Time Register bhavesh
Route::get('/over_time_register', [PdfController::class, 'overTimeRegister'])
     ->name('over_time_register');

Route::post('/over_time_register', [PdfController::class, 'printOverTimeRegister'])
     ->name('print_over_time_register');

Route::get('/get_over_time_branches', [MasterController::class, 'getOverTimeBranches'])
     ->name('get_over_time_branches');

// damage Register bhavesh
Route::get('/damage_register', [PdfController::class, 'damageRegister'])
     ->name('damage_register');

Route::post('/damage_register', [PdfController::class, 'printDamageRegister'])
     ->name('print_damage_register');

Route::get('/get_damage_branches', [MasterController::class, 'getDamageBranches'])
     ->name('get_damage_branches');

// Fine Register bhavesh
Route::get('/fine_register', [PdfController::class, 'fineRegister'])
     ->name('fine_register');

Route::post('/fine_register', [PdfController::class, 'printFineRegister'])
     ->name('print_fine_register');

Route::get('/get_fine_branches', [MasterController::class, 'getFineBranches'])
     ->name('get_fine_branches');

// Indentity Card Register bhavesh
Route::get('/identity_card_register', [PdfController::class, 'identityCardRegister'])
     ->name('identity_card_register');

Route::post('/identity_card_register', [PdfController::class, 'printIdentityCardRegister'])
     ->name('print_identity_card_register');

Route::get('/get_identity_card_branches', [MasterController::class, 'getIdentityCardBranches'])
     ->name('get_identity_card_branches');

// employed by contractor bhavesh
Route::get('/employed_by_contractor_register', [PdfController::class, 'employedByContractorRegister'])
     ->name('employed_by_contractor_register');

Route::post('/employed_by_contractor_register', [PdfController::class, 'printEmployedByContractorRegister'])
     ->name('print_employed_by_contractor_register');

Route::get('/get_employed_by_contractor_branches', [MasterController::class, 'getEmployedByContractorBranches'])
     ->name('get_employed_by_contractor_branches');

// police verification bhavesh
Route::get('/police_verification_register', [PdfController::class, 'policeVerificationRegister'])
     ->name('police_verification_register');

Route::post('/police_verification_register', [PdfController::class, 'printPoliceVerificationRegister'])
     ->name('print_police_verification_register');

Route::get('/get_police_verification_branches', [MasterController::class, 'getPoliceVerificationBranches'])
     ->name('get_police_verification_branches');

// police verification bhavesh
Route::get('/police_verification_for_leave_register', [PdfController::class, 'policeVerificationForLeaveRegister'])
     ->name('police_verification_for_leave_register');

Route::post('/police_verification_for_leave_register', [PdfController::class, 'printPoliceVerificationForLeaveRegister'])
     ->name('print_police_verification_for_leave_register');

Route::get('/get_police_verification_for_leave_branches', [MasterController::class, 'getPoliceVerificationForLeaveBranches'])
     ->name('get_police_verification_for_leave_branches');

//debit voucher advance salary bhavesh 03-04-2023
Route::get('employees/{employee}/leave_register', [PdfController::class, 'printLeaveRegister'])
     ->name('employees.employee.leave_register')->where('id', '[0-9]+');

//debit voucher advance salary bhavesh 29-03-2023
Route::get('advance_salaries/{advance_salary}/debit_voucher', [PdfController::class, 'printDebitVoucher'])
     ->name('advance_salaries.advance_salary.debit_voucher')->where('id', '[0-9]+');

// Branchwise employee fetch 
Route::post('fetch_employee', [MasterController::class, 'fetchEmployee'])
     ->name('fetch_employee');

//Dependent Dropdown for country, state,destrict,city and pincode  
Route::post('/fetch_state', [MasterController::class, 'fetchState'])
     ->name('fetch_state');
Route::post('/fetch_district', [MasterController::class, 'fetchDistrict'])
     ->name('fetch_district');
Route::post('/fetch_city', [MasterController::class, 'fetchCity'])
     ->name('fetch_city');
Route::post('/fetch_pincode', [MasterController::class, 'fetchPincode'])
     ->name('fetch_pincode');

// fetch bank branch and ifsc code
Route::post('/fetch_branch', [MasterController::class, 'fetchBranch'])
     ->name('fetch_branch');

Route::post('/fetch_ifsc', [MasterController::class, 'fetchIfsc'])
     ->name('fetch_ifsc');

// Fetch Ifsc Code For Company Legal 
Route::post('/fetch_ifsc_company_legal', [MasterController::class, 'fetchIfscCompanyLegal'])
     ->name('fetch_ifsc_company_legal');
     

Route::post('/fetch_account', [MasterController::class, 'fetchAccount'])
     ->name('fetch_account');

Route::post('/fetch_cbranch', [MasterController::class, 'fetchCbranch'])
->name('fetch_cbranch');     


// fetch designation and employee branch transfer wise - attendance
Route::post('fetch_branch_designation', [MasterController::class, 'fetchBranchDesignation'])
     ->name('/fetch_branch_designation');

Route::post('fetch_designation_employee', [MasterController::class, 'fetchDesignationEmployee'])
     ->name('/fetch_designation_employee');

 // fetch employee branch transfer wise - salary
 Route::post('fetch_branch_employee', [MasterController::class, 'fetchBranchEmployee'])
     ->name('/fetch_branch_employee');



// Fetch Designation throw Branch 
Route::post('fetch_designation', [MasterController::class, 'fetchDesignation'])
     ->name('/fetch_designation');

// Fetch All data from branch_designation
Route::post('fetch_designation_data', [MasterController::class, 'fetchDesignationData'])
     ->name('/fetch_designation_data');

// route of Add Pf Code
Route::get('add-pf/{id}', [MasterController::class, 'addPf']);
Route::put('update-employee-pf', [MasterController::class, 'updatePf']);

Route::prefix('account')->group(function () {
     Route::get('settings', [SettingsController::class, 'index'])->name('settings.index');
     Route::put('settings', [SettingsController::class, 'update'])->name('settings.update');
     Route::put('settings/email', [SettingsController::class, 'changeEmail'])->name('settings.changeEmail');
     Route::put('settings/password', [SettingsController::class, 'changePassword'])->name('settings.changePassword');
});


Route::resource('profile', ProfileController::class);

Route::post('/profile/{id}', [App\Http\Controllers\ProfileController::class, 'change'])->name('profile.change');


//Attendance excel sheet import & Export
Route::get('import', [PdfController::class, 'importIndex'])->name('import');
Route::get('export', [PdfController::class, 'exportIndex'])->name('export');
// Route::get('ad_export', [AttendanceController::class, 'export'])->name('ad_export');
Route::post('ad_import', [PdfController::class, 'import'])->name('ad_import');
Route::post('ad_export', [PdfController::class, 'export'])->name('ad_export');

//Attendacne Report PDF
Route::get('/attendance_report', [PdfController::class, 'attendanceReport'])->name('attendance_report');
Route::post('attendance_report', [PdfController::class, 'printAttendanceReport'])->name('attendance_report_pdf');

//Branchwise Professional Tax List
Route::get('/branchwise_profesional_tax', [PdfController::class, 'branchWiseProfesionalTax'])->name('branchwise_profesional_tax');
Route::post('branchwise_profesional_tax', [PdfController::class, 'printBranchWiseProfesionalTax'])->name('branchwise_profesional_tax_pdf');

//All Professional Tax List
Route::get('/all_professional_tax_list', [PdfController::class, 'allProfessionalTaxList'])->name('all_professional_tax_list');
Route::post('all_professional_tax_list', [PdfController::class, 'printAllProfessionalTaxList'])->name('all_professional_tax_list_pdf');

//Branch Wise PF Statement
Route::get('/branch_wise_pf_statement', [PdfController::class, 'branchWisePfStatement'])->name('branch_wise_pf_statement');
Route::post('branch_wise_pf_statement', [PdfController::class, 'printBranchWisePfStatement'])->name('branch_wise_pf_statement_pdf');

//Branchwise PF Excel Export
Route::get('/branchwise_pf_excel', [PdfController::class, 'branchWisePfExcel'])->name('branchwise_pf_excel');
Route::post('branchwise_pf_excel', [PdfController::class, 'exportBranchWisePfExcel'])->name('branchwise_pf_excel_export');

// PF All Branch
Route::get('/all_pf_excel', [PdfController::class, 'allPfExcel'])->name('all_pf_excel');
Route::post('all_pf_excel', [PdfController::class, 'exportAllPfExcel'])->name('all_pf_excel_export');

// Register Of Overtime
Route::get('/register_of_overtime', [PdfController::class, 'registerOfOvertime'])->name('register_of_overtime');
Route::post('register_of_overtime', [PdfController::class, 'printRegisterOfOvertime'])->name('register_of_overtime_pdf');

// Upload Excel For Pf AC Code/ Pf UAN Code
Route::get('/upload_pf_code', [PdfController::class, 'uploadPfCode'])->name('upload_pf_code');
Route::post('upload_pf_code', [PdfController::class, 'uploadPfCodeImport'])->name('upload_pf_code_import');

// Register Of Walfare Report
Route::get('/register_of_walfare_report', [PdfController::class, 'registerOfWalfare'])->name('register_of_walfare_report');
Route::post('register_of_walfare_report', [PdfController::class, 'registerOfWalfareReport'])->name('register_of_walfare_report_pdf');

// Bonus Reports Yearly
Route::get('/bonus_report', [PdfController::class, 'bonusReport'])->name('bonus_report');
Route::post('bonus_report', [PdfController::class, 'printBonusReport'])->name('bonus_report_pdf');

// Bonus Reports Monthly
Route::get('/bonus_report_monthly', [PdfController::class, 'bonusReportMonthly'])->name('bonus_report_monthly');
Route::post('bonus_report_monthly', [PdfController::class, 'printbonusReportMonthly'])->name('bonus_report_monthly_pdf');

// Manage Warfare Fund 
Route::get('/manage_warfare_fund', [PdfController::class, 'manageWarfareFund'])->name('manage_warfare_fund');
// Route::post('manage_warfare_fund', [PdfController::class, 'manageWarfareFund'])->name('bonus_report_monthly_pdf');

// Police Verification Report Excel Format
Route::get('/police_verification_report_excel', [PdfController::class, 'policeVerificationReportExcel'])->name('police_verification_report_excel');
Route::post('police_verification_report_excel', [PdfController::class, 'policeVerificationReportExcelExport'])->name('police_verification_report_excel_export');

// Male/Female Gross Salary Report (Monthwise)
Route::get('/male_female_salary', [PdfController::class, 'maleFemaleSalary'])->name('male_female_salary');
Route::post('male_female_salary', [PdfController::class, 'printMaleFemaleSalary'])->name('male_female_salary_pdf');

// Import Salary Excel
Route::get('/import_salary_excel', [PdfController::class, 'importSalaryExcel'])->name('import_salary_excel');
Route::post('/import_salary_excel', [PdfController::class, 'addImportSalaryExcel'])->name('add_import_salary_excel');

//Export Salary Excel
Route::get('/export_salary_excel', [PdfController::class, 'exportSalaryExcel'])->name('export_salary_excel');
Route::post('export_salary_excel', [PdfController::class, 'printexportSalaryExcel'])->name('print_export_salary_excel');

// Branchwise Salary Report
Route::get('/branch_wise_salary', [PdfController::class, 'branchWiseSalary'])->name('branch_wise_salary');
Route::post('branch_wise_salary', [PdfController::class, 'printBranchWiseSalary'])->name('branch_wise_salary_pdf');

// Branch wise Delete Salary
Route::get('/delete_salary', [PdfController::class, 'deleteSalary'])->name('delete_salary');
Route::post('/delete_salary', [PdfController::class, 'deleteSalaryAdd'])->name('delete_salary_add');

// Branch wise Delete Attendence
Route::get('/delete_attendance', [PdfController::class, 'deleteAttendance'])->name('delete_attendance');
Route::post('/delete_attendance', [PdfController::class, 'deleteAttendanceAdd'])->name('delete_attendance_add');

//Employee Month Salary Excel
Route::get('/employee_month_salary', [PdfController::class, 'employeeMonthSalary'])->name('employee_month_salary');
Route::post('/employee_month_salary', [PdfController::class, 'employeeMonthSalaryExcel'])->name('employee_month_salary_excel');

// Wage Slip 
Route::get('/wage_slip', [PdfController::class, 'wageSlip'])->name('wage_slip');
Route::post('wage_slip', [PdfController::class, 'printWageSlip'])->name('wage_slip_pdf');

// Month To Month Wage Slip 
Route::get('/month_to_month_wage_slip', [PdfController::class, 'monthWageSlip'])->name('month_wage_slip');
Route::post('/month_to_month_wage_slip', [PdfController::class, 'printMonthWageSlip'])->name('month_wage_slip_pdf');

// Esic Reports
Route::get('/esic', [PdfController::class, 'esic'])->name('esic');
Route::post('/esic', [PdfController::class, 'printEsic'])->name('esic_pdf');

// Stock Reports
Route::get('/stock', [PdfController::class, 'stock'])->name('stock');
Route::post('/stock', [PdfController::class, 'printStock'])->name('stock_pdf');

Route::group([
     'prefix' => 'branch_transfers',
], function () {

     Route::get('/', [BranchTransfersController::class, 'index'])
          ->name('branch_transfers.branch_transfer.index');
     Route::get('/create', [BranchTransfersController::class, 'create'])
          ->name('branch_transfers.branch_transfer.create');
     Route::get('/show/{branchTransfer}', [BranchTransfersController::class, 'show'])
          ->name('branch_transfers.branch_transfer.show')->where('id', '[0-9]+');
     Route::get('/{branchTransfer}/edit', [BranchTransfersController::class, 'edit'])
          ->name('branch_transfers.branch_transfer.edit')->where('id', '[0-9]+');
     Route::post('/', [BranchTransfersController::class, 'store'])
          ->name('branch_transfers.branch_transfer.store');
     Route::put('branch_transfer/{branchTransfer}', [BranchTransfersController::class, 'update'])
          ->name('branch_transfers.branch_transfer.update')->where('id', '[0-9]+');
     Route::delete('/branch_transfer/{branchTransfer}', [BranchTransfersController::class, 'destroy'])
          ->name('branch_transfers.branch_transfer.destroy')->where('id', '[0-9]+');
});

Route::get('/get-designations', 'App\Http\Controllers\BranchTransfersController@getDesignations')->name('getDesignations');

Route::group([
     'prefix' => 'attendances',
], function () {
     Route::get('/', [AttendancesController::class, 'index'])
          ->name('attendances.attendance.index');
     Route::get('/create', [AttendancesController::class, 'create'])
          ->name('attendances.attendance.create');
     Route::get('/show/{attendance}', [AttendancesController::class, 'show'])
          ->name('attendances.attendance.show')->where('id', '[0-9]+');
     Route::get('/{attendance}/edit', [AttendancesController::class, 'edit'])
          ->name('attendances.attendance.edit')->where('id', '[0-9]+');
     Route::post('/', [AttendancesController::class, 'store'])
          ->name('attendances.attendance.store');
     Route::put('attendance/{attendance}', [AttendancesController::class, 'update'])
          ->name('attendances.attendance.update')->where('id', '[0-9]+');
     Route::delete('/attendance/{attendance}', [AttendancesController::class, 'destroy'])
          ->name('attendances.attendance.destroy')->where('id', '[0-9]+');
});

// All Employee Reports
Route::get('/all_employee', [PdfController::class, 'printAllEmployee'])->name('all_employee');

// All Left Employee Reports
Route::get('/left_employee', [PdfController::class, 'printLeftEmployee'])->name('left_employee');

Route::group([
     'prefix' => 'salaries',
], function () {
     Route::get('/', [SalariesController::class, 'index'])
          ->name('salaries.salary.index');
     Route::get('/create', [SalariesController::class, 'create'])
          ->name('salaries.salary.create');
     Route::get('/show/{salary}', [SalariesController::class, 'show'])
          ->name('salaries.salary.show')->where('id', '[0-9]+');
     Route::get('/{salary}/edit', [SalariesController::class, 'edit'])
          ->name('salaries.salary.edit')->where('id', '[0-9]+');
     Route::post('/', [SalariesController::class, 'store'])
          ->name('salaries.salary.store');
     Route::put('salary/{salary}', [SalariesController::class, 'update'])
          ->name('salaries.salary.update')->where('id', '[0-9]+');
     Route::delete('/salary/{salary}', [SalariesController::class, 'destroy'])
          ->name('salaries.salary.destroy')->where('id', '[0-9]+');
});

Route::group([
     'prefix' => 'credit_salaries',
], function () {
     Route::get('/', [CreditSalariesController::class, 'index'])
          ->name('credit_salaries.credit_salary.index');
     Route::get('/create', [CreditSalariesController::class, 'create'])
          ->name('credit_salaries.credit_salary.create');
     Route::get('/show/{creditSalary}', [CreditSalariesController::class, 'show'])
          ->name('credit_salaries.credit_salary.show')->where('id', '[0-9]+');
     Route::get('/{creditSalary}/edit', [CreditSalariesController::class, 'edit'])
          ->name('credit_salaries.credit_salary.edit')->where('id', '[0-9]+');
     Route::post('/', [CreditSalariesController::class, 'store'])
          ->name('credit_salaries.credit_salary.store');
     Route::put('credit_salary/{creditSalary}', [CreditSalariesController::class, 'update'])
          ->name('credit_salaries.credit_salary.update')->where('id', '[0-9]+');
     Route::delete('/credit_salary/{creditSalary}', [CreditSalariesController::class, 'destroy'])
          ->name('credit_salaries.credit_salary.destroy')->where('id', '[0-9]+');
});

// Credit Salary Reports
Route::get('/print_credit_salary', [PdfController::class, 'printCreditSalary'])->name('print_credit_salary')->where('id', '[0-9]+');

//debit voucher advance salary bhavesh 03-04-2023
Route::get('credit_salaries/{credit_salary}/print_credit_salary', [PdfController::class, 'printCreditSalary'])
     ->name('credit_salaries.credit_salary.print_credit_salary')->where('id', '[0-9]+');

Route::group([
     'prefix' => 'walfers',
], function () {
     Route::get('/', [WalferController::class, 'index'])
          ->name('walfers.walfer.index');
     Route::get('/create', [WalferController::class, 'create'])
          ->name('walfers.walfer.create');
     Route::get('/show/{walfer}', [WalferController::class, 'show'])
          ->name('walfers.walfer.show')->where('id', '[0-9]+');
     Route::get('/{walfer}/edit', [WalferController::class, 'edit'])
          ->name('walfers.walfer.edit')->where('id', '[0-9]+');
     Route::post('/', [WalferController::class, 'store'])
          ->name('walfers.walfer.store');
     Route::put('walfer/{walfer}', [WalferController::class, 'update'])
          ->name('walfers.walfer.update')->where('id', '[0-9]+');
     Route::delete('/walfer/{walfer}', [WalferController::class, 'destroy'])
          ->name('walfers.walfer.destroy')->where('id', '[0-9]+');
});

Route::group([
    'prefix' => 'branch_bills',
], function () {
    Route::get('/', [BranchBillsController::class, 'index'])
         ->name('branch_bills.branch_bill.index');
    Route::get('/create', [BranchBillsController::class, 'create'])
         ->name('branch_bills.branch_bill.create');
    Route::get('/show/{branchBill}',[BranchBillsController::class, 'show'])
         ->name('branch_bills.branch_bill.show')->where('id', '[0-9]+');
    Route::get('/{branchBill}/edit',[BranchBillsController::class, 'edit'])
         ->name('branch_bills.branch_bill.edit')->where('id', '[0-9]+');
    Route::post('/', [BranchBillsController::class, 'store'])
         ->name('branch_bills.branch_bill.store');
    Route::put('branch_bill/{branchBill}', [BranchBillsController::class, 'update'])
         ->name('branch_bills.branch_bill.update')->where('id', '[0-9]+');
    Route::delete('/branch_bill/{branchBill}',[BranchBillsController::class, 'destroy'])
         ->name('branch_bills.branch_bill.destroy')->where('id', '[0-9]+');
});

//debit voucher advance salary bhavesh 05-05-2023
Route::get('branch_bill/{branchBill}/branch_bill_register', [PdfController::class, 'printBranchBillRegister'])
     ->name('branch_bills.branch_bill.branch_bill_register')->where('id', '[0-9]+');

Route::get('/download-attendance-sheet', function () {
     $path = public_path('sample-attendance-sheet.xlsx');
     return response()->download($path);
})->name('download-attendance-sheet');

Route::get('/download-salary-sheet', function () {
     $path = public_path('sample-salary-sheet.xlsx');
     return response()->download($path);
})->name('download-salary-sheet');

Route::get('/download-pfcode-sheet', function () {
     $path = public_path('sample-pfcode-sheet.xlsx');
     return response()->download($path);
})->name('download-pfcode-sheet');

Route::group([
    'prefix' => 'company_banks',
], function () {
    Route::get('/', [CompanyBanksController::class, 'index'])
         ->name('company_banks.company_bank.index');
    Route::get('/create', [CompanyBanksController::class, 'create'])
         ->name('company_banks.company_bank.create');
    Route::get('/show/{companyBank}',[CompanyBanksController::class, 'show'])
         ->name('company_banks.company_bank.show')->where('id', '[0-9]+');
    Route::get('/{companyBank}/edit',[CompanyBanksController::class, 'edit'])
         ->name('company_banks.company_bank.edit')->where('id', '[0-9]+');
    Route::post('/', [CompanyBanksController::class, 'store'])
         ->name('company_banks.company_bank.store');
    Route::put('company_bank/{companyBank}', [CompanyBanksController::class, 'update'])
         ->name('company_banks.company_bank.update')->where('id', '[0-9]+');
    Route::delete('/company_bank/{companyBank}',[CompanyBanksController::class, 'destroy'])
         ->name('company_banks.company_bank.destroy')->where('id', '[0-9]+');
});

// employee leave history module 
Route::group([
    'prefix' => 'employee_leave_histories',
], function () {
    Route::get('/', [EmployeeLeaveHistoriesController::class, 'index'])
         ->name('employee_leave_histories.employee_leave_history.index');
    Route::get('/create', [EmployeeLeaveHistoriesController::class, 'create'])
         ->name('employee_leave_histories.employee_leave_history.create');
    Route::get('/show/{employeeLeaveHistory}',[EmployeeLeaveHistoriesController::class, 'show'])
         ->name('employee_leave_histories.employee_leave_history.show')->where('id', '[0-9]+');
    Route::get('/{employeeLeaveHistory}/edit',[EmployeeLeaveHistoriesController::class, 'edit'])
         ->name('employee_leave_histories.employee_leave_history.edit')->where('id', '[0-9]+');
    Route::post('/', [EmployeeLeaveHistoriesController::class, 'store'])
         ->name('employee_leave_histories.employee_leave_history.store');
    Route::put('employee_leave_history/{employeeLeaveHistory}', [EmployeeLeaveHistoriesController::class, 'update'])
         ->name('employee_leave_histories.employee_leave_history.update')->where('id', '[0-9]+');
    Route::delete('/employee_leave_history/{employeeLeaveHistory}',[EmployeeLeaveHistoriesController::class, 'destroy'])
         ->name('employee_leave_histories.employee_leave_history.destroy')->where('id', '[0-9]+');
});


// employee leave history reports
Route::get('/employee_leave_histories', [PdfController::class, 'employeeLeaveHistory'])
     ->name('employee_leave_histories');

Route::post('/employee_leave_histories_register', [PdfController::class, 'printEmployeeLeaveHistory'])
     ->name('print_employee_leave_histories_register');

// Stock History Reports
Route::get('/stock_history', [PdfController::class, 'stockHistory'])
     ->name('stock_history');

Route::post('/stock_history', [PdfController::class, 'printStockHistory'])
     ->name('print_stock_history');

