<?php
namespace App\Http\Composers;
use Illuminate\Contracts\View\View;
use App\Models\SystemConfiguration;

class ViewsComposer
{
    public function compose(View $view){
        $view->with('Config', SystemConfiguration::first());
    }
}
?>