<?php

namespace App\Http\Controllers;

use App\Models\Branch;
use Illuminate\Http\Request;
use App\Models\Employee;
use App\Models\Attendance;
use App\Models\Fine;
use App\Models\Damage;
use App\Models\OverTime;
use App\Models\AdvanceSalary;
use App\Models\StockCategory;
use App\Models\StockRegister;
use App\Models\Salary;
use App\Models\BranchTransfer;
use App\Models\BranchBill;
use PDF;
use Carbon\Carbon;
use ZipArchive;
use App\Exports\AttendanceUploadExport;
use App\Exports\PfExport;
use App\Exports\SalaryExport;
use App\Exports\EmployeeSalaryExport;
use App\Exports\PoliceVerificationLeaveExport;
use Maatwebsite\Excel\Facades\Excel;
use App\Imports\AttendanceImport;
use App\Imports\SalaryImport;
use App\Imports\PfImport;
use App\Exports\PfAllExport;
use Illuminate\Http\RedirectResponse;
use Maatwebsite\Excel\Concerns\FromQuery;
use App\Invoice;
use App\Models\CreditSalary;
use App\Models\TaxManager;
use App\Models\EmployeeLeaveHistory;
use Illuminate\Support\Facades\DB;
use Auth;
use DateTime;

class PdfController extends Controller
{
    //Application Form Index
    public function applictionForm()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.application_form.index', $data);
    }

    //ApplicationForm  PDF Generate
    public function printApplicationForm(Request $request, $employee_id)
    {
        $employee_id = $request->employee_id;
        foreach ($employee_id as $key => $value) {
            $employee = Employee::find($value);
            $pdf = PDF::loadView('pdf.application_form.appform_pdf', ['employee' => $employee]);
            $pdf->setPaper('A4', 'potrait');
            $pdf->render();
            $pdfs[] = $pdf->output();
        }
        // Create ZIP archive
        $zip = new ZipArchive();
        $filename = 'application_form.zip';
        $zip->open($filename, ZipArchive::CREATE);
        foreach ($pdfs as $i => $pdf) {
            $zip->addFromString("application-form-$i.pdf", $pdf);
        }
        $zip->close();

        // Download ZIP archive
        $headers = [
            'Content-Type' => 'application/zip',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',
        ];
        return response()->download($filename, $filename, $headers)->deleteFileAfterSend(true);
    }

    // Goverment Form Index
    public function govermentForm()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.goverment_form.index', $data);
    }
    // Goverment Form PDF
    public function printGovermentForm(Request $request, $employee_id)
    {
        $employee_id = $request->employee_id;
        foreach ($employee_id as $key => $value) {
            $employee = Employee::find($value);
            $pdf = PDF::loadView('pdf.goverment_form.govermentform_pdf', ['employee' => $employee]);
            $pdf->setPaper('A4', 'potrait');
            $pdf->render();
            $pdfs[] = $pdf->output();
        }
        // Create ZIP archive
        $zip = new ZipArchive();
        $filename = 'government_form.zip';
        $zip->open($filename, ZipArchive::CREATE);
        foreach ($pdfs as $i => $pdf) {
            $zip->addFromString("goverment-form-$i.pdf", $pdf);
        }
        $zip->close();

        // Download ZIP archive
        $headers = [
            'Content-Type' => 'application/zip',
            'Content-Disposition' => 'attachment; filename="' . $filename . '"',
        ];
        return response()->download($filename, $filename, $headers)->deleteFileAfterSend(true);
    }

    // Icard PDF index
    public function icard()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.icard.index', $data);
    }
    //Icard PDF Generate
    public function icardPdf(Request $request, $employee_id)
    {

        $id = $request->employee_id;
        foreach ($id as $key => $value) {
            $branch_id = $request->branch_id;

            $employee[$key] = Employee::find($value);
        }
        $pdf = PDF::loadView('pdf.icard.icard', ['employees' => $employee]);
        $pdf->setPaper('A4', 'potrait');
        $pdf->download('Icardpdf.pdf');
        return $pdf->download('Icardpdf.pdf');
    }

    // Fine devloper by Bhavesh on 27-3-2023
    public function fineRegister()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.fine.index', $data);
    }

    public function printFineRegister(Request $request)
    {

        $monthyear = $request->get('monthyear');
        $branch_id = $request->get('branch_id');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $data = Fine::where('branch_id', $request->get('branch_id'))->where('monthyear', $request->get('monthyear'))->get();
        if ($data->count() >= 1) { 
            $pdf = PDF::loadView('pdf.fine.fine_register_pdf', [
                'data' => $data,
                'branch_name'=>$branch_name,
                'branch_address'=>$branch_address,
                'monthyear' => $monthyear
            ]);
            return $pdf->download('fine.pdf');
        }else {
            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // Damage devloper by Bhavesh on 27-3-2023
    public function damageRegister()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.damage.index', $data);
    }

    public function printDamageRegister(Request $request)
    {
        $monthyear = $request->get('monthyear');
        $branch_id = $request->get('branch_id');
        $data = Damage::where('branch_id', $request->get('branch_id'))->where('monthyear', $request->get('monthyear'))->get();

        if ($data->count() >= 1) {
            $pdf = PDF::loadView('pdf.damage.damage_register_pdf', ['data' => $data]);
        return $pdf->download('damage.pdf');
         }else {
            return redirect()->back()->with('fail', 'Data does not exists');
        }
        
    }

    // Over Time devloper by Bhavesh on 27-3-2023
    public function overTimeRegister()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.over_time.index', $data);
    }

    public function printOverTimeRegister(Request $request)
    {
        $monthyear = $request->get('monthyear');
        $branch_id = $request->get('branch_id');
        $data = OverTime::where('branch_id', $request->get('branch_id'))->where('monthyear', $request->get('monthyear'))->with('overTimeDays')->get();
        $pdf = PDF::loadView('pdf.over_time.over_time_register_pdf', ['data' => $data]);
        return $pdf->download('over_time.pdf');
    }

    // Advance Salary devloper by Bhavesh on 27-3-2023
    public function advanceSalaryRegister()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.advance_salary.index', $data);
    }

    public function printAdvanceSalaryRegister(Request $request)
    {
        $monthyear = $request->get('monthyear');
        $branch_id = $request->get('branch_id');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $data = AdvanceSalary::where('branch_id', $request->get('branch_id'))->where('monthyear', $request->get('monthyear'))->get();
    
        if ($data->count() >= 1) { 
        $pdf = PDF::loadView('pdf.advance_salary.advance_salary_register_pdf', [
            'data' => $data,
            'branch_name' => $branch_name,
            'branch_address' => $branch_address,
            'monthyear' => $monthyear
        ]);
        return $pdf->download('advance_salary.pdf');
    }else {
        return redirect()->back()->with('fail', 'Data does not exists');
    }
    }

    // Identity Card devloper by Bhavesh on 27-3-2023
    public function identityCardRegister()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.identity_card.index', $data);
    }

    public function printIdentityCardRegister(Request $request)
    {
        $month = $request->get('monthyear');
        $branch_id = $request->get('branch_id');
        $monthyear = Carbon::createFromFormat('Y-m', $month);
        $toMonthName = $monthyear->format('Y-M');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $data = Employee::where('branch_id', $branch_id)->where('monthyear', $toMonthName)->get();
        if ($data->count() >= 1) {
            $pdf = PDF::loadView('pdf.identity_card.identity_card_register_pdf', ['data' => $data, 'branch_name' => $branch_name, 'branch_address' => $branch_address, 'toMonthName' => $toMonthName]);
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download('identity_card.pdf');
    }

    // Employed By Contractor devloper by Bhavesh on 28-3-2023
    public function employedByContractorRegister()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.employed_by_contractor.index', $data);
    }

    public function printEmployedByContractorRegister(Request $request)
    {
        $month = $request->get('monthyear');
        $branch_id = $request->get('branch_id');
        $monthyear = Carbon::createFromFormat('Y-m', $month);
        $toMonthName = $monthyear->format('Y-M');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $data = Employee::where('branch_id', $branch_id)->where('monthyear', $toMonthName)->get();
        if ($data->count() >= 1) {
            $pdf = PDF::loadView('pdf.employed_by_contractor.employed_by_contractor_register_pdf', ['data' => $data, 'branch_name' => $branch_name, 'branch_address' => $branch_address, 'toMonthName' => $toMonthName]);
        } else {
            return redirect()->back()->with('success', 'Data does not exists');
        }
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download('employed_by_contractor.pdf');
    }

    // advance salary debit voucher by bhavesh on 30-03-2023
    public function printDebitVoucher($id)
    {
        $data = AdvanceSalary::findOrFail($id);
        $branches = Branch::where('status', 'active')->pluck('name', 'id')->all();
        $employees = Employee::where('status', 'active')->pluck('full_name', 'id')->all();
        $stockCategories = StockCategory::where('status', 'active')->pluck('stock_category_name', 'id')->all();

        $pdf = PDF::loadView('pdf.advance_salary.debit_voucher_pdf', ['data' => $data]);
        return $pdf->download('debit_voucher.pdf');
    }

    // Police Verification devloper by Bhavesh on 28-3-2023
    public function policeVerificationRegister()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.police_verification.index', $data);
    }

    public function printPoliceVerificationRegister(Request $request)
    {
        $month = $request->get('monthyear');
        $branch_id = $request->get('branch_id');
        $monthyear = Carbon::createFromFormat('Y-m', $month);
        $toMonthName = $monthyear->format('Y-M');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $data = Employee::where('branch_id', $branch_id)->where('monthyear', $toMonthName)->get();
        if ($data->count() >= 1) {
            $pdf = PDF::loadView('pdf.police_verification.police_verification_register_pdf', ['data' => $data, 'branch_name' => $branch_name, 'branch_address' => $branch_address, 'toMonthName' => $toMonthName]);
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download('police_verification.pdf');
        
        } else {
            return redirect()->back()->with('success', 'Data does not exists');
        }
    }

    // Police Verification devloper by Bhavesh on 28-3-2023
    public function policeVerificationForLeaveRegister()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.police_verification_for_leave.index', $data);
    }

    public function printPoliceVerificationForLeaveRegister(Request $request)
    { 
        $startdate = date('Y-m-d', strtotime($request->get('start_date')));
        $enddate = date('Y-m-d', strtotime($request->get('end_date')));
        $branch = $request->get('branch_id');
        $data = Employee::with('branch')->where('branch_id', $branch)->whereBetween('relieve_date', [$startdate, $enddate])->get();
        if ($data->count() >= 1) {
            $export_type = $request->input('export_type');
            if ($export_type === 'excel') {
                return Excel::download(new PoliceVerificationLeaveExport($data), "police_verification_for_leave.xlsx");
            } else if ($export_type === 'pdf') {
                $pdf = PDF::loadView('pdf.police_verification_for_leave.police_verification_for_leave_register_pdf', ['data' => $data, 'startdate' => $startdate, 'enddate' => $enddate]);
                $pdf->setPaper('A4', 'landscape');
                return $pdf->download('police_verification_for_leave.pdf');
            }
        } else {
            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // advance salary debit voucher by bhavesh on 30-03-2023
    public function printLeaveRegister($id)
    {
        $data = Employee::findOrFail($id);
        $pdf = PDF::loadView('pdf.leave.leave_register_pdf', ['data' => $data]);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download('leave_register.pdf');
    }

    // Branch bill report by bhavesh on 5-05-2023
    public function printBranchBillRegister($id)
    {
        $data = BranchBill::findOrFail($id);
        if($data->count() >= 1) 
        {    
            //echo "<pre>";print_r($_POST['BranchBill']);die;
            $date = $data->monthyear;
            $branch_id = $data->branch_id;
            $bill_amt = $data->bill_amount;
            $service_tax = $data->service_tax;
            $extra = $data->other_amt;
            $extra_exp = $data->extra_exp;
            $recover_payment = $data->recover_payment;
            $service_charge = $data->service_charge;
            $tds = $data->tds;
            
            $branch = Branch::where('id', $branch_id)->first();
            if(empty($branch_id) && empty($bill_amt)){
                return redirect()->back()->with('success', 'Branch Data does not exists');
            }
            else
            {	
                /* $bill_exist = $this->getBillexist($date, $branch_id);
                if(!empty($bill_exist)){
                    return redirect()->back()->with('success', 'Bill Report Already Exist for this Branch.');
                    }else{ */
                    $billamount = $this->getBillamount($date, $branch_id, $bill_amt, $service_tax, $service_charge, $extra, $recover_payment, $extra_exp, $tds);
                    //dd($billamount);
                    $pdf = PDF::loadView('pdf.branch_bill.branch_bill_register_pdf', ['branch' => $branch, 'monthyear' => $date, 'data' => $billamount]);
                    $pdf->setPaper('A4', 'landscape');
                    return $pdf->download('branch_bill_register.pdf');
                /* } */
            }
        } else {
            return redirect()->back()->with('fail', 'Data does not exists');
        }  
    }

    public function getBillamount($date, $branch_id, $bill_amt, $service_tax, $service_charge, $extra, $recover_payment, $extra_exp, $tds)
    {
        $row = Salary::where('branch_id', $branch_id)->where('monthYear', $date)->get();
        $net_bill_amt = $bill_amt - $service_tax;
        
        $pf = 0;
        $esi = 0;
        $profetional_tax = 0;
        $basis_wages = 0;
        $bonus = 0;
        $leave_amt = 0;
        $total = 0;
        $dress = 0;
        $sal_paid = 0;

        foreach($row as $key => $data)
        {	
            $pf	+= round($data['basis_wages'] * 13.61 / 100);
            $esi += round($data['esi']);
            $profetional_tax += $data['profetional_tax'];
            $bonus += $data['bonus'];
            $basicda = $data['basic'] + $data['da'];
            $extra_allw = $data['extra_allowance'] / $data['no_of_days_work_done'];
            $leave_amt += ($basicda + $extra_allw) * $data['no_of_leave'];
            $sal_paid += $data['nettotal']; 
            $total += $data['total'];
        }

        
        $gross_pay = $net_bill_amt - $pf - $bonus - $leave_amt + $service_charge;
        $net_amt = $gross_pay - $extra;
        $diff = $net_amt - $sal_paid;
        $profit = $diff - $tds;
        $net_profit = $profit + $recover_payment - $extra_exp;

        return array($bill_amt, $service_tax, $net_bill_amt, $pf, $esi, $bonus, $leave_amt,
        $service_charge, $gross_pay, $extra, $net_amt, $sal_paid, $diff, $tds, $profit, $recover_payment, $extra_exp, $net_profit);
    }
    
    /* public function getBillexist($date, $branch_id)
    {
        $connection=Yii::app()->db; 
        $command= $connection->createCommand("Select * from branch_bill where monthyear = '$date' and branch_id = '$branch_id'" );
        $rows = $command->queryAll(); 
        return $rows;
    } */

    //Attendance Report Index
    public function attendanceReport()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.attendance_report.index', compact('branch', 'employee'));
    }
    //Attendance Report PDF Generate
    public function printAttendanceReport(Request $request)
    {
        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');
        $employee = Attendance::where('branch_id', $branch_id)->where('monthyear', $monthName)->get();
        if ($employee->count() >= 1) {
            $present = 0;
            $absent = 0;
            $holiday = 0;
            // Loop through the attendance data and calculate the totals
            foreach ($employee as $attendance) {
                $employeeId = $attendance->employee_id;
                $present += $attendance->total_present;
                $absent += $attendance->total_absent;
                $holiday += $attendance->total_holiday;
            }
            $pdf = PDF::loadView('pdf.attendance_report.attendance_pdf', ['employee' => $employee, 'monthName' => $monthName, 'branch_name' => $branch_name, 'branch_address' => $branch_address, 'present' => $present, 'absent' => $absent, 'holiday' => $holiday]);
            $pdf->setPaper('A4', 'landscape');

            return $pdf->download("AttendaceReport.$monthName.pdf");
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }


    // Branchwise Profesional Tax
    public function branchWiseProfesionalTax()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.branchwise_profesional_tax.index', compact('branch', 'employee'));
    }

    public function printBranchWiseProfesionalTax(Request $request)
    {
        $month = $request->input('monthyear');
        $taxManager = TaxManager::where('monthyear', $month)->first();
        if(!$taxManager){
            return redirect()->back()->with('fail', 'Tax manager does not exist');
        }
        $minimumSalary = $taxManager['min_salary'];

        $branch_id = $request->input('branch_id');
        $branch = DB::table('branches')->where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');

        $employees = Employee::where('status', 'active')->get();

        $salaries = DB::table('salaries')
            ->join('employees', function ($join) use ($branch_id, $monthName) {
                $join->on('salaries.employee_id', '=', 'employees.id')
                    ->where('salaries.branch_id', '=', $branch_id)
                    ->where('salaries.monthyear', '=', $monthName);
            })
            ->select('salaries.*')
            ->get();

        if ($salaries->count() >= 1) {
            $total = 0;
            $total200 = 0;
            foreach ($salaries as $salary) {
                foreach ($employees as $employee) {
                    if ($salary->employee_id == $employee->id) {
                        if ($salary->nettotal >= $minimumSalary) {
                            $employee->nettotal = $salary->nettotal - 200;
                            $employee->employee200 = 200;
                            $total200 += $employee->employee200;
                        } else {
                            $employee->nettotal = $salary->nettotal;
                            $employee->employee00 = 00;
                        }
                        $total += $salary->nettotal;
                    }
                }
            }

            $pdf = PDF::loadView('pdf.branchwise_profesional_tax.branchwise_tax', [
                'branch_name' => $branch_name,
                'branch_address' => $branch_address,
                'monthName' => $monthName,
                'employees' => $employees,
                'total' => $total,
                'total200' => $total200

            ]);
            $pdf->setPaper('A4', 'potrait');
            $pdf->download('branch.professional_tax.pdf');
            return $pdf->download('branch.professional_tax.pdf');
        } else {
            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // All Professional Tax
    public function allProfessionalTaxList()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.all_professional_tax_list.index', compact('branch', 'employee'));
    }

    public function printAllProfessionalTaxList(Request $request)
    {
        $month = $request->get('monthyear');
        $taxManager = TaxManager::where('monthyear', $month)->first();
        if(!$taxManager){
            return redirect()->back()->with('fail', 'Tax manager does not exist');
        }
        $minimumSalary = $taxManager['min_salary'];
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');  
        $branches = Branch::all();
        $total200 = 0;
        $finalTotal = 0;
        $emp200 = 00;
        $emp00 = 00;

        foreach ($branches as $branch) {
            $salariesByBranch = Salary::where('branch_id', $branch->id)->where('monthyear', $monthName)->get(); 
            $netTotalByBranch = 0;
            foreach ($salariesByBranch as $salaryByBranch) {
                if ($salaryByBranch->nettotal >= $minimumSalary) {
                    $branch->nettotal = $salaryByBranch->nettotal - 200;
                    $branch->employee200 = 200;
                    $branch->branch200 += $branch->employee200;
                    $total200 += $branch->employee200;
                    $emp200++;
                } else {
                    $branch->nettotal = $salaryByBranch->nettotal;
                    $branch->employee00 = 00;
                    $emp00++;
                }
                $netTotalByBranch += $salaryByBranch->nettotal;
            }
            // Assign the net total to the branch
            $branch->netTotal = $netTotalByBranch;
            $finalTotal += $branch->netTotal;
            $totalemployee = $emp200 + $emp00;
            $proTaxAmount =  $emp200 * $total200;
        }
 
        if ($salariesByBranch->count() >= 1) {
            $pdf = PDF::loadView('pdf.all_professional_tax_list.professional_tax', [
                'branches' => $branches,
                'finalTotal' => $finalTotal,
                'total200' => $total200,
                'monthName' => $monthName,
                'emp200' => $emp200,
                'emp00' => $emp00,
                'totalemployee' => $totalemployee,
                'proTaxAmount' => $proTaxAmount
            ]);
            $pdf->setPaper('A4', 'potrait');
            $pdf->download('professional_tax.pdf');
            return $pdf->download('professional_tax.pdf');
        } else {
            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // Branch Wise PF Statement
    public function branchWisePfStatement()
    {
        $branch = Branch::all();
        $employees = Employee::all();
        return view('pdf.branch_wise_pf_statement.index', compact('branch', 'employees'));
    }

    public function printBranchWisePfStatement(Request $request)
    {
        $month = $request->get('monthyear');
        $taxManager = TaxManager::where('monthyear', $month)->first();
        if(!$taxManager){
            return redirect()->back()->with('fail', 'Tax manager does not exist');
        }
        $branch_id = $request->get('branch_id');
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');

        if(is_null($branch_id)){
                $salaries = Salary::with('branch','employee')->where('monthyear', $monthName)->get();
        }else{
                $branch = DB::table('branches')->where('id', $branch_id)->first();
                $branch_name = $branch->name;
                $branch_address = $branch->address;
                $salaries = Salary::where('branch_id', $branch_id)->where('monthyear', $monthName)->get();
        }
      
        if ($salaries->count() >= 1) {
            $employeePF = $taxManager['employee_pf']; 
            $empPF = $taxManager['employer_pf'];
            $empFPF = $taxManager['employer_fpf'];
            $empEDLI = $taxManager['employer_edli'];
            $empINEPC = $taxManager['employer_inepc'];
            $empADMIN = $taxManager['employee_admin'];
            
            $totalempPF = 0;
            $totalempFPF = 0;
            $totalempEDLI = 0;
            $totalempINEPC = 0;
            $totalempADMIN = 0;
            $totalEmployeePF = 0;

            $a = 0;
            $b = 0;
            $c = 0;
            $d = 0;
            $f = 0;
            $g = 0;
            $h = 0;
            $k = 0;
            $l = 0;
            
            foreach ($salaries as $salary) {
               $salary->totalempPF = (((($salary['basis_wages'] != '0.00' ? $salary['basis_wages'] : $salary['fix_salary']) * $empPF) / 100));
                $salary->totalempFPF = (((($salary['basis_wages'] != '0.00' ? $salary['basis_wages'] : $salary['fix_salary']) * $empFPF) / 100));
                $salary->totalempEDLI = (((($salary['basis_wages'] != '0.00' ? $salary['basis_wages'] : $salary['fix_salary']) * $empEDLI) / 100));
                $salary->totalempINEPC = (((($salary['basis_wages'] != '0.00' ? $salary['basis_wages'] : $salary['fix_salary']) * $empINEPC) / 100));
                $salary->totalempADMIN = (((($salary['basis_wages'] != '0.00' ? $salary['basis_wages'] : $salary['fix_salary']) * $empADMIN) / 100));
                $salary->totalEmployeePF = (((($salary['basis_wages'] != '0.00' ? $salary['basis_wages'] : $salary['fix_salary']) * $employeePF) / 100));
                             
                $a = $a + $salary['basis_wages'] + $salary['fix_salary'];
                $b = $b + $salary['pf'];
                $c = $c + $salary->totalempEDLI;
                $d = $d + $salary->totalempINEPC;
                $f = $f + $salary->totalempADMIN;
                $g = $g + $salary->totalempPF;
                $h = $h + $salary->totalempFPF;
                $k = $b + $b;
                $l = $k+$h+$c+$d+$f;
            }
            $export_type = $request->input('export_type');
            if ($export_type === 'excel') {
                if(is_null($branch_id)){
                    return Excel::download(new PfAllExport($salaries,
                    $empPF,
                    $empFPF,
                    $empEDLI,
                    $empINEPC,
                    $empADMIN,
                    $employeePF,
                    $a,
                    $b,
                    $c,
                    $d,
                    $f,
                    $g,
                    $h,
                    $k,
                    $l), "pf_statement.xlsx");
                }else{
                    return Excel::download(new PfExport($salaries,
                    $monthName,
                    $branch_id,
                    $empPF,
                    $empFPF,
                    $empEDLI,
                    $empINEPC,
                    $empADMIN,
                    $employeePF,
                    $a,
                    $b,
                    $c,
                    $d,
                    $f,
                    $g,
                    $h,
                    $k,
                    $l), "pf_statement.xlsx");
                }
            } else if ($export_type === 'pdf') {
                $pdf = PDF::loadView('pdf.branch_wise_pf_statement.pf_statement', [
                    'salaries' => $salaries,
                    'monthName' => $monthName,
                    'branch_name' => $branch_id?$branch_name:'',
                    'branch_address' => $branch_id?$branch_address:'',
                    'empPF' => $empPF,
                    'empFPF' => $empFPF,
                    'empEDLI' => $empEDLI,
                    'empINEPC' => $empINEPC,
                    'empADMIN' => $empADMIN,
                    'employeePF'=>$employeePF,
                    'a' => $a,
                    'b' => $b,
                    'c' => $c,
                    'd' => $d,
                    'f' => $f,
                    'g' => $g,
                    'h' => $h,
                    'k' => $k,
                    'l' => $l
    
                ]);
                $pdf->setPaper('A4', 'potrait');
                return $pdf->download('branch_wise_pf_statement.pdf');
            }
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // Branchwise PF Excel Export
    public function branchWisePfExcel()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('excel.branchwise_pf_export.export', compact('branch', 'employee'));
    }

    public function exportBranchWisePfExcel(Request $request)
    {
        $month = $request->get('monthyear');
        $taxManager = TaxManager::where('monthyear', $month)->first();
        if(!$taxManager){
            return redirect()->back()->with('fail', 'Tax manager does not exist');
        }
        $branch_id = $request->get('branch_id');
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');
        //   dd('hii');
        if (is_null($branch_id)) {
            $salaries = Salary::where('monthyear', $monthName)->get();
        } else {
            $branch = DB::table('branches')->where('id', $branch_id)->first();
            $branch_name = $branch->name;
            $salaries = Salary::where('branch_id', $branch_id)->where('monthyear', $monthName)->get();
        }

        if ($salaries->count() >= 1) {
            $empPF = $taxManager['employer_pf'];
            $empFPF = $taxManager['employer_fpf'];
            $empEDLI = $taxManager['employer_edli'];
            $empINEPC = $taxManager['employer_inepc'];
            $empADMIN = $taxManager['employee_admin'];

            $totalempPF = 0;
            $totalempFPF = 0;
            $totalempEDLI = 0;
            $totalempINEPC = 0;
            $totalempADMIN = 0;

            $a = 0;
            $b = 0;
            $c = 0;
            $d = 0;
            $f = 0;
            $g = 0;
            $h = 0;
            $k = 0;
            $l = 0;

            foreach ($salaries as $salary) {
                $totalempPF = ((($salary['basis_wages'] * $empPF) / 100));
                $totalempFPF = ((($salary['basis_wages'] * $empFPF) / 100));
                $totalempEDLI = ((($salary['basis_wages'] * $empEDLI) / 100));
                $totalempINEPC = ((($salary['basis_wages'] * $empINEPC) / 100));
                $totalempADMIN = ((($salary['basis_wages'] * $empADMIN) / 100));

                $a = $a + $salary['basis_wages'];
                $b = $b + $salary['pf'];
                $c = $c + $totalempEDLI;
                $d = $d + $totalempINEPC;
                $f = $f + $totalempADMIN;
                $g = $g + $totalempPF;
                $h = $h + $totalempFPF;
                $k = $b + $b;
                $l = $k + $h + $c + $d + $f;
            }
            return Excel::download(new PfExport($salaries), "Pf.$monthName.xlsx");
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // All Pf Excel Export 
    public function allPfExcel()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('excel.pf_report_all_branch.export', compact('branch', 'employee'));
    }

    public function exportAllPfExcel()
    {
        echo "hello";
    }

    // Register Of Overtime
    public function registerOfOvertime()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.register_of_overtime.index', compact('branch', 'employee'));
    }

    public function printRegisterOfOvertime()
    {
        echo "hello";
    }

    // Register Of Walfare
    public function registerOfWalfare()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.register_of_walfer.index', compact('branch', 'employee'));
    }

    public function registerOfWalfareReport(Request $request)
    {
        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');
        $salaries = Salary::where('branch_id', $branch_id)
            ->where('monthyear', $monthName)
            ->get();
        if ($salaries->count() >= 1) {
            $totalWalferAmount = 0;
            foreach ($salaries as $salary) {
                $totalWalferAmount += $salary->walfare_amount;
            }

            $pdf = PDF::loadView('pdf.register_of_walfer.register_walfer', [
                'salaries' => $salaries,
                'branch_name' => $branch_name,
                'branch_address' => $branch_address,
                'monthName' => $monthName,
                'totalWalferAmount' => $totalWalferAmount
            ]);
            $pdf->setPaper('A4', 'potrait');
            $pdf->download('register_walfer.pdf');
            return $pdf->download("register_walfer.$monthName.pdf");
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // Bonus Reports
    public function bonusReport()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.bonus_reports.index', compact('branch', 'employee'));
    }

    public function printBonusReport(Request $request)
    {
        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $year = substr($month, 0, 4);
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $employees = Employee::where('status', 'active')->get();

        foreach ($employees as $employee) {
            $employee->salaries = Salary::with('employee', 'designation')
                ->where('employee_id', $employee->id)
                ->where('branch_id', $branch_id)
                ->where('year', $year)
                ->get();
        }
        $has_salaries = false;
        foreach ($employees as $employee) {
            if (count($employee->salaries) > 0) {
                $employee->total_working_days = 0;
                $employee->total_basis_wages = 0;
                $employee->total_bonus = 0;
                $employee->total_paid_amount = 0;
                $employee->designation_name = '';
                foreach ($employee->salaries as $salary) {
                    $employee->designation_name = $salary->designation->designation_name;
                    $employee->total_working_days += $salary->no_of_days_work_done;
                    $employee->total_basis_wages += $salary->basis_wages;
                    $employee->total_bonus += $salary->bonus;
                    $employee->total_paid_amount += $salary->nettotal;
                }

                $has_salaries = true;
            }
        }

        if ($has_salaries) {
            $pdf = PDF::loadView('pdf.bonus_reports.bonus_reports', [
                'branch_name' => $branch_name,
                'year' => $year,
                'employees' => $employees
            ]);
            $pdf->setPaper('A4', 'potrait');
            $pdf->download('bonus_reports.pdf');
            return $pdf->download('bonus_reports.pdf');
        } else {
            // No employees with salaries found, return back from the function

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }


    // Bonus Report Monthly
    public function bonusReportMonthly()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.bonus_reports_monthly.index', compact('branch', 'employee'));
    }

    public function printbonusReportMonthly(Request $request)
    {
        $fromMonthyear = $request->input('startmonthyear');
        $toMonthyear = $request->input('endmonthyear');

        $date1 = Carbon::createFromFormat('Y-m', $fromMonthyear);
        $fromMonthName = $date1->format('Y-M');
        $date2 = Carbon::createFromFormat('Y-m', $toMonthyear);
        $toMonthName = $date2->format('Y-M');

        $branch_id = $request->input('branch_id');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $employees = Employee::where('status', 'active')->get();

        foreach ($employees as $employee) {
            $employee->salaries = Salary::with('employee', 'designation')
                ->whereBetween('monthyear', [$fromMonthName, $toMonthName])
                ->where('employee_id', $employee->id)
                ->where('branch_id', $branch_id)
                ->get();
        }

        $has_salaries = false;
        foreach ($employees as $employee) {
            if (count($employee->salaries) > 0) {
                $employee->total_working_days = 0;
                $employee->total_basis_wages = 0;
                $employee->total_bonus = 0;
                $employee->total_paid_amount = 0;
                $employee->designation_name = '';
                foreach ($employee->salaries as $salary) {
                    $employee->designation_name = $salary->designation->designation_name;
                    $employee->total_working_days += $salary->no_of_days_work_done;
                    $employee->total_basis_wages += $salary->basis_wages;
                    $employee->total_bonus += $salary->bonus;
                    $employee->total_paid_amount += $salary->nettotal;
                }

                $has_salaries = true;
            }
        }
        if ($has_salaries) {
            $pdf = PDF::loadView('pdf.bonus_reports_monthly.bonus_report', [
                'branch_name' => $branch_name,
                'employees' => $employees,
                'fromMonthName' => $fromMonthName,
                'toMonthName' => $toMonthName
            ]);
            $pdf->setPaper('A4', 'potrait');
            $pdf->download('bonus_reports_monthly.pdf');
            return $pdf->download('bonus_reports_monthly.pdf');
        } else {
            // No employees with salaries found, return back from the function

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // Manage Warefare Fund
    public function manageWarfareFund()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('manage_walfare_fund.index', compact('branch', 'employee'));
    }

    // Police Verification Report Excel Format
    public function policeVerificationReportExcel()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('excel.police_verification_report_excel.export', compact('branch', 'employee'));
    }

    public function policeVerificationReportExcelExport()
    {
        echo "hello";
    }

    // Male/Female Gross Salary Report (Monthwise)
    public function maleFemaleSalary()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.male_female_gross_salary.index', compact('branch', 'employee'));
    }

    //attandace import view
    public function importIndex()
    {
        $branch = Branch::all();
        return view('excel.attendance.import', compact('branch'));
    }

    //attandace export view
    public function exportIndex()
    {
        $branch = Branch::all();
        return view('excel.attendance.export', compact('branch'));
    }

    // attandace export action
    public function export(Request $request)
    {
        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');
        $attendance = Attendance::with('company', 'branch', 'designation', 'employee')->where('branch_id', $branch_id)->where('monthyear', $monthName)->get();
        if ($attendance->count() >= 1) {

            return Excel::download(new AttendanceUploadExport($attendance), "Attendance.$monthName.xlsx");
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // attandace import action
    public function import(Request $request)
    {
        try {
            $file = $request->file('file');
            $branch_id = $request['branch_id'];
            Excel::import(new AttendanceImport($branch_id), $file);
            return redirect()->back()->with('success', 'Attendance data imported successfully.');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Error importing attendance data: ' . $e->getMessage());
        }
    }

    // importSalaryExcel
    public function importSalaryExcel()
    {
        $branch = Branch::all();
        return view('excel.salary.import_salary', compact('branch'));
    }
    public function addImportSalaryExcel(Request $request)
    {
        try {
            $file = $request->file('file');
            $branch_id = $request['branch_id'];
            Excel::import(new SalaryImport($branch_id), $file);
            return redirect()->back()->with('success', 'Salary data imported successfully.');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Error importing salary data: ' . $e->getMessage());
        }
    }

    // Upload Excel For Pf AC Code/ Pf UAN Code
    public function uploadPfCode()
    {
        return view('excel.import_pf_code.import');
    }

    public function uploadPfCodeImport(Request $request)
    {
        try {
            $file = $request->file('file');
            Excel::import(new PfImport, $file);
            return redirect()->back()->with('success', 'Pf AC Code/ PF UAN Code data imported successfully.');
        } catch (\Exception $e) {
            return redirect()->back()->with('error', 'Error importing Pf AC Code/ PF UAN Code data: ' . $e->getMessage());
        }
    }

    public function printMaleFemaleSalary(Request $request)
    {
        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');

        $salaries = Salary::with(['employee', 'branch'])
            ->where('monthyear', $monthName)
            ->where('branch_id', $branch_id)
            ->get();
        
        if ($salaries->count() >= 1) {
            $working_day = $salaries->first()->working_days;
            $totalMale = 0;
            $totalFemale = 0;
            $totalWorker = 0;
            $totalPfEsicMale = 0;
            $totalpfEsicFemale = 0;
            $totalSalaryMale = 0;
            $totalSalaryFemale = 0;

            foreach ($salaries as $salary) {
                if ($salary->employee->sex == 'male') {
                    $totalMale++;
                    $totalPfEsicMale += $salary->pf + $salary->esic;
                    $totalSalaryMale += $salary->nettotal;
                } elseif ($salary->employee->sex == 'female') {
                    $totalFemale++;
                    $totalpfEsicFemale += $salary->pf + $salary->esic;
                    $totalSalaryFemale += $salary->nettotal;
                }
                $totalWorker++;
            }
            $totalMaleDays =  $working_day * $totalMale;
            $totalFemaleDays = $working_day * $totalFemale;
            $totalDays = $totalMaleDays + $totalFemaleDays;
            $totalGrossSalary = $totalSalaryMale + $totalSalaryFemale;
            $totalPfEsic = $totalPfEsicMale + $totalSalaryFemale;
            $pdf = PDF::loadView('pdf.male_female_gross_salary.male_female_salary', [
                'branch_name' => $branch_name,
                'branch_address' => $branch_address,
                'monthName' => $monthName,
                'working_day' => $working_day,
                'totalMale' => $totalMale,
                'totalFemale' => $totalFemale,
                'totalWorker' => $totalWorker,
                'totalPfEsicMale' => $totalPfEsicMale,
                'totalpfEsicFemale' => $totalpfEsicFemale,
                'totalPfEsic' => $totalPfEsic,
                'totalSalaryMale' => $totalSalaryMale,
                'totalSalaryFemale' => $totalSalaryFemale,
                'totalMaleDays' => $totalMaleDays,
                'totalFemaleDays' => $totalFemaleDays,
                'totalDays' => $totalDays,
                'totalGrossSalary' => $totalGrossSalary,
            ]);
            $pdf->setPaper('A4', 'potrait');
            $pdf->download('male_female_salary.pdf');
            return $pdf->download('male_female_salary.pdf');
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }


    // importSalaryExcel
    // public function importSalaryExcel()
    // {
    //     $branch = Branch::all();
    //     return view('excel.salary.import_salary', compact('branch'));
    // }
    // public function addImportSalaryExcel(Request $request)
    // {
    //     $file = $request->file('file');
    //     $data = Excel::toArray([], $file);
    //     $branch_id = $request['branch_id'];
    //     $employee_id = $data[0][1][0];
    //     $monthyear = $data[0][1][2];
    //     // $salaries = Salary::all();
    //     $employee = Employee::where('id', $employee_id)->first();

    //     // Get Overtime Value
    //     $overtimeAmount = $this->getOvertimeamount($employee_id, $monthyear);

    //     if (count($overtimeAmount) > 0) {
    //         $totalOvertimeAmount = $overtimeAmount[0]->total_overtime_amount;
    //         $totalOvertimeDays = $overtimeAmount[0]->total_overtime_days;
    //     } else {
    //         $totalOvertimeAmount = 0;
    //         $totalOvertimeDays = 0;
    //     }
    //     //Get Damage Value 
    //     $damage = $this->getDamageAmount($employee_id, $monthyear);

    //     // Get Fine Value
    //     $totalFine = $this->getFineAmount($employee_id, $monthyear);

    //     // Advance Salary
    //     $totalAdvance = $this->getAdvance($employee_id, $monthyear);
    //     if (isset($employee)) {
    //         // Initialize a variable to track whether an attendance record was found for the given employee and monthyear
    //         $branchtansfer = BranchTransfer::where('employee_id', $employee_id)->where('branch_id', $branch_id)->first();
    //         if (isset($branchtansfer)) {
    //             $salary = Salary::where('employee_id', $employee_id)->where('branch_id', $branch_id)->where('designation_id', $branchtansfer->designation_id)->where('monthyear', $monthyear)->get();
    //             if (count($salary) == 0) {
    //                 $attandace = Attendance::where('employee_id', $employee_id)->where('branch_id', $branch_id)->where('designation_id', $branchtansfer->designation_id)->where('monthyear', $monthyear)->get();
    //                 if (isset($attandace)) {
    //                     $branch_id =  $branchtansfer->branch_id;
    //                     $designation_id =  $branchtansfer->designation_id;
    //                     $department_id =  $branchtansfer->department_id;
    //                     $grade_id =  $branchtansfer->grade_id;
    //                     $employee_id = $employee_id;

    //                     Excel::import(new SalaryImport($branch_id, $designation_id, $department_id, $grade_id, $employee_id, $monthyear, $totalOvertimeAmount, $totalOvertimeDays, $damage, $totalFine, $totalAdvance), $file);
    //                     return redirect()->back()->with('success', 'File imported successfully.');
    //                 } else {
    //                     return redirect()->back()->with('fail', 'attandace not Exist');
    //                 }
    //             } else {

    //                 return redirect()->back()->with('fail', 'salary all-ready exist');
    //             }
    //         } else {
    //             return redirect()->back()->with('fail', 'branch transfer is not found.');
    //         }
    //     } else {
    //         // If $employee is not set, print 'back'
    //         return redirect()->back()->with('fail', 'employee is not found.');
    //     }
    // }

    // GET OVERTIME
    public function getOvertimeamount($employee_id, $monthyear)
    {
        $row = OverTime::where('employee_id', $employee_id)
            ->where('monthyear', $monthyear)
            ->get();
        return $row;
    }

    // GET DAMAGE
    public function getDamageAmount($employee_id, $monthyear)
    {
        $damage = Damage::where('employee_id', $employee_id)
            ->where('monthyear', $monthyear)
            ->get();
        $totalDamage = 0;
        foreach ($damage as $data) {
            $totalDamage += $data->damage_amount;
        }
        return $totalDamage;
    }

    // GET FINE
    public function getFineAmount($employee_id, $monthyear)
    {
        $rows = Fine::where('employee_id', $employee_id)
            ->where('monthyear', $monthyear)
            ->get();

        $totalFineAmount = 0;
        foreach ($rows as $row) {
            $totalFineAmount += $row->fine_amount;
        }

        return $totalFineAmount;
    }

    // GET ADVANCE
    public function getAdvance($employee_id, $monthyear)
    {
        $rows = AdvanceSalary::where('employee_id', $employee_id)
            ->where('monthyear', $monthyear)
            ->get();
        $advanceAmount = 0;
        foreach ($rows as $row) {
            $advanceAmount += $row->amount;
        }
        return $advanceAmount;
    }

    // Export Branch wise Salary
    public function exportSalaryExcel()
    {
        $branch = Branch::all();
        return view('excel.salary.export_salary', compact('branch'));
    }

    public function printexportSalaryExcel(Request $request)
    {

        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');
        $salaries = Salary::with('company', 'branch', 'designation', 'employee')->where('branch_id', $branch_id)->where('monthyear', $monthName)->get();
        if ($salaries->count() >= 1) {

            return Excel::download(new SalaryExport($salaries), "Salary.$monthName.xlsx");
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // branchwise salary Report
    public function branchWiseSalary()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.branch_wise_salary.index', compact('branch', 'employee'));
    }

    public function printBranchWiseSalary(Request $request)
    {
        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');

        $salaries = Salary::with('employee.bank')->with('employee.bankBranch')->where('monthyear', $monthName)
            ->where('branch_id', $branch_id)
            ->get();
        if ($salaries->count() >= 1) {
            $workingday = 0;
            $workingdayDone = 0;
            $basicDa = 0;
            $extraAllowance = 0;
            $fixSalary = 0;
            $basicWages = 0;
            $hra = 0;
            $c = 0;
            $otAmount = 0;
            $total = 0;
            $branchBonus = 0;
            $totalBranchBonus = 0;
            $damage = 0;
            $fine = 0;
            $pf = 0;
            $professionalTax = 0;
            $esic = 0;
            $walferFund = 0;
            $salaryAdvance = 0;
            $mess = 0;
            $netAmountPaid = 0;

            // Loop through the salary data and calculate the totals
            foreach ($salaries as $salary) {
                $workingday += $salary->working_days;
                $workingdayDone += $salary->no_of_days_work_done;
                $basicDa += $salary->basic_da;
                $extraAllowance += $salary->extra_allowance;
                $fixSalary += $salary->fix_salary;
                $basicWages += $salary->basis_wages;
                $hra += $salary->hra;
                $c += $salary->c;
                $otAmount += $salary->ot_amount;
                $total += $salary->total;
                $branchBonus += $salary->bonus;
                $damage += $salary->damage_amount;
                $fine += $salary->fines;
                $pf += $salary->pf;
                $professionalTax += $salary->profetional_tax;
                $esic += $salary->esic;
                $walferFund += $salary->walfare_amount;
                $salaryAdvance += $salary->sal_advance;
                $mess += $salary->mess;
                $netAmountPaid += $salary->nettotal;
            }
            $totalBranchBonus = $total + $branchBonus;
            $pdf = PDF::loadView('pdf.branch_wise_salary.salary_pdf', [
                'salaries' => $salaries,
                'branch_name' => $branch_name,
                'branch_address' => $branch_address,
                'monthName' => $monthName,
                'workingday' => $workingday,
                'workingdayDone' => $workingdayDone,
                'basicDa' => $basicDa,
                'extraAllowance' => $extraAllowance,
                'fixSalary' => $fixSalary,
                'basicWages' => $basicWages,
                'hra' => $hra,
                'c' => $c,
                'otAmount' => $otAmount,
                'total' => $total,
                'branchBonus' => $branchBonus,
                'damage' => $damage,
                'fine' => $fine,
                'pf' => $pf,
                'professionalTax' => $professionalTax,
                'esic' => $esic,
                'walferFund' => $walferFund,
                'salaryAdvance' => $salaryAdvance,
                'mess' => $mess,
                'netAmountPaid' => $netAmountPaid,
                'totalBranchBonus' => $totalBranchBonus
            ]);
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download("BranchwiseSalary.$monthName.pdf");
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // branch wise delete salary
    public function deleteSalary()
    {
        $branch = Branch::all();
        return view('branchwise_delete.delete_salary', compact('branch'));
    }

    public function deleteSalaryAdd(Request $request)
    {
        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');
        $deleted = Salary::where('branch_id', $branch_id)
            ->where('monthyear', $monthName)
            ->delete();

        if ($deleted == 0) {
            return redirect()->back()->with('fail', 'Salary records not found.');
        }
        return redirect()->back()->with('success', 'Salary records deleted successfully.');
    }

    //Branchwise Delete Attendance
    public function deleteAttendance()
    {
        $branch = Branch::all();
        return view('branchwise_delete.delete_attendance', compact('branch'));
    }

    // public function deleteAttendanceAdd(Request $request)
    // {
    //     $month = $request->input('monthyear');
    //     $branch_id = $request->input('branch_id');
    //     $date = Carbon::createFromFormat('Y-m', $month);
    //     $monthName = $date->format('Y-M');
    //     DB::table('attendance')
    //         ->where('branch_id', $branch_id)
    //         ->where('monthyear', $monthName)
    //         ->delete();
    //     return redirect()->back()->with('success', 'Attendance records deleted successfully.');
    // }


    public function deleteAttendanceAdd(Request $request)
    {
        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');
        $deleted = Attendance::where('branch_id', $branch_id)
            ->where('monthyear', $monthName)
            ->delete();

        if ($deleted == 0) {
            return redirect()->back()->with('fail', 'Attendance records not found.');
        }
        return redirect()->back()->with('success', 'Attendance records deleted successfully.');
    }

    // Employee Month Salary
    public function employeeMonthSalary()
    {
        $branch = Branch::all();
        return view('excel.employee_salary.index', compact('branch'));
    }

    public function employeeMonthSalaryExcel(Request $request)
    { 
     
        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');
        $salaries = Salary::with('company', 'branch', 'designation', 'employee')->where('branch_id', $branch_id)->where('monthyear', $monthName)->get();
        if ($salaries->count() >= 1) {
            return Excel::download(new EmployeeSalaryExport($salaries), "Salary.$monthName.csv");
        } else {

            return redirect()->back()->with('fail', 'Data does not exists.');
        }
    }

    // Wage Slip 
    public function wageSlip()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.wage_slip.index', compact('branch', 'employee'));
    }

    public function printWageSlip(Request $request)
    {
        $month = $request->input('monthyear');
        $branch_id = $request->input('branch_id');
        $branch = Branch::where('id', $branch_id)->first();
        $branch_name = $branch->name;
        $branch_address = $branch->address;
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');

        $salaries = Salary::with('employee.bank')->with('employee.bankBranch')->where('monthyear', $monthName)
            ->where('branch_id', $branch_id)
            ->get();

        if ($salaries->count() >= 1) {
            $pdf = PDF::loadView('pdf.wage_slip.wage_slip', [
                'salaries' => $salaries,
                'branch_name' => $branch_name,
                'branch_address' => $branch_address
            ]);
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download("Wage_slip.$monthName.pdf");
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // month to month Wage Slip 
    public function monthWageSlip()
    {
        $employees = Employee::all();
        return view('pdf.month_to_month_wage_slip.index', compact('employees'));
    }

    public function printMonthWageSlip(Request $request)
    {
        $fromMonthyear = $request->input('from_monthyear');
        $toMonthyear = $request->input('to_monthyear');
        $employee_id = $request->input('employee_id');
        $employee = Employee::where('id', $employee_id)->first();
        $employeeName = $employee->full_name;
        $pfUanCode = $employee->pf_uan_code;
        $pfAccountCode = $employee->pf_ac_code;

        // find Employee FK Detail
        $employee = Employee::with(['department', 'designation', 'branch'])->find($employee_id);
        $departmentName = $employee->department->department_name;
        $designationName = $employee->designation->designation_name;
        $branch_name = $employee->branch->name;
        $branch_address = $employee->branch->address;

        // find esic
        $esic = Salary::where('employee_id', $employee_id)->value('esic');

        // get monthyear Value
        $date1 = Carbon::createFromFormat('Y-m', $fromMonthyear);
        $fromMonthName = $date1->format('Y-M');
        $date2 = Carbon::createFromFormat('Y-m', $toMonthyear);
        $toMonthName = $date2->format('Y-M');

        $salaries = Salary::whereBetween('monthyear', [$fromMonthName, $toMonthName])->where('employee_id', $employee_id)->get();

        if ($salaries->count() >= 1) {
            $pdf = PDF::loadView('pdf.month_to_month_wage_slip.wage_slip', [
                'salaries' => $salaries,
                'employeeName' => $employeeName,
                'employee_id' => $employee_id,
                'branch_name' => $branch_name,
                'branch_address' => $branch_address,
                'designationName' => $designationName,
                'departmentName' => $departmentName,
                'pfUanCode' => $pfUanCode,
                'pfAccountCode' => $pfAccountCode,
                'esic' => $esic
            ]);
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download("Wage_slip.pdf");
        } else {

            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // Esic Reports
    public function esic()
    {
        $branch = Branch::all();
        $employee = Employee::all();
        return view('pdf.esic_report.index', compact('branch', 'employee'));
    }



    // public function printEsic(Request $request)
    // {
    //     $monthyear = Carbon::createFromFormat('Y-n', $request->monthyear)->format('Y-M');
    //     $branch_id = $request->branch_id;
    //     $salaries = Salary::with('employee')->where('branch_id', $branch_id)
    //         ->where('monthyear', $monthyear)
    //         ->get();

    //     $totalEsic1 = 0;
    //     $totalEsic2 = 0;
    //     $totalEsic3 = 0;
    //     $totalEsic4 = 0;

    //     foreach ($salaries as $salary) {
    //         // Calculate ESIC deduction for current employee
    //         $salary->esic1 = ($salary->basic_da * 1.75) / 100;
    //         $salary->esic2 = ($salary->basic_da * 4.75) / 100;
    //         $salary->esic3 = ($salary->fix_salary * 1.75) / 100;
    //         $salary->esic4 = ($salary->fix_salary * 4.75) / 100;

    //         // Add ESIC deduction to the total
    //         $totalEsic1 += $salary->esic1;
    //         $totalEsic2 += $salary->esic2;
    //         $totalEsic3 += $salary->esic3;
    //         $totalEsic4 += $salary->esic4;

    //         $salary->totalEsicaaa = $salary->esic1 + $salary->esic2 +  $salary->esic3 + $salary->esic4;
    //     }

    //     $branch = DB::table('branches')->where('id', $branch_id)->first();
    //     $branch_name = $branch->name;
    //     $branch_address = $branch->address;
    //     $pdf = PDF::loadView('pdf.esic_report.esic_report', [
    //         'monthyear' => $monthyear,
    //         'branch_name' => $branch_name,
    //         'branch_address' => $branch_address,
    //         'salaries' => $salaries,
    //         'salary' => $salary,
    //         'totalEsic1' => $totalEsic1,
    //         'totalEsic2' => $totalEsic2,
    //         'totalEsic3' => $totalEsic3,
    //         'totalEsic4' => $totalEsic4,
    //     ]);
    //     $pdf->setPaper('A4', 'landscape');
    //     return $pdf->download("esic.pdf");
    // }


    public function printEsic(Request $request)
    {
        $monthyear = Carbon::createFromFormat('Y-n', $request->monthyear)->format('Y-M');
        $taxManager = TaxManager::where('monthyear', $request->monthyear)->first();
        $employeeEsic =$taxManager['employee_esic'];
        $employerEsic =$taxManager['employer_esic'];

        $branch_id = $request->branch_id;
        $salary = Salary::with('employee')->where('branch_id', $branch_id)
            ->where('monthyear', $monthyear)
            ->get();

        $esic1 = 0;
        $esic2 = 0;
        $esic3 = 0;
        $esic4 = 0;
        $totalEsic1 = 0;
        $totalEsic2 = 0;
        $totalEsic3 = 0;
        $totalEsic4 = 0;
        $totalFix = 0;
        $totalBasic = 0;
        $totalFinal = 0;

        foreach ($salary as $salaries) {
            // Calculate ESIC deduction for current employee
            $salaries->esic1  = ($salaries->basic_da * $employeeEsic) / 100;
            $salaries->esic2 = ($salaries->basic_da * $employerEsic) / 100;
            $salaries->esic3 = ($salaries->fix_salary * $employeeEsic) / 100;
            $salaries->esic4 = ($salaries->fix_salary * $employerEsic) / 100;
            // Add ESIC deduction to the total
            $totalEsic1 += $salaries->esic1;
            $totalEsic2 += $salaries->esic2;
            $totalEsic3 += $salaries->esic3;
            $totalEsic4 += $salaries->esic4;

            // count fix and basic
             $totalFix += $salaries->fix_salary; 
             $totalBasic += $salaries->basic_da; 

            $salaries->totalEsicaaa = $salaries->esic1 + $salaries->esic2 +  $salaries->esic3 + $salaries->esic4;

            // netTotal 
             $totalFinal += $salaries->totalEsicaaa;

        }

        if ($salary->count() >= 1) {
            $branch = DB::table('branches')->where('id', $branch_id)->first();
            $branch_name = $branch->name;
            $branch_address = $branch->address;
            $pdf = PDF::loadView('pdf.esic_report.esic_report', [
                'monthyear' => $monthyear,
                'branch_name' => $branch_name,
                'branch_address' => $branch_address,
                'salaries' => $salaries,
                'salary' => $salary,
                'totalEsic1' => $totalEsic1,
                'totalEsic2' => $totalEsic2,
                'totalEsic3' => $totalEsic3,
                'totalEsic4' => $totalEsic4,
                'totalFix'=>$totalFix,
                'totalBasic'=>$totalBasic,
                'totalFinal'=> $totalFinal
            ]);
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download("esic.pdf");
        } else {
            return redirect()->back()->with('fail', 'records not exist ');
        }
    }

    // Stock Reports
    public function stock()
    {
        $categories = StockCategory::all();
        return view('pdf.stock_report.index', compact('categories'));
    }

    public function printStock(Request $request)
    {
        try {
            $total_added_stock = 0;
            $start_date = $request['startdate'];
            $end_date = $request['enddate'];
            $category = $request['category_id'];

            $dateTime = DateTime::createFromFormat("m/d/Y h:i A", $start_date);
            $dateTime2 = DateTime::createFromFormat("m/d/Y h:i A", $end_date);

            $startDate = $dateTime->format("Y-m-d");
            $endDate = $dateTime2->format("Y-m-d");

            $data = array();
            $rows = DB::table('advance_salaries')
                ->whereBetween('date_of_advance', [$startDate, $endDate])
                ->where('stock_category_id', $category)
                ->whereNotNull('no_of_qty')
                ->get();

                
            foreach ($rows as $row) {
               // $designationId = $row->designation_id;
                $stockCategory = StockCategory::where('id', (int) $row->stock_category_id)->first();
                $category_name = $stockCategory->stock_category_name;
                // $employee_name = $row->full_name;
                // $advanceSalary = AdvanceSalary::find($designationId);
                // $designation_name = $advanceSalary->designation->designation_name;

                $employee = DB::table('employees')->where('id', $row->employee_id)->first();
                $employee_name = $employee->full_name;
                $date = $row->date_of_advance;

                $stockRegister = StockRegister::where('stock_category_id', (int) $row->stock_category_id)->first();
                $total_stock = $stockRegister->stock_quantity;
                $added_stock = $row->no_of_qty;

                $total_added_stock += $added_stock; // perform some calculation
                $data[] = array(
                    'branch_id' => $row->branch_id,
                    'designation_id' => $row->designation_id,
                    'employee_code' => $row->employee_id,
                    'date' => $date,
                    'designation_name' => "hello",
                    'category_name' => $category_name,
                    'employee_name' => $employee_name,
                    'total_stock' => $total_stock,
                    'added_stock' => $added_stock,
                    'total_added_stock' => $total_added_stock
                );
            }

            if ($data == null) {
                return redirect()->back()->with('fail', 'records not found.');
            } else {
                $total_remaining_stock =   $total_stock - $total_added_stock;
                $pdf = PDF::loadView('pdf.stock_report.stock', ['data' => $data, 'total_remaining_stock' => $total_remaining_stock]);
                $pdf->setPaper('A4', 'landscape');
                return $pdf->download("stock.pdf");
            }
        } catch (\Exception $e) {
            return redirect()->back()->with('fail', 'Unexcepted error occurred: ' . $e->getMessage());
        }
    }



    // public function getStockReport($start_date, $end_date, $register_category)
    // {
    //     $data = array();

    //     $rows = DB::table('sal_advance')
    //         ->whereBetween('advance_date', [$start_date, $end_date])
    //         ->where('register_category', $register_category)
    //         ->whereNotNull('no_of_qty')
    //         ->get();

    //     foreach ($rows as $row) {
    //         $stockCategory = StockCategory::where('category_id', (int) $row->register_category)->first();
    //         $category_name = $stockCategory->category_name;
    //         $employee_name = $row->full_name;
    //         $date = $row->date;

    //         $stockRegister = StockRegister::where('stock_category_id', (int) $row->register_category)->first();
    //         $total_stock = $stockRegister->stock_qty;

    //         $data[] = array(
    //             'branch_id' => $row->branch_id,
    //             'designation_id' => $row->designation_id,
    //             'employee_code' => $row->employee_code,
    //             'date' => $date,
    //             'category_name' => $category_name,
    //             'employee_name' => $employee_name,
    //             'total_stock' => $total_stock,
    //             'added_stock' => $row->no_of_qty
    //         );
    //     }

    //     return $data;
    // }




    // ALL Employee Reports
    public function printAllEmployee()
    {
        $employee = Employee::where('status', '=', 'active')->get();
        $pdf = PDF::loadView('pdf.all_employee.index', ['employees' => $employee]);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download("all_employee.pdf");
    }

    // ALL Employee Reports
    public function printLeftEmployee()
    {
        $employee = Employee::where('status', '=', 'inactive')->get();
        $pdf = PDF::loadView('pdf.all_employee.index', ['employees' => $employee]);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download("all_employee.pdf");
    }
    // Print Credit Salary
    public function printCreditSalary($id)
    {
        $creditSalaries = CreditSalary::with('companyBank')->where('branch_id', $id)->get();   

            foreach ($creditSalaries as $creditSalary) {
                $bankName = $creditSalary->companyBank->bank_name ;
                $bankAdreess = $creditSalary->companyBank->bankBranch;
                $monthyear = $creditSalary->monthyear;
                $accountNo = $creditSalary->companyBank->account_no;
            }

            $branch = DB::table('branches')->where('id', $id)->first();
            $branch_name = $branch->name;
            $branch_address = $branch->address;
            $salaries = Salary::with('employee')->where('branch_id', $id)->get();
            if ($salaries->count() >= 1) {      
            $amountCredit = 0;
            foreach ($salaries as $salary) {
                $amountCredit += $salary->nettotal;
            }

            $pdf = PDF::loadView('pdf.credit_salary.credit_salary', [
                'creditSalaries' => $creditSalaries,
                'bankName' => $bankName,
                'bankAdreess' => $bankAdreess,
                'branch_name' => $branch_name,
                'branch_address' => $branch_address,
                'salaries' => $salaries,
                'monthyear' => $monthyear,
                'accountNo' => $accountNo,
                'amountCredit' => $amountCredit
            ]);
            $pdf->setPaper('A4', 'landscape');
            return $pdf->download("credit_salary.$monthyear.pdf");
            }   else {
                return redirect()->back()->with('fail', 'records not exist ');
            }                 
      
    }


   // Employee leave history
    public function employeeLeaveHistory()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('pdf.employee_leave_history.index', $data);
    }

    public function printEmployeeLeaveHistory(Request $request)
    { 
        $monthyear = $request->get('monthyear');
        $branch_id = $request->get('branch_id');
        $branch = DB::table('branches')->where('id', $branch_id)->first();
        $branchName = $branch->name;

        $data = EmployeeLeaveHistory::where('created_at', 'LIKE', $monthyear . '%')->where('branch_id', $branch_id)->get(); 
        if ($data->count() >= 1) {
                $pdf = PDF::loadView('pdf.employee_leave_history.employee_leave_history', [
                    'data' => $data,
                    'monthyear'=>$monthyear,
                    'branchName'=>$branchName,
                ]);
                $pdf->setPaper('A4', 'landscape');
                return $pdf->download('employee_leave_history.pdf');
        } else {
            return redirect()->back()->with('fail', 'Data does not exists');
        }
    }

    // Stock History Reports
    public function stockHistory()
    {
       $data['branches'] = Branch::get(["name", "id"]);         
       return view('pdf.stock_history.index', $data);         
    }


// public function printStockHistory(Request $request)
// {
//     $monthyear = $request->get('monthyear');
//     $data = StockRegister::whereYear('stock_in_date', date('Y', strtotime($monthyear)))
//         ->whereMonth('stock_in_date', date('m', strtotime($monthyear)))
//         ->get();

//     $totalCredit = 0;
//     $totalDebit = 0;

//     foreach ($data as $list) {
//         $stockId = $list->stock_category_id;
//         $balanceCode = $list->balance_code;
//         if ($balanceCode == 'cr') {
//             $stockRegisters = StockRegister::where('stock_category_id', $stockId)
//                 ->where('balance_code', $balanceCode)
//                 ->groupBy('stock_category_id') // Include stock_registers.id in GROUP BY
//                 ->get();
//             $list->totalCredit += $stockRegisters->sum('stock_quantity');
//         } else {
//             $stockRegisters = StockRegister::where('stock_category_id', $stockId)
//                 ->where('balance_code', $balanceCode)
//                 ->groupBy('stock_category_id') // Include stock_registers.id in GROUP BY
//                 ->get();
//             $list->totalDebit += $stockRegisters->sum('stock_quantity');
//         }
//     }

//     if ($data->count() >= 1) {
//         $pdf = PDF::loadView('pdf.stock_history.stock_history', [
//             'data' => $data,
//             'monthyear' => $monthyear
//         ]);
//         $pdf->setPaper('A4', 'landscape');
//         return $pdf->download('stock_history.pdf');
//     } else {
//         return redirect()->back()->with('fail', 'Data does not exist');
//     }
// }

public function printStockHistory(Request $request)
{
    $monthyear = $request->get('monthyear');

    $data = StockRegister::whereYear('stock_in_date', date('Y', strtotime($monthyear)))
        ->whereMonth('stock_in_date', date('m', strtotime($monthyear)))
        ->get();
     
    $stockCategoryIds = $data->pluck('stock_category_id')->unique();
    foreach ($stockCategoryIds as $stockId) {
        $stockRegisters = StockRegister::where('stock_category_id', $stockId)->get();
        $totalCredit = $stockRegisters->where('balance_code', 'cr')->sum('stock_quantity');
        $totalDebit = $stockRegisters->where('balance_code', 'dr')->sum('stock_quantity');

        // Update the corresponding records in $data
        $data->where('stock_category_id', $stockId)->each(function ($item) use ($totalCredit, $totalDebit) {
            $item->totalCredit = $totalCredit;
            $item->totalDebit = $totalDebit;
        });
    }
    if ($data->count() >= 1) {
        $pdf = PDF::loadView('pdf.stock_history.stock_history', [
            'data' => $data,
            'monthyear' => $monthyear
        ]);
        $pdf->setPaper('A4', 'landscape');
        return $pdf->download('stock_history.pdf');
    } else {
        return redirect()->back()->with('fail', 'Data does not exist');
    }
}

}
