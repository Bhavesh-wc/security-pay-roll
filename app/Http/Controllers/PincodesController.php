<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\PincodesFormRequest;
use App\Models\City;
use App\Models\Pincode;
use Auth;
use Exception;

class PincodesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the pincodes.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $pincodes = Pincode::orderBy('created_at', 'desc')->with('city', 'company')->get();

        return view('pincodes.index', compact('pincodes'));
    }

    /**
     * Show the form for creating a new pincode.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $cities = City::where('status', 'active')->pluck('city_name', 'id')->all();

        return view('pincodes.create', compact('cities'));
    }

    /**
     * Store a new pincode in the storage.
     *
     * @param App\Http\Requests\PincodesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(PincodesFormRequest $request)
    {
        try {
            if ($request['hidden']) {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                $data = Pincode::create($data);
                // return redirect()->back();
                return response()->json($data);
            } else {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                Pincode::create($data);
                return redirect()->route('pincodes.pincode.index')
                    ->with('success_message', 'Pincode was successfully added.');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified pincode.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $pincode = Pincode::with('city')->findOrFail($id);

        return view('pincodes.show', compact('pincode'));
    }

    /**
     * Show the form for editing the specified pincode.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $pincode = Pincode::findOrFail($id);
        $cities = City::where('status', 'active')->pluck('city_name', 'id')->all();

        return view('pincodes.edit', compact('pincode', 'cities'));
    }

    /**
     * Update the specified pincode in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\PincodesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, PincodesFormRequest $request)
    {
        try {

            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $pincode = Pincode::findOrFail($id);
            $pincode->update($data);

            return redirect()->route('pincodes.pincode.index')
                ->with('success_message', 'Pincode was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified pincode from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $pincode = Pincode::findOrFail($id);
            if ($pincode->employees()->exists()) {
                throw new Exception("This pincode has employee and cannot be deleted.");
            }
            $pincode->delete();

            return redirect()->route('pincodes.pincode.index')
                ->with('success_message', 'Pincode was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
