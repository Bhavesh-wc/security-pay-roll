<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CompanyBanksFormRequest;
use App\Models\CompanyBank;
use Exception;
use Auth;

class CompanyBanksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the company banks.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $companyBanks = CompanyBank::with('company')->paginate(25);

        return view('company_banks.index', compact('companyBanks'));
    }

    /**
     * Show the form for creating a new company bank.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('company_banks.create');
    }

    /**
     * Store a new company bank in the storage.
     *
     * @param App\Http\Requests\CompanyBanksFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(CompanyBanksFormRequest $request)
    { 
        try {
            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            CompanyBank::create($data);
            return redirect()->route('company_banks.company_bank.index')
                ->with('success_message', 'Company Bank was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified company bank.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $companyBank = CompanyBank::findOrFail($id);

        return view('company_banks.show', compact('companyBank'));
    }

    /**
     * Show the form for editing the specified company bank.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $companyBank = CompanyBank::findOrFail($id);
        

        return view('company_banks.edit', compact('companyBank'));
    }

    /**
     * Update the specified company bank in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\CompanyBanksFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, CompanyBanksFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            
            $companyBank = CompanyBank::findOrFail($id);
            $companyBank->update($data);

            return redirect()->route('company_banks.company_bank.index')
                ->with('success_message', 'Company Bank was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified company bank from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $companyBank = CompanyBank::findOrFail($id);
            $companyBank->delete();

            return redirect()->route('company_banks.company_bank.index')
                ->with('success_message', 'Company Bank was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }



}
