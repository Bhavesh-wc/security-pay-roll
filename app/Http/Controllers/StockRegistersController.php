<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StockRegistersFormRequest;
use App\Models\StockCategory;
use App\Models\StockRegister;
use Auth;
use Exception;

class StockRegistersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the stock registers.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $stockRegisters = StockRegister::orderBy('created_at', 'desc')->with('stockcategory','company')->get();

        return view('stock_registers.index', compact('stockRegisters'));
    }

    /**
     * Show the form for creating a new stock register.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $stockCategories = StockCategory::where('status','active')->pluck('stock_category_name','id')->all();
        
        return view('stock_registers.create', compact('stockCategories'));
    }

    /**
     * Store a new stock register in the storage.
     *
     * @param App\Http\Requests\StockRegistersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(StockRegistersFormRequest $request)
    {          
        
        try {
            // calculate and update stock category 
            $categoryId = $request['stock_category_id'];
            $quantity = $request['stock_quantity'];
            $existingQuantity = StockCategory::where('id', $categoryId)->value('stock_quantity');
            $newTotalQuantity = $existingQuantity + $quantity;
            StockCategory::where('id', $categoryId)->update(['stock_quantity' => $newTotalQuantity]);

            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $data['balance_code'] = 'cr';
            StockRegister::create($data);

            return redirect()->route('stock_registers.stock_register.index')
                ->with('success_message', 'Stock Register added successfully ');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        } 

    }

    /**
     * Display the specified stock register.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $stockRegister = StockRegister::with('stockcategory')->findOrFail($id);

        return view('stock_registers.show', compact('stockRegister'));
    }

    /**
     * Show the form for editing the specified stock register.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $stockRegister = StockRegister::findOrFail($id);
        $stockCategories = StockCategory::where('status','active')->pluck('stock_category_name','id')->all();

        return view('stock_registers.edit', compact('stockRegister','stockCategories'));
    }

    /**
     * Update the specified stock register in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\StockRegistersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, StockRegistersFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $stockRegister = StockRegister::findOrFail($id);
            $stockRegister->update($data);

            return redirect()->route('stock_registers.stock_register.index')
                ->with('success_message', 'Stock Register updated successfully');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified stock register from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $stockRegister = StockRegister::findOrFail($id);
            if ($stockCategory->stockRegisters()->exists()) {
                throw new Exception("This stock category has stock register and cannot be deleted.");
            }
            if ($stockCategory->advanceSalaries()->exists()) {
                throw new Exception("This stock category has advance salary and cannot be deleted.");
            }
            $stockRegister->delete();

            return redirect()->route('stock_registers.stock_register.index')
                ->with('success_message', 'Stock Register deleted successfully ');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }



}
