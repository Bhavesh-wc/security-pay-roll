<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\UsersFormRequest;
use App\Models\User;
use Exception;
use Illuminate\Support\Facades\Hash;

class UsersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the users.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $users = User::paginate(25);

        return view('users.index', compact('users'));
    }

    /**
     * Show the form for creating a new user.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('users.create');
    }

    /**
     * Store a new user in the storage.
     *
     * @param App\Http\Requests\UsersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(UsersFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            
            $data['password'] = Hash::make($request['password']);

            User::create($data);

            return redirect()->route('users.user.index')
                ->with('success_message', 'Company was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified user.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $user = User::findOrFail($id);

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified user.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $user = User::findOrFail($id);
        
        return view('users.edit', compact('user'));
    }

    /**
     * Update the specified user in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\UsersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, UsersFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            
            $user = User::findOrFail($id);
            $user->update($data);

            return redirect()->route('users.user.index')
                ->with('success_message', 'Company was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified user from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $user = User::findOrFail($id);
            if ($user->banks()->exists()) {
                throw new Exception("This company has bank and cannot be deleted.");
            }
            if ($user->bankBranches()->exists()) {
                throw new Exception("This company has bank branche and cannot be deleted.");
            }
            if ($user->countries()->exists()) {
                throw new Exception("This company has country and cannot be deleted.");
            }
            if ($user->states()->exists()) {
                throw new Exception("This company has state and cannot be deleted.");
            }
            if ($user->districts()->exists()) {
                throw new Exception("This company has district and cannot be deleted.");
            }
            if ($user->cities()->exists()) {
                throw new Exception("This company has city and cannot be deleted.");
            }
            if ($user->pincodes()->exists()) {
                throw new Exception("This company has pincode and cannot be deleted.");
            }
            if ($user->departments()->exists()) {
                throw new Exception("This company has department and cannot be deleted.");
            }
            if ($user->qualifications()->exists()) {
                throw new Exception("This company has qualifications and cannot be deleted.");
            }
            if ($user->motherTongues()->exists()) {
                throw new Exception("This company has mother tongue and cannot be deleted.");
            }
            if ($user->grades()->exists()) {
                throw new Exception("This company has grade and cannot be deleted.");
            }
            if ($user->stockCategories()->exists()) {
                throw new Exception("This company has stock category and cannot be deleted.");
            }
            if ($user->taxManagers()->exists()) {
                throw new Exception("This company has tax manager and cannot be deleted.");
            }
            if ($user->stockRegisters()->exists()) {
                throw new Exception("This company has stock register and cannot be deleted.");
            }
            if ($user->employees()->exists()) {
                throw new Exception("This company has employee and cannot be deleted.");
            }
            $user->delete();

            return redirect()->route('users.user.index')
                ->with('success_message', 'User was successfully deleted.');
                
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }



}
