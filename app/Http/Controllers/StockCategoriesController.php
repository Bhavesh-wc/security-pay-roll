<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\StockCategoriesFormRequest;
use App\Models\StockCategory;
use Auth;
use Exception;

class StockCategoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the stock categories.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $stockCategories = StockCategory::orderBy('created_at', 'desc')->with('company')->get();

        return view('stock_categories.index', compact('stockCategories'));
    }

    /**
     * Show the form for creating a new stock category.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('stock_categories.create');
    }

    /**
     * Store a new stock category in the storage.
     *
     * @param App\Http\Requests\StockCategoriesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(StockCategoriesFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            StockCategory::create($data);

            return redirect()->route('stock_categories.stock_category.index')
                ->with('success_message', 'Stock Category was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified stock category.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $stockCategory = StockCategory::findOrFail($id);

        return view('stock_categories.show', compact('stockCategory'));
    }

    /**
     * Show the form for editing the specified stock category.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $stockCategory = StockCategory::findOrFail($id);
        

        return view('stock_categories.edit', compact('stockCategory'));
    }

    /**
     * Update the specified stock category in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\StockCategoriesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, StockCategoriesFormRequest $request)
    {
        try {
            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $stockCategory = StockCategory::findOrFail($id);
            $stockCategory->update($data);
            return redirect()->route('stock_categories.stock_category.index')
                ->with('success_message', 'Stock Category was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified stock category from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $stockCategory = StockCategory::findOrFail($id);
            if ($stockCategory->stockRegisters()->exists()) {
                throw new Exception("This stock category has stock register and cannot be deleted.");
            }
            if ($stockCategory->advanceSalaries()->exists()) {
                throw new Exception("This stock category has advance salary and cannot be deleted.");
            }
            $stockCategory->delete();

            return redirect()->route('stock_categories.stock_category.index')
                ->with('success_message', 'Stock Category was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }



}
