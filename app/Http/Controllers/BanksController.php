<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\BanksFormRequest;
use App\Models\Bank;
use Exception;
use Illuminate\Support\Facades\Auth;

class BanksController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the banks.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $banks = Bank::orderBy('created_at', 'desc')->with('company')->get();

        return view('banks.index', compact('banks'));
    }

    /**
     * Show the form for creating a new bank.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


        return view('banks.create');
    }

    /**
     * Store a new bank in the storage.
     *
     * @param App\Http\Requests\BanksFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(BanksFormRequest $request)
    {
        try {
            if ($request['hidden']) {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                $data = Bank::create($data);
                // return redirect()->back();
                return response()->json($data);
            } else {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                Bank::create($data);
                return redirect()->route('banks.bank.index')
                    ->with('success_message', 'Bank was successfully added.');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified bank.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $bank = Bank::findOrFail($id);

        return view('banks.show', compact('bank'));
    }

    /**
     * Show the form for editing the specified bank.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $bank = Bank::findOrFail($id);

        return view('banks.edit', compact('bank'));
    }

    /**
     * Update the specified bank in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\BanksFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, BanksFormRequest $request)
    {
        try {

            $data = $request->getData();

            $data['company_id'] = Auth::user()->id;

            $bank = Bank::findOrFail($id);
            $bank->update($data);

            return redirect()->route('banks.bank.index')
                ->with('success_message', 'Bank was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified bank from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $bank = Bank::findOrFail($id);
            if ($bank->bankBranches()->exists()) {
                throw new Exception("This bank has bank branch and cannot be deleted.");
            }
            if ($bank->employees()->exists()) {
                throw new Exception("This bank has employee and cannot be deleted.");
            }
            $bank->delete();
            return redirect()->route('banks.bank.index')
                ->with('success_message', 'Bank was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
