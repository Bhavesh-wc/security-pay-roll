<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AdvanceSalariesFormRequest;
use App\Models\AdvanceSalary;
use App\Models\Branch;
use App\Models\Employee;
use App\Models\StockCategory;
use Exception;
use App\Models\StockRegister;
use Auth;

class AdvanceSalariesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the advance salaries.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $advanceSalaries = AdvanceSalary::orderBy('created_at', 'desc')->with('branch', 'employee', 'stockcategory')->get();
        return view('advance_salaries.index', compact('advanceSalaries'));
    }

    /**
     * Show the form for creating a new advance salary.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        $stockCategories = StockCategory::where('status', 'active')->pluck('stock_category_name', 'id')->all();
        return view('advance_salaries.create', $data, compact('stockCategories'));
    }

    /**
     * Store a new advance salary in the storage.
     *
     * @param App\Http\Requests\AdvanceSalariesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
   
    public function store(AdvanceSalariesFormRequest $request)
    {
        try {
            $data = $request->getData();
            foreach ($data['employee_id'] as $employeeId) {
                $data['employee_id'] = $employeeId;
                $employee = Employee::findOrFail($data['employee_id']);
                $data['designation_id'] = $employee->designation_id;
                AdvanceSalary::create($data);
    
            if ($request['advance_for'] == "Advance - 2") {
                $stockRegisterData = [
                    'company_id' => Auth::user()->id,
                    'balance_code' => 'dr',
                    'stock_category_id' => $data['stock_category_id'],
                    'stock_quantity' => $data['no_of_qty'],
                    'stock_in_date' => $data['date_of_advance'],
                ];
                StockRegister::create($stockRegisterData);
                
                 // calculate and update stock category 
                $categoryId = $request['stock_category_id'];
                $quantity = $request['no_of_qty'];
                $existingQuantity = StockCategory::where('id', $categoryId)->value('stock_quantity');
                $newTotalQuantity = $existingQuantity - $quantity;
                StockCategory::where('id', $categoryId)->update(['stock_quantity' => $newTotalQuantity]);
            }
        }
        return redirect()->route('advance_salaries.advance_salary.index')
            ->with('success_message', 'Advance Salary was successfully added.');
        } catch (\Exception $exception) {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
        
}

    /**
     * Display the specified advance salary.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $advanceSalary = AdvanceSalary::with('branch', 'employee', 'stockcategory')->findOrFail($id);
        return view('advance_salaries.show', compact('advanceSalary'));
    }
    /**
     * Show the form for editing the specified advance salary.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $advanceSalary = AdvanceSalary::findOrFail($id);
        $branches = Branch::where('status', 'active')->pluck('name', 'id')->all();
        $employees = Employee::where('status', 'active')->pluck('full_name', 'id')->all();
        $stockCategories = StockCategory::where('status', 'active')->pluck('stock_category_name', 'id')->all();
        return view('advance_salaries.edit', compact('advanceSalary', 'branches', 'employees', 'stockCategories'));
    }

    /**
     * Update the specified advance salary in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\AdvanceSalariesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, AdvanceSalariesFormRequest $request)
    {

        try {
            $data = $request->getData();
            $advanceSalary = AdvanceSalary::findOrFail($id);
            $advanceSalary->update($data);

            return redirect()->route('advance_salaries.advance_salary.index')
                ->with('success_message', 'Advance Salary was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified advance salary from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $advanceSalary = AdvanceSalary::findOrFail($id);
            $advanceSalary->delete();

            return redirect()->route('advance_salaries.advance_salary.index')
                ->with('success_message', 'Advance Salary was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }
}
