<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\BankBranchesFormRequest;
use App\Models\Bank;
use App\Models\BankBranch;
use Auth;
use Exception;

class BankBranchesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the bank branches.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $bankBranches = BankBranch::orderBy('created_at', 'desc')->with('bank', 'company')->get();

        return view('bank_branches.index', compact('bankBranches'));
    }

    /**
     * Show the form for creating a new bank branch.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $banks = Bank::where('status', 'active')->pluck('bank_name', 'id')->all();

        return view('bank_branches.create', compact('banks'));
    }

    /**
     * Store a new bank branch in the storage.
     *
     * @param App\Http\Requests\BankBranchesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(BankBranchesFormRequest $request)
    {
        try {
            if ($request['hidden']) {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                $data = BankBranch::create($data);
                return response()->json($data);
            } else {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                BankBranch::create($data);
                return redirect()->route('bank_branches.bank_branch.index')
                    ->with('success_message', 'Bank Branch was successfully added.');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified bank branch.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $bankBranch = BankBranch::with('bank')->findOrFail($id);

        return view('bank_branches.show', compact('bankBranch'));
    }

    /**
     * Show the form for editing the specified bank branch.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $bankBranch = BankBranch::findOrFail($id);
        $banks = Bank::where('status', 'active')->pluck('bank_name', 'id')->all();

        return view('bank_branches.edit', compact('bankBranch', 'banks'));
    }

    /**
     * Update the specified bank branch in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\BankBranchesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, BankBranchesFormRequest $request)
    {
        try {

            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $bankBranch = BankBranch::findOrFail($id);
            $bankBranch->update($data);

            return redirect()->route('bank_branches.bank_branch.index')
                ->with('success_message', 'Bank Branch was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified bank branch from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $bankBranch = BankBranch::findOrFail($id);
            if ($bankBranch->employees()->exists()) {
                throw new Exception("This bank branch has employee and cannot be deleted.");
            }
            $bankBranch->delete();

            return redirect()->route('bank_branches.bank_branch.index')
                ->with('success_message', 'Bank Branch was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
