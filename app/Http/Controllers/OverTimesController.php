<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\OverTimesFormRequest;
use App\Models\Branch;
use App\Models\Employee;
use App\Models\OverTime;
use App\Models\OverTimeDay;
use Exception;
use Carbon\Carbon;

class OverTimesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the over times.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $overTimes = OverTime::orderBy('created_at', 'desc')->with('branch', 'employee')->get();

        return view('over_times.index', compact('overTimes'));
    }

    /**
     * Show the form for creating a new over time.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $branches = Branch::get(["name", "id"]);
        $employees = Employee::where('status', 'active')->pluck('full_name', 'id')->all();
        $overtime = new OverTime();
        $overtimeDays = collect();

        return view('over_times.create', compact('branches','overtimeDays','overtime'));

    }

    /**
     * Store a new over time in the storage.
     *
     * @param App\Http\Requests\OverTimesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(OverTimesFormRequest $request)
    {
      
        try {
            
            $data = $request->getData();

            $validatedData = $request->validate([
                'overtime.*.over_time_date' => 'required|date',
                'overtime.*.over_time_amount' => 'required|numeric',
            ]);
    
            foreach ($data['employee_id'] as $employeeId) {

                $data['employee_id'] = $employeeId;
                $employee = Employee::findOrFail($data['employee_id']);

                $data['designation_id'] = $employee->designation_id;
                $data['total_overtime_amount'] = 0;
                $data['total_overtime_days'] = 0;

                $overtimeid = OverTime::create($data);
                foreach ($request['overtime'] as $overtimeday) {
                    OverTimeDay::create([
                        'over_time_id' => $overtimeid->id,
                        'over_time_date' => $overtimeday['over_time_date'],
                        'over_time_amount' => $overtimeday['over_time_amount']
                    ]);
                    $data['total_overtime_amount'] += $overtimeday['over_time_amount'];
                    $data['total_overtime_days'] = count($request['overtime']);
                    
                }
                OverTime::findOrFail($overtimeid->id)->update($data);
            }   
       
            return redirect()->route('over_times.over_time.index')
                ->with('success_message', 'Over Time was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified over time.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $overTime = OverTime::with('branch', 'employee')->findOrFail($id);

        return view('over_times.show', compact('overTime'));
    }

    /**
     * Show the form for editing the specified over time.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $overTime = OverTime::findOrFail($id);
        $branches = Branch::where('status', 'active')->pluck('name', 'id')->all();
        $employees = Employee::where('status', 'active')->pluck('full_name', 'id')->all();
        //$overTimeDays = OverTimeDay::where('over_time_id','19')->get();
        //dd($overTime->overTimeDays);
        $overTimeDays = $overTime->overTimeDays;
       
        return view('over_times.edit', compact('overTime','overTimeDays','branches', 'employees'));
    }

    /**
     * Update the specified over time in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\OverTimesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, OverTimesFormRequest $request)
    {
        try {

            $data = $request->getData();

            $validatedData = $request->validate([
                'overtime.*.over_time_date' => 'required|date',
                'overtime.*.over_time_amount' => 'required|numeric',
            ]);
    
            $overTime = OverTime::findOrFail($id);
            $overTime['total_overtime_amount'] = 0;
            $overTime['total_overtime_days'] = 0;
            $overTime->overTimeDays()->delete();

            foreach ($request['overtime'] as $overtimeday) {
                OverTimeDay::create([
                    'over_time_id' => $id,
                    'over_time_date' => $overtimeday['over_time_date'],
                    'over_time_amount' => $overtimeday['over_time_amount']
                ]);

                $overTime['total_overtime_amount'] += $overtimeday['over_time_amount'];
                $overTime['total_overtime_days'] = count($request['overtime']);
                
            }
            $overTime->update($data);

            return redirect()->route('over_times.over_time.index')
                ->with('success_message', 'Over Time was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified over time from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $overTime = OverTime::findOrFail($id);
            if ($overTime->overTimeDays()->exists()) {
                throw new Exception("This over time has over time day and cannot be deleted.");
            }
            $overTime->delete();

            return redirect()->route('over_times.over_time.index')
                ->with('success_message', 'Over Time was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
