<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\SalariesFormRequest;
use App\Models\Branch;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\Grade;
use App\Models\Salary;
use App\Models\TaxManager;
use Exception;



use Auth;
use App\Models\Walfer;
use App\Models\BranchDesignation;
use App\Models\Fine;
use App\Models\Attendance;
use App\Models\BranchTransfer;
use Carbon\Carbon;
use App\Models\OverTime;
use App\Models\Damage;
use App\Models\AdvanceSalary;

class SalariesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the salaries.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $salaries = Salary::orderBy('created_at', 'desc')->with('company','branch','designation','department','grade','employee')->get();

        return view('salaries.index', compact('salaries'));
    }

    /**
     * Show the form for creating a new salary.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $branches = Branch::pluck('name','id')->all();
        $designations = Designation::pluck('designation_name','id')->all();
        $departments = Department::pluck('department_name','id')->all();
        $grades = Grade::pluck('grade_name','id')->all();
        $employees = Employee::pluck('full_name','id')->all();
        
        return view('salaries.create', compact('branches','designations','departments','grades','employees'));
    }

    /**
     * Store a new salary in the storage.
     *
     * @param App\Http\Requests\SalariesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(SalariesFormRequest $request)
    {
        
        try {
            // store tax based on tax manager
            $taxManager = TaxManager::where('monthyear', $request['monthyear'])->first();
            if (!is_null($taxManager)) {
            $esicValue=$taxManager['employee_esic'];
            $pfValue=$taxManager['employee_pf'];
            $walfer=$taxManager['welfare_fund'];
            $professionalTax = $taxManager['professional_tax'];
                
            $data = $request->getData();
            // $walfer = Walfer::pluck('walfare_amount')->first();
            $employee_id = $data['employee_id'];
            $monthyear = $data['monthyear'];
            $employee = Employee::where('id', $employee_id)->first();
            // find branch overtime amount
            $branchovertimeAmount = Branch::where('id', $data['branch_id'])->value('over_time_amount');
            if (!is_null($employee)) {
                // Initialize a variable to track whether an attendance record was found for the given employee and monthyear
                $branchtansfer = BranchTransfer::where('employee_id', $employee_id)->where('branch_id', $data['branch_id'])->where('transfer', '=', 'current')->first();
                if (!is_null($branchtansfer)) {
                    // calculate basic and fix salary
                    $branch_designation = BranchDesignation::where('branch_id', $data['branch_id'])->where('designation_id', $branchtansfer->designation_id)->get();
                    if ($branch_designation[0]['basic_fix'] == "basic") {
                        $basic_da  =  $branch_designation[0]['basic_da'];
                        $extra_allowance  =  $branch_designation[0]['extra_allowance'];
                        $hra  =  $branch_designation[0]['hra'];
                        $bonus  =  $branch_designation[0]['bonus'];
                        $fix_salary = 0;
                    } else {
                        $basic_da  = 0;
                        $extra_allowance  = 0;
                        $hra  = 0;
                        $bonus  = 0;
                        $fix_salary = $branch_designation[0]['fix_salary'];
                    }
                    $date = Carbon::createFromFormat('Y-m',$monthyear);
                    $formattedMonth = $date->format('Y-M');
                    $salary = Salary::where('employee_id', $employee_id)->where('branch_id', $data['branch_id'])->where('designation_id', $branchtansfer->designation_id)->where('monthyear', $formattedMonth)->first();
                
                    //salary allready exist
                    if(is_null($salary)) {
                        $attandace = Attendance::where('employee_id', $employee_id)->where('branch_id', $data['branch_id'])->where('designation_id', $branchtansfer->designation_id)->where('monthyear', $formattedMonth)->get();
                        if (count($attandace) > 0) {
                            // Count working Days
                            if (count($attandace) > 0) {
                                $total = $attandace[0]['total'];
                                $total_holiday = $attandace[0]['total_holiday'];
                                $working_day = $total - $total_holiday;
                            } else {
                                $total = 0;
                                $total_holiday = 0;
                                $working_day = $total - $total_holiday;
                            } 
                            // calculate number of working day done
                            $presents = 0;
                            $absents = 0;
                            $holidays = 0;
                            foreach ($attandace as $item) {
                                $days = collect($item->toArray())->except(['id', 'employee_id', 'designation_id', 'company_id', 'branch_id', 'monthyear', 'month', 'year', 'created_at', 'updated_at', 'deleted_at'])->toArray();
                                foreach ($days as $day) {
                                    if ($day == 'P') {
                                        $presents++;
                                    } elseif ($day == 'A') {
                                        $absents++;
                                    } elseif ($day == 'H') {
                                        $holidays++;
                                    }
                                }
                            }
                            $no_of_days_work_done  =  $working_day - $absents;

                            // Get Overtime Value
                            $overtimeAmount = $this->getOvertimeamount($employee_id, $monthyear);
                            if (count($overtimeAmount) > 0) {
                                $totalOvertimeAmount = $overtimeAmount[0]->total_overtime_amount;
                                $totalOvertimeDays = $overtimeAmount[0]->total_overtime_days;
                            } else {
                                $totalOvertimeAmount = 0;
                                $totalOvertimeDays = 0;
                            } 

                            //Get Damage Value 
                            $totaldamage = $this->getDamageAmount($employee_id, $monthyear);
                            // Get Fine Value
                            $totalFine = $this->getFineAmount($employee_id, $monthyear);

                            // Advance Salary
                            $totalAdvance = $this->getAdvance($employee_id, $monthyear);
                            $branch_id =  $branchtansfer->branch_id;
                            $designation_id =  $branchtansfer->designation_id;
                            $department_id =  $branchtansfer->department_id;
                            $grade_id =  $branchtansfer->grade_id;
                            $employee_id = $employee_id;

                            // Calculate the salary data for this employee
                            $monthyear = explode('-', $data['monthyear']);
                            // dd($monthyear);

                            $data['year'] = $monthyear[0];
                            $data['month'] = $monthyear[1];

                            $month = $data['month']; // April
                            $year = $data['year']; // 2023
                            
                            if ($month == 6 || $month == 12) {
                                $data['walfare_amount'] = $walfer;
                            } else {
                                $data['walfare_amount'] = 0;
                            }
                        

                            // calculate Pf
                            // $pfValue = Branch::where('id', $data['branch_id'])->value('pf');
                            if ($basic_da > 0) {
                                $pf = $basic_da * $working_day;
                                $calculatepf = $pf * $pfValue / 100;
                                $esic = $pf * $esicValue / 100;
                                $basic_wages = $basic_da * $no_of_days_work_done;
                                $total =  $basic_da +  $extra_allowance + $hra + $bonus + $data['a'] + $data['b'] + $data['c'] + $totalOvertimeAmount + $data['attendance_allowance'] + $basic_wages;
                                $minusTotal =  $totalAdvance + $data['dress'] + $data['mess'] + $calculatepf + $professionalTax + $esic + $data['walfare_amount'] + $totalFine;
                                $netTotal = $total - $minusTotal;
                            } else {
                                $calculatepf = ($pfValue / 100) * $fix_salary;
                                $esic = ($esicValue / 100) * $fix_salary;
                                $basic_wages = 0;
                                $total =  $fix_salary + $data['a'] + $data['b'] + $data['c'] + $totalOvertimeAmount + $data['attendance_allowance'] + $basic_wages;
                                $minusTotal =  $totalAdvance + $data['dress'] + $data['mess'] + $calculatepf + $professionalTax + $esic + $data['walfare_amount'] + $totalFine;
                                $netTotal = $total - $minusTotal;
                            }
                            // Store the salary data in the salary table
                            $salary = new Salary;
                            $salary->branch_id = $branch_id;
                            $salary->designation_id = $designation_id;
                            $salary->department_id = $department_id;
                            $salary->grade_id = $grade_id;
                            $salary->employee_id = $employee_id;
                            $salary->month = $data['month'];
                            $salary->year = $data['year'];
                            $salary->salary_date = now();
                            $salary->working_days = $working_day;
                            $salary->no_of_days_work_done = $attandace[0]['total_present'];
                            $salary->no_of_leave = $attandace[0]['total_absent'];
                            $salary->basic_da = $basic_da;
                            $salary->extra_allowance = $extra_allowance;
                            $salary->hra = $hra;
                            $salary->bonus = $bonus;
                            $salary->fix_salary = $fix_salary;
                            $salary->sal_advance = $totalAdvance;
                            $salary->a = $data['a'];
                            $salary->b = $data['b'];
                            $salary->c = $data['c'];
                            $salary->ot_amount = $totalOvertimeAmount;
                            $salary->ot_days = $totalOvertimeDays;
                            $salary->attendance_allowance = $data['attendance_allowance'];
                            $salary->dress = $data['dress'];
                            $salary->mess = $data['mess'];
                            $salary->pf = $calculatepf;
                            $salary->profetional_tax = $professionalTax;
                            $salary->basis_wages = $basic_wages;
                            $salary->esic = $esic;
                            $salary->walfare_amount = $data['walfare_amount'];
                            $salary->damage_amount = $totaldamage;
                            $salary->fines = $totalFine;
                            $salary->total = $total;
                            $salary->nettotal = $netTotal;
                            $salary->branch_ot_amount = $branchovertimeAmount;
                            $salary->company_id = Auth::user()->id;
                            $salary->monthyear = $formattedMonth;

                            $salary->save();
                            return redirect()->route('salaries.salary.index')
                            ->with('success_message', 'Salary was successfully added.');

                        } else {
                            //throw new \Exception("Employee Attendance Not Avilable: {$employee_id}");
                            return back()->withInput()
                    ->withErrors(['unexpected_error' => 'attandace not exist']);
                        }
                    } else {
                        //throw new \Exception("Employee Salary Already Exist: {$employee_id}");
                        return back()->withInput()
                    ->withErrors(['unexpected_error' => 'salary already exist']);
                    }
                } else {
                    //throw new \Exception("Branch Transfer Data Not Available: {$employee_id}" );
                    return back()->withInput()
                    ->withErrors(['unexpected_error' => 'branch transfer is not found.']);
                }
            } else {
                // If $employee is not set, print 'back'
            // throw new \Exception("Employee Data Not Available: {$employee_id}" );
                return back()->withInput()
                    ->withErrors(['unexpected_error' => 'employee is not found.']);
            } 
        } else {
            //throw new \Exception("Branch Transfer Data Not Available: {$employee_id}" );
            return back()->withInput()
            ->withErrors(['unexpected_error' => 'tax manager for this month is not found.']);
        } 
        } catch (Exception $exception) {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified salary.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $salary = Salary::with('branch','designation','department','grade','employee')->findOrFail($id);

        return view('salaries.show', compact('salary'));
    }

    /**
     * Show the form for editing the specified salary.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $salary = Salary::findOrFail($id);
        $branches = Branch::pluck('name','id')->all();
        $designations = Designation::pluck('designation_name','id')->all();
        $departments = Department::pluck('department_name','id')->all();
        $grades = Grade::pluck('grade_name','id')->all();
        $employees = Employee::pluck('full_name','id')->all();

        return view('salaries.edit', compact('salary','branches','designations','departments','grades','employees'));
    }

    /**
     * Update the specified salary in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\SalariesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, SalariesFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            
            $salary = Salary::findOrFail($id);
            $salary->update($data);

            return redirect()->route('salaries.salary.index')
                ->with('success_message', 'Salary was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified salary from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $salary = Salary::findOrFail($id);
            $salary->delete();

            return redirect()->route('salaries.salary.index')
                ->with('success_message', 'Salary was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

// GET OVERTIME
public function getOvertimeamount($employee_id, $monthyear)
{

    $row = OverTime::where('employee_id', $employee_id)
        ->where('monthyear', $monthyear)
        ->get();

    return $row;
}

// GET DAMAGE
public function getDamageAmount($employee_id, $monthyear)
{

    $damage = Damage::where('employee_id', $employee_id)
        ->where('monthyear', $monthyear)
        ->get();
        
    $totalDamage = 0;
    foreach ($damage as $data) {
        $totalDamage += $data->damage_amount;
    }

    return $totalDamage;
}

// GET FINE
public function getFineAmount($employee_id, $monthyear)
{

    $rows = Fine::where('employee_id', $employee_id)
        ->where('monthyear', $monthyear)
        ->get();

    $totalFineAmount = 0;
    foreach ($rows as $row) {
        $totalFineAmount += $row->fine_amount;
    }

    return $totalFineAmount;
}

// GET ADVANCE
public function getAdvance($employee_id, $monthyear)
{

    $rows = AdvanceSalary::where('employee_id', $employee_id)
        ->where('monthyear', $monthyear)
        ->get();
    $advanceAmount = 0;
    foreach ($rows as $row) {
        $advanceAmount += $row->amount;
    }
    return $advanceAmount;
}
}
