<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\SystemConfigurationsFormRequest;
use App\Models\SystemConfiguration;
use Exception;

class SystemConfigurationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the system configurations.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $systemConfigurations = SystemConfiguration::paginate(25);

        return view('system_configurations.index', compact('systemConfigurations'));
    }

    /**
     * Show the form for creating a new system configuration.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('system_configurations.create');
    }

    /**
     * Store a new system configuration in the storage.
     *
     * @param App\Http\Requests\SystemConfigurationsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(SystemConfigurationsFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            
            SystemConfiguration::create($data);

            return redirect()->route('system_configurations.system_configuration.index')
                ->with('success_message', 'System Configuration was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified system configuration.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $systemConfiguration = SystemConfiguration::findOrFail($id);

        return view('system_configurations.show', compact('systemConfiguration'));
    }

    /**
     * Show the form for editing the specified system configuration.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $systemConfiguration = SystemConfiguration::findOrFail($id);
        

        return view('system_configurations.edit', compact('systemConfiguration'));
    }

    /**
     * Update the specified system configuration in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\SystemConfigurationsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, SystemConfigurationsFormRequest $request)
    {
         
        try {
            
           $data = $request->getData();
            
            $systemConfiguration = SystemConfiguration::findOrFail($id);
            $systemConfiguration->update($data);

            return redirect()->route('system_configurations.system_configuration.edit',$id)
                ->with('success_message', 'System Configuration was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified system configuration from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $systemConfiguration = SystemConfiguration::findOrFail($id);
            $systemConfiguration->delete();

            return redirect()->route('system_configurations.system_configuration.index')
                ->with('success_message', 'System Configuration was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }



}
