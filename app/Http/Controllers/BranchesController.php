<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\BranchesFormRequest;
use App\Models\Branch;
use App\Models\BranchDesignation;
use App\Models\BranchWorkOrder;
use App\Models\BranchDesignationHistory;
use Auth;
use Exception;
use App\Models\Designation;

class BranchesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the branches.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $branches = Branch::orderBy('created_at', 'desc')->with('company')->get();

        return view('branches.index', compact('branches'));
    }

    /**
     * Show the form for creating a new branch.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {

        $designations = Designation::where('status','active')->pluck('designation_name','id')->all();

        return view('branches.create', compact('designations'));
    }

    /**
     * Store a new branch in the storage.
     *
     * @param App\Http\Requests\BranchesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(BranchesFormRequest $request)
    {
            
        try {
            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $branch = Branch::create($data);
            if($branch->id)
            {
                foreach ($request['branchdesignation'] as $branchdesignation) {
                    BranchDesignation::create([
                        'branch_id' => $branch->id,
                        'designation_id' => $branchdesignation['designation_id'],
                        'basic_fix' => $branchdesignation['basic_fix'],
                        'basic_da' => $branchdesignation['basic_da'],
                        'extra_allowance' => $branchdesignation['extra_allowance'],
                        'hra' => $branchdesignation['hra'],
                        'bonus' => $branchdesignation['bonus'],
                        'fix_salary' => $branchdesignation['fix_salary']
                    ]);

                    BranchDesignationHistory::create([
                        'branch_id' => $branch->id,
                        'company_id' => $data['company_id'],
                        'designation_id' => $branchdesignation['designation_id'],
                        'basic_fix' => $branchdesignation['basic_fix'],
                        'basic_da' => $branchdesignation['basic_da'],
                        'extra_allowance' => $branchdesignation['extra_allowance'],
                        'hra' => $branchdesignation['hra'],
                        'bonus' => $branchdesignation['bonus'],
                        'fix_salary' => $branchdesignation['fix_salary'],
                        'monthyear' => $data['monthyear'],
                        'month' => $data['month'],
                        'year' => $data['year']
                    ]);
                }
                foreach ($request['workorder'] as $workorder) {

                    if (isset($workorder['work_order']) && $workorder['work_order']->isFile()) {
                        $workorder['work_order'] = $this->moveFile($workorder['work_order']);
                    }

                    BranchWorkOrder::create([
                        'branch_id' => $branch->id,
                        'work_order' => $workorder['work_order'],
                        'work_start_date' => $workorder['work_start_date'],
                        'work_expire_date' => $workorder['work_expire_date']
                    ]);
                }
            }
            
            return redirect()->route('branches.branch.index')
                ->with('success_message', 'Branch was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified branch.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $branch = Branch::findOrFail($id);
        $branchDesignation = BranchDesignation::with('designation')->where('branch_id',$id)->get();
        $branchWorkOrder = BranchWorkOrder::where('branch_id',$id)->get();
        $branchDesignationHistory = BranchDesignationHistory::with('designation','branch','company')->where('branch_id',$id)->get();

        return view('branches.show', compact('branch','branchDesignation','branchWorkOrder','branchDesignationHistory'));
    }

    /**
     * Show the form for editing the specified branch.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $branch = Branch::findOrFail($id);
        $branchDesignation = $branch->branchDesignation;
        $branchWorkOrder = $branch->branchWorkOrder;
        $designations = Designation::where('status','active')->pluck('designation_name','id')->all();

        return view('branches.edit', compact('branch','designations','branchDesignation','branchWorkOrder'));
    }

    /**
     * Update the specified branch in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\BranchesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, BranchesFormRequest $request)
    {
        try {
            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $branch = Branch::findOrFail($id);
            
            foreach ($request['branchdesignation'] as $branchdesignation) {
                if(!is_null($branchdesignation['branchdesignation_id']))
                {
                    BranchDesignation::findOrFail($branchdesignation['branchdesignation_id'])->Update([
                        'designation_id' => $branchdesignation['designation_id'],
                        'basic_fix' => $branchdesignation['basic_fix'],
                        'basic_da' => $branchdesignation['basic_da'],
                        'extra_allowance' => $branchdesignation['extra_allowance'],
                        'hra' => $branchdesignation['hra'],
                        'bonus' => $branchdesignation['bonus'],
                        'fix_salary' => $branchdesignation['fix_salary']
                    ]);
                }
                else
                {
                    BranchDesignation::create([
                        'branch_id' => $id,
                        'designation_id' => $branchdesignation['designation_id'],
                        'basic_fix' => $branchdesignation['basic_fix'],
                        'basic_da' => $branchdesignation['basic_da'],
                        'extra_allowance' => $branchdesignation['extra_allowance'],
                        'hra' => $branchdesignation['hra'],
                        'bonus' => $branchdesignation['bonus'],
                        'fix_salary' => $branchdesignation['fix_salary']
                    ]);
                }

                BranchDesignationHistory::create([
                    'branch_id' => $branch->id,
                    'company_id' => $data['company_id'],
                    'designation_id' => $branchdesignation['designation_id'],
                    'basic_fix' => $branchdesignation['basic_fix'],
                    'basic_da' => $branchdesignation['basic_da'],
                    'extra_allowance' => $branchdesignation['extra_allowance'],
                    'hra' => $branchdesignation['hra'],
                    'bonus' => $branchdesignation['bonus'],
                    'fix_salary' => $branchdesignation['fix_salary'],
                    'monthyear' => $data['monthyear'],
                    'month' => $data['month'],
                    'year' => $data['year']
                ]);
            }
            foreach ($request['workorder'] as $workorder) {

                if (isset($workorder['custom_delete_work_order']) && ($workorder['custom_delete_work_order'] == '1')) {
                    $workorder['work_order'] = null;
                }
                if (isset($workorder['work_order']) && $workorder['work_order']->isFile()) {
                    $workorder['work_order'] = $this->moveFile($workorder['work_order']);
                }
                if(!is_null($workorder['workorder_id']))
                {
                    BranchWorkOrder::findOrFail($workorder['workorder_id'])->Update([
                        'work_start_date' => $workorder['work_start_date'],
                        'work_expire_date' => $workorder['work_expire_date']
                    ]);
                    if(isset($workorder['work_order']))
                    {
                        BranchWorkOrder::findOrFail($workorder['workorder_id'])->Update([
                            'work_order' => $workorder['work_order']
                        ]);
                    }
                }
                else
                {
                    BranchWorkOrder::create([
                        'branch_id' => $id,
                        'work_order' => $workorder['work_order'],
                        'work_start_date' => $workorder['work_start_date'],
                        'work_expire_date' => $workorder['work_expire_date']
                    ]);
                }
            }

            $branch->update($data);
            return redirect()->route('branches.branch.index')
                ->with('success_message', 'Branch was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified branch from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $branch = Branch::findOrFail($id);
            /*  if ($branch->branchDesignation()->exists()) {
                throw new Exception("This branch has branch designation and cannot be deleted.");
            } */
            /* if ($branch->branchWorkOrder()->exists()) {
                throw new Exception("This branch has branch work order and cannot be deleted.");
            } */
            if ($branch->damages()->exists()) {
                throw new Exception("This branch has demages and cannot be deleted.");
            }
            if ($branch->employees()->exists()) {
                throw new Exception("This branch has employees and cannot be deleted.");
            }
            if ($branch->fines()->exists()) {
                throw new Exception("This branch has fine and cannot be deleted.");
            }
            if ($branch->overTimes()->exists()) {
                throw new Exception("This branch has overtime and cannot be deleted.");
            }
            $branch->delete();

            return redirect()->route('branches.branch.index')
                ->with('success_message', 'Branch was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }

    /**
     * Moves the attached file to the server.
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return string
     */
    protected function moveFile($file)
    {
        if (!$file->isValid()) {
            return '';
        }
        
        $path = config('laravel-code-generator.files_upload_path', 'uploads');
        $saved = $file->store('public/' . $path, config('filesystems.default'));

        return substr($saved, 7);
    }


    public function destroyBranchDesignation($id)
    {
        $item = BranchDesignation::findOrFail($id);
        $item->delete();
        return response()->json(['success' => true]);
    }

    public function destroyWorkOrder($id)
    {
        $item = BranchWorkOrder::findOrFail($id);
        $item->delete();
        return response()->json(['success' => true]);
    }

}
