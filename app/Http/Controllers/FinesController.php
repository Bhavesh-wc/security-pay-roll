<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\FinesFormRequest;
use App\Models\Branch;
use App\Models\Employee;
use App\Models\Fine;
use Exception;

class FinesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the fines.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $fines = Fine::orderBy('created_at', 'desc')->with('branch', 'employee')->get();
        return view('fines.index', compact('fines'));
    }

    /**
     * Show the form for creating a new fine.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('fines.create', $data);
    }

    /**
     * Store a new fine in the storage.
     *
     * @param App\Http\Requests\FinesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(FinesFormRequest $request)
    {
        try {
            $data = $request->getData();
            foreach ($data['employee_id'] as $employeeId) {
                $data['employee_id'] = $employeeId;
                $employee = Employee::findOrFail($data['employee_id']);
                $data['designation_id'] = $employee->designation_id;
                Fine::create($data);
            }
            return redirect()->route('fines.fine.index')
                ->with('success_message', 'Fine was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified fine.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $fine = Fine::with('branch', 'employee')->findOrFail($id);
        return view('fines.show', compact('fine'));
    }

    /**
     * Show the form for editing the specified fine.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $fine = Fine::findOrFail($id);
        $branches = Branch::where('status', 'active')->pluck('name', 'id')->all();
        $employees = Employee::where('status', 'active')->pluck('full_name', 'id')->all();
        return view('fines.edit', compact('fine', 'branches', 'employees'));
    }

    /**
     * Update the specified fine in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\FinesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, FinesFormRequest $request)
    {
        try {

            $data = $request->getData();
            $fine = Fine::findOrFail($id);
            $fine->update($data);
            return redirect()->route('fines.fine.index')
                ->with('success_message', 'Fine was successfully updated.');
        } catch (Exception $exception) {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified fine from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $fine = Fine::findOrFail($id);
            $fine->delete();

            return redirect()->route('fines.fine.index')
                ->with('success_message', 'Fine was successfully deleted.');
        } catch (Exception $exception) {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }
}
