<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests\EmployeesFormRequest;
use App\Models\Bank;
use App\Models\BankBranch;
use App\Models\Branch;
use App\Models\CitizenOfIndiaBy;
use App\Models\City;
use App\Models\Country;
use App\Models\Department;
use App\Models\Designation;
use App\Models\District;
use App\Models\Employee;
use App\Models\Grade;
use App\Models\MotherTongue;
use App\Models\Pincode;
use App\Models\Qualification;
use App\Models\BranchDesignation;
use App\Models\State;
use App\Models\BranchTransfer;
use App\Models\user;
use Auth;
use Exception;
use Carbon\Carbon;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $employees = Employee::orderBy('created_at', 'desc')->where('status', '=', 'active')->with('designation', 'country', 'state', 'district', 'city', 'pincode', 'mothertongue', 'qualification', 'rtostate', 'bank', 'bankbranch', 'branch', 'department', 'grade', 'company', 'branchDesignation')->paginate(5);
        $users = user::orderBy('created_at', 'desc')->where('status', '=', 'active')->paginate(5);
        $branches = Branch::orderBy('created_at', 'desc')->where('status', '=', 'active')->paginate(5);

        return view('home',compact('employees','users','branches'));
    }

    public function report()
    {
        return view('report.index');
    }
}
