<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\DepartmentsFormRequest;
use App\Models\Department;
use Auth;
use Exception;

class DepartmentsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the departments.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $departments = Department::orderBy('created_at', 'desc')->with('company')->get();

        return view('departments.index', compact('departments'));
    }

    /**
     * Show the form for creating a new department.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


        return view('departments.create');
    }

    /**
     * Store a new department in the storage.
     *
     * @param App\Http\Requests\DepartmentsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(DepartmentsFormRequest $request)
    {
        try {
            if ($request['hidden']) {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                $data  =  Department::create($data);
                // return redirect()->back();
                return response()->json($data);
            } else {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                Department::create($data);
                return redirect()->route('departments.department.index')
                    ->with('success_message', 'Department was successfully added.');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified department.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $department = Department::findOrFail($id);

        return view('departments.show', compact('department'));
    }

    /**
     * Show the form for editing the specified department.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $department = Department::findOrFail($id);


        return view('departments.edit', compact('department'));
    }

    /**
     * Update the specified department in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\DepartmentsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, DepartmentsFormRequest $request)
    {
        try {

            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $department = Department::findOrFail($id);
            $department->update($data);

            return redirect()->route('departments.department.index')
                ->with('success_message', 'Department was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified department from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $department = Department::findOrFail($id);
            if ($department->employees()->exists()) {
                throw new Exception("This department has employee and cannot be deleted.");
            }
            $department->delete();

            return redirect()->route('departments.department.index')
                ->with('success_message', 'Department was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
