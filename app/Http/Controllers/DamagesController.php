<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\DamagesFormRequest;
use App\Models\Branch;
use App\Models\Damage;
use App\Models\Employee;
use Exception;

class DamagesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the damages.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $damages = Damage::orderBy('created_at', 'desc')->with('branch', 'employee')->get();
        return view('damages.index', compact('damages'));
    }

    /**
     * Show the form for creating a new damage.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $data['branches'] = Branch::get(["name", "id"]);
        return view('damages.create', $data);
    }

    /**
     * Store a new damage in the storage.
     *
     * @param App\Http\Requests\DamagesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(DamagesFormRequest $request)
    {
        try {
            $data = $request->getData();
            foreach ($data['employee_id'] as $employeeId) {
                $data['employee_id'] = $employeeId;
                $employee = Employee::findOrFail($data['employee_id']);
                $data['designation_id'] = $employee->designation_id;
                Damage::create($data);
            }
            return redirect()->route('damages.damage.index')
                ->with('success_message', 'Damage was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified damage.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $damage = Damage::with('branch', 'employee')->findOrFail($id);
        return view('damages.show', compact('damage'));
    }

    /**
     * Show the form for editing the specified damage.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $damage = Damage::findOrFail($id);
        $branches = Branch::where('status', 'active')->pluck('name', 'id')->all();
        $employees = Employee::where('status', 'active')->pluck('full_name', 'id')->all();
        return view('damages.edit', compact('damage', 'branches', 'employees'));
    }

    /**
     * Update the specified damage in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\DamagesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, DamagesFormRequest $request)
    {
        try {

            $data = $request->getData();
            $damage = Damage::findOrFail($id);
            $damage->update($data);
            return redirect()->route('damages.damage.index')
                ->with('success_message', 'Damage was successfully updated.');
        } catch (Exception $exception) {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified damage from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $damage = Damage::findOrFail($id);
            $damage->delete();
            return redirect()->route('damages.damage.index')
                ->with('success_message', 'Damage was successfully deleted.');
        } catch (Exception $exception) {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }
}
