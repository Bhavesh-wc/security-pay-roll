<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\MotherTonguesFormRequest;
use App\Models\MotherTongue;
use Auth;
use Exception;

class MotherTonguesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the mother tongues.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $motherTongues = MotherTongue::orderBy('created_at', 'desc')->with('company')->get();

        return view('mother_tongues.index', compact('motherTongues'));
    }

    /**
     * Show the form for creating a new mother tongue.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        return view('mother_tongues.create');
    }

    /**
     * Store a new mother tongue in the storage.
     *
     * @param App\Http\Requests\MotherTonguesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(MotherTonguesFormRequest $request)
    {
        try {
            if ($request['hidden']) {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                $data =  MotherTongue::create($data);
                // return redirect()->back();
                return response()->json($data);
            } else {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                MotherTongue::create($data);
                return redirect()->route('mother_tongues.mother_tongue.index')
                    ->with('success_message', 'Mother Tongue was successfully added.');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Display the specified mother tongue.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $motherTongue = MotherTongue::findOrFail($id);

        return view('mother_tongues.show', compact('motherTongue'));
    }

    /**
     * Show the form for editing the specified mother tongue.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $motherTongue = MotherTongue::findOrFail($id);


        return view('mother_tongues.edit', compact('motherTongue'));
    }

    /**
     * Update the specified mother tongue in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\MotherTonguesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, MotherTonguesFormRequest $request)
    {
        try {

            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $motherTongue = MotherTongue::findOrFail($id);
            $motherTongue->update($data);

            return redirect()->route('mother_tongues.mother_tongue.index')
                ->with('success_message', 'Mother Tongue was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified mother tongue from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $motherTongue = MotherTongue::findOrFail($id);
            if ($motherTongue->employees()->exists()) {
                throw new Exception("This mother tongue has employee and cannot be deleted.");
            }
            $motherTongue->delete();

            return redirect()->route('mother_tongues.mother_tongue.index')
                ->with('success_message', 'Mother Tongue was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
