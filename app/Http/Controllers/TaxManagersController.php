<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\TaxManagersFormRequest;
use App\Models\TaxManager;
use Auth;
use Exception;

class TaxManagersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the tax managers.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $taxManagers = TaxManager::orderBy('created_at', 'desc')->with('company')->get();

        return view('tax_managers.index', compact('taxManagers'));
    }

    /**
     * Show the form for creating a new tax manager.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        
        
        return view('tax_managers.create');
    }

    /**
     * Store a new tax manager in the storage.
     *
     * @param App\Http\Requests\TaxManagersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(TaxManagersFormRequest $request)
    {  
        try {
            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;

            TaxManager::create($data);

            return redirect()->route('tax_managers.tax_manager.index')
                ->with('success_message', 'Tax Manager was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified tax manager.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $taxManager = TaxManager::findOrFail($id);

        return view('tax_managers.show', compact('taxManager'));
    }

    /**
     * Show the form for editing the specified tax manager.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $taxManager = TaxManager::findOrFail($id);
        

        return view('tax_managers.edit', compact('taxManager'));
    }

    /**
     * Update the specified tax manager in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\TaxManagersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, TaxManagersFormRequest $request)
    {
        $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $taxManager = TaxManager::findOrFail($id);
            $taxManager->update($data);

            return redirect()->route('tax_managers.tax_manager.index')
                ->with('success_message', 'Tax Manager was successfully updated.');
        try {
            
            
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified tax manager from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $taxManager = TaxManager::findOrFail($id);
            $taxManager->delete();

            return redirect()->route('tax_managers.tax_manager.index')
                ->with('success_message', 'Tax Manager was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }



}
