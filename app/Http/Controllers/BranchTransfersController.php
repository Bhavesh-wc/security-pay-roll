<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\BranchTransfersFormRequest;
use App\Models\Branch;
use App\Models\BranchTransfer;
use App\Models\Department;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\Grade;
use App\Models\BranchDesignation;
use Exception;
use Auth;
class BranchTransfersController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the branch transfers.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $branchTransfers = BranchTransfer::orderBy('created_at', 'desc')->with('company','branch','employee','designation','department','grade')->get();

        return view('branch_transfers.index', compact('branchTransfers'));
    }

    /**
     * Show the form for creating a new branch transfer.
     *
     * @return \Illuminate\View\View
     */
    public function create($id)
    {
        $branches = Branch::where('status', 'active')->pluck('name', 'id')->all();
        $employees = Employee::where('status', 'active')->pluck('full_name', 'id')->all();
        $designations = Designation::where('status', 'active')->pluck('designation_name', 'id')->all();
        $departments = Department::where('status', 'active')->pluck('department_name','id')->all();
        $grades = Grade::where('status', 'active')->pluck('grade_name','id')->all();
        $oldemployees = Employee::with('designation', 'country', 'state', 'district', 'city', 'pincode', 'mothertongue', 'qualification', 'rtostate', 'bank', 'bankbranch', 'branch', 'department', 'grade', 'company')->where('id',$id)->first();
        return view('branch_transfers.create', compact('branches','employees','designations','departments','grades','oldemployees'));
    }

    /**
     * Store a new branch transfer in the storage.
     *
     * @param App\Http\Requests\BranchTransfersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(BranchTransfersFormRequest $request)
    {
        try {      
            
            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            BranchTransfer::where('employee_id',$data['employee_id'])->update(['transfer' => 'transfer']);
            $branchTransfer = BranchTransfer::create($data);
            $employee = Employee::findOrFail($branchTransfer->employee_id);
            $employee->branch_id=$branchTransfer->branch_id;
            $employee->designation_id=$branchTransfer->designation_id;
            $employee->update();

            return redirect()->route('employees.employee.index')
                ->with('success_message', 'Branch Transfer was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified branch transfer.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $branchTransfer = BranchTransfer::with('branch','employee','designation','department','grade')->findOrFail($id);

        return view('branch_transfers.show', compact('branchTransfer'));
    }

    /**
     * Show the form for editing the specified branch transfer.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $branchTransfer = BranchTransfer::findOrFail($id);
        $branches = Branch::pluck('name','id')->all();
        $employees = Employee::pluck('full_name','id')->all();
        $designations = Designation::pluck('designation_name','id')->all();
        $departments = Department::pluck('department_name','id')->all();
        $grades = Grade::pluck('grade_name','id')->all();

        return view('branch_transfers.edit', compact('branchTransfer','branches','employees','designations','departments','grades'));
    }

    /**
     * Update the specified branch transfer in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\BranchTransfersFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, BranchTransfersFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            
            $branchTransfer = BranchTransfer::findOrFail($id);
            $branchTransfer->update($data);

            return redirect()->route('branch_transfers.branch_transfer.index')
                ->with('success_message', 'Branch Transfer was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified branch transfer from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $branchTransfer = BranchTransfer::findOrFail($id);
            $branchTransfer->delete();

            return redirect()->route('branch_transfers.branch_transfer.index')
                ->with('success_message', 'Branch Transfer was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

}
