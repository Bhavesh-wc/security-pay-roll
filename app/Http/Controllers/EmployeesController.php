<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeesFormRequest;
use App\Models\Bank;
use App\Models\BankBranch;
use App\Models\Branch;
use App\Models\CitizenOfIndiaBy;
use App\Models\City;
use App\Models\Country;
use App\Models\Department;
use App\Models\Designation;
use App\Models\District;
use App\Models\Employee;
use App\Models\Grade;
use App\Models\MotherTongue;
use App\Models\Pincode;
use App\Models\Qualification;
use App\Models\BranchDesignation;
use App\Models\State;
use App\Models\BranchTransfer;
use App\Models\EmployeeLeaveHistory;
use Auth;
use Exception;
use Illuminate\Http\Request;
use Carbon\Carbon;

class EmployeesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the employees.
     *
     * @return \Illuminate\View\View
     */

    public function index()
    {
        $employees = Employee::With('designation', 'country', 'state', 'district', 'city', 'pincode', 'mothertongue', 'qualification', 'rtostate', 'bank', 'bankbranch', 'branch', 'department', 'grade', 'company', 'branchDesignation')->where('status', '=', 'active')->orderBy('created_at', 'desc')->paginate(10);

        // foreach ($employee as $employees) {
        //     $registrationDate = Carbon::parse($employees->registration_date)->format('d-m-Y');
        //     $employees->registration_date = $registrationDate;
        // }
        return view('employees.index', compact('employees'));
    }

    public function leftEmployee()
    {
        $employees = Employee::with('designation', 'country', 'state', 'district', 'city', 'pincode', 'mothertongue', 'qualification', 'rtostate', 'bank', 'bankbranch', 'branch', 'department', 'grade', 'company', 'branchDesignation')->where('status', '=', 'inactive')->orderBy('created_at', 'desc')->get();

        // foreach ($employee as $employees) {
        //     $registrationDate = Carbon::parse($employees->registration_date)->format('d-m-Y');
        //     $employees->registration_date = $registrationDate;
        // }
        return view('employees.left_employee', compact('employees'));
    }

    /**
     * Show the form for creating a new employee.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $designations = Designation::where('status', 'active')->pluck('designation_name', 'id')->all();
        $data['country'] = country::where('status', 'active')->orderBy('country_name', 'asc')->get();
        $motherTongues = MotherTongue::where('status', 'active')->pluck('language_name', 'id')->all();
        $qualifications = Qualification::where('status', 'active')->pluck('qualification_name', 'id')->all();
        $countries = Country::where('status', 'active')->pluck('country_name', 'id')->all();
        $states = State::where('status', 'active')->pluck('state_name', 'id')->all();
        $districts = District::where('status', 'active')->pluck('district_name', 'id')->all();
        $cities = City::where('status', 'active')->pluck('city_name', 'id')->all();
        $pincodes = Pincode::where('status', 'active')->pluck('pincode', 'id')->all();
        $branches = Branch::where('status', 'active')->pluck('name', 'id')->all();
        $departments = Department::where('status', 'active')->pluck('department_name', 'id')->all();
        $grades = Grade::where('status', 'active')->pluck('grade_name', 'id')->all();

        return view('employees.create', $data, compact('designations', 'branches', 'motherTongues', 'qualifications', 'countries', 'states', 'districts', 'cities', 'pincodes', 'departments', 'grades'));
    }
    /**
     * Store a new employee in the storage.
     *
     * @param App\Http\Requests\EmployeesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(EmployeesFormRequest $request)
    {
        $data = $request->getData();
        $data['company_id'] = Auth::user()->id;
        $data['entry_date']=$request['entry_date'];
        $data['experience'] = $request['experience'];
        $data['experience_company'] = $request['experience_company'];

        $dateTime = $data['entry_date'];
        $Month = explode('-', $dateTime);
        $data['year'] = $Month[0];
        $data['month'] = $Month[1];
        $convertedDate = Carbon::createFromFormat('m', $data['month'])->format('F');
       // dd($convertedDate);
        $data['monthyear'] = $data['year'] . '-' . $convertedDate;
        
        $employee = Employee::create($data);
        
        EmployeeLeaveHistory::create([
            'company_id' => Auth::user()->id,
            'employee_id' => $employee->id,
            'join_date' => $request['entry_date'],
            'branch_id'=> $employee->branch_id
        ]);

        $branchdesignation = BranchDesignation::where('branch_id', $data['branch_id'])->where('designation_id', $data['designation_id'])->first();
        $currentMonth = date('Y-m');
        $Month = explode('-', $currentMonth);
        BranchTransfer::create([
            'branch_id' => $employee->branch_id,
            'designation_id' => $employee->designation_id,
            'company_id' => $employee->company_id,
            'department_id' => $employee->department_id,
            'grade_id' => $employee->grade_id,
            'employee_id' => $employee->id,
            'basic_fix' => $branchdesignation['basic_fix'],
            'basic_da' => $branchdesignation['basic_da'],
            'extra_allowance' => $branchdesignation['extra_allowance'],
            'hra' => $branchdesignation['hra'],
            'bonus' => $branchdesignation['bonus'],
            'fix_salary' => $branchdesignation['fix_salary'],
            'transfer' => 'current',
            'created_date' => $employee->registartion_date,
            'updated_month' => $currentMonth,
            'monthyear' => $currentMonth,
            'year' => $Month[0],
            'month' => $Month[1]
        ]);
        return redirect()->route('employees.employee.index')
            ->with('success_message', 'Employee added successfully .');
        try {
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified employee.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employee = Employee::with('designation', 'country', 'state', 'district', 'city', 'pincode', 'mothertongue', 'qualification', 'rtostate', 'bank', 'bankbranch', 'branch', 'department', 'grade', 'branchDesignation')->findOrFail($id);

        $branchTransfers = BranchTransfer::with('company', 'branch', 'employee', 'designation', 'department', 'grade')->where('employee_id', $id)->get();

        return view('employees.show', compact('employee', 'branchTransfers'));
    }

    /**
     * Show the form for editing the specified employee.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employee = Employee::with('designation', 'country', 'state', 'district', 'city', 'pincode', 'mothertongue', 'qualification', 'rtostate', 'bank', 'bankbranch', 'branch', 'department', 'grade', 'branchDesignation')->findOrFail($id);
        $designations = Designation::where('status', 'active')->pluck('designation_name', 'id')->all();
        $countries = Country::where('status', 'active')->pluck('country_name', 'id')->all();
        $states = State::where('status', 'active')->pluck('state_name', 'id')->all();
        $districts = District::where('status', 'active')->pluck('district_name', 'id')->all();
        $cities = City::where('status', 'active')->pluck('city_name', 'id')->all();
        $pincodes = Pincode::where('status', 'active')->pluck('pincode', 'id')->all();
        $motherTongues = MotherTongue::where('status', 'active')->pluck('language_name', 'id')->all();
        $qualifications = Qualification::where('status', 'active')->pluck('qualification_name', 'id')->all();
        $rtoStates = State::where('status', 'active')->pluck('state_name', 'id')->all();
        $banks = Bank::where('status', 'active')->pluck('bank_name', 'id')->all();
        $bankBranches = BankBranch::where('status', 'active')->pluck('bank_branch_name', 'id')->all();
        $branches = Branch::where('status', 'active')->pluck('name', 'id')->all();
        $departments = Department::where('status', 'active')->pluck('department_name', 'id')->all();
        $grades = Grade::where('status', 'active')->pluck('grade_name', 'id')->all();
        return view('employees.edit', compact('employee', 'designations', 'countries', 'states', 'districts', 'cities', 'pincodes', 'motherTongues', 'qualifications', 'rtoStates', 'banks', 'bankBranches', 'branches', 'departments', 'grades'));
    }


    /**
     * Update the specified employee in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\EmployeesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, EmployeesFormRequest $request)
    {

        if($request['relieve_date'] != null &&  $request['status'] == 'inactive'){
            $employee = Employee::findOrFail($id);
            $lastJoinDate = EmployeeLeaveHistory::where('employee_id', $employee->id)
                ->orderBy('join_date', 'desc')
                ->pluck('join_date')
                ->first();
            EmployeeLeaveHistory::create([
                'company_id' => Auth::user()->id,
                'employee_id' => $employee->id,
                'join_date' => $lastJoinDate,
                'leave_date'=> $request['relieve_date'],
                'branch_id' => $employee->branch_id
            ]); 

         }

        // $data = $request->getData();
        // $data['company_id'] = Auth::user()->id;
        // $employee = Employee::findOrFail($id);
        // $employee->update($data);
        // try {

        $data = $request->getData();
        $data1 = $request->getDataInfo();

        //dd($data1);

        if ($data) {
            $data['company_id'] = Auth::user()->id;
            $employee = Employee::findOrFail($id);

            $employee->update($data);
        } else {

            $employee = Employee::findOrFail($id);

            $storeMonth = $employee->entry_date;

            $monthyear = Carbon::parse($storeMonth)->format('Y-M');

            $employee->monthyear = $monthyear;

            $Month = explode('-', $monthyear);

            $employee->year = $Month[0];
            $employee->month = $Month[1];

            $employee->update($data1);
        }

        try {



            return redirect()->route('employees.employee.index')
                ->with('success_message', 'Employee updated successfully .');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Remove the specified employee from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $employee = Employee::findOrFail($id);
            if ($employee->damages()->exists()) {
                throw new Exception("This employee has damage and cannot be deleted.");
            }
            if ($employee->fines()->exists()) {
                throw new Exception("This employee has fine and cannot be deleted.");
            }
            if ($employee->overTimes()->exists()) {
                throw new Exception("This employee has over time and cannot be deleted.");
            }
            if ($employee->advanceSalaries()->exists()) {
                throw new Exception("This employee has advance salary and cannot be deleted.");
            }
            $employee->delete();
            return redirect()->route('employees.employee.index')
                ->with('success_message', 'Employee deleted successfully .');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }

    public function otherInfo($id)
    {
        $employee = Employee::findOrFail($id);
        $rtoStates = State::where('status', 'active')->pluck('state_name', 'id')->all();
        return view('employees.other_info', compact('employee', 'rtoStates'));
    }

    public function companyLegal($id)
    {
        $employee = Employee::findOrFail($id);
        $bankData['bank'] = Bank::where('status', 'active')->get(["bank_name", "id"]);
        $bankData['bankBranches'] = BankBranch::where('status', 'active')->get(["bank_branch_name", "id"]);
        $branches = Branch::where('status', 'active')->pluck('name', 'id')->all();
        $departments = Department::where('status', 'active')->pluck('department_name', 'id')->all();
        $grades = Grade::where('status', 'active')->pluck('grade_name', 'id')->all();
        $banks = Bank::all();
        return view('employees.company_legal', $bankData, compact('employee', 'branches', 'departments', 'grades', 'banks'));
    }

    public function Document($id)
    {
        $employee = Employee::findOrFail($id);
        return view('employees.document', compact('employee'));
    }

    public function changePf(Request $request, $id)
    {
        try {
            $employee = Employee::findOrFail($id);
            $employee->pf_ac_code = $request->pf_ac_code;
            $employee->pf_uan_code = $request->pf_uan_code;
            $employee->save();
            return redirect()->route('employees.employee.index')
                ->with('success_messages', 'PF Information Updated successfully.');
        } catch (Exception $exception) {
            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    public function Join($id,Request $request)
    {
        $employee = Employee::findOrFail($id);
        EmployeeLeaveHistory::create([
            'company_id' => Auth::user()->id,
            'employee_id' => $employee->id,
            'join_date' => now(),
            'branch_id' => $employee->branch_id
        ]); 

        $employee->status = 'active';
        $employee->relieve_date = null;
        $employee->final_relieve_date = null;
        $employee->save();

        return redirect()->route('employees.employee.leftemp')
            ->with('success_messages', 'Employee rejoin  successfully.');
    }
}
