<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Http\Requests\UsersFormRequest;
class ProfileController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        return view('profile.index',compact('user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $user->name = $request->input('name');
        $user->company_code = $request->input('company_code');
        $user->contact_person_name = $request->input('contact_person_name');
        $user->contact_person_phone_no = $request->input('contact_person_phone_no');
        $user->gst_no = $request->input('gst_no');
        $user->pf_no = $request->input('pf_no');
        $user->esic = $request->input('esic');
        $user->police_registration_number = $request->input('police_registration_number');
        $user->shop_and_establishment = $request->input('shop_and_establishment');
        $user->professional_tax = $request->input('professional_tax');
        $user->msme_registaration_no = $request->input('msme_registaration_no');
        $user->welfare_fund = $request->input('welfare_fund');
        $user->email = $request->input('email');

        if($image = $request->file('profile_photo')) {
            // $path = 'public/uploads';
            // $uniq = rand();
            // $profile_photo = date('YmdHis'). $uniq. "." .$image->getClientOriginalExtension();
            // $image->move($path, $profile_photo);

            // 
            $path = config('laravel-code-generator.files_upload_path', 'uploads');
            $saved = $image->store($path, config('filesystems.default'));
            $user->profile_photo = "$saved";
        }

        //dd($user);
        $user->update();
        return redirect()->route('profile.index')
            ->with('success_message', 'Profile updated successfully');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function change(Request $request, $id)
    {
        $userId = Auth::user()->id;
        $user = User::find($userId);
        $psw = $request->input('password');
        $cnfpsw = $request->input('confirm-password');
        $user->password = Hash::make($psw);

        if ($psw === $cnfpsw) {
            $user->update();

            return redirect()->route('profile.index')
                ->with('success_message', 'Password changed successfully');
        } else {
            return redirect()->route('profile.index')
                ->with('error', 'Password and Confirm Password must be same !');
        }

    }
}
