<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\DesignationsFormRequest;
use App\Models\Designation;
use Auth;
use Exception;

class DesignationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the designations.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $designations = Designation::orderBy('created_at', 'desc')->with('company')->get();

        return view('designations.index', compact('designations'));
    }

    /**
     * Show the form for creating a new designation.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


        return view('designations.create');
    }

    /**
     * Store a new designation in the storage.
     *
     * @param App\Http\Requests\DesignationsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(DesignationsFormRequest $request)
    {
        try {
            if ($request['hidden']) {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                Designation::create($data);
                // return redirect()->back();
                return response()->json($data);
            } else {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                Designation::create($data);
                return redirect()->route('designations.designation.index')
                    ->with('success_message', 'Designation was successfully added.');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified designation.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $designation = Designation::findOrFail($id);

        return view('designations.show', compact('designation'));
    }

    /**
     * Show the form for editing the specified designation.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $designation = Designation::findOrFail($id);


        return view('designations.edit', compact('designation'));
    }

    /**
     * Update the specified designation in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\DesignationsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, DesignationsFormRequest $request)
    {
        try {

            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $designation = Designation::findOrFail($id);
            $designation->update($data);

            return redirect()->route('designations.designation.index')
                ->with('success_message', 'Designation was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified designation from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $designation = Designation::findOrFail($id);
            if ($designation->branchDesignation()->exists()) {
                throw new Exception("This designation has branch designation and cannot be deleted.");
            }
            if ($designation->damages()->exists()) {
                throw new Exception("This designation has damage and cannot be deleted.");
            }
            if ($designation->employees()->exists()) {
                throw new Exception("This designation has employee and cannot be deleted.");
            }
            if ($designation->fines()->exists()) {
                throw new Exception("This designation has fine and cannot be deleted.");
            }
            if ($designation->overTimes()->exists()) {
                throw new Exception("This designation has over time and cannot be deleted.");
            }
            if ($designation->advanceSalaries()->exists()) {
                throw new Exception("This designation has advance salary and cannot be deleted.");
            }
            $designation->delete();

            return redirect()->route('designations.designation.index')
                ->with('success_message', 'Designation was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
