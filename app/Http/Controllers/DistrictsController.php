<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\DistrictsFormRequest;
use App\Models\District;
use App\Models\State;
use Auth;
use Exception;

class DistrictsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the districts.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $districts = District::orderBy('created_at', 'desc')->with('state', 'company')->get();

        return view('districts.index', compact('districts'));
    }

    /**
     * Show the form for creating a new district.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $states = State::where('status', 'active')->pluck('state_name', 'id')->all();

        return view('districts.create', compact('states'));
    }

    /**
     * Store a new district in the storage.
     *
     * @param App\Http\Requests\DistrictsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(DistrictsFormRequest $request)
    {
        try {
            if ($request['hidden']) {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                $data = District::create($data);
                // return redirect()->back();
                return response()->json($data);
            } else {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                District::create($data);
                return redirect()->route('districts.district.index')
                    ->with('success_message', 'District was successfully added.');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified district.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $district = District::with('state')->findOrFail($id);

        return view('districts.show', compact('district'));
    }

    /**
     * Show the form for editing the specified district.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $district = District::findOrFail($id);
        $states = State::where('status', 'active')->pluck('state_name', 'id')->all();

        return view('districts.edit', compact('district', 'states'));
    }

    /**
     * Update the specified district in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\DistrictsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, DistrictsFormRequest $request)
    {
        try {

            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $district = District::findOrFail($id);
            $district->update($data);

            return redirect()->route('districts.district.index')
                ->with('success_message', 'District was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified district from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $district = District::findOrFail($id);
            if ($district->cities()->exists()) {
                throw new Exception("This district has city and cannot be deleted.");
            }
            if ($district->employees()->exists()) {
                throw new Exception("This district has employee and cannot be deleted.");
            }
            $district->delete();

            return redirect()->route('districts.district.index')
                ->with('success_message', 'District was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
