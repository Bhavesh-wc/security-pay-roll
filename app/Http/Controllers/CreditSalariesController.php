<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Bank;
use App\Http\Requests\CreditSalariesFormRequest;
use App\Models\BankBranch;
use App\Models\Branch;
use App\Models\CreditSalary;
use App\Models\User;
use App\Models\CompanyBank;
use App\Models\Salary;
use Illuminate\Http\Request;
use Exception;
use Carbon\Carbon;
use Auth;

class CreditSalariesController extends Controller
{

    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the credit salaries.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {

        $creditSalaries = CreditSalary::orderBy('created_at', 'desc')->with('branch', 'companyBank', 'company')->get();
        return view('credit_salaries.index', compact('creditSalaries'));
    }

    /**
     * Show the form for creating a new credit salary.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $branches = Branch::pluck('name', 'id')->all();
        $banks = CompanyBank::pluck('bank_name', 'id')->all();
        $bankBranches = BankBranch::pluck('bank_branch_name', 'id')->all();
        $companies = User::pluck('id', 'id')->all();

        return view('credit_salaries.create', compact('branches', 'banks', 'bankBranches', 'companies'));
    }

    /**
     * Store a new credit salary in the storage.
     *
     * @param Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(CreditSalariesFormRequest $request)
    {   
        try {
            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $data['bank_id'] = $request['bank_id'];
            $data['bank_branch_name'] = $request['bank_branch_name'];
            $data['account_no'] = $request['account_no'];
            CreditSalary::create($data);
            return redirect()->route('credit_salaries.credit_salary.index')
                ->with('success_message', 'Credit Salary was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified credit salary.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $creditSalary = CreditSalary::with('branch', 'bank', 'bankbranch', 'company')->findOrFail($id);

        return view('credit_salaries.show', compact('creditSalary'));
    }

    /**
     * Show the form for editing the specified credit salary.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $creditSalary = CreditSalary::findOrFail($id);
        $branches = Branch::pluck('name', 'id')->all();
        $banks = Bank::pluck('bank_name', 'id')->all();
        $bankBranches = BankBranch::pluck('bank_branch_name', 'id')->all();
        $companies = User::pluck('id', 'id')->all();

        return view('credit_salaries.edit', compact('creditSalary', 'branches', 'banks', 'bankBranches', 'companies'));
    }

    /**
     * Update the specified credit salary in the storage.
     *
     * @param int $id
     * @param Illuminate\Http\Request $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, Request $request)
    {
        try {

            $data = $this->getData($request);

            $creditSalary = CreditSalary::findOrFail($id);
            $creditSalary->update($data);

            return redirect()->route('credit_salaries.credit_salary.index')
                ->with('success_message', 'Credit Salary was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified credit salary from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $creditSalary = CreditSalary::findOrFail($id);
            $creditSalary->delete();

            return redirect()->route('credit_salaries.credit_salary.index')
                ->with('success_message', 'Credit Salary was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }


    /**
     * Get the request's data from the request.
     *
     * @param Illuminate\Http\Request\Request $request 
     * @return array
     */
    protected function getData(Request $request)
    {
        $rules = [
            'monthyear' => 'nullable|date_format:j/n/Y g:i A',
            'branch_id' => 'nullable',
            'cheque_no' => 'nullable|string|min:0',
            'ac_no' => 'nullable|string|min:0',
            'amount_credit' => 'number|min:1|nullable|string',
            'bank_id' => 'nullable',
            'bank_branch_id' => 'nullable',
            'credit_date' => 'nullable|date_format:j/n/Y',
            'company_id' => 'nullable',
        ];

        $data = $request->validate($rules);


        return $data;
    }
}
