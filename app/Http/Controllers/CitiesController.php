<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\CitiesFormRequest;
use App\Models\City;
use App\Models\District;
use Auth;
use Exception;

class CitiesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the cities.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $cities = City::orderBy('created_at', 'desc')->with('district', 'company')->get();

        return view('cities.index', compact('cities'));
    }

    /**
     * Show the form for creating a new city.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $districts = District::where('status', 'active')->pluck('district_name', 'id')->all();

        return view('cities.create', compact('districts'));
    }

    /**
     * Store a new city in the storage.
     *
     * @param App\Http\Requests\CitiesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(CitiesFormRequest $request)
    {
        try {
            if ($request['hidden']) {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                $data = City::create($data);
                // return redirect()->back();
                return response()->json($data);
            } else {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                City::create($data);
                return redirect()->route('cities.city.index')
                    ->with('success_message', 'City was successfully added.');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified city.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $city = City::with('district')->findOrFail($id);

        return view('cities.show', compact('city'));
    }

    /**
     * Show the form for editing the specified city.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $city = City::findOrFail($id);
        $districts = District::where('status', 'active')->pluck('district_name', 'id')->all();

        return view('cities.edit', compact('city', 'districts'));
    }

    /**
     * Update the specified city in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\CitiesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, CitiesFormRequest $request)
    {
        try {

            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $city = City::findOrFail($id);
            $city->update($data);

            return redirect()->route('cities.city.index')
                ->with('success_message', 'City was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified city from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $city = City::findOrFail($id);
            if ($city->employees()->exists()) {
                throw new Exception("This city has employee and cannot be deleted.");
            }
            if ($city->pincodes()->exists()) {
                throw new Exception("This city has pincode and cannot be deleted.");
            }
            $city->delete();

            return redirect()->route('cities.city.index')
                ->with('success_message', 'City was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
