<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\EmployeeLeaveHistoriesFormRequest;
use App\Models\Employee;
use App\Models\EmployeeLeaveHistory;
use Exception;

class EmployeeLeaveHistoriesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the employee leave histories.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $employeeLeaveHistories = EmployeeLeaveHistory::with('employee','company')->paginate(25);

        return view('employee_leave_histories.index', compact('employeeLeaveHistories'));
    }

    /**
     * Show the form for creating a new employee leave history.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $employees = Employee::pluck('full_name','id')->all();
        
        return view('employee_leave_histories.create', compact('employees'));
    }

    /**
     * Store a new employee leave history in the storage.
     *
     * @param App\Http\Requests\EmployeeLeaveHistoriesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(EmployeeLeaveHistoriesFormRequest $request)
    {
        // try {
            
            $data = $request->getData();
            
            EmployeeLeaveHistory::create($data);

        //     return redirect()->route('employee_leave_histories.employee_leave_history.index')
        //         ->with('success_message', 'Employee Leave History was successfully added.');
        // } catch (Exception $exception) {

        //     return back()->withInput()
        //         ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        // }
    }

    /**
     * Display the specified employee leave history.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $employeeLeaveHistory = EmployeeLeaveHistory::with('employee')->findOrFail($id);

        return view('employee_leave_histories.show', compact('employeeLeaveHistory'));
    }

    /**
     * Show the form for editing the specified employee leave history.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $employeeLeaveHistory = EmployeeLeaveHistory::findOrFail($id);
        $employees = Employee::pluck('full_name','id')->all();

        return view('employee_leave_histories.edit', compact('employeeLeaveHistory','employees'));
    }

    /**
     * Update the specified employee leave history in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\EmployeeLeaveHistoriesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, EmployeeLeaveHistoriesFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            
            $employeeLeaveHistory = EmployeeLeaveHistory::findOrFail($id);
            $employeeLeaveHistory->update($data);

            return redirect()->route('employee_leave_histories.employee_leave_history.index')
                ->with('success_message', 'Employee Leave History was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified employee leave history from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $employeeLeaveHistory = EmployeeLeaveHistory::findOrFail($id);
            $employeeLeaveHistory->delete();

            return redirect()->route('employee_leave_histories.employee_leave_history.index')
                ->with('success_message', 'Employee Leave History was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }



}
