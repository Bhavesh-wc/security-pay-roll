<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Employee;
use App\Models\Fine;
use App\Models\State;
use App\Models\City;
use App\Models\Branch;
use App\Models\Country;
use App\Models\District;
use App\Models\Bank;
use App\Models\BankBranch;
use App\Models\Attendance;
use App\Models\Designation;
use App\Models\BranchDesignation;
use App\Models\Pincode;
use App\Models\BranchTransfer;

class MasterController extends Controller
{
    public function fetchEmployee(Request $request)
    {
        $data['Employees'] = Employee::where("branch_id", $request->branch_id)
            ->get(["full_name", "id"]);
        return response()->json($data);
    }

    public function getFineBranches(Request $request)
    {
        $monthyear = $request->get('monthyear');
        $branches = DB::table('fines')
            ->join('branches', 'fines.branch_id', '=', 'branches.id')
            ->select('fines.branch_id', 'branches.name')
            ->where('fines.monthyear', '=', $monthyear)
            ->groupBy('fines.branch_id')
            ->get();
        $options = [];

        foreach ($branches as $branch) {
            $options[$branch->branch_id] = $branch->name;
        }
        return response()->json($options);
    }

    public function getDamageBranches(Request $request)
    {
        $monthyear = $request->get('monthyear');
        $branches = DB::table('damages')
            ->join('branches', 'damages.branch_id', '=', 'branches.id')
            ->select('damages.branch_id', 'branches.name')
            ->where('damages.monthyear', '=', $monthyear)
            ->groupBy('damages.branch_id')
            ->get();
        $options = [];

        foreach ($branches as $branch) {
            $options[$branch->branch_id] = $branch->name;
        }
        return response()->json($options);
    }

    public function getOverTimeBranches(Request $request)
    {
        $monthyear = $request->get('monthyear');
        $branches = DB::table('over_times')
            ->join('branches', 'over_times.branch_id', '=', 'branches.id')
            ->select('over_times.branch_id', 'branches.name')
            ->where('over_times.monthyear', '=', $monthyear)
            ->groupBy('over_times.branch_id')
            ->get();
        $options = [];

        foreach ($branches as $branch) {
            $options[$branch->branch_id] = $branch->name;
        }
        return response()->json($options);
    }

    public function getAdvanceSalaryBranches(Request $request)
    {
        $monthyear = $request->get('monthyear');
        $branches = DB::table('advance_salaries')
            ->join('branches', 'advance_salaries.branch_id', '=', 'branches.id')
            ->select('advance_salaries.branch_id', 'branches.name')
            ->where('advance_salaries.monthyear', '=', $monthyear)
            ->groupBy('advance_salaries.branch_id')
            ->get();
        $options = [];

        foreach ($branches as $branch) {
            $options[$branch->branch_id] = $branch->name;
        }
        return response()->json($options);
    }

    public function getIdentityCardBranches(Request $request)
    {
        $monthyear = $request->get('monthyear');
        $branches = DB::table('employees')
            ->join('branches', 'employees.branch_id', '=', 'branches.id')
            ->select('employees.branch_id', 'branches.name')
            ->where('employees.monthyear', '=', $monthyear)
            ->groupBy('employees.branch_id')
            ->get();
        $options = [];

        foreach ($branches as $branch) {
            $options[$branch->branch_id] = $branch->name;
        }

        // please remove below code after employee transfer by bhavesh 28-3-2023 
        //$options[1] = 'bhavesh';

        return response()->json($options);
    }

    public function getEmployedByContractorBranches(Request $request)
    {
        $monthyear = $request->get('monthyear');
        $branches = DB::table('employees')
            ->join('branches', 'employees.branch_id', '=', 'branches.id')
            ->select('employees.branch_id', 'branches.name')
            ->where('employees.monthyear', '=', $monthyear)
            ->groupBy('employees.branch_id')
            ->get();
        $options = [];

        foreach ($branches as $branch) {
            $options[$branch->branch_id] = $branch->name;
        }

        // please remove below code after employee transfer by bhavesh 28-3-2023 
        $options[1] = 'bhavesh';

        return response()->json($options);
    }

    public function getPoliceVerificationBranches(Request $request)
    {
        $monthyear = $request->get('monthyear');
        $branches = DB::table('employees')
            ->join('branches', 'employees.branch_id', '=', 'branches.id')
            ->select('employees.branch_id', 'branches.name')
            ->where('employees.monthyear', '=', $monthyear)
            ->where('employees.relieve_date', '=', '')
            ->groupBy('employees.branch_id')
            ->get();
        $options = [];

        foreach ($branches as $branch) {
            $options[$branch->branch_id] = $branch->name;
        }

        // please remove below code after employee transfer by bhavesh 28-3-2023 
        //$options[1] = 'bhavesh';

        return response()->json($options);
    }

    public function getPoliceVerificationForLeaveBranches(Request $request)
    {

        $status = $request->get('status');
        $startdate = date('Y-m-d', strtotime($request->get('startdate')));
        $enddate = date('Y-m-d', strtotime($request->get('enddate')));

        if ($status == 'leave') {
            $branches = DB::table('employees')
                ->join('branches', 'employees.branch_id', '=', 'branches.id')
                ->select('employees.branch_id', 'branches.name')
                ->whereBetween('employees.final_relieve_date', [$startdate, $enddate])
                ->where('employees.relieve_date', '!=', '')
                ->groupBy('employees.branch_id')
                ->get();
        } else {
            $branches = DB::table('employees')
                ->join('branches', 'employees.branch_id', '=', 'branches.id')
                ->select('employees.branch_id', 'branches.name')
                ->whereBetween('entry_date', [$startdate, $enddate])
                ->where('relieve_date', '=', '')
                ->groupBy('employees.branch_id')
                ->get();
        }

        $options = [];

        foreach ($branches as $branch) {
            $options[$branch->branch_id] = $branch->name;
        }

        // please remove below code after employee transfer by bhavesh 28-3-2023 

        return response()->json($options);
    }

    public function fetchState(Request $request)
    {
        $cid = $request->post('cid');
        $state = State::where('country_id', $cid)->where('status', 'active')->orderBy('state_name', 'asc')->get();
        $html = '<option value="">Select State</option>';
        foreach ($state as $list) {
            $html .= '<option value="' . $list->id . '">' . $list->state_name . '</option>';
        }
        echo $html;
    }

    public function fetchDistrict(Request $request)
    {
        $sid = $request->post('sid');
        $district = District::where('state_id', $sid)->where('status', 'active')->orderBy('district_name', 'asc')->get();
        $html = '<option value="">Select Destrict</option>';
        foreach ($district as $list) {
            $html .= '<option value="' . $list->id . '">' . $list->district_name . '</option>';
        }
        echo $html;
    }

    public function fetchCity(Request $request)
    {
        $did = $request->post('did');
        $city = City::where('district_id', $did)->where('status', 'active')->orderBy('city_name', 'asc')->get();
        $html = '<option value="">Select City</option>';
        foreach ($city as $list) {
            $html .= '<option value="' . $list->id . '">' . $list->city_name . '</option>';
        }
        echo $html;
    }

    public function fetchPincode(Request $request)
    {
        $cid = $request->post('cid');
        $pincode = Pincode::where('city_id', $cid)->where('status', 'active')->orderBy('pincode', 'asc')->get();
        $html = '<option value="">Select Pincode</option>';
        foreach ($pincode as $list) {
            $html .= '<option value="' . $list->id . '">' . $list->pincode . '</option>';
        }
        echo $html;
    }

    // fetch bank branch and Ifsc code
    public function fetchBranch(Request $request)
    {
        $bid = $request->post('bid');
        $bank_branch = BankBranch::where('bank_id', $bid)->orderBy('bank_branch_name', 'asc')->get();
        $html = '<option value="">Select Bank Branch</option>';
        foreach ($bank_branch as $list) {
            $html .= '<option value="' . $list->id . '">' . $list->bank_branch_name . '</option>';
        }
        echo $html;
    }

    // fetch bank branch and Account No code and fetch bank_branch  name in company banks modules
    public function fetchAccount(Request $request)
    {
        $bid = $request->post('bid');
        $account_no = DB::table('company_banks')->where('id', $bid)->pluck('account_no')->first();
        return response()->json(['account_no' => $account_no]);
    }

    public function  fetchCbranch(Request $request)
    {
        $bid = $request->post('bid');
        $cbranch = DB::table('company_banks')->where('id', $bid)->pluck('bank_branch_name')->first();
        return response()->json(['cbranch' => $cbranch]);
    }

    public function fetchIfsc(Request $request)
    {
        $bid = $request->post('bid');
        $ifsc_code = CompanyBank::where('id', $bid)->pluck('ifsc_code')->first();
        return response()->json(['ifsc_code' => $ifsc_code]);
    }


    public function fetchIfscCompanyLegal(Request $request)
    {
        $bid = $request->post('bid');
        $ifsc_code = BankBranch::where('id', $bid)->pluck('ifsc_code')->first();
        return response()->json(['ifsc_code' => $ifsc_code]);
    }
  

    // // Fetch Designation
    // public function fetchDesignation(Request $request)
    // {
    //     $data['branch_designations'] = BranchDesignation::where("branch_id", $request->branch_id)->with('designations')
    //         ->get(["designation_name", "id"]);
    //     return response()->json($data);
    // }

    public function fetchDesignation(Request $request)
    {
        $data = BranchDesignation::with('designation')->where('branch_id', $request->input('branch_id'))->get();

        return response()->json(['data' => $data]);
    }


    // Fetch Branch and Designation branch Transfer wise
    public function fetchBranchDesignation(Request $request)
    {
        $data = BranchTransfer::with('designation')->where('branch_id', $request->input('branch_id'))->get();
        
        return response()->json(['data' => $data]);
    }

    public function fetchDesignationEmployee(Request $request)
    {
        $data = BranchTransfer::with('employee')->where('designation_id', $request->input('designation_id'))->get();
        return response()->json(['data' => $data]);
    }

    public function fetchBranchEmployee(Request $request)
    {
        $data = BranchTransfer::with('employee')->where('branch_id', $request->input('branch_id'))->get();
        return response()->json(['data' => $data]);
    }

    // fetch all data from branch_designation
    public function fetchDesignationData(Request $request)
    {
        $data = BranchDesignation::with('designation')->where('branch_id', $request->input('branch_id'))->where('designation_id', $request->input('designation_id'))->first();

        return response()->json(['data' => $data]);
    }
}
