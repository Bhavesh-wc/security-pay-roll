<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\BranchBillsFormRequest;
use App\Models\Branch;
use App\Models\BranchBill;
use Exception;
use Illuminate\Support\Facades\Auth;

class BranchBillsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the branch bills.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $branchBills = BranchBill::with('company','branch')->paginate(25);

        return view('branch_bills.index', compact('branchBills'));
    }

    /**
     * Show the form for creating a new branch bill.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $branches = Branch::pluck('name','id')->all();
        
        return view('branch_bills.create', compact('branches'));
    }

    /**
     * Store a new branch bill in the storage.
     *
     * @param App\Http\Requests\BranchBillsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(BranchBillsFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            BranchBill::create($data);
            
            return redirect()->route('branch_bills.branch_bill.index')
                ->with('success_message', 'Branch Bill was successfully added.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified branch bill.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $branchBill = BranchBill::with('branch')->findOrFail($id);

        return view('branch_bills.show', compact('branchBill'));
    }

    /**
     * Show the form for editing the specified branch bill.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $branchBill = BranchBill::findOrFail($id);
        $branches = Branch::pluck('name','id')->all();

        return view('branch_bills.edit', compact('branchBill','branches'));
    }

    /**
     * Update the specified branch bill in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\BranchBillsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, BranchBillsFormRequest $request)
    {
        try {
            
            $data = $request->getData();
            
            $branchBill = BranchBill::findOrFail($id);
            $branchBill->update($data);

            return redirect()->route('branch_bills.branch_bill.index')
                ->with('success_message', 'Branch Bill was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }        
    }

    /**
     * Remove the specified branch bill from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $branchBill = BranchBill::findOrFail($id);
            $branchBill->delete();

            return redirect()->route('branch_bills.branch_bill.index')
                ->with('success_message', 'Branch Bill was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }



}
