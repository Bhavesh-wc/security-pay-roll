<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\QualificationsFormRequest;
use App\Models\Qualification;
use Auth;
use Exception;

class QualificationsController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the qualifications.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $qualifications = Qualification::orderBy('created_at', 'desc')->with('company')->get();

        return view('qualifications.index', compact('qualifications'));
    }

    /**
     * Show the form for creating a new qualification.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


        return view('qualifications.create');
    }

    /**
     * Store a new qualification in the storage.
     *
     * @param App\Http\Requests\QualificationsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(QualificationsFormRequest $request)
    {
        try {
            if ($request['hidden']) {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                $data = Qualification::create($data);
                // return redirect()->back();
                return response()->json($data);
            } else {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                Qualification::create($data);
                return redirect()->route('qualifications.qualification.index')
                    ->with('success_message', 'Qualification was successfully added.');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified qualification.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $qualification = Qualification::findOrFail($id);

        return view('qualifications.show', compact('qualification'));
    }

    /**
     * Show the form for editing the specified qualification.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $qualification = Qualification::findOrFail($id);


        return view('qualifications.edit', compact('qualification'));
    }

    /**
     * Update the specified qualification in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\QualificationsFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, QualificationsFormRequest $request)
    {
        try {

            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $qualification = Qualification::findOrFail($id);
            $qualification->update($data);

            return redirect()->route('qualifications.qualification.index')
                ->with('success_message', 'Qualification was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified qualification from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $qualification = Qualification::findOrFail($id);
            if ($qualification->employees()->exists()) {
                throw new Exception("This qualification has employee and cannot be deleted.");
            }
            $qualification->delete();

            return redirect()->route('qualifications.qualification.index')
                ->with('success_message', 'Qualification was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
