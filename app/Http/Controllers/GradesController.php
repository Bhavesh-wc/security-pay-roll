<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\GradesFormRequest;
use App\Models\Grade;
use Auth;
use Exception;

class GradesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Display a listing of the grades.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $grades = Grade::orderBy('created_at', 'desc')->with('company')->get();

        return view('grades.index', compact('grades'));
    }

    /**
     * Show the form for creating a new grade.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {


        return view('grades.create');
    }

    /**
     * Store a new grade in the storage.
     *
     * @param App\Http\Requests\GradesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function store(GradesFormRequest $request)
    {
        try {
            if ($request['hidden']) {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                $data = Grade::create($data);
                // return redirect()->back();
                return response()->json($data);
            } else {
                $data = $request->getData();
                $data['company_id'] = Auth::user()->id;
                Grade::create($data);
                return redirect()->route('grades.grade.index')
                    ->with('success_message', 'Grade was successfully added.');
            }
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Display the specified grade.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $grade = Grade::findOrFail($id);

        return view('grades.show', compact('grade'));
    }

    /**
     * Show the form for editing the specified grade.
     *
     * @param int $id
     *
     * @return \Illuminate\View\View
     */
    public function edit($id)
    {
        $grade = Grade::findOrFail($id);


        return view('grades.edit', compact('grade'));
    }

    /**
     * Update the specified grade in the storage.
     *
     * @param int $id
     * @param App\Http\Requests\GradesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function update($id, GradesFormRequest $request)
    {
        try {

            $data = $request->getData();
            $data['company_id'] = Auth::user()->id;
            $grade = Grade::findOrFail($id);
            $grade->update($data);

            return redirect()->route('grades.grade.index')
                ->with('success_message', 'Grade was successfully updated.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

    /**
     * Remove the specified grade from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $grade = Grade::findOrFail($id);
            if ($grade->employees()->exists()) {
                throw new Exception("This grade has employee and cannot be deleted.");
            }
            $grade->delete();

            return redirect()->route('grades.grade.index')
                ->with('success_message', 'Grade was successfully deleted.');
        } catch (Exception $e) {
            return back()->withInput()->withErrors([$e->getMessage()]);
        }
    }
}
