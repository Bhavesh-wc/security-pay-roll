<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Http\Requests\AttendancesFormRequest;
use App\Models\Attendance;
use App\Models\Branch;
use App\Models\Designation;
use App\Models\Employee;
use App\Models\BranchTransfer;
use Carbon\Carbon;
use Auth;
use Exception;

class AttendancesController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
	public function __construct()
	{
	    $this->middleware('auth');
	}
	
    /**
     * Display a listing of the attendances.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        $attendances = Attendance::orderBy('created_at', 'desc')->with('company','branch','designation','employee')->get();

        return view('attendances.index', compact('attendances'));
    }

    /**
     * Show the form for creating a new attendance.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $branches = Branch::pluck('name','id')->all();
        $designations = Designation::pluck('designation_name','id')->all();
        $employees = Employee::pluck('full_name','id')->all();

        return view('attendances.create', compact('branches','designations','employees'));
    }

    /**
     * Store a new attendance in the storage.
     *
     * @param App\Http\Requests\AttendancesFormRequest $request
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    // public function store(AttendancesFormRequest $request)
    // {   
    //     try {      
    //         $month = $request['monthyear'];
    //         $date = Carbon::createFromFormat('Y-m',$month);
    //         $formattedDate = $date->format('Y-M');

       
    //         $data = $request->getData();
    //         $data['company_id'] = Auth::user()->id;
    //         $data['monthyear'] = $formattedDate;
    //         Attendance::create($data);
            
    //         return redirect()->route('attendances.attendance.index')
    //             ->with('success_message', 'Attendance was successfully added.');
    //     } catch (Exception $exception) {

    //         return back()->withInput()
    //             ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
    //     }
    // }

      
    public function store(AttendancesFormRequest $request)
   {   
    try { 
        $branch_id = $request['branch_id'];     
        $monthYear = $request['monthyear'];
        $date = Carbon::createFromFormat('Y-m', $monthYear);
        $formattedDate = $date->format('Y-M');
        $employeeId = $request['employee_id'];
        $attendances = Attendance::all();
         
        // Check if attendance record already exists
        $existingRecord = Attendance::where('monthyear', $formattedDate)
            ->where('employee_id', $employeeId)
            ->where('branch_id', $branch_id)
            ->first();
        
        if ($existingRecord) {
            return back()->withInput()
                ->withErrors(['attendance_exists' => 'Attendance record already exists for the selected month and branch for this employee.']);
        }

        $data = $request->getData();
        $data['company_id'] = Auth::user()->id;
        $data['monthyear'] = $formattedDate;
        Attendance::create($data);

        return redirect()->route('attendances.attendance.index')
            ->with('success_message', 'Attendance was successfully added.');
    } catch (Exception $exception) {
        return back()->withInput()
            ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
    }
}



//     /**
//      * Display the specified attendance.
//      *
//      * @param int $id
//      *
//      * @return \Illuminate\View\View
//      */
//     public function show($id)
//     {
//         $attendance = Attendance::with('branch','designation','employee')->findOrFail($id);

//         return view('attendances.show', compact('attendance'));
//     }

//     /**
//      * Show the form for editing the specified attendance.
//      *
//      * @param int $id
//      *
//      * @return \Illuminate\View\View
//      */
//     public function edit($id)
//     {
//         $attendance = Attendance::findOrFail($id);
//         $branches = Branch::pluck('name','id')->all();
//         $designations = Designation::pluck('designation_name','id')->all();
//         $employees = Employee::pluck('full_name','id')->all();

//         return view('attendances.edit', compact('attendance','branches','designations','employees'));
//     }

//     /**
//      * Update the specified attendance in the storage.
//      *
//      * @param int $id
//      * @param App\Http\Requests\AttendancesFormRequest $request
//      *
//      * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
//      */
//     public function update($id, AttendancesFormRequest $request)
//     {
//         try {
            
//             $data = $request->getData();
            
//             $attendance = Attendance::findOrFail($id);
//             $attendance->update($data);

//             return redirect()->route('attendances.attendance.index')
//                 ->with('success_message', 'Attendance was successfully updated.');
//         } catch (Exception $exception) {

//             return back()->withInput()
//                 ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
//         }        
//     }

    /**
     * Remove the specified attendance from the storage.
     *
     * @param int $id
     *
     * @return \Illuminate\Http\RedirectResponse | \Illuminate\Routing\Redirector
     */
    public function destroy($id)
    {
        try {
            $attendance = Attendance::findOrFail($id);
            $attendance->delete();

            return redirect()->route('attendances.attendance.index')
                ->with('success_message', 'Attendance was successfully deleted.');
        } catch (Exception $exception) {

            return back()->withInput()
                ->withErrors(['unexpected_error' => 'Unexpected error occurred while trying to process your request.']);
        }
    }

}
