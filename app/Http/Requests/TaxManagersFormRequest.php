<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Carbon\Carbon;

class TaxManagersFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'monthyear',
            'employee_pf' => 'required',
            'employer_pf' => 'required',
            'employee_admin' => 'required',
            'employer_fpf' => 'required',
            'employer_edli' => 'required',
            'employer_inepc' => 'required',
            'employee_esic' => 'required',
            'employer_esic' => 'required',
            'professional_tax' => 'required',
            'min_salary' => 'required',
            'welfare_fund' => 'required',
            'status' => 'required',
        ];
        return $rules;

    }
  
    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['monthyear', 'employee_pf','employer_pf','employee_admin','employer_fpf', 'employer_edli','employee_esic','employer_esic','employer_inepc','min_salary','professional_tax', 'welfare_fund', 'status']);


        return $data;
    }

}