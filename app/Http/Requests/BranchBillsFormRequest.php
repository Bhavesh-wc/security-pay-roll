<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class BranchBillsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'monthyear' => 'required|string|min:1',
            'branch_id' => 'required',
            'net_bill_amt' => 'numeric|nullable',
            'bill_amount' => 'numeric|nullable',
            'service_tax' => 'numeric|nullable',
            'gross_amount' => 'numeric|nullable',
            'service_charge' => 'numeric|nullable',
            'pf_amt_12' => 'numeric|nullable',
            'esi_amt' => 'numeric|nullable',
            'leave_amt' => 'numeric|nullable',
            'bonus_amt' => 'numeric|nullable',
            'recover_payment' => 'numeric|nullable',
            'professional_tax' => 'numeric|nullable',
            'other_amt' => 'numeric|nullable',
            'extra_exp' => 'numeric|nullable',
            'tds' => 'numeric|nullable',
            'net_amt' => 'numeric|nullable',
            'gross_salary' => 'numeric|nullable',
            'payroll_gross' => 'numeric|nullable',
            'diff_amt' => 'numeric|nullable',
            'salary_paid' => 'numeric|nullable',
            'profit' => 'numeric|nullable',
            'net_profit' => 'numeric|nullable',
        ];

        return $rules;
    }
    
    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['monthyear', 'branch_id', 'net_bill_amt', 'bill_amount', 'service_tax', 'gross_amount', 'service_charge', 'pf_amt_12', 'esi_amt', 'leave_amt', 'bonus_amt', 'recover_payment', 'professional_tax', 'other_amt', 'extra_exp', 'tds', 'net_amt', 'gross_salary', 'payroll_gross', 'diff_amt', 'salary_paid', 'profit', 'net_profit']);

        return $data;
    }

}