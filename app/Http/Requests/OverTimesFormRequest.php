<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class OverTimesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'date_overtime_paid' => 'date_format:Y-m-d|required',
            'remarks' => 'required',
        ];
        
        if ($this->route()->getAction()['as'] == 'over_times.over_time.store') {
            $rules = array_merge($rules,['monthyear' => 'required',
            'branch_id' => 'required',
            'employee_id' => 'required']);
        }
        
        return $rules;
    }

    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['monthyear', 'branch_id', 'employee_id', 'total_overtime_amount', 'total_overtime_days', 'date_overtime_paid', 'remarks']);

        $monthyear = explode('-', $data['monthyear']);

        $data['year'] = $monthyear[0];
        $data['month'] = $monthyear[1];

        return $data;
    }
}
