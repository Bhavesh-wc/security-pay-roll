<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class CompanyBanksFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'bank_name' => 'required|string|min:1',
            'account_no' => 'numeric|digits_between:12,16|required',
            'bank_branch_name' => 'required|string|min:1',
            'ifsc_code' => 'required|string|min:1',
            'address' => 'required',
            'status' => 'required',
        ];

        return $rules;
    }
    
    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['bank_name', 'account_no', 'bank_branch_name', 'ifsc_code', 'address', 'status']);

        return $data;
    }

}