<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class SalariesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'branch_id' => 'required',
            'designation_id' => 'nullable',
            'department_id' => 'nullable',
            'grade_id' => 'nullable',
            'employee_id' => 'required',
            'monthyear' => 'nullable|string|min:0',
            'month' => 'nullable|string|min:0',
            'year' => 'nullable|string|min:0',
            'salary_date' => 'date_format:Y-m-d|date_format:dd/mm/yyy',
            'working_days' => 'numeric|nullable',
            'no_of_days_work_done' => 'numeric|nullable',
            'no_of_leave' => 'numeric|nullable',
            'basic_da' => 'numeric|min:-99999999.99|max:99999999.99',
            'extra_allowance' => 'numeric|min:-99999999.99|max:99999999.99',
            'hra' => 'numeric|min:-99999999.99|max:99999999.99',
            'bonus' => 'numeric|min:-99999999.99|max:99999999.99',
            'fix_salary' => 'numeric|min:-99999999.99|max:99999999.99',
            'sal_advance' => 'numeric|min:-99999999.99|max:99999999.99',
            'a' => 'numeric|min:-99999999.99|max:99999999.99|nullable',
            'b' => 'numeric|min:-99999999.99|max:99999999.99|nullable',
            'c' => 'numeric|min:-99999999.99|max:99999999.99|nullable',
            'ot_amount' => 'numeric|min:-99999999.99|max:99999999.99',
            'ot_days' => 'numeric|nullable',
            'attendance_allowance' => 'numeric|min:-99999999.99|max:99999999.99',
            'dress' => 'numeric|min:-99999999.99|max:99999999.99',
            'mess' => 'numeric|min:-99999999.99|max:99999999.99',
            'pf' => 'numeric|min:-99999999.99|max:99999999.99',
            'profetional_tax' => 'numeric|min:-99999999.99|max:99999999.99',
            'basis_wages' => 'numeric|min:-99999999.99|max:99999999.99',
            'esic' => 'numeric|min:-99999999.99|max:99999999.99',
            'walfare_amount' => 'numeric|min:-99999999.99|max:99999999.99',
            'damage_amount' => 'numeric|min:-99999999.99|max:99999999.99',
            'fines' => 'numeric|min:-99999999.99|max:99999999.99',
            'total' => 'numeric|min:-99999999.99|max:99999999.99',
            'nettotal' => 'string|min:1|numeric|max:99999999.99',
            'branch_ot_amount' => 'string|min:1|numeric|max:99999999.99',
        ];

        return $rules;
    }
    
    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['branch_id', 'designation_id', 'department_id', 'grade_id', 'employee_id', 'monthyear', 'month', 'year', 'salary_date', 'working_days', 'no_of_days_work_done', 'no_of_leave', 'basic_da', 'extra_allowance', 'hra', 'bonus', 'fix_salary', 'sal_advance', 'a', 'b', 'c', 'ot_amount', 'ot_days', 'attendance_allowance', 'dress', 'mess', 'pf', 'profetional_tax', 'basis_wages', 'esic', 'walfare_amount', 'damage_amount', 'fines', 'total', 'nettotal', 'branch_ot_amount']);

        return $data;
        
    }

}