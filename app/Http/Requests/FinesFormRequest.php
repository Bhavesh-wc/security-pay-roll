<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class FinesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'act_for_which_fine_impossed' => 'required',
            'date_of_offence' => 'date_format:Y-m-d|required',
            'reasone_against_fine' => 'required',
            'name_of_person_explanation_heard' => 'required',
            'wages_payable' => 'required',
            'fine_amount' => 'required',
            'fine_released_date' => 'date_format:Y-m-d|required',
            'fine_released_amount' => 'required',
            'remarks' => 'required',
        ];

        if ($this->route()->getAction()['as'] == 'fines.fine.store') {
            $rules = array_merge($rules,['monthyear' => 'required',
            'branch_id' => 'required',
            'employee_id' => 'required']);
        }

        return $rules;
    }

    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['monthyear', 'branch_id', 'employee_id', 'act_for_which_fine_impossed', 'date_of_offence', 'reasone_against_fine', 'name_of_person_explanation_heard', 'wages_payable', 'fine_amount', 'fine_released_date', 'fine_released_amount', 'remarks']);

        $monthyear = explode('-', $data['monthyear']);

        $data['year'] = $monthyear[0];
        $data['month'] = $monthyear[1];

        return $data;
    }
}
