<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class AttendancesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'branch_id' => 'required',
            'designation_id' => 'required',
            'employee_id' => 'required',
            'monthyear' => 'required|string|min:1',
            'month' => 'nullable|string|min:0',
            'year' => 'nullable|string|min:0',
            'day_1' => 'nullable',
            'day_2' => 'nullable',
            'day_3' => 'nullable',
            'day_4' => 'nullable',
            'day_5' => 'nullable',
            'day_6' => 'nullable',
            'day_7' => 'nullable',
            'day_8' => 'nullable',
            'day_9' => 'nullable',
            'day_10' => 'nullable',
            'day_11' => 'nullable',
            'day_12' => 'nullable',
            'day_13' => 'nullable',
            'day_14' => 'nullable',
            'day_15' => 'nullable',
            'day_16' => 'nullable',
            'day_17' => 'nullable',
            'day_18' => 'nullable',
            'day_19' => 'nullable',
            'day_20' => 'nullable',
            'day_21' => 'nullable',
            'day_22' => 'nullable',
            'day_23' => 'nullable',
            'day_24' => 'nullable',
            'day_25' => 'nullable',
            'day_26' => 'nullable',
            'day_27' => 'nullable',
            'day_28' => 'nullable',
            'day_29' => 'nullable',
            'day_30' => 'nullable',
            'day_31' => 'nullable',
            'total' => 'numeric|nullable',
            'total_present' => 'numeric|nullable',
            'total_absent' => 'numeric|nullable',
            'total_holiday' => 'numeric|nullable',
        ];

        return $rules;
    }
    
    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['branch_id', 'designation_id','company_id','employee_id', 'monthyear', 'month', 'year', 'day_1', 'day_2', 'day_3', 'day_4', 'day_5', 'day_6', 'day_7', 'day_8', 'day_9', 'day_10', 'day_11', 'day_12', 'day_13', 'day_14', 'day_15', 'day_16', 'day_17', 'day_18', 'day_19', 'day_20', 'day_21', 'day_22', 'day_23', 'day_24', 'day_25', 'day_26', 'day_27', 'day_28', 'day_29', 'day_30', 'day_31', 'total', 'total_present', 'total_absent', 'total_holiday']);

        $monthyear = explode('-', $data['monthyear']);

        $data['year'] = $monthyear[0];
        $data['month'] = $monthyear[1];

        $month = $data['month']; // April
        $year = $data['year']; // 2023
        $daysInMonth = date('t', strtotime($year . '-' . $month . '-01'));
        $totalDays = 0;
        $presentDays = 0;
        $absentDays = 0;
        $holidayDays = 0;

        for ($i = 1; $i <= $daysInMonth; $i++)
        {
            $attendance = $data["day_$i"];
            
            if ($attendance == 'P'){
                $presentDays++;
            }
            else if ($attendance == 'A'){
                $absentDays++;
            }
            else if ($attendance == 'H'){
                $holidayDays++;
            }

            $totalDays++;
        }

        $data['total'] = $totalDays;
        $data['total_present'] = $presentDays;
        $data['total_absent'] = $absentDays;
        $data['total_holiday'] = $holidayDays;

    //     $total_present = 0;
    //     $total_absent = 0;
    //     $total_holiday = 0;
    //     $total = 0;
        
    //     for ($day = 1; $day <= 30; $day++) {
    //         if ($data["day_$day"] == "P") {
    //             $total_present++;
    //         } elseif ($data["day_$day"] == "A") {
    //             $total_absent++;
    //         } else {
    //             $total_holiday++;
    //         }
    //        $total++; 
    //     }

        // $data['total'] = $total;
        // $data['total_present'] = $total_present;
        // $data['total_absent'] = $total_absent;
        // $data['total_holiday'] = $total_holiday;

        return $data;
    }

}