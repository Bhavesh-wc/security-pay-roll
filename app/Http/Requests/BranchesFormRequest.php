<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class BranchesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'address' => 'required',
            'over_time_amount' => 'required|numeric',
            'monthyear' => 'required',
            'status' => 'required',
        ];

        return $rules;
    }
    
    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['name', 'address', 'over_time_amount', 'monthyear', 'month', 'year', 'status']);
        
        $monthyear = explode('-',$data['monthyear']);

        $data['year'] = $monthyear[0];
        $data['month'] = $monthyear[1];

        return $data;
    }

}