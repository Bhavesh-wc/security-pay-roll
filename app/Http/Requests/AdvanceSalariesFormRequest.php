<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class AdvanceSalariesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'wages_payable' => 'required|numeric',
            'date_of_advance' => 'date_format:Y-m-d|required',
            'purpose_of_advance' => 'required',
            'remarks' => 'required',
            'advance_for' => 'required',
            'amount' => 'required'
        ];

        if ($this->route()->getAction()['as'] == 'advance_salaries.advance_salary.store') {
            $rules = array_merge($rules, [
                'monthyear' => 'required',
                'branch_id' => 'required',
                'employee_id' => 'required'
            ]);
        }

        return $rules;
    }

    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['monthyear', 'branch_id', 'employee_id', 'wages_payable', 'date_of_advance', 'amount', 'purpose_of_advance', 'remarks', 'stock_category_id', 'no_of_qty', 'advance_for']);

        $monthyear = explode('-', $data['monthyear']);

        $data['year'] = $monthyear[0];
        $data['month'] = $monthyear[1];

        return $data;
    }
}
