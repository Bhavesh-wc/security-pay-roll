<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class BankBranchesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'bank_branch_name' => 'required',
            'ifsc_code' => 'required',
            'address' => 'required',
            'bank_id' => 'required',
            'status' => 'required',
        ];

        return $rules;
    }

    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['bank_branch_name', 'ifsc_code', 'address', 'bank_id', 'status']);

        return $data;
    }
}
