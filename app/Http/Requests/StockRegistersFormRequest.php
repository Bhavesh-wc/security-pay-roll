<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class StockRegistersFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'stock_category_id' => 'required',
            'stock_quantity' => 'required|string|min:1',
            'stock_in_date' => 'date_format:Y-m-d|required',
            
        ];
        return $rules;
    }
    
    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['stock_category_id', 'stock_quantity', 'stock_in_date']);

        return $data;
    }

}