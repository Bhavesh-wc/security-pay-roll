<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class BranchTransfersFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'branch_id' => 'required',
            'employee_id' => 'required',
            'designation_id' => 'required',
            'department_id' => 'required|nullable',
            'grade_id' => 'required|nullable',
            'created_date' => 'date_format:Y-m-d|required',
            'updated_month' => 'required|nullable|string|min:1',
            'basic_fix' => 'required',
            'basic_da' => 'numeric|nullable|string|min:0',
            'extra_allowance' => 'numeric|nullable',
            'hra' => 'numeric|nullable',
            'bonus' => 'numeric|nullable',
            'fix_salary' => 'numeric|nullable',
            'monthyear' => 'nullable|string|min:0',
            'month' => 'nullable|string|min:0',
            'year' => 'nullable|string|min:0',
        ];

        return $rules;
    }
    
    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['branch_id', 'company_id', 'employee_id', 'designation_id', 'department_id', 'grade_id', 'created_date', 'updated_month', 'basic_fix', 'basic_da', 'extra_allowance', 'hra', 'bonus', 'fix_salary', 'monthyear', 'month', 'year']);

        $data['monthyear'] = $data['updated_month'];

        $monthyear = explode('-', $data['monthyear']);

        $data['year'] = $monthyear[0];
        $data['month'] = $monthyear[1];

        return $data;
    }

}