<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class UsersFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'name' => 'required',
            'company_code' => 'required',
            'phone_no' => 'required|string|min:10|max:10',
            'profile_photo' => ['file'],
            'email' => ['nullable'],
            'password' => ['nullable'],
        ];
        
        if ($this->route()->getAction()['as'] == 'users.user.store') {
            array_push($rules['email'], ['email','required','unique:users']);
            array_push($rules['password'], ['required','confirmed']);
        }
        
        return $rules;
    }
    
    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['name', 'company_code', 'email', 'phone_no', 'contact_person_name', 'contact_person_phone_no', 'address', 'gst_no', 'pf_no', 'esic', 'police_registration_number', 'shop_and_establishment', 'professional_tax', 'msme_registaration_no', 'welfare_fund', 'password']);
        if ($this->has('custom_delete_profile_photo')) {
            $data['profile_photo'] = null;
        }
        if ($this->hasFile('profile_photo')) {
            $data['profile_photo'] = $this->moveFile($this->file('profile_photo'));
        }

        return $data;
    }
  
    /**
     * Moves the attached file to the server.
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return string
     */
    protected function moveFile($file)
    {
        if (!$file->isValid()) {
            return '';
        }
        
        $path = config('laravel-code-generator.files_upload_path', 'uploads');
        $saved = $file->store('public/' . $path, config('filesystems.default'));

        return substr($saved, 7);
    }

}