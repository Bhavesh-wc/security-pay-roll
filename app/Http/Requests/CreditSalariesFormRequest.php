<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Illuminate\Support\Carbon;
use App\Models\Salary;

class CreditSalariesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'monthyear' => 'nullable|string',
            'branch_id' => 'required',
            'cheque_no' => 'nullable|string',
            'ac_no' => 'nullable|string',
            'bank_id' => 'required',
            'bank_branch_name' => 'required',
        ];

        return $rules;
    }

    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['monthyear', 'branch_id', 'cheque_no', 'ac_no', 'amount_credit', 'bank_id', 'bank_branch_id', 'credit_date', 'company_id']);

        $month = $data['monthyear'];
        $branch_id = $data['branch_id'];
        $date = Carbon::createFromFormat('Y-m', $month);
        $monthName = $date->format('Y-M');

        $salaries = Salary::where('branch_id', $branch_id)
            ->where('monthyear', $monthName)
            ->get();
        $netTotal = 0;
        foreach ($salaries as $salary) {
            $netTotal += $salary->nettotal;
        }
        $data['amount_credit'] = $netTotal;
        $data['credit_date'] = now();
        return $data;
    }
}
