<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;
use Carbon\Carbon;

class EmployeesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'police_varification_date' => 'date_format:Y-m-d|nullable',
            'birth_date_1' => 'date_format:Y-m-d|nullable',
            'bank_ac_no' => 'numeric|digits_between:12,16|nullable',
            'interview_date' => 'date_format:Y-m-d|nullable',
            'entry_date' => 'date_format:Y-m-d',
            'relieve_date' => 'date_format:Y-m-d|nullable',
            'final_relieve_date' => 'date_format:Y-m-d|nullable',
            'due_date' => 'date_format:Y-m-d|nullable',
            'refund_date' => 'date_format:Y-m-d|nullable',
            'probation_date' => 'date_format:Y-m-d|nullable',
            'confirmation_date' => 'date_format:Y-m-d|nullable',
            'police_varification' => ['file'],
            'leaving_certificate' => ['file'],
            'bank_passbook' => ['file'],
            'photo' => ['file'],
            'document' => ['file'],
            'signature' => ['file'],
            'monthyear' => 'nullable|string|min:0',
            'month' => 'nullable|string|min:0',
            'year' => 'nullable|string|min:0',
        ];

        if ($this->route()->getAction()['as'] == 'employees.employee.store') {
            $rules = array_merge($rules, [
                'registartion_date' => 'date_format:Y-m-d|required',
                'branch_id' => 'required',
                'designation_id' => 'required',
                'department_id' => 'required',
                'grade_id' => 'required',
                'first_name' => 'required',
                'middle_name' => 'required',
                'last_name' => 'required',
                'full_name' => 'required',
                'date_of_birth' => 'date_format:Y-m-d|required',
                'country_id' => 'required',
                'state_id' => 'required',
                'district_id' => 'required',
                'city_id' => 'required',
                'pincode_id' => 'required',
                'mother_tongue_id' => 'required',
                'religion' => 'required',
                'marital_status' => 'required',
                'sex' => 'required',
                'local_address' => 'required',
                'permanent_address' => 'required',
                'permanent_phone_no' => 'required',
            ]);
        }

        return $rules;
    }

    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {

        if ($this->route()->getAction()['as'] == 'employees.employee.store' || $this->route()->getAction()['as'] == 'employees.employee.update') {
            $data = $this->only(['registartion_date', 'branch_id', 'designation_id', 'department_id', 'grade_id', 'first_name', 'middle_name', 'last_name', 'full_name', 'date_of_birth', 'country_id', 'state_id', 'district_id', 'city_id', 'pincode_id', 'father_name', 'mother_name', 'spouse_name', 'visible_distinguishing_mark_1', 'visible_distinguishing_mark_2', 'birth_place', 'mother_tongue_id', 'religion', 'marital_status', 'height', 'weight', 'blood_group', 'sex', 'qualification_id', 'local_address', 'permanent_address', 'local_phone_no', 'permanent_phone_no', 'permanent_mobile_no', 'salary','monthyear','year','month']);  
        }

        if ($this->boolean('custom_delete_police_varification')) {
            $data['police_varification'] = null;
        }
        if ($this->hasFile('police_varification')) {
            $data['police_varification'] = $this->moveFile($this->file('police_varification'));
        }
        if ($this->boolean('custom_delete_leaving_certificate')) {
            $data['leaving_certificate'] = null;
        }
        if ($this->hasFile('leaving_certificate')) {
            $data['leaving_certificate'] = $this->moveFile($this->file('leaving_certificate'));
        }
        if ($this->boolean('custom_delete_bank_passbook')) {
            $data['bank_passbook'] = null;
        }
        if ($this->hasFile('bank_passbook')) {
            $data['bank_passbook'] = $this->moveFile($this->file('bank_passbook'));
        }
        if ($this->boolean('custom_delete_photo')) {
            $data['photo'] = null;
        }
        if ($this->hasFile('photo')) {
            $data['photo'] = $this->moveFile($this->file('photo'));
        }
        if ($this->boolean('custom_delete_document')) {
            $data['document'] = null;
        }
        if ($this->hasFile('document')) {
            $data['document'] = $this->moveFile($this->file('document'));
        }
        if ($this->boolean('custom_delete_signature')) {
            $data['signature'] = null;
        }
        if ($this->hasFile('signature')) {
            $data['signature'] = $this->moveFile($this->file('signature'));
        }
        if ($this->boolean('custom_delete_election_card')) {
            $data['election_card'] = null;
        }
        if ($this->hasFile('election_card')) {
            $data['election_card'] = $this->moveFile($this->file('election_card'));
        }
        if ($this->boolean('custom_delete_bank_form')) {
            $data['bank_form'] = null;
        }
        if ($this->hasFile('bank_form')) {
            $data['bank_form'] = $this->moveFile($this->file('bank_form'));
        }

        return $data;
    }

    public function getDataInfo()
    {
        if ($this->route()->getAction()['as'] == 'employees.employee.update') {
            $data1 = $this->only(['experience', 'experience_company', 'police_varification_date', 'police_station_name', 'citizen_of_india_by', 'black_list', 'black_list_reasone', 'election_card_no', 'driving_licenses_no', 'rto', 'rto_state_id', 'passport_no', 'pancard_no', 'aadhar_card_no', 'reference_relation_contact_1', 'reference_relation_contact_2', 'reference_relation_contact_3', 'emergency_contact_name', 'emergency_relation', 'emergency_contact_no', 'nominee_1', 'birth_date_1', 'relation_1', 'bank_id', 'bank_branch_id', 'ifsc_code', 'bank_ac_no', 'interview_date', 'entry_date', 'senior_citizen', 'relieve_date', 'final_relieve_date', 'security_deposit', 'deposit_detail', 'due_date', 'refund_date', 'refund_detail', 'probation_date', 'confirmation_date', 'monthyear', 'year', 'month','status']);
        }
        return $data1;
    }

    /**
     * Moves the attached file to the server.
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return string
     */
    protected function moveFile($file)
    {
        if (!$file->isValid()) {
            return '';
        }

        $path = config('laravel-code-generator.files_upload_path', 'uploads');
        $saved = $file->store('public/' . $path, config('filesystems.default'));

        return substr($saved, 7);
    }
}
