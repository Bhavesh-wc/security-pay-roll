<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class SystemConfigurationsFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'logo' => ['file'],
            'Favicon' => ['file'],
            'title' => 'required',
            'address' => 'required',
            'mobile' => 'required',
            'email' => 'required|email',
            'footer' => 'required',
        ];
        
        if ($this->route()->getAction()['as'] == 'system_configurations.systemconfiguration.store' || $this->has('custom_delete_logo')) {
            array_push($rules['logo'], 'required');
        }
        if ($this->route()->getAction()['as'] == 'system_configurations.systemconfiguration.store' || $this->has('custom_delete_Favicon')) {
            array_push($rules['Favicon'], 'required');
        }
        return $rules;
    }
    
    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['title', 'address', 'mobile', 'email', 'footer']);
        if ($this->has('custom_delete_logo')) {
            $data['logo'] = '';
        }
        if ($this->hasFile('logo')) {
            $data['logo'] = $this->moveFile($this->file('logo'));
        }
        if ($this->has('custom_delete_Favicon')) {
            $data['Favicon'] = '';
        }
        if ($this->hasFile('Favicon')) {
            $data['Favicon'] = $this->moveFile($this->file('Favicon'));
        }

        return $data;
    }
  
    /**
     * Moves the attached file to the server.
     *
     * @param \Symfony\Component\HttpFoundation\File\UploadedFile $file
     *
     * @return string
     */
    protected function moveFile($file)
    {
        if (!$file->isValid()) {
            return '';
        }
        
        $path = config('laravel-code-generator.files_upload_path', 'uploads');
        $saved = $file->store('public/' . $path, config('filesystems.default'));

        return substr($saved, 7);
    }

}