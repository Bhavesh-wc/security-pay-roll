<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Auth;

class DamagesFormRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'damage_details' => 'required',
            'damage_date' => 'date_format:Y-m-d|required',
            'damage_amount' => 'required',
            'employee_reason_for_deduction' => 'required',
            'name_of_person_explanation_heard' => 'required',
            'total_installments' => 'required|numeric',
            'installment_date' => 'date_format:Y-m-d|required',
            'installment_amount' => 'required',
            'remarks' => 'required',
        ];

        if ($this->route()->getAction()['as'] == 'damages.damage.store') {
            $rules = array_merge($rules,['monthyear' => 'required',
            'branch_id' => 'required',
            'employee_id' => 'required']);
        }

        return $rules;
    }

    /**
     * Get the request's data from the request.
     *
     * 
     * @return array
     */
    public function getData()
    {
        $data = $this->only(['monthyear', 'branch_id', 'employee_id', 'damage_details', 'damage_date', 'damage_amount', 'employee_reason_for_deduction', 'name_of_person_explanation_heard', 'total_installments', 'installment_date', 'installment_amount', 'remarks']);

        $monthyear = explode('-', $data['monthyear']);

        $data['year'] = $monthyear[0];
        $data['month'] = $monthyear[1];

        return $data;
    }
}
