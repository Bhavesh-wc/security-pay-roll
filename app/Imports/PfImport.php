<?php

namespace App\Imports;

use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Employee;
use Maatwebsite\Excel\Facades\Excel;
use Auth;

class PfImport implements ToModel, WithHeadingRow
{
    public function model(array $row)
    {
        try {
            $employee_id = $row['employee_code'];
            $pf_ac_code = $row['pf_ac_code'];
            $pf_uan_code = $row['pf_uan_code'];
            $employee = Employee::where('id', $employee_id)->first();
            if ($employee) {
                $employee->pf_ac_code = $pf_ac_code;
                $employee->pf_uan_code = $pf_uan_code;
                $employee->save();
            } else{
                throw new \Exception("Employee Data Not Available: {$employee_id}" );
            }
        } catch (\Exception $e) {
            // Log the error or return an error message
            \Log::error('Attendance import error: ' . $e->getMessage());
            return null;
        }
    }
}
