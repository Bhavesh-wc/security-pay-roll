<?php

namespace App\Imports;

use App\Models\Salary;
use Maatwebsite\Excel\Concerns\ToModel;
use App\Models\Designation;
use App\Models\Walfer;
use App\Models\BranchDesignation;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use Auth;
use App\Models\Branch;
use App\Models\Employee;
use App\Models\Fine;
use App\Models\Attendance;
use App\Models\TaxManager;
use App\Models\BranchTransfer;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromQuery;
use App\Models\OverTime;
use App\Models\Damage;
use App\Models\AdvanceSalary;

class SalaryImport implements ToModel, WithHeadingRow
{
    private $branch_id;

    public function __construct($branch_id)
    {
        $this->branch_id = $branch_id;
    }

    public function model(array $row)
    {
    
        try {

        //     store tax based on tax manager
            $employee_id = $row['employee_code'];
            $employee = Employee::where('id', $employee_id)->first();
            $monthyear = $row['monthyear'];
            $date = Carbon::createFromFormat('Y-M', $monthyear)->format('Y-m');
            $taxManager = TaxManager::where('monthyear', $date)->first();
            if (!is_null($taxManager)) {
            $esicValue=$taxManager['employee_esic'];
            $pfValue=$taxManager['employee_pf'];
            $walfer=$taxManager['welfare_fund'];
            $professionalTax = $taxManager['professional_tax'];
           
            // find branch overtime amount
            $branchovertimeAmount = Branch::where('id', $this->branch_id)->value('over_time_amount');
            if (!is_null($employee)) {
                
                // Initialize a variable to track whether an attendance record was found for the given employee and monthyear
                $branchtansfer = BranchTransfer::where('employee_id', $employee_id)->where('branch_id', $this->branch_id)->where('transfer', '=', 'current')->first();
              
                if (!is_null($branchtansfer)) {

                    // calculate basic and fix salary
                    $branch_designation = BranchDesignation::where('branch_id', $this->branch_id)->where('designation_id', $branchtansfer->designation_id)->get();
                    if ($branch_designation[0]['basic_fix'] == "basic") {
                        $basic_da  =  $branch_designation[0]['basic_da'];
                        $extra_allowance  =  $branch_designation[0]['extra_allowance'];
                        $hra  =  $branch_designation[0]['hra'];
                        $bonus  =  $branch_designation[0]['bonus'];
                        $fix_salary = 0;
                    } else {
                        $basic_da  = 0;
                        $extra_allowance  = 0;
                        $hra  = 0;
                        $bonus  = 0;
                        $fix_salary = $branch_designation[0]['fix_salary'];
                    }

                    $salary = Salary::where('employee_id', $employee_id)->where('branch_id', $this->branch_id)->where('designation_id', $branchtansfer->designation_id)->where('monthyear', $monthyear)->first();
                   //salary allready exist
                   if(is_null($salary)) {
                        $attandace = Attendance::where('employee_id', $employee_id)->where('branch_id', $this->branch_id)->where('designation_id', $branchtansfer->designation_id)->where('monthyear', $monthyear)->get();
                         
                        // dd($monthyear);
                        //if attendance not exist
                        if (count($attandace) > 0) {
                           // $working_day = $attandace['total'] - $attandace['total_holiday'];

                            // Count working Days
                            if (count($attandace) > 0) {
                                $total = $attandace[0]['total'];
                                $total_holiday = $attandace[0]['total_holiday'];
                                $working_day = $total - $total_holiday;
                            } else {
                                $total = 0;
                                $total_holiday = 0;
                                $working_day = $total - $total_holiday;
                            } 

                            // calculate number of working day done
                            $presents = 0;
                            $absents = 0;
                            $holidays = 0;
                            foreach ($attandace as $item) {
                                $days = collect($item->toArray())->except(['id', 'employee_id', 'designation_id', 'company_id', 'branch_id', 'monthyear', 'month', 'year', 'created_at', 'updated_at', 'deleted_at'])->toArray();
                                foreach ($days as $day) {
                                    if ($day == 'P') {
                                        $presents++;
                                    } elseif ($day == 'A') {
                                        $absents++;
                                    } elseif ($day == 'H') {
                                        $holidays++;
                                    }
                                }
                            }
                            $no_of_days_work_done  =  $working_day - $absents;

                             // Get Overtime Value
                            $overtimeAmount = $this->getOvertimeamount($employee_id, $monthyear);
                            //dd($overtimeAmount[0]->total_overtime_amount);
                            if (count($overtimeAmount) > 0) {
                                $totalOvertimeAmount = $overtimeAmount[0]->total_overtime_amount;
                                $totalOvertimeDays = $overtimeAmount[0]->total_overtime_days;
                            } else {
                                $totalOvertimeAmount = 0;
                                $totalOvertimeDays = 0;
                            } 

                            //Get Damage Value 
                            $totaldamage = $this->getDamageAmount($employee_id, $monthyear);
                            // Get Fine Value
                            $totalFine = $this->getFineAmount($employee_id, $monthyear);

                            // Advance Salary
                            $totalAdvance = $this->getAdvance($employee_id, $monthyear);

                            $branch_id =  $branchtansfer->branch_id;
                            $designation_id =  $branchtansfer->designation_id;
                            $department_id =  $branchtansfer->department_id;
                            $grade_id =  $branchtansfer->grade_id;
                            $employee_id = $employee_id;

                            // Calculate the salary data for this employee
                            $monthYear = explode('-', $row['monthyear']);

                            $row['year'] = $monthYear[0];
                            $row['month'] = $monthYear[1];

                            $month = $row['month']; // April
                            $year = $row['year']; // 2023

                            if ($month == 'Dec' || $month == 'Jun') {
                                $row['walfare_amount'] = $walfer;
                            } else {
                                $row['walfare_amount'] = 0;
                            }

                            // calculate Pf
                            $pfValue = Branch::where('id', $this->branch_id)->value('pf');
                            if ($basic_da > 0) {
                                $pf = $basic_da * $working_day;
                                $calculatepf = $pf * $pfValue / 100;
                                $esic = $pf * $esicValue / 100;
                                $basic_wages = $basic_da * $no_of_days_work_done;
                                $total =  $basic_da +  $extra_allowance + $hra + $bonus + $row['a'] + $row['b'] + $row['c'] + $totalOvertimeAmount + $row['attendance_allowance'] + $basic_wages;
                                $minusTotal =  $totalAdvance + $row['dress'] + $row['mess'] + $calculatepf + $professionalTax + $esic + $row['walfare_amount'] + $totalFine;
                                $netTotal = $total - $minusTotal;
                            } else {
                                $calculatepf = ($pfValue / 100) * $fix_salary;
                                $esic = ($esicValue / 100) * $fix_salary;
                                $basic_wages = 0;
                                $total =  $fix_salary + $row['a'] + $row['b'] + $row['c'] + $totalOvertimeAmount + $row['attendance_allowance'] + $basic_wages;
                                $minusTotal =  $totalAdvance + $row['dress'] + $row['mess'] + $calculatepf + $professionalTax + $esic + $row['walfare_amount'] + $totalFine;
                                $netTotal = $total - $minusTotal;
                            }

                            // Store the salary data in the salary table
                            $salary = new Salary;
                            $salary->branch_id = $branch_id;
                            $salary->designation_id = $designation_id;
                            $salary->department_id = $department_id;
                            $salary->grade_id = $grade_id;
                            $salary->employee_id = $employee_id;
                            $salary->monthyear = $row['monthyear'];
                            $salary->month = $row['month'];
                            $salary->year = $row['year'];
                            $salary->salary_date = now();
                            $salary->working_days = $working_day;
                            $salary->no_of_days_work_done = $attandace[0]['total_present'];
                            $salary->no_of_leave = $attandace[0]['total_absent'];
                            $salary->basic_da = $basic_da;
                            $salary->extra_allowance = $extra_allowance;
                            $salary->hra = $hra;
                            $salary->bonus = $bonus;
                            $salary->fix_salary = $fix_salary;
                            $salary->sal_advance = $totalAdvance;
                            $salary->a = $row['a'];
                            $salary->b = $row['b'];
                            $salary->c = $row['c'];
                            $salary->ot_amount = $totalOvertimeAmount;
                            $salary->ot_days = $totalOvertimeDays;
                            $salary->attendance_allowance = $row['attendance_allowance'];
                            $salary->dress = $row['dress'];
                            $salary->mess = $row['mess'];
                            $salary->pf = $calculatepf;
                            $salary->profetional_tax = $professionalTax;
                            $salary->basis_wages = $basic_wages;
                            $salary->esic = $esic;
                            $salary->walfare_amount = $row['walfare_amount'];
                            $salary->damage_amount = $totaldamage;
                            $salary->fines = $totalFine;
                            $salary->total = $total;
                            $salary->nettotal = $netTotal;
                            $salary->branch_ot_amount = $branchovertimeAmount;
                            $salary->company_id = Auth::user()->id;
                            $salary->save();

                            return $salary;
                        } else {
                            throw new \Exception("Employee Attendance Not Avilable: {$employee_id}");
                            //return redirect()->back()->with('fail', 'attandace not exist');
                        }
                    } else {
                        throw new \Exception("Employee Salary Already Exist: {$employee_id}");
                        //return redirect()->back()->with('fail', 'salary allready exist');
                    }
                } else {
                    throw new \Exception("Branch Transfer Data Not Available: {$employee_id}" );
                    //return redirect()->back()->with('fail', 'branch transfer is not found.');
                }
            } else {
                // If $employee is not set, print 'back'
                throw new \Exception("Employee Data Not Available: {$employee_id}" );
                //return redirect()->back()->with('fail', 'employee is not found.');
            }
         
        } else {
            //throw new \Exception("Branch Transfer Data Not Available: {$employee_id}" );
          
            throw new \Exception("tax manager for this month is not found.");
        } 

        } catch (\Exception $e) {
            // Log the error or return an error message
            \Log::error('Attendance import error: ' . $e->getMessage());
            return null;
        }
    }

    // GET OVERTIME
    public function getOvertimeamount($employee_id, $monthyear)
    {
        // $date = Carbon::createFromFormat('Y-M', $monthyear);
        // $monthName = $date->format('Y-m');
        
        $row = OverTime::where('employee_id', $employee_id)
            ->where('monthyear', $monthyear)
            ->get();

        return $row;
    }

    // GET DAMAGE
    public function getDamageAmount($employee_id, $monthyear)
    {
        // $date = Carbon::createFromFormat('Y-M', $monthyear);
        // $monthName = $date->format('Y-m');

        $damage = Damage::where('employee_id', $employee_id)
            ->where('monthyear', $monthyear)
            ->get();
            
        $totalDamage = 0;
        foreach ($damage as $data) {
            $totalDamage += $data->damage_amount;
        }

        return $totalDamage;
    }

    // GET FINE
    public function getFineAmount($employee_id, $monthyear)
    {
        // $date = Carbon::createFromFormat('Y-M', $monthyear);
        // $monthName = $date->format('Y-m');

        $rows = Fine::where('employee_id', $employee_id)
            ->where('monthyear', $monthyear)
            ->get();

        $totalFineAmount = 0;
        foreach ($rows as $row) {
            $totalFineAmount += $row->fine_amount;
        }

        return $totalFineAmount;
    }

    // GET ADVANCE
    public function getAdvance($employee_id, $monthyear)
    {
        // $date = Carbon::createFromFormat('Y-M', $monthyear);
        // $monthName = $date->format('Y-m');

        $rows = AdvanceSalary::where('employee_id', $employee_id)
            ->where('monthyear', $monthyear)
            ->get();
        $advanceAmount = 0;
        foreach ($rows as $row) {
            $advanceAmount += $row->amount;
        }
        return $advanceAmount;
    }
}
