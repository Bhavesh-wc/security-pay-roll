<?php

namespace App\Imports;

use App\Models\Attendance;
use Maatwebsite\Excel\Concerns\ToModel;
use Maatwebsite\Excel\Concerns\WithHeadingRow;
use App\Models\Branch;
use App\Models\Employee;
use App\Models\Salary;
use App\Models\BranchTransfer;
use Carbon\Carbon;
use Maatwebsite\Excel\Facades\Excel;
use Maatwebsite\Excel\Concerns\FromQuery;
use App\Invoice;
use Auth;

class AttendanceImport implements ToModel, WithHeadingRow
{
    private $branch_id;

    public function __construct($branch_id)
    {
        $this->branch_id = $branch_id;
    }

    public function model(array $row)
    {
        try {
        $employee_id = $row['employee_code'];
        $monthyear = $row['monthyear'];
        $attendances = Attendance::all();
        $employee = Employee::where('id', $employee_id)->first();

            if (isset($employee)) {
                // Initialize a variable to track whether an attendance record was found for the given employee and monthyear
                $employeetransfer = BranchTransfer::where('employee_id', $employee_id)->where('branch_id', $this->branch_id)->where('transfer', '=', 'current')->first();
                 // dd($employeetransfer);
                if (!empty($employeetransfer)) {
                    $attendanceFound = false;
                    foreach ($attendances as $attendance) {
                        if ($attendance->employee_id == $employee_id && $attendance->monthyear == $monthyear && $attendance->designation_id == $employeetransfer['designation_id'] && $attendance->branch_id == $this->branch_id) {
                            // If an attendance record is found, set the $attendanceFound variable to true and break out of the loop
                            $attendanceFound = true;
                            break;
                        }
                    }
                    if ($attendanceFound) {
                        // If an attendance record is found, redirect back with a message
                        throw new \Exception('Employee Attendance Already Exist.');
                        //return redirect()->back()->with('fail', 'Employee Attendance already Exist.');
                    } else {
                        
                        // If an attendance record is not found, import the attendance data and redirect back with a success message
                        $company_id =  $employee->company_id;
                        $designation_id =  $employee->designation_id;
                        $branch_id =  $this->branch_id;

                        // Calculate the attendance data for this employee
                        $monthyears = explode('-', $monthyear);
                        $row['year'] = $monthyears[0];
                        $row['month'] = $monthyears[1];

                        $month = $row['month']; // April
                        $year = $row['year']; // 2023
                        $daysInMonth = date('t', strtotime($year . '-' . $month . '-01'));

                        $totalDays = 0;
                        $presentDays = 0;
                        $absentDays = 0;
                        $holidayDays = 0;

                        for ($i = 1; $i <= $daysInMonth; $i++) {

                            $attendance = $row["day_$i"];

                            if ($attendance == 'P') {
                                $presentDays++;
                            } else if ($attendance == 'A') {
                                $absentDays++;
                            } else if ($attendance == 'H') {
                                $holidayDays++;
                            }

                            $totalDays++;
                        }

                        // Store the attendance data in the attendance table
                        $attendance = new Attendance;
                        $attendance->employee_id = $row['employee_code'];
                        $attendance->monthyear = $row['monthyear'];
                        $attendance->day_1 = $row['day_1'];
                        $attendance->day_2 = $row['day_2'];
                        $attendance->day_3 = $row['day_3'];
                        $attendance->day_4 = $row['day_4'];
                        $attendance->day_5 = $row['day_5'];
                        $attendance->day_6 = $row['day_6'];
                        $attendance->day_7 = $row['day_7'];
                        $attendance->day_8 = $row['day_8'];
                        $attendance->day_9 = $row['day_9'];
                        $attendance->day_10 = $row['day_10'];
                        $attendance->day_11 = $row['day_11'];
                        $attendance->day_12 = $row['day_12'];
                        $attendance->day_13 = $row['day_13'];
                        $attendance->day_14 = $row['day_14'];
                        $attendance->day_15 = $row['day_15'];
                        $attendance->day_16 = $row['day_16'];
                        $attendance->day_17 = $row['day_17'];
                        $attendance->day_18 = $row['day_18'];
                        $attendance->day_19 = $row['day_19'];
                        $attendance->day_20 = $row['day_20'];
                        $attendance->day_21 = $row['day_21'];
                        $attendance->day_22 = $row['day_22'];
                        $attendance->day_23 = $row['day_23'];
                        $attendance->day_24 = $row['day_24'];
                        $attendance->day_25 = $row['day_25'];
                        $attendance->day_26 = $row['day_26'];
                        $attendance->day_27 = $row['day_27'];
                        $attendance->day_28 = $row['day_28'];
                        $attendance->day_29 = $row['day_29'];
                        $attendance->day_30 = $row['day_30'];
                        $attendance->day_31 = $row['day_31'];
                        $attendance->total_present = $presentDays;
                        $attendance->total_absent = $absentDays;
                        $attendance->total_holiday = $holidayDays;
                        $attendance->branch_id = $this->branch_id;
                        $attendance->company_id = Auth::user()->id;
                        $attendance->designation_id = $employeetransfer['designation_id'];
                        $attendance->total = $totalDays;
                        $attendance->month = $month;
                        $attendance->year = $year;
                        //dd("hii");
                        $attendance->save();

                        //return redirect()->back()->with('success', 'File imported successfully.');
                        return $attendance;   
                    }
                } else {
                    throw new \Exception("Branch Transfer Data Not Available: {$employee['id']}");
                    //return redirect()->back()->with('fail', 'Employee Data not Available.');
                    //throw new Exception("Employee not found for employee_id: {$employee['id']}");
                }
            } else {
                // If $employee is not set, print 'back'
                throw new \Exception("Employee Data Not Available: {$employee['id']}" );
                //return redirect()->back()->with('fail', 'Employee Data not Available.');
                //throw new Exception("Employee not found for employee_id: {$employee['id']}");
            }
        } catch (\Exception $e) {
            // Log the error or return an error message
            \Log::error('Attendance import error: ' . $e->getMessage());
            return null;
        }
    }
}
