<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use Carbon\Carbon;

class EmployeeSalaryExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */
    public function __construct($salaries)
    {
        $this->salaries = $salaries;
    }

    public function collection()
    {
        $models = $this->salaries;

        return $models->map(function ($model) {
            $empCode = 11;
            $formattedDate = Carbon::createFromFormat('Y-M', $model->monthyear)->format('YM');
            $convertedDate = Carbon::parse($model->salary_date)->format('Ymd');
            $model->company_id = $model->branch->name . $formattedDate;
            $model->branch_id = $model->employee->full_name;
            $model->designation_id = $model->employee->bank_ac_no;
            $model->employee_id = optional($model->employee->bankBranch)->ifsc_code;
            $model->monthyear = $empCode;
            $model->salary_date = $model->nettotal;
            $model->nettotal = $convertedDate;


            // $model->full_name = $model->employee->full_name;
            // $model->bank_name = $model->employee->bank_ac_no;
            // $model->ifsc_code = $model->employee->bankBranch->ifsc_code;
            // $model->branch_id = $model->branch->name;
            // $model->designation_id = $model->designation->designation_name;
            // $model->company_id = $model->company->name;
    

            return $model->makeHidden(['id', 'created_at', 'updated_at', 'department_id', 'deleted_at', 'grade_id', 'month', 'year',  'working_days', 'no_of_days_work_done', 'no_of_leave', 'basic_da', 'extra_allowance', 'hra', 'bonus', 'fix_salary', 'sal_advance', 'a', 'b', 'c', 'ot_amount', 'ot_days', 'attendance_allowance', 'dress', 'mess', 'pf', 'profetional_tax', 'basis_wages', 'esic', 'walfare_amount', 'damage_amount', 'fines', 'total', 'branch_ot_amount']);
        });
    }

    public function headings(): array
    {
        return [
            'Unit Name',
            'Employee Name',
            'Bank Account No',
            'IFSC Code',
            'Employee Code',
            'Salary Amount',
            'Date'
        ];
    }
}
