<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Employee;
use Maatwebsite\Excel\Concerns\WithStyles;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;

class PoliceVerificationLeaveExport implements FromCollection, WithHeadings
{

    public function __construct($data)
    {
        $this->data = $data;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
  
     public function collection()
     {
         return $this->data->map(function ($model) {

            // Load the photo data as a PhpSpreadsheet Drawing object
            //  $photo = new Drawing();
            //  $photo->setName('Photo');
            //  $photo->setDescription('Employee photo');
            //  $photo->setPath(asset('storage/' .$model->photo)); // Replace 'photo' with the actual column name for the photo data
            //  $photo->setCoordinates('I' . ($model + 1)); // Set the cell where the photo will be displayed

            static $rowNumber = 0;
             $id  = $model->id;
             $regData=$model->registartion_date;
             $perAddress = $model->permanent_address;
             $model->id   =   ++$rowNumber;
             $model->registartion_date = $model->full_name; 
             $model->full_name = $model->father_name;
             $model->father_name = $model->local_address;
             $model->local_address = $model->permanent_phone_no;
             $model->permanent_address = $regData;
             $model->permanent_phone_no = $model->relieve_date;
             $model->permanent_mobile_no = $perAddress;
             $model->designation_id = $id;
 
             $model->makeHidden([ 'created_at', 'updated_at', 'deleted_at',
             'first_name', 'middle_name','last_name',
                 'date_of_birth', 'country_id',
                 'state_id', 'district_id', 'city_id', 'pincode_id',  'mother_name',
                 'spouse_name', 'visible_distinguishing_mark_1', 'visible_distinguishing_mark_2',
                 'birth_place', 'mother_tongue_id', 'religion', 'marital_status', 'height', 'weight',
                 'blood_group', 'sex', 'qualification_id', 
                 'local_phone_no',  'salary', 'experience',
                 'experience_company', 'police_varification_date', 'police_station_name', 'citizen_of_india_by',
                 'black_list', 'black_list_reasone', 'election_card_no', 'driving_licenses_no', 'rto',
                 'rto_state_id', 'passport_no', 'pancard_no', 'aadhar_card_no', 'reference_relation_contact_1',
                 'reference_relation_contact_2', 'reference_relation_contact_3', 'emergency_contact_name',
                 'emergency_relation', 'emergency_contact_no', 'nominee_1', 'birth_date_1', 'relation_1',
                 'bank_id', 'bank_branch_id', 'ifsc_code', 'bank_ac_no', 'branch_id', 'interview_date',
                 'entry_date', 'department_id', 'grade_id', 'senior_citizen', 'relieve_date', 'final_relieve_date',
                 'security_deposit', 'deposit_detail', 'due_date', 'refund_date', 'refund_detail', 'probation_date',
                 'confirmation_date', 'police_varification', 'leaving_certificate', 'bank_passbook',  'document',
                 'signature', 'month', 'year', 'election_card', 'bank_form', 'monthyear', 'pf_ac_code', 'pf_uan_code',
                  'rejoin_date', 'company_id'
             ]);

             $rowData = [
                $model->id,
                $model->registartion_date,
                $model->full_name,
                $model->father_name,
                $model->local_address,
                $model->permanent_address,
                $model->permanent_phone_no,
                $model->permanent_mobile_no,
                $model->photo, // Add the photo Drawing object to the row data
                $model->designation_id,
            ];
            return $rowData;
         });
     }

    public function headings(): array
    {
        return [
            'Sr.No',
            'Name Of Supervisor/Guards',
            'Father Name',
            'Present Address',
            'Phone No',
            'Date Of Joining',
            'Leaving date',
            'Permanent Address',
            'Photograph',
            'Bedge No',
        ];
    }

    /* public function drawings()
    {
        return $this->data->map(function ($model) {
            $drawing = new Drawing();
            $drawing->setName('Photo');
            $drawing->setDescription('Photo');
            $drawing->setPath(asset('storage/' .$model->photo));
            $drawing->setHeight(120);
            $drawing->setCoordinates('I' . ($model->id + 1)); // Assuming the photo column is at index 10 (column J)
            return $drawing;
        });
    } */
}