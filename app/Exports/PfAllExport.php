<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Salary;
use App\Models\Bank;


class PfAllExport implements FromCollection, WithHeadings
{      
    private $salaries;
    private $empPF;

    public function __construct($salaries,$empPF)
    { 
        $this->salaries = $salaries;
        $this->empPF = $empPF;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $models = $this->salaries;

        return $models->map(function ($model) {
            //dd($model->employee->bank->bank_ac_no);
            $model->company_id = $model->branch->name;
            $model->branch_id = $model->branch->id;
            $model->designation_id = $model->monthyear;
            $model->department_id = $model->employee->pf_ac_code;
            $model->grade_id = $model->employee->pf_uan_code;
            $model->employee_id = $model->employee->id;
            $model->monthyear = $model->employee->full_name;
            $model->month = $model->no_of_days_work_done;
            $model->year = $model->basis_wages;
            $model->salary_date = $model->fix_salary;
            $model->working_days = $model->extra_allowance;
            $model->no_of_days_work_done = $model->total;
            $model->no_of_leave = $model->pf;
            $model->account_no = $model->employee->bank_ac_no;
            $model->bank_name = optional($model->employee->bank)->bank_name;
            $model->ifsc_code = optional($model->employee->bankBranch)->ifsc_code;
            $model->bank_branch_name = optional($model->employee->bankBranch)->bank_branch_name;
            $model->aadhar_card_no = $model->employee->aadhar_card_no;
            $model->date_of_birth = $model->employee->date_of_birth;

            return $model->makeHidden(['id', 'created_at', 'updated_at','fix_salary', 'deleted_at','basic_da' ,'extra_allowance','hra','bonus','sal_advance','a', 'b', 'c', 'ot_amount', 'ot_days', 'attendance_allowance', 'dress', 'mess', 'pf', 'profetional_tax', 'basis_wages', 'esic', 'walfare_amount', 'damage_amount', 'fines', 'branch_ot_amount','totalempPF','totalempFPF','totalempEDLI','totalempINEPC','totalempADMIN','total','nettotal']);
        });
    }
    public function headings(): array
    {
        return [
            'Branchname',
            'BranchId',
            'Monthyear',
            'Pf Ac. No.',
            'Pf UAN no.',
            'Employee Code',
            'Full Name',
            'Days',
            'PfBasic',
            'PfFix',
            'Other',
            'Total',
            'Pf',
            'bank Ac. no.',
            'Bankname',
            'IFSC Code',
            'Bank Branch',
            'Adhaar Card no.',
            'DOB',
        ];
    }
}
