<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Salary;


class PfExport implements FromCollection, WithHeadings
{
    private $salaries;
    private $monthName;
   

    public function __construct($salaries)
    { 
        $this->salaries = $salaries;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $models = $this->salaries;
        return $models->map(function ($model) {
            $model->pf_ac_code = $model->employee->pf_ac_code;
            $model->pf_ac_code = $model->employee->full_name;
            $model->pf_uan_code = $model->employee->pf_ac_code;  
            $model->employee_id = $model->employee->id;
            $model->full_name = $model->employee->pf_uan_code;
            return $model->makeHidden(['id','branch_id','monthyear','designation_id', 'created_at', 'updated_at', 'department_id', 'deleted_at', 'grade_id', 'month', 'year', 'salary_date', 'working_days', 'no_of_days_work_done', 'no_of_leave', 'basic_da', 'extra_allowance', 'hra', 'bonus', 'sal_advance', 'a', 'b', 'c', 'ot_amount', 'ot_days', 'attendance_allowance', 'dress', 'mess', 'pf', 'profetional_tax', 'esic', 'walfare_amount', 'damage_amount', 'fines', 'total','nettotal', 'branch_ot_amount']);
        });
    }
    public function headings(): array
    {
        return [
            'SR No.',
            'EMP Code ',
            'PF Basic',
            'Fix Salary',
            'Employee PF',
            'Employee FPF',
            'EDLI 0.5%',
            'INPEC 0.01%',
            'Admin 0.85%',
            'Emp Name',
            'PF Acount No',
            'Pf UAN No',
        ];
    }
}
