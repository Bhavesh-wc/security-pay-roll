<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Salary;



class SalaryExport implements FromCollection, WithHeadings
{

    public function __construct($salaries)
    {

        $this->salaries = $salaries;
    }

    /**
     * @return \Illuminate\Support\Collection
     */
    public function collection()
    {
        $models = $this->salaries;
        return $models->map(function ($model) {
            $model->monthyear = $model->employee->full_name;
            $model->branch_id = $model->branch->name;
            $model->designation_id = $model->designation->designation_name;
            $model->company_id = $model->company->name;
            
            return $model->makeHidden(['created_at', 'updated_at', 'deleted_at']);
        });
    }
    public function headings(): array
    {
        return [
            'Salary id',
            'Company',
            'Branch',
            'Designation',
            'Department',
            'Grade',
            'Employee id',
            'Full Name',
            'Month',
            'Year',
            'Salary date',
            'Working days',
            'No of working days done',
            'No of leave',
            'Basic+da',
            'Extra allowance',
            'Hra',
            'Bonus',
            'Fix salary',
            'Salary advance',
            'a',
            'b',
            'c',
            'Overtime amount',
            'Overtime days',
            'Attendance allowance',
            'Dress',
            'Mess',
            'PF',
            'Professional tax',
            'Basic wages',
            'Esic',
            'Walfer amount',
            'Damage amount',
            'Fine',
            'Total',
            'Net total',
            'Branch overtime amount',
            

        ];
    }
}
