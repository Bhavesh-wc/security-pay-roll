<?php

namespace App\Exports;

use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\WithHeadings;
use App\Models\Attendance;
use App\Models\Employee;

class AttendanceUploadExport implements FromCollection, WithHeadings
{
    /**
     * @return \Illuminate\Support\Collection
     */

    private $attendances;

    public function __construct($Attendance)
    {
        $this->Attendance = $Attendance;
    }

    public function collection()
    {
        $collection = collect($this->Attendance);

        return $modifiedCollection = $collection->map(function ($item, $key) {
            // Add a new field 'age' with a value of 30 after the 'name' field
            $index = array_search('employee_id', array_keys($item->toArray())) + 1;
            $item = array_merge(array_slice($item->toArray(), 0, $index, true), ['full_name' => $item->employee->full_name], array_slice($item->toArray(), $index, count($item->toArray()) - $index, true));
            $item['designation_id'] = $item['designation']['designation_name'];
            $item['branch_id'] = $item['branch']['name'];
            $item['company_id'] = $item['company']['name'];
            return collect($item)->except(['created_at', 'updated_at', 'deleted_at', 'month', 'year','company','branch','designation','employee']);
        });
    }

    public function headings(): array
    {
        return [
            'Attendence id',
            'Employee id',
            'Full Name',
            'Designation',
            'Company',
            'Branch',
            'Monthyear',
            'Day 1',
            'Day 2',
            'Day 3',
            'Day 4',
            'Day 5',
            'Day 6',
            'Day 7',
            'Day 8',
            'Day 9',
            'Day 10',
            'Day 11',
            'Day 12',
            'Day 13',
            'Day 14',
            'Day 15',
            'Day 16',
            'Day 17',
            'Day 18',
            'Day 19',
            'Day 20',
            'Day 21',
            'Day 22',
            'Day 23',
            'Day 24',
            'Day 25',
            'Day 26',
            'Day 27',
            'Day 28',
            'Day 29',
            'Day 30',
            'Day 31',
            'Total',
            'Total Present',
            'Total Absent',
            'Total Holiday',
        ];
    }
}
