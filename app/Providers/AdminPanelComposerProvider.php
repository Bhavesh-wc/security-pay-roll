<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

class AdminPanelComposerProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->compose();
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }
    public function compose(){
        view()->composer([
            'layouts',
            'layouts.partials.footer',
            'layouts.partials.header',
            'layouts.partials.main',
            'layouts.partials.sidebar',
            'layouts.app',
            'layouts.master',
            'master.department',
            'master.team',
            'auth',
            'auth.login',
            'auth.verify',
            'auth.register',
            'auth.passwords.confirm',
            'auth.passwords.email',
            'auth.passwords.reset',
            'account',
            'home',
            ], 
            'App\Http\Composers\ViewsComposer');
    }
}
