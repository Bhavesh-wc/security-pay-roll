<?php

if (!function_exists('fetchCountry')) {
    function fetchCountry(Request $request)
    {
        $data = Country::where("country_id",$request->country_id)->get(["country_name", "id"]);
        return response()->json($data);
    } 
}

if (!function_exists('fetchState')) {
    function fetchState(Request $request)
    {
        $data = State::where("country_id",$request->country_id)->get(["state_name", "id"]);
        return response()->json($data);
    } 
}

if (!function_exists('fetchDistrict')) {
    function fetchDistrict(Request $request)
    {
        $data = District::where("country_id",$request->country_id)->get(["district_name", "id"]);
        return response()->json($data);
    } 
}

if (!function_exists('fetchCity')) {
    function fetchCity(Request $request)
    {
        $data = City::where("state_id",$request->state_id)->get(["city_name", "id"]);
        return response()->json($data);
    }
}

if (!function_exists('fetchPincode')) {
    function fetchPincode(Request $request)
    {
        $data = Pincode::where("state_id",$request->state_id)->get(["pincode", "id"]);
        return response()->json($data);
    }
}


   


