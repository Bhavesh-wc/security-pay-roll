<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeLeaveHistory extends Model
{
    
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'employee_leave_histories';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'employee_id',
                  'join_date',
                  'leave_date',
                  'company_id',
                  'branch_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
               'deleted_at'
           ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the employee for this model.
     *
     * @return App\Models\Employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee','employee_id');
    }

    /**
     * Get the company for this model.
     *
     * @return App\Models\User
     */
    public function company()
    {
        return $this->belongsTo('App\Models\User','company_id');
    }


    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id');
    }

    /**
     * Set the join_date.
     *
     * @param  string  $value
     * @return void
     */
    // public function setJoinDateAttribute($value)
    // {
    //     $this->attributes['join_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    // }

    /**
     * Set the leave_date.
     *
     * @param  string  $value
     * @return void
     */
    // public function setLeaveDateAttribute($value)
    // {
    //     $this->attributes['leave_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    // }

    /**
     * Get join_date in array format
     *
     * @param  string  $value
     * @return array
     */
    // public function getJoinDateAttribute($value)
    // {
    //     return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    // }

    /**
     * Get leave_date in array format
     *
     * @param  string  $value
     * @return array
     */
    // public function getLeaveDateAttribute($value)
    // {
    //     return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    // }

}