<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class OverTime extends Model
{

    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'over_times';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'monthyear',
        'month',
        'year',
        'branch_id',
        'employee_id',
        'total_overtime_amount',
        'total_overtime_days',
        'date_overtime_paid',
        'remarks',
        'designation_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id');
    }

    /**
     * Get the employee for this model.
     *
     * @return App\Models\Employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }

    /**
     * Get the designation for this model.
     *
     * @return App\Models\Designation
     */
    public function designation()
    {
        return $this->belongsTo('App\Models\Designation', 'designation_id');
    }

    /**
     * Set the date_overtime_paid.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setDateOvertimePaidAttribute($value)
    {
        $this->attributes['date_overtime_paid'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Get date_overtime_paid in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getDateOvertimePaidAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */


    public function overTimeDays()
    {
        return $this->hasMany(OverTimeDay::class);
    }

   /*  public function addOvertimeDay($overtimeDay)
    {
        return $this->overtimeDays()->create($overtimeDay);
    } */
}
