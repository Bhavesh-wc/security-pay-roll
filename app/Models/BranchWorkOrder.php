<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BranchWorkOrder extends Model
{
    
    /* use SoftDeletes; */


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branch_work_orders';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'branch_id',
                  'work_order',
                  'work_start_date',
                  'work_expire_date'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    /* protected $dates = [
               'deleted_at'
           ]; */
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id');
    }

    /**
     * Set the work_start_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setWorkStartDateAttribute($value)
    {
        $this->attributes['work_start_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the work_expire_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setWorkExpireDateAttribute($value)
    {
        $this->attributes['work_expire_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Get work_start_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getWorkStartDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get work_expire_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getWorkExpireDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

}
