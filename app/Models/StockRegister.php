<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class StockRegister extends Model
{
    
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'stock_registers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'stock_category_id',
                  'stock_quantity',
                  'stock_in_date',
                  'balance_code',
                  'company_id',
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
               'deleted_at'
           ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the stockCategory for this model.
     *
     * @return App\Models\StockCategory
     */
    public function stockCategory()
    {
        return $this->belongsTo('App\Models\StockCategory','stock_category_id');
    }

    /**
     * Get the company for this model.
     *
     * @return App\Models\User
     */
    public function company()
    {
        return $this->belongsTo('App\Models\User','company_id');
    }

    /**
     * Set the stock_in_date.
     *
     * @param  string  $value
     * @return void
     */
    // public function setStockInDateAttribute($value)
    // {
    //     $this->attributes['stock_in_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    // }

    /**
     * Get stock_in_date in array format
     *
     * @param  string  $value
     * @return array
     */
    // public function getStockInDateAttribute($value)
    // {
    //     return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    // }

}
