<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CreditSalary extends Model
{


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'credit_salaries';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'monthyear',
        'branch_id',
        'cheque_no',
        'account_no',
        'bank_id',
        'bank_branch_name',
        'credit_date',
        'company_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id');
    }

    /**
     * Get the bank for this model.
     *
     * @return App\Models\Bank
     */
    public function companyBank()
    {
        return $this->belongsTo('App\Models\CompanyBank', 'bank_id');
    }

    /**
     * Get the bankBranch for this model.
     *
     * @return App\Models\BankBranch
     */

    /**
     * Get the company for this model.
     *
     * @return App\Models\User
     */
    public function company()
    {
        return $this->belongsTo('App\Models\User','company_id');
    }

    /**
     * Set the monthyear.
     *
     * @param  string  $value
     * @return void
     */
    // public function setMonthyearAttribute($value)
    // {
    //     $this->attributes['monthyear'] = !empty($value) ? \DateTime::createFromFormat('j/n/Y g:i A', $value) : null;
    // }

    /**
     * Set the credit_date.
     *
     * @param  string  $value
     * @return void
     */
    // public function setCreditDateAttribute($value)
    // {
    //     $this->attributes['credit_date'] = !empty($value) ? \DateTime::createFromFormat('j/n/Y', $value) : null;
    // }

    /**
     * Get monthyear in array format
     *
     * @param  string  $value
     * @return array
     */
    // public function getMonthyearAttribute($value)
    // {
    //     return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('j/n/Y g:i A');
    // }

    /**
     * Get credit_date in array format
     *
     * @param  string  $value
     * @return array
     */
    // public function getCreditDateAttribute($value)
    // {
    //     return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('j/n/Y');
    // }

}
