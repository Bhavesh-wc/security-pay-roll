<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Attendance extends Model
{
    
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'attendances';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'branch_id',
                  'designation_id',
                  'company_id',
                  'employee_id',
                  'monthyear',
                  'month',
                  'year',
                  'day_1',
                  'day_2',
                  'day_3',
                  'day_4',
                  'day_5',
                  'day_6',
                  'day_7',
                  'day_8',
                  'day_9',
                  'day_10',
                  'day_11',
                  'day_12',
                  'day_13',
                  'day_14',
                  'day_15',
                  'day_16',
                  'day_17',
                  'day_18',
                  'day_19',
                  'day_20',
                  'day_21',
                  'day_22',
                  'day_23',
                  'day_24',
                  'day_25',
                  'day_26',
                  'day_27',
                  'day_28',
                  'day_29',
                  'day_30',
                  'day_31',
                  'total',
                  'total_present',
                  'total_absent',
                  'total_holiday'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
               'deleted_at'
           ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the company for this model.
     *
     * @return App\Models\User
     */
    public function company()
    {
        return $this->belongsTo('App\Models\User','company_id');
    }

    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id');
    }

    /**
     * Get the designation for this model.
     *
     * @return App\Models\Designation
     */
    public function designation()
    {
        return $this->belongsTo('App\Models\Designation','designation_id');
    }

    /**
     * Get the employee for this model.
     *
     * @return App\Models\Employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee','employee_id');
    }

}
