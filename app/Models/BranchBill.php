<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class BranchBill extends Model
{
    
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branch_bills';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'monthyear',
                  'branch_id',
                  'net_bill_amt',
                  'bill_amount',
                  'service_tax',
                  'gross_amount',
                  'service_charge',
                  'pf_amt_12',
                  'esi_amt',
                  'leave_amt',
                  'bonus_amt',
                  'recover_payment',
                  'professional_tax',
                  'other_amt',
                  'extra_exp',
                  'tds',
                  'net_amt',
                  'gross_salary',
                  'payroll_gross',
                  'diff_amt',
                  'salary_paid',
                  'profit',
                  'net_profit',
                  'company_id'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
               'deleted_at'
           ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the company for this model.
     *
     * @return App\Models\User
     */
    public function company()
    {
        return $this->belongsTo('App\Models\User','company_id');
    }

    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id');
    }



}
