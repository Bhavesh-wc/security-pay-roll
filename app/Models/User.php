<?php

namespace App\Models;

// use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Laravel\Sanctum\HasApiTokens;
use Illuminate\Database\Eloquent\SoftDeletes;

class User extends Authenticatable
{
    use HasApiTokens, HasFactory, Notifiable, SoftDeletes;

    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'users';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array<int, string>
     */
    protected $fillable = [
        'name',
        'company_code',
        'email',
        'phone_no',
        'contact_person_name',
        'contact_person_phone_no',
        'address',
        'gst_no',
        'pf_no',
        'esic',
        'police_registration_number',
        'shop_and_establishment',
        'professional_tax',
        'msme_registaration_no',
        'welfare_fund',
        'password',
        'profile_photo'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that should be hidden for serialization.
     *
     * @var array<int, string>
     */
    protected $hidden = [
        'password',
        'remember_token',
    ];

    /**
     * The attributes that should be cast.
     *
     * @var array<string, string>
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    // public function getAvatarUrlAttribute()
    // {
    //     if ($this->info) {
    //         return asset($this->info->avatar_url);
    //     }

    //     return Redirect::back();
    // }

    /**
     * User relation to info model
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasOne
     */
    public function info()
    {
        return $this->hasOne(UserInfo::class);
    }

    public function banks()
    {
        return $this->hasMany(Bank::class, 'company_id');
    }

    public function bankBranches()
    {
        return $this->hasMany(BankBranch::class, 'company_id');
    }

    public function countries()
    {
        return $this->hasMany(Country::class, 'company_id');
    }

    public function states()
    {
        return $this->hasMany(State::class, 'company_id');
    }

    public function districts()
    {
        return $this->hasMany(District::class, 'company_id');
    }

    public function cities()
    {
        return $this->hasMany(City::class, 'company_id');
    }

    public function pincodes()
    {
        return $this->hasMany(Pincode::class, 'company_id');
    }

    public function departments()
    {
        return $this->hasMany(Department::class, 'company_id');
    }

    public function qualifications()
    {
        return $this->hasMany(Qualification::class, 'company_id');
    }

    public function motherTongues()
    {
        return $this->hasMany(MotherTongue::class, 'company_id');
    }

    public function grades()
    {
        return $this->hasMany(Grade::class, 'company_id');
    }

    public function stockCategories()
    {
        return $this->hasMany(StockCategory::class, 'company_id');
    }

    public function taxManagers()
    {
        return $this->hasMany(TaxManager::class, 'company_id');
    }

    public function stockRegisters()
    {
        return $this->hasMany(StockRegister::class, 'company_id');
    }

    public function employees()
    {
        return $this->hasMany(Employee::class, 'company_id');
    }

    public function branchDesignationHistory()
    {
        return $this->hasMany(BranchDesignationHistory::class, 'company_id');
    }

    public function attendances()
    {
        return $this->hasMany(Attendance::class, 'company_id');
    }
}
