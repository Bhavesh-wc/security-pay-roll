<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\BranchDesignation;

class Employee extends Model
{

    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'employees';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'registartion_date',
        'designation_id',
        'first_name',
        'middle_name',
        'last_name',
        'full_name',
        'date_of_birth',
        'country_id',
        'state_id',
        'district_id',
        'city_id',
        'pincode_id',
        'father_name',
        'mother_name',
        'spouse_name',
        'visible_distinguishing_mark_1',
        'visible_distinguishing_mark_2',
        'birth_place',
        'mother_tongue_id',
        'religion',
        'marital_status',
        'height',
        'weight',
        'blood_group',
        'sex',
        'qualification_id',
        'local_address',
        'permanent_address',
        'local_phone_no',
        'permanent_phone_no',
        'permanent_mobile_no',
        'salary',
        'experience',
        'experience_company',
        'police_varification_date',
        'police_station_name',
        'citizen_of_india_by',
        'black_list',
        'black_list_reasone',
        'election_card_no',
        'driving_licenses_no',
        'rto',
        'rto_state_id',
        'passport_no',
        'pancard_no',
        'aadhar_card_no',
        'reference_relation_contact_1',
        'reference_relation_contact_2',
        'reference_relation_contact_3',
        'emergency_contact_name',
        'emergency_relation',
        'emergency_contact_no',
        'nominee_1',
        'birth_date_1',
        'relation_1',
        'bank_id',
        'bank_branch_id',
        'ifsc_code',
        'bank_ac_no',
        'branch_id',
        'interview_date',
        'entry_date',
        'department_id',
        'grade_id',
        'senior_citizen',
        'relieve_date',
        'final_relieve_date',
        'security_deposit',
        'deposit_detail',
        'due_date',
        'refund_date',
        'refund_detail',
        'probation_date',
        'confirmation_date',
        'police_varification',
        'leaving_certificate',
        'bank_passbook',
        'photo',
        'document',
        'signature',
        'company_id',
        'status',
        'month',
        'year',
        'monthyear'
    ];


    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the designation for this model.
     *
     * @return App\Models\Designation
     */
    public function designation()
    {
        return $this->belongsTo('App\Models\Designation', 'designation_id');
    }

    public function branchDesignation()
    {
        return $this->belongsTo(BranchDesignation::class);
    }

    /* public function branchDesignation()
    {
        return $this->belongsTo('App\Models\BranchDesignation', 'designation_id');
    } */

    /**
     * Get the country for this model.
     *
     * @return App\Models\Country
     */
    public function country()
    {
        return $this->belongsTo('App\Models\Country', 'country_id');
    }

    /**
     * Get the state for this model.
     *
     * @return App\Models\State
     */
    public function state()
    {
        return $this->belongsTo('App\Models\State', 'state_id');
    }

    /**
     * Get the district for this model.
     *
     * @return App\Models\District
     */
    public function district()
    {
        return $this->belongsTo('App\Models\District', 'district_id');
    }

    /**
     * Get the city for this model.
     *
     * @return App\Models\City
     */
    public function city()
    {
        return $this->belongsTo('App\Models\City', 'city_id');
    }

    /**
     * Get the pincode for this model.
     *
     * @return App\Models\Pincode
     */
    public function pincode()
    {
        return $this->belongsTo('App\Models\Pincode', 'pincode_id');
    }

    /**
     * Get the motherTongue for this model.
     *
     * @return App\Models\MotherTongue
     */
    public function motherTongue()
    {
        return $this->belongsTo('App\Models\MotherTongue', 'mother_tongue_id');
    }

    /**
     * Get the qualification for this model.
     *
     * @return App\Models\Qualification
     */
    public function qualification()
    {
        return $this->belongsTo('App\Models\Qualification', 'qualification_id');
    }

    /**
     * Get the rtoState for this model.
     *
     * @return App\Models\State
     */
    public function rtoState()
    {
        return $this->belongsTo('App\Models\State', 'rto_state_id');
    }

    /**
     * Get the bank for this model.
     *
     * @return App\Models\Bank
     */
    public function bank()
    {
        return $this->belongsTo('App\Models\Bank', 'bank_id');
    }

    /**
     * Get the bankBranch for this model.
     *
     * @return App\Models\BankBranch
     */
    public function bankBranch()
    {
        return $this->belongsTo('App\Models\BankBranch', 'bank_branch_id');
    }

    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id');
    }

    /**
     * Get the department for this model.
     *
     * @return App\Models\Department
     */
    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'department_id');
    }

    /**
     * Get the grade for this model.
     *
     * @return App\Models\Grade
     */
    public function grade()
    {
        return $this->belongsTo('App\Models\Grade', 'grade_id');
    }

    /**
     * Get the company for this model.
     *
     * @return App\Models\User
     */
    public function company()
    {
        return $this->belongsTo('App\Models\User', 'company_id');
    }


    /**
     * Get the employee for this model.
     *
     * @return App\Models\Employee
     */
    public function salary()
    {
        return $this->hasMany('App\Models\Salary', 'employee_id', 'id');
    }

    /**
     * Set the registartion_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setRegistartionDateAttribute($value)
    {
        $this->attributes['registartion_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the date_of_birth.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setDateOfBirthAttribute($value)
    {
        $this->attributes['date_of_birth'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the police_varification_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setPoliceVarificationDateAttribute($value)
    {
        $this->attributes['police_varification_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the birth_date_1.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setBirthDate1Attribute($value)
    {
        $this->attributes['birth_date_1'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the interview_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setInterviewDateAttribute($value)
    {
        $this->attributes['interview_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the entry_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setEntryDateAttribute($value)
    {
        $this->attributes['entry_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the relieve_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setRelieveDateAttribute($value)
    {
        $this->attributes['relieve_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the due_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setDueDateAttribute($value)
    {
        $this->attributes['due_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the refund_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setRefundDateAttribute($value)
    {
        $this->attributes['refund_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the probation_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setProbationDateAttribute($value)
    {
        $this->attributes['probation_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the confirmation_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setConfirmationDateAttribute($value)
    {
        $this->attributes['confirmation_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the leave_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setLeaveDateAttribute($value)
    {
        $this->attributes['leave_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the rejoin_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setRejoinDateAttribute($value)
    {
        $this->attributes['rejoin_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Get registartion_date in array format
     *
     * @param  string  $value
     * @return array
     */

    /* public function getRegistartionDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */


    /**
     * Get date_of_birth in array format
     *
     * @param  string  $value
     * @return array
     */

    /*  public function getDateOfBirthAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */


    /**
     * Get police_varification_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /*  public function getPoliceVarificationDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get birth_date_1 in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getBirthDate1Attribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get interview_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getInterviewDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get entry_date in array format
     *
     * @param  string  $value
     * @return array
     */

    /*  public function getEntryDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get relieve_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getRelieveDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get due_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /*  public function getDueDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get refund_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /*  public function getRefundDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get probation_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getProbationDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get confirmation_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getConfirmationDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get leave_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getLeaveDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get rejoin_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getRejoinDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');

    } */

    public function damages()
    {
        return $this->hasMany(Damage::class);
    }

    public function fines()
    {
        return $this->hasMany(Fine::class);
    }

    public function overTimes()
    {
        return $this->hasMany(OverTime::class);
    }

    public function advanceSalaries()
    {
        return $this->hasMany(AdvanceSalary::class);
    }

    public function attendances()
    {
        return $this->hasMany(Attendance::class);
    }
}
