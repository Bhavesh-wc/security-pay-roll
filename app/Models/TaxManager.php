<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class TaxManager extends Model
{
    
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'tax_managers';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'monthyear',
                  'employee_pf',
                  'employee_esic',
                  'professional_tax',
                  'welfare_fund',
                  'status',
                  'company_id',
                  'employee_admin',
                  'employer_pf',
                  'employer_fpf',
                  'employer_edli',
                  'employer_inepc',
                  'employer_esic',
                  'min_salary',
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
               'deleted_at'
           ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the company for this model.
     *
     * @return App\Models\User
     */
    public function company()
    {
        return $this->belongsTo('App\Models\User','company_id');
    }

    /**
     * Set the start_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setStartDateAttribute($value)
    {
        $this->attributes['start_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the end_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setEndDateAttribute($value)
    {
        $this->attributes['end_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Get start_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getStartDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get end_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getEndDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

}
