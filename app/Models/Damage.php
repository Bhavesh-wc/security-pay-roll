<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Damage extends Model
{
    use SoftDeletes;
    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'damages';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'monthyear',
        'month',
        'year',
        'branch_id',
        'employee_id',
        'damage_details',
        'damage_date',
        'damage_amount',
        'employee_reason_for_deduction',
        'name_of_person_explanation_heard',
        'total_installments',
        'installment_date',
        'installment_amount',
        'designation_id',
        'remarks',
        'designation_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id');
    }

    /**
     * Get the employee for this model.
     *
     * @return App\Models\Employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }

    /**
     * Get the designation for this model.
     *
     * @return App\Models\Designation
     */
    public function designation()
    {
        return $this->belongsTo('App\Models\Designation', 'designation_id');
    }

    /**
     * Set the damage_date.
     *
     * @param  string  $value
     * @return void
     */
    // public function setDamageDateAttribute($value)
    // {
    //     $this->attributes['damage_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    // }

    /**
     * Set the installment_date.
     *
     * @param  string  $value
     * @return void
     */
    // public function setInstallmentDateAttribute($value)
    // {
    //     $this->attributes['installment_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    // }

    /**
     * Get damage_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /*  public function getDamageDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get installment_date in array format
     *
     * @param  string  $value
     * @return array
     */
    // public function getInstallmentDateAttribute($value)
    // {
    //     return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    // }
}
