<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\Models\Employee;

class BranchDesignation extends Model
{
    
   /*  use SoftDeletes; */


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'branch_designations';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'branch_id',
                  'designation_id',
                  'basic_fix',
                  'basic_da',
                  'extra_allowance',
                  'hra',
                  'bonus',
                  'fix_salary'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    /* protected $dates = [
               'deleted_at'
           ]; */
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id');
    }

    /**
     * Get the designation for this model.
     *
     * @return App\Models\Designation
     */
    public function designation()
    {
        return $this->belongsTo('App\Models\Designation','designation_id');
    }

    public function employee()
    {
        return $this->hasMany(Employee::class);
    }

}
