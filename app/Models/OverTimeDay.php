<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
/* use Illuminate\Database\Eloquent\SoftDeletes; */

class OverTimeDay extends Model
{
    
    /* use SoftDeletes; */


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'over_time_days';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'over_time_id',
                  'over_time_date',
                  'over_time_amount'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    /* protected $dates = [
               'deleted_at'
           ]; */
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the overTime for this model.
     *
     * @return App\Models\OverTime
     */
    public function overTime()
    {
        return $this->belongsTo('App\Models\OverTime','over_time_id');
    }

    /**
     * Set the over_time_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setOverTimeDateAttribute($value)
    {
        $this->attributes['over_time_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Get over_time_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getOverTimeDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

}
