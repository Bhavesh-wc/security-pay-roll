<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Salary extends Model
{

    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'salaries';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'branch_id',
        'designation_id',
        'department_id',
        'grade_id',
        'employee_id',
        'monthyear',
        'month',
        'year',
        'salary_date',
        'working_days',
        'no_of_days_work_done',
        'no_of_leave',
        'basic_da',
        'extra_allowance',
        'hra',
        'bonus',
        'fix_salary',
        'sal_advance',
        'a',
        'b',
        'c',
        'ot_amount',
        'ot_days',
        'attendance_allowance',
        'dress',
        'mess',
        'pf',
        'profetional_tax',
        'basis_wages',
        'esic',
        'walfare_amount',
        'damage_amount',
        'fines',
        'total',
        'nettotal',
        'branch_ot_amount'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the company for this model.
     *
     * @return App\Models\User
     */
    public function company()
    {
        return $this->belongsTo('App\Models\User', 'company_id');
    }

    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id');
    }

    /**
     * Get the designation for this model.
     *
     * @return App\Models\Designation
     */
    public function designation()
    {
        return $this->belongsTo('App\Models\Designation', 'designation_id');
    }

    /**
     * Get the department for this model.
     *
     * @return App\Models\Department
     */
    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'department_id');
    }

    /**
     * Get the grade for this model.
     *
     * @return App\Models\Grade
     */
    public function grade()
    {
        return $this->belongsTo('App\Models\Grade', 'grade_id');
    }

    /**
     * Get the employee for this model.
     *
     * @return App\Models\Employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }

    /**
     * Set the salary_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setSalaryDateAttribute($value)
    {
        $this->attributes['salary_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Get salary_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getSalaryDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    public function getBasisWages()
    {
        $basis_wages = $this->basis_wages;
        return (int) $basis_wages;
    }

    public function getBonus()
    {
        $bonus = $this->bonus;
        return (int) $bonus;
    }
}
