<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Fine extends Model
{

    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'fines';

    /**
     * The database primary key value.
     *
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
        'monthyear',
        'month',
        'year',
        'branch_id',
        'employee_id',
        'act_for_which_fine_impossed',
        'date_of_offence',
        'reasone_against_fine',
        'name_of_person_explanation_heard',
        'wages_payable',
        'fine_amount',
        'fine_released_date',
        'fine_released_amount',
        'remarks',
        'designation_id'
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at'
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];

    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch', 'branch_id');
    }

    /**
     * Get the employee for this model.
     *
     * @return App\Models\Employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee', 'employee_id');
    }

    /**
     * Get the designation for this model.
     *
     * @return App\Models\Designation
     */
    public function designation()
    {
        return $this->belongsTo('App\Models\Designation', 'designation_id');
    }

    /**
     * Set the date_of_offence.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setDateOfOffenceAttribute($value)
    {
        $this->attributes['date_of_offence'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Set the fine_released_date.
     *
     * @param  string  $value
     * @return void
     */
    /* public function setFineReleasedDateAttribute($value)
    {
        $this->attributes['fine_released_date'] = !empty($value) ? \DateTime::createFromFormat('dd/mm/yyy', $value) : null;
    } */

    /**
     * Get date_of_offence in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getDateOfOffenceAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */

    /**
     * Get fine_released_date in array format
     *
     * @param  string  $value
     * @return array
     */
    /* public function getFineReleasedDateAttribute($value)
    {
        return \DateTime::createFromFormat($this->getDateFormat(), $value)->format('dd/mm/yyy');
    } */
}
