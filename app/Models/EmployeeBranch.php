<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class EmployeeBranch extends Model
{
    
    use SoftDeletes;


    /**
     * The database table used by the model.
     *
     * @var string
     */
    protected $table = 'employee_branches';

    /**
    * The database primary key value.
    *
    * @var string
    */
    protected $primaryKey = 'id';

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = [
                  'branch_id',
                  'employee_id',
                  'designation_id',
                  'department_id',
                  'grade_id',
                  'basic_fix',
                  'basic_da',
                  'extra_allowance',
                  'hra',
                  'bonus',
                  'fix_salary',
                  'monthyear',
                  'month',
                  'year'
              ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
               'deleted_at'
           ];
    
    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [];
    
    /**
     * Get the company for this model.
     *
     * @return App\Models\User
     */
    public function company()
    {
        return $this->belongsTo('App\Models\User','company_id');
    }

    /**
     * Get the branch for this model.
     *
     * @return App\Models\Branch
     */
    public function branch()
    {
        return $this->belongsTo('App\Models\Branch','branch_id');
    }

    /**
     * Get the employee for this model.
     *
     * @return App\Models\Employee
     */
    public function employee()
    {
        return $this->belongsTo('App\Models\Employee','employee_id');
    }

    /**
     * Get the designation for this model.
     *
     * @return App\Models\Designation
     */
    public function designation()
    {
        return $this->belongsTo('App\Models\Designation','designation_id');
    }

    /**
     * Get the department for this model.
     *
     * @return App\Models\Department
     */
    public function department()
    {
        return $this->belongsTo('App\Models\Department','department_id');
    }

    /**
     * Get the grade for this model.
     *
     * @return App\Models\Grade
     */
    public function grade()
    {
        return $this->belongsTo('App\Models\Grade','grade_id');
    }



}
