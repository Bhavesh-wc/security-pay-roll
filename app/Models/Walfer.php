<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;


class Walfer extends Model
{
    use HasFactory;
    use SoftDeletes;

    protected $table = 'walfers';

    protected $primaryKey = 'id';

    protected $fillable = [
        'walfare_amount',
        'company_id',
    ];

    protected $dates = [
        'deleted_at'
    ];


    protected $casts = [];


    public function company()
    {
        return $this->belongsTo('App\Models\User', 'company_id');
    }
}
