$('#kt_docs_repeater_advanced').repeater({
    initEmpty: false,
    isFirstItemUndeletable: true,

    defaultValues: {
        'text-input': 'foo'
    },

    show: function () {
        $(this).slideDown();

        // Re-init select2
        $(this).find('[data-kt-repeater="select2"]').select2();

        // Re-init flatpickr
        $(this).find('[data-kt-repeater="datepicker"]').flatpickr();

    },

    hide: function (deleteElement) {
        if(confirm('Are you sure you want to delete this element?')) {
            $(this).slideUp(deleteElement);
        }
    },

    ready: function(){
        // Init select2
        $('[data-kt-repeater="select2"]').select2();

        // Init flatpickr
        $('[data-kt-repeater="datepicker"]').flatpickr();

    }
});
$('#kt_docs_repeater_advanced_1').repeater({
    initEmpty: false,
    isFirstItemUndeletable: true,

    defaultValues: {
        'text-input': 'foo'
    },

    show: function () {
        $(this).slideDown();

        // Re-init select2
        $(this).find('[data-kt-repeater="select2"]').select2();

        // Re-init flatpickr
        $(this).find('[data-kt-repeater="datepicker"]').flatpickr();

    },

    hide: function (deleteElement) {
        if(confirm('Are you sure you want to delete this element?')) {
            $(this).slideUp(deleteElement);
        }
    },

    /* ready: function (setIndexes) {
        $dragAndDrop.on('drop', setIndexes);
    }, */
    
    ready: function(){
        // Init select2
        $('[data-kt-repeater="select2"]').select2();

        // Init flatpickr
        $('[data-kt-repeater="datepicker"]').flatpickr();

    }
    
});

$('#kt_docs_repeater_basic').repeater({
    initEmpty: false,

    defaultValues: {
        'text-input': 'foo'
    },

    show: function () {
        $(this).slideDown();
    },

    hide: function (deleteElement) {
        $(this).slideUp(deleteElement);
    }
});


$(document).ready(function () {

    // Apply show/hide functionality to existing repeater items on page load
    $('#kt_docs_repeater_advanced .kt-repeater-row').each(function (index, element) {
        showHideDiv(element);
    });

    // Apply show/hide functionality to any new repeater items added to the page
    $('#kt_docs_repeater_advanced').on('afterAddRow', function (e, addedRow) {
        showHideDiv(addedRow);
    });

    // Apply show/hide functionality to any new repeater items created using the "Add" button
    $('#kt_docs_repeater_advanced [data-repeater-create]').click(function () {
        var repeaterItem = $(this).parents('#kt_docs_repeater_advanced').find('.kt-repeater-row').last();
        showHideDiv(repeaterItem);
    });

    
    // Get the select dropdown element and the div element to show/hide
    function showHideDiv(element){
        var dropdown = $(element).find('.basic_fix');
        var basicshowhide = $(element).find('.basic_show_hide');
        var fixshowhide = $(element).find('.fix_show_hide');

        // Show/hide the div on initial page load
        if (dropdown.val() === 'basic') {
            basicshowhide.show();
            fixshowhide.hide();
        } else if (dropdown.val() === 'fix') {
            basicshowhide.hide();
            fixshowhide.show();
        } else {
            basicshowhide.hide();
            fixshowhide.hide();
        }

        // Show/hide the div on select dropdown change
        dropdown.change(function () {
            if (dropdown.val() === 'basic') {
                console.log("Showing div");
                basicshowhide.show();
                fixshowhide.hide();
            } else if (dropdown.val() === 'fix') {
                basicshowhide.hide();
                fixshowhide.show();
            } else {
                basicshowhide.hide();
                fixshowhide.hide();
            }
        });
    }
});

$(document).ready(function(){

    var dropdownValue = $('#professional_tax_req').val();

    // Show or hide the div based on the initial dropdown value
    if (dropdownValue === 'yes') {
        $('#professional_tax_show_hide').show();
    } else {
        $('#professional_tax_show_hide').hide();
    }

    $('#professional_tax_req').on('change', function() {
        if ( this.value == 'yes')
        {
            $("#professional_tax_show_hide").show();
        }
        else
        {
            $("#professional_tax_show_hide").hide(); 
        }
    });

});

$(document).ready(function () {

    // Apply show/hide functionality to existing repeater items on page load
    $('#kt_docs_repeater_advanced_1 .kt-repeater-row').each(function (index, element) {
        showDatepicker(element);
    });

    // Apply show/hide functionality to any new repeater items added to the page
    $('#kt_docs_repeater_advanced_1').on('afterAddRow', function (e, addedRow) {
        showDatepicker(addedRow);
    });

    // Apply show/hide functionality to any new repeater items created using the "Add" button
    $('#kt_docs_repeater_advanced_1 [data-repeater-create]').click(function () {
        var repeaterItem = $(this).parents('#kt_docs_repeater_advanced_1').find('.kt-repeater-row').last();
        showDatepicker(repeaterItem);
    });

    
    // Get the select dropdown element and the div element to show/hide
    function showDatepicker(element){
        $('.datetimepicker7').datetimepicker();
        $('.datetimepicker8').datetimepicker({
            useCurrent: false
        });
        $(".datetimepicker7").on("change.datetimepicker", function (e) {
            $('.datetimepicker8').datetimepicker('minDate', e.date);
        });
        $(".datetimepicker8").on("change.datetimepicker", function (e) {
            $('.datetimepicker7').datetimepicker('maxDate', e.date);
        });
    }
});
