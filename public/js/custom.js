if ($("#create_bank_form,#edit_bank_form").length > 0) {
    $("#create_bank_form,#edit_bank_form").validate({
        rules: {
            bank_name: {
                required: true,
            },
            account_no: {
                required: true,
                number: true,
                minlength: 12,
                maxlength: 16,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_bank_branche_form,#edit_bank_branche_form").length > 0) {
    $("#create_bank_branche_form,#edit_bank_branche_form").validate({
        rules: {
            bank_branch_name: {
                required: true,
            },
            ifsc_code: {
                required: true,
            },
            address: {
                required: true,
            },
            bank_id: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_company_bank_form,#edit_company_bank_form").length > 0) {
    $("#create_company_bank_form,#edit_company_bank_form").validate({
        rules: {
            bank_name: {
                required: true,
            },
            account_no: {
                required: true,
                number: true,
                minlength: 12,
                maxlength: 16,
            },
            bank_branch_name: {
                required: true,
            },
            ifsc_code: {
                required: true,
            },
            address: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_department_form,#edit_department_form").length > 0) {
    $("#create_department_form,#edit_department_form").validate({
        rules: {
            department_name: {
                required: true,
            },
            department_code: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_qualification_form,#edit_qualification_form").length > 0) {
    $("#create_qualification_form,#edit_qualification_form").validate({
        rules: {
            qualification_name: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_mother_tongue_form,#edit_mother_tongue_form").length > 0) {
    $("#create_mother_tongue_form,#edit_mother_tongue_form").validate({
        rules: {
            language_name: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_grade_form,#edit_grade_form").length > 0) {
    $("#create_grade_form,#edit_grade_form").validate({
        rules: {
            grade_name: {
                required: true,
            },
            garde_code: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_country_form,#edit_country_form").length > 0) {
    $("#create_country_form,#edit_country_form").validate({
        rules: {
            country_name: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_state_form,#edit_state_form").length > 0) {
    $("#create_state_form,#edit_state_form").validate({
        rules: {
            state_name: {
                required: true,
            },
            country_id: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_district_form,#edit_district_form").length > 0) {
    $("#create_district_form,#edit_district_form").validate({
        rules: {
            district_name: {
                required: true,
            },
            state_id: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_city_form,#edit_city_form").length > 0) {
    $("#create_city_form,#edit_city_form").validate({
        rules: {
            city_name: {
                required: true,
            },
            district_id: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_pincode_form,#edit_pincode_form").length > 0) {
    $("#create_pincode_form,#edit_pincode_form").validate({
        rules: {
            pincode: {
                required: true,
            },
            city_id: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_stock_category_form,#edit_stock_category_form").length > 0) {
    $("#create_stock_category_form,#edit_stock_category_form").validate({
        rules: {
            stock_category_name: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_tax_manager_form,#edit_tax_manager_form").length > 0) {
    $("#create_tax_manager_form,#edit_tax_manager_form").validate({
        rules: {
            start_date: {
                date: true,
                required: true,
            },
            end_date: {
                date: true,
                required: true,
            },
            employee_pf: {
                required: true,
            },
            employee_esic: {
                required: true,
            },
            professional_tax: {
                required: true,
            },
            welfare_fund: {
                required: true,
            },
            employee_admin:{
                required: true,
            },
            employer_pf:{
                required: true,
            },
            employer_fpf:{
                required: true,
            },
            employer_edli:{
                required: true,
            },
            employer_inepc:{
                required: true,
            },
            employer_esic:{
                required: true,
            },
            min_salary:{
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_advance_salary_form,#edit_advance_salary_form").length > 0) {
    $("#create_advance_salary_form,#edit_advance_salary_form").validate({
        rules: {
            monthyear: {
                required: true,
            },
            branch_id: {
                required: true,
            },
            employee_id: {
                required: true,
            },
            wages_payable: {
                required: true,
            },
            date_of_advance: {
                date: true,
                required: true,
            },
            purpose_of_advance: {
                required: true,
            },
            remarks: {
                required: true,
            },
            amount: {
                required: true,
            },
            advance_for: {
                required: true,
            },
        },
    });
}

if ($("#create_over_time_form,#edit_over_time_form").length > 0) {
    $("#create_over_time_form,#edit_over_time_form").validate({
        rules: {
            monthyear: {
                required: true,
            },
            branch_id: {
                required: true,
            },
            employee_id: {
                required: true,
            },
            total_overtime_amount: {
                required: true,
            },
            total_overtime_days: {
                required: true,
            },
            date_overtime_paid: {
                required: true,
                date: true,
            },
            remarks: {
                required: true,
            },
        },
    });
}

if ($("#create_damage_form,#edit_damage_form").length > 0) {
    $("#create_damage_form,#edit_damage_form").validate({
        rules: {
            monthyear: {
                required: true,
            },
            branch_id: {
                required: true,
            },
            employee_id: {
                required: true,
            },
            damage_details: {
                required: true,
            },
            damage_date: {
                required: true,
                date: true,
            },
            damage_amount: {
                required: true,
            },
            employee_reason_for_deduction: {
                required: true,
            },
            name_of_person_explanation_heard: {
                required: true,
            },
            total_installments: {
                required: true,
                number: true,
            },
            installment_date: {
                required: true,
                date: true,
            },
            installment_amount: {
                required: true,
            },
            remarks: {
                required: true,
            },
        },
    });
}

if ($("#create_fine_form,#edit_fine_form").length > 0) {
    $("#create_fine_form,#edit_fine_form").validate({
        rules: {
            monthyear: {
                required: true,
            },
            branch_id: {
                required: true,
            },
            employee_id: {
                required: true,
            },
            act_for_which_fine_impossed: {
                required: true,
            },
            date_of_offence: {
                required: true,
                date: true,
            },
            reasone_against_fine: {
                required: true,
            },
            name_of_person_explanation_heard: {
                required: true,
            },
            wages_payable: {
                required: true,
            },
            fine_amount: {
                required: true,
            },
            fine_released_date: {
                required: true,
                date: true,
            },
            fine_released_amount: {
                required: true,
            },
            remarks: {
                required: true,
            },
        },
    });
}

if ($("#edit_system_configuration_form").length > 0) {
    $("#edit_system_configuration_form").validate({
        rules: {
            title: {
                required: true,
            },
            address: {
                required: true,
            },
            mobile: {
                required: true,
            },
            email: {
                required: true,
                email: true,
            },
            footer: {
                required: true,
            },
        },
    });
}

if ($("#create_user_form,#edit_user_form").length > 0) {
    $("#create_user_form,#edit_user_form").validate({
        rules: {
            name: {
                required: true,
            },
            company_code: {
                required: true,
            },
            email: {
                email: true,
                reuired: true,
                unique: true,
            },
            phone_no: {
                required: true,
            },
            password: {
                required: true,
            },
            password_confirmation: {
                required: true,
                equalTo: "#password",
            },
        },
    });
}

if ($("#create_branch_form,#edit_branch_form").length > 0) {
    $("#create_branch_form").validate({
        rules: {
            name: {
                required: true,
            },
            address: {
                required: true,
            },
            pf: {
                required: true,
            },
            professional_tax_req: {
                required: true,
            },
            esic: {
                required: true,
            },
            over_time_amount: {
                required: true,
                number: true,
            },
            monthyear: {
                required: true,
            },
            month: {
                required: true,
            },
            year: {
                required: true,
            },
            status: {
                required: true,
            },
            "branchdesignation[0][designation_id]": {
                required: true,
            },
            "branchdesignation[0][basic_fix]": {
                required: true,
            },
            "workorder[0][work_start_date]": {
                required: true,
            },
            "workorder[0][work_order]": {
                required: true,
            },
        },
    });
    $("#edit_branch_form").validate({
        rules: {
            name: {
                required: true,
            },
            address: {
                required: true,
            },
            pf: {
                required: true,
            },
            professional_tax_req: {
                required: true,
            },
            esic: {
                required: true,
            },
            over_time_amount: {
                required: true,
                number: true,
            },
            monthyear: {
                required: true,
            },
            month: {
                required: true,
            },
            year: {
                required: true,
            },
            status: {
                required: true,
            },
        },
    });
}

if ($("#create_employee_form,#edit_employee_form").length > 0) {
    $("#create_employee_form,#edit_employee_form").validate({
        rules: {
            registartion_date: {
                date: true,
                required: true,
            },
            designation_id: {
                required: true,
            },
            first_name: {
                required: true,
            },
            middle_name: {
                required: true,
            },
            last_name: {
                required: true,
            },
            full_name: {
                required: true,
            },
            date_of_birth: {
                date: true,
                required: true,
            },
            country_id: {
                required: true,
            },
            state_id: {
                required: true,
            },
            district_id: {
                required: true,
            },
            city_id: {
                required: true,
            },
            pincode_id: {
                required: true,
            },
            mother_tongue_id: {
                required: true,
            },
            religion: {
                required: true,
            },
            marital_status: {
                required: true,
            },
            sex: {
                required: true,
            },
            local_address: {
                required: true,
            },
            permanent_address: {
                required: true,
            },
            permanent_phone_no: {
                required: true,
            },
            qualification_id: {
                required: true,
            },
        },
    });
}

if ($("#edit_other_information_form").length > 0) {
    $("#edit_other_information_form").validate({
        rules: {
            citizen_of_india_by: {
                required: true,
            },
            police_varification_date: {
                date: true,
            },
            birth_date_1: {
                date: true,
            },
        },
    });
}

if ($("#edit_company_legal_form").length > 0) {
    $("#edit_company_legal_form").validate({
        rules: {
            branch_id: {
                required: true,
            },
            grade_id: {
                required: true,
            },
            bank_ac_no: {
                number: true,
                minlength: 12,
                maxlength: 16,
            },
            interview_date: {
                date: true,
            },
            entry_date: {
                required: true,
                date: true,
            },
            relieve_date: {
                date: true,
            },
            final_relieve_date: {
                date: true,
            },
            probation_date: {
                date: true,
            },
            confirmation_date: {
                date: true,
            },
        },
    });
}

if ($("#create_branch_bill_form,#edit_branch_bill_form").length > 0) {
    $("#create_branch_bill_form,#edit_branch_bill_form").validate({
        rules: {
            monthyear: {
                required: true,
            },
            branch_id: {
                required: true,
            },
            bill_amount: {
                required: true,
                number: true,
            },
            service_tax: {
                required: true,
                number: true,
            },
            recover_payment: {
                required: true,
                number: true,
            },
            extra_exp: {
                required: true,
                number: true,
            },
            service_charge: {
                required: true,
                number: true,
            },
            tds: {
                required: true,
                number: true,
            },
        },
    });
}

/* $('.scrollTo').click(function(){
    $('html, body').animate({
        scrollTop: $( $(this).attr('href') ).offset().top - 220
    }, 500);
    return false;
}); */