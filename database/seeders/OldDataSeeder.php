<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Carbon;

class OldDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {

        // Company

        $companies = DB::table('company')->get();

        foreach ($companies as $company) {
            DB::table('users')->insert([
                'name' => $company->company_name,
                'company_code' => $company->company_code,
                'email' => $company->email,
                'phone_no' => $company->phone,
                'contact_person_name' => $company->contact_person_name,
                'contact_person_phone_no' => $company->contact_person_phone_no,
                'address' => $company->company_address,
                'gst_no' => '',
                'pf_no' => '',
                'esic' => '',
                'police_registration_number' => '',
                'shop_and_establishment' => '',
                'professional_tax' => '',
                'msme_registaration_no' => '',
                'welfare_fund' => '',
                'password' => $company->password,
                'status' => 'active',
                'profile_photo' => '',
            ]);
        }

        // branch & branch work order

        $branches = DB::table('branch')->get();

        foreach ($branches as $branch) {
            DB::table('branches')->insert([
            
                'id' => $branch->branch_id,
                'name' => $branch->branch_name,
                'address'=> $branch->address,
                'pf' => $branch->pf,
                'professional_tax_req' => $branch->professional_tax_req,
                'professional_tax' => $branch->profetional_tax,
                'esic' => $branch->esi,
                'over_time_amount' => $branch->over_time_amount,
                'monthyear' => $branch->monthyear,
                'month' => '',
                'year' => '',
                'company_id' => $branch->company_id,
                'status' => 'active',
            ]);

            DB::table('branch_work_orders')->insert([
                'branch_id' => $branch->branch_id,
                'work_order' => $branch->workorder,
                'work_start_date' => now(),
                'work_expire_date' => now(),
                /* 'work_start_date' => $branch->start_work_date ? Carbon::createFromFormat(['d-m-Y', 'd/m/Y', 'Y/m/d'], $branch->start_work_date)->format('Y-m-d') : now(),
                'work_expire_date' => $branch->expiry_date ? Carbon::createFromFormat(['d-m-Y', 'd/m/Y', 'Y/m/d'], $branch->expiry_date)->format('Y-m-d') : now(), */
            ]); 
        }

        // Bank

        $banks = DB::table('bank')->get();

        foreach ($banks as $bank) {
            DB::table('banks')->insert([
                'id' => $bank->bank_id,
                'bank_name' => $bank->bank_name,
                'account_no' => $bank->ac_no,
                'company_id' => $bank->company_id,
                'status' => "active",
            ]);
        }

        // Bank Branch
        $bank_branches = DB::table('bank_branch')->get();

        foreach ($bank_branches as $bank_branch) {
            DB::table('bank_branches')->insert([
                'id' => $bank_branch->bank_branch_id,
                'bank_branch_name' => $bank_branch->bank_branch_name,
                'ifsc_code' => $bank_branch->ifsc_code,
                'address' => $bank_branch->address,
                'bank_id' => $bank_branch->bank_id,
                'company_id' => $bank_branch->company_id,
                'status' => "active",
            ]);
        }

        // Department

        $departments = DB::table('department')->get();

        foreach ($departments as $department) {
            DB::table('departments')->insert([
                'id' => $department->department_id,
                'department_name' => $department->department_name,
                'department_code' => $department->department_code,
                'company_id' => $department->company_id,
                'status' => "active",
            ]);
        }

        // designation

        $designations = DB::table('designation')->get();

        foreach ($designations as $designation) {
            DB::table('designations')->insert([
                'id' => $designation->designation_id,
                'designation_name' => $designation->designation_name,
                'category' => $designation->category,
                'company_id' => $designation->company_id,
                'status' => "active",
            ]);
        }

        // qualification

        $qualifications = DB::table('qualification')->get();

        foreach ($qualifications as $qualification) {
            DB::table('qualifications')->insert([
                'id' => $qualification->qualification_id,
                'qualification_name' => $qualification->qualification_name,
                'company_id' => $qualification->company_id,
                'status' => "active",
            ]);
        }

        // mother_tongues

        $mother_tongues = DB::table('mothertongue')->get();

        foreach ($mother_tongues as $mothertongue) {
            DB::table('mother_tongues')->insert([
                'id' => $mothertongue->mt_id,
                'language_name' => $mothertongue->language,
                'company_id' => $mothertongue->company_id,
                'status' => "active",
            ]);
        }

        // grades

        $grades = DB::table('grade')->get();

        foreach ($grades as $grade) {
            DB::table('grades')->insert([
                'id' => $grade->grade_id,
                'grade_name' => $grade->grade_name,
                'garde_code' => $grade->grade_code,
                'company_id' => $grade->company_id,
                'status' => "active",
            ]);
        }

        // countries

        $countries = DB::table('country')->get();

        foreach ($countries as $country) {
            DB::table('countries')->insert([
                'id' => $country->country_id,
                'country_name' => $country->country_name,
                'company_id' => $country->company_id,
                'status' => "active",
            ]);
        }

        // states

        $states = DB::table('state')->get();

        foreach ($states as $state) {
            DB::table('states')->insert([
                'id' => $state->state_id,
                'state_name' => $state->state_name,
                'country_id' => $state->country_id,
                'company_id' => $state->company_id,
                'status' => "active",
            ]);
        }

        // districts

        $districts = DB::table('district')->get();

        foreach ($districts as $district) {
            DB::table('districts')->insert([
                'id' => $district->district_id,
                'district_name' => $district->district_name,
                'state_id' => $district->state_id,
                'company_id' => $district->company_id,
                'status' => "active",
            ]);
        }

        // stock_categories

        $stock_categories = DB::table('stock_category')->get();

        foreach ($stock_categories as $stock_category) {
            DB::table('stock_categories')->insert([
                'id' => $stock_category->category_id,
                'stock_category_name' => $stock_category->category_name,
                'company_id' => $stock_category->company_id,
                'status' => "active",
            ]);
        }

        // stock_registers

        $stock_registers = DB::table('stock_register')->get();

        foreach ($stock_registers as $stock_register) {
            DB::table('stock_registers')->insert([
                'id' => $stock_register->stock_register_id,
                'stock_category_id' => $stock_register->stock_category_id,
                'stock_quantity' => $stock_register->stock_qty,
                'stock_in_date' => $stock_register->stock_in_date,
                'company_id' => $stock_register->company_id,
            ]);
        }

        // cities

        DB::table('cities')->insert([
            'id' => 1,
            'city_name' => 'palanpur',
            'district_id' => 1,
            'company_id' => 1,
            'status' => 'active',
        ]);

        // pincodes

        DB::table('pincodes')->insert([
            'id' => 1,
            'pincode' => '385001',
            'city_id' => 1,
            'company_id' => 1,
            'status' => 'active',
        ]);

        // walfers

        DB::table('walfers')->insert([
            'id' => 1,
            'walfare_amount' => '6.00',
            'company_id' => 1,
        ]);

        //banchdesignation

        $branchDesignations = DB::table('branch_designation')->get();

        foreach ($branchDesignations as $branchDesignation) {

            $branchExists = DB::table('branches')
                ->where('id', $branchDesignation->branch_id)
                ->exists();
            $designationExists = DB::table('designations')
                ->where('id', $branchDesignation->designation_id)
                ->exists();

            if ($branchExists && $designationExists) {
                DB::table('branch_designations')
                    ->insert([
                    'id' => $branchDesignation->branch_designation_id,
                    'branch_id' => $branchDesignation->branch_id,
                    'designation_id'=> $branchDesignation->designation_id,
                    'basic_fix' => "basic",
                    'basic_da' => $branchDesignation->basic + $branchDesignation->da,
                    'extra_allowance' => $branchDesignation->extra_allowance,
                    'hra' => null,
                    'bonus' => null,
                    'fix_salary' => null,
                    
                ]);
            }
        }

        // employees

        $employees = DB::table('registration')->get();

        foreach ($employees as $employee) {

            $branchExists = DB::table('branches')
                    ->where('id', $employee->branch_id)
                    ->exists();
            $designationExists = DB::table('designations')
                    ->where('id', $employee->designation_id)
                    ->exists();
            $departmentExists = DB::table('departments')
                    ->where('id', $employee->department_id)
                    ->exists();
            $gradeExists = DB::table('grades')
                    ->where('id', $employee->grade_id)
                    ->exists();
            $countryExists = DB::table('countries')
                    ->where('id', $employee->country_id)
                    ->exists();
            $stateExists = DB::table('states')
                    ->where('id', $employee->state_id)
                    ->exists();
            $districtExists = DB::table('districts')
                    ->where('id', $employee->district_id)
                    ->exists();
            $qualificationExists = DB::table('qualifications')
                    ->where('id', $employee->qualification_id)
                    ->exists();
            $bankExists = DB::table('banks')
                    ->where('id', $employee->bank_id)
                    ->exists();
            $bankbranchExists = DB::table('bank_branches')
                    ->where('id', $employee->bank_branch_id)
                    ->exists();
            $mothertongueExists = DB::table('mother_tongues')
                    ->where('id', $employee->mother_tongue)
                    ->exists();
            $companyExists = DB::table('users')
                ->where('id', $employee->company_id)
                ->exists();

            if ($branchExists && $designationExists && $departmentExists && $gradeExists && $countryExists && $stateExists && $districtExists && $qualificationExists && $bankExists && $bankbranchExists && $mothertongueExists && $companyExists) {

                DB::table('employees')->insert([
                'id' => $employee->registration_id,
                'registartion_date' => now(),
                'designation_id' => $employee->designation_id,
                'first_name' => $employee->first_name,
                'middle_name' => $employee->middle_name,
                'last_name' => $employee->last_name,
                'full_name' => $employee->full_name,
                'date_of_birth' => now(),
                'country_id' => $employee->country_id,
                'state_id' => $employee->state_id,
                'district_id' => $employee->district_id,
                'city_id' => 1,
                'pincode_id' => 1,
                'father_name' => $employee->father_name,
                'mother_name' => $employee->mother_name,
                'spouse_name' => $employee->spouse_name,
                'visible_distinguishing_mark_1' => $employee->visible_distinguishing_mark1,
                'visible_distinguishing_mark_2' => $employee->visible_distinguishing_mark2,
                'birth_place' => $employee->birth_place,
                'mother_tongue_id' => $employee->mother_tongue,
                'religion' => 'hindu',
                'marital_status' => $employee->marital_status,
                'height' => $employee->height,
                'weight' => $employee->weight,
                'blood_group' => 'b+',
                'sex' => $employee->experience != 0 ? 'male' : 'female',
                'qualification_id' => $employee->qualification_id,
                'local_address' => $employee->local_address,
                'permanent_address' => $employee->permanent_address,
                'local_phone_no' => $employee->local_phone_no,
                'permanent_phone_no' => $employee->permanent_phone_no,
                'permanent_mobile_no' => $employee->permanent_mobile_no,
                'salary' => 0,
                'experience' => $employee->experience != 0 ? 'no' : 'yes',
                'experience_company' => $employee->experience_company1,
                'police_varification_date' => now(),
                'police_station_name' => $employee->police_station_name,
                'citizen_of_india_by' => null,
                'black_list' => 'no',
                'black_list_reasone' => $employee->black_list_reasone,
                'election_card_no' => $employee->election_card,
                'driving_licenses_no' => $employee->driving_licenses_no,
                'rto' => $employee->rto,
                'rto_state_id' => $employee->rto_state_id,
                'passport_no' => $employee->passport_no,
                'pancard_no' => $employee->pancard_no,
                'aadhar_card_no' => $employee->aadhar_card_no,
                'reference_relation_contact_1' => $employee->reference_relation_contact1,
                'reference_relation_contact_2' => $employee->reference_relation_contact2,
                'reference_relation_contact_3' => $employee->reference_relation_contact3,
                'emergency_contact_name' => $employee->emergency_contact_name,
                'emergency_relation' => $employee->emergency_relation,
                'emergency_contact_no' => $employee->emergency_contact_no,
                'nominee_1' => $employee->nominee1,
                'birth_date_1' => now(),
                'relation_1' => $employee->relation1,
                'bank_id' => $employee->bank_id,
                'bank_branch_id' => $employee->bank_branch_id,
                'ifsc_code' => $employee->ifsc_code,
                'bank_ac_no' => $employee->bank_ac_no,
                'branch_id' => $employee->branch_id,
                'interview_date' => now(),
                'entry_date' => now(),
                'department_id' => $employee->department_id,
                'grade_id' => $employee->grade_id,
                'senior_citizen' => $employee->senior_citizen,
                'relieve_date' => now(),
                'final_relieve_date' => now(),
                'security_deposit' => $employee->security_deposit,
                'deposit_detail' => $employee->deposit_detail,
                'due_date' => now(),
                'refund_date' => now(),
                'refund_detail' => $employee->refund_detail,
                'probation_date' => now(),
                'confirmation_date' => now(),
                'police_varification' => $employee->police_varification,
                'leaving_certificate' => $employee->leaving_certificate,
                'bank_passbook' => null,
                'photo' => $employee->photo,
                'document' => $employee->document,
                'signature' => null,
                'company_id' => $employee->company_id,
                'status' => 'active',
                'month' => $employee->month_id,
                'year' => $employee->year_id,
                'election_card' => $employee->election_card,
                'bank_form' => $employee->bank_form,
                'monthyear' => $employee->monthyear,
                'pf_ac_code' => $employee->pf_ac_code,
                'pf_uan_code' => $employee->pf_uan_code,
                'leave_date' => now(),
                'rejoin_date' => now(),
                ]);
            }
        }

        //employee Branch

        $employeeBranches = DB::table('employee_branch')->get();

        foreach ($employeeBranches as $employeeBranch) {
           
                $branchExists = DB::table('branches')
                        ->where('id', $employeeBranch->branch_id)
                        ->exists();
                $designationExists = DB::table('designations')
                        ->where('id', $employeeBranch->designation_id)
                        ->exists();
                $employeeExists = DB::table('employees')
                        ->where('id', $employeeBranch->employee_code)
                        ->exists();
                $departmentExists = DB::table('departments')
                        ->where('id', $employeeBranch->department)
                        ->exists();
                $gradeExists = DB::table('grades')
                        ->where('id', $employeeBranch->grade_id)
                        ->exists();
                $companyExists = DB::table('users')
                    ->where('id', $employeeBranch->company_id)
                    ->exists();

            if ($branchExists && $designationExists && $employeeExists && $departmentExists && $gradeExists && $companyExists) {

                DB::table('employee_branches')->insert([
                'id' => $employeeBranch->employee_branch_id,
                'company_id' => $employeeBranch->company_id,
                'branch_id' => $employeeBranch->branch_id,
                'designation_id'=> $employeeBranch->designation_id,
                'employee_id'=> $employeeBranch->employee_code,
                'department_id'=> $employeeBranch->department,
                'grade_id'=> $employeeBranch->grade_id,
                'basic_fix' => "basic",
                'basic_da' => $employeeBranch->basic + $employeeBranch->da,
                'extra_allowance' => $employeeBranch->extra_allowance,
                'hra' => null,
                'bonus' => null,
                'fix_salary' => null,
                'monthyear' => $employeeBranch->monthyear,
                'month' => $employeeBranch->month_id,
                'year' => $employeeBranch->year_id,
                
                ]);
            }
        }

        // Branch Designation History
        
        $employeeBranches = DB::table('normal_branch')->get();

        foreach ($employeeBranches as $employeeBranch) {
           
                $branchExists = DB::table('branches')
                        ->where('id', $employeeBranch->branch_id)
                        ->exists();
                $designationExists = DB::table('designations')
                        ->where('id', $employeeBranch->designation_id)
                        ->exists();
                $companyExists = DB::table('users')
                    ->where('id', $employeeBranch->company_id)
                    ->exists();

            if ($branchExists && $designationExists && $companyExists) {
                
                DB::table('branch_designation_histories')->insert([

                'id' => $employeeBranch->normal_branch_id,
                'company_id' => $employeeBranch->company_id,
                'branch_id' => $employeeBranch->branch_id,
                'designation_id'=> $employeeBranch->designation_id,
                'basic_fix' => "basic",
                'basic_da' => $employeeBranch->basic + $employeeBranch->da,
                'extra_allowance' => $employeeBranch->extra_allowance,
                'hra' => null,
                'bonus' => null,
                'fix_salary' => null,
                'monthyear' => $employeeBranch->monthyear,
                'month' => $employeeBranch->monthyear,
                'year' => $employeeBranch->monthyear,
                
                ]);
            }
        }

        // Branch Tansfer
        
        $branchTransfers = DB::table('employee_branch')->get();

        foreach ($branchTransfers as $branchTransfer) {
           
            $branchExists = DB::table('branches')
                    ->where('id', $branchTransfer->branch_id)
                    ->exists();
            $designationExists = DB::table('designations')
                    ->where('id', $branchTransfer->designation_id)
                    ->exists();
            $employeeExists = DB::table('employees')
                    ->where('id', $branchTransfer->employee_code)
                    ->exists();
            $departmentExists = DB::table('departments')
                    ->where('id', $branchTransfer->department)
                    ->exists();
            $gradeExists = DB::table('grades')
                    ->where('id', $branchTransfer->grade_id)
                    ->exists();
            $companyExists = DB::table('users')
                ->where('id', $branchTransfer->company_id)
                ->exists();

            if ($branchExists && $designationExists && $employeeExists && $departmentExists && $gradeExists && $companyExists) {
        
                DB::table('branch_transfers')->insert([
                'id' => $branchTransfer->employee_branch_id,
                'branch_id' => $branchTransfer->branch_id,
                'employee_id'=> $branchTransfer->employee_code,
                'company_id' => $branchTransfer->company_id,
                'designation_id' => $branchTransfer->designation_id,
                'department_id' => $branchTransfer->department,
                'grade_id' => $branchTransfer->grade_id,
                'created_date' => now(),
                'updated_month' => $branchTransfer->monthyear,
                'basic_fix' => "basic",
                'transfer' => $branchTransfer->transfer == 'T' ? 'transfer' : 'current',
                'basic_da' => $branchTransfer->basic + $branchTransfer->da,
                'extra_allowance' => $branchTransfer->extra_allowance,
                'hra' => null,
                'bonus' => null,
                'fix_salary' => null,
                'monthyear' => $branchTransfer->monthyear,
                'month' => $branchTransfer->month_id,
                'year' => $branchTransfer->year_id,
                
                ]);
            } 
        }

    }
}