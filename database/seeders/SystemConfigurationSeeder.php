<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SystemConfigurationSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('system_configurations')->insert([
            'logo' => '',
            'Favicon' => '',
            'title' => 'Vir Security', 
            'address' => 'Palanpur, Banaskantha', 
            'mobile' => '1234567890',
            'email' => 'virsecurity@gmail.com',
            'footer' => 'Vir Security All Rights Reserved',
        ]);
    }
}
