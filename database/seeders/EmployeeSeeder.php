<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;


class EmployeeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('employees')->insert([
            'registartion_date' => '2022-10-10',
            'designation_id' => 4,
            'first_name' => 'bhavesh',
            'middle_name' => 'p',
            'last_name' => 'patel',
            'full_name' => 'bhavesh p patel',
            'date_of_birth' => '1994-08-02',
            'country_id' => 1,
            'state_id' => 1,
            'district_id' => 1,
            'city_id' => 1,
            'pincode_id' => 1,
            'mother_tongue_id' => 1,
            'religion' => 'hindu',
            'marital_status' => 'married',
            'blood_group' => 'a+',
            'sex' => 'male',
            'qualification_id' => 1,
            'local_address'=> 'motera',
            'permanent_address'=> 'ahmedabad',
            'permanent_phone_no'=> '3214657890',
            'salary'=> '10000',
            'relation_1' => 'test',
            'refund_detail' => 'test',
            'reference_relation_contact_2' => '3214659870',
            'birth_date_1'=> '1994-08-02',
            'relieve_date'=> '2024-08-02',
            'probation_date'=> '2024-08-02',
            'company_id' => 1,
            'status' => 'active',
            'bank_id' => 1,
            'bank_branch_id' => 1,
            'branch_id' => 1,
            'department_id' => 1,
            'grade_id'=> 1
        ]);

        DB::table('employees')->insert([
            'registartion_date' => '2022-10-10',
            'designation_id' => 4,
            'first_name' => 'rajiv',
            'middle_name' => 'p',
            'last_name' => 'patel',
            'full_name' => 'rajiv p patel',
            'date_of_birth' => '1994-08-02',
            'country_id' => 1,
            'state_id' => 1,
            'district_id' => 1,
            'city_id' => 1,
            'pincode_id' => 1,
            'mother_tongue_id' => 1,
            'religion' => 'hindu',
            'marital_status' => 'married',
            'blood_group' => 'a+',
            'sex' => 'male',
            'qualification_id' => 1,
            'local_address'=> 'motera',
            'permanent_address'=> 'ahmedabad',
            'permanent_phone_no'=> '3214657890',
            'salary'=> '10000',
            'relation_1' => 'test',
            'refund_detail' => 'test',
            'reference_relation_contact_2' => '3214659870',
            'birth_date_1'=> '1994-08-02',
            'relieve_date'=> '2024-08-02',
            'probation_date'=> '2024-08-02',
            'company_id' => 1,
            'status' => 'active',
            'bank_id' => 1,
            'bank_branch_id' => 1,
            'branch_id' => 1,
            'department_id' => 1,
            'grade_id'=> 1
        ]);
    }
}
