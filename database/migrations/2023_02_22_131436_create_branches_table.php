<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branches', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('name');
            $table->longText('address');
            $table->decimal('pf', 10, 2)->default(0);
            $table->enum('professional_tax_req', ['yes','no']);
            $table->integer('professional_tax')->default(0)->nullable();
            $table->decimal('esic', 10, 2)->default(0);
            $table->decimal('over_time_amount', 10, 2)->default(0);
            $table->string('monthyear');
            $table->string('month');
            $table->string('year');
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('status', ['active','inactive']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branches');
    }
}
