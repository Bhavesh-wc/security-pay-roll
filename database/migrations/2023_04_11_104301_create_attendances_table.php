<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAttendancesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('attendances', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->foreignId('employee_id')->constrained('employees')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('designation_id')->constrained('designations')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('branch_id')->constrained('branches')->onDelete('restrict')->onUpdate('cascade');
            $table->string('monthyear');
            $table->enum('day_1', ['P','A','H'])->nullable();
            $table->enum('day_2', ['P','A','H'])->nullable();
            $table->enum('day_3', ['P','A','H'])->nullable();
            $table->enum('day_4', ['P','A','H'])->nullable();
            $table->enum('day_5', ['P','A','H'])->nullable();
            $table->enum('day_6', ['P','A','H'])->nullable();
            $table->enum('day_7', ['P','A','H'])->nullable();
            $table->enum('day_8', ['P','A','H'])->nullable();
            $table->enum('day_9', ['P','A','H'])->nullable();
            $table->enum('day_10', ['P','A','H'])->nullable();
            $table->enum('day_11', ['P','A','H'])->nullable();
            $table->enum('day_12', ['P','A','H'])->nullable();
            $table->enum('day_13', ['P','A','H'])->nullable();
            $table->enum('day_14', ['P','A','H'])->nullable();
            $table->enum('day_15', ['P','A','H'])->nullable();
            $table->enum('day_16', ['P','A','H'])->nullable();
            $table->enum('day_17', ['P','A','H'])->nullable();
            $table->enum('day_18', ['P','A','H'])->nullable();
            $table->enum('day_19', ['P','A','H'])->nullable();
            $table->enum('day_20', ['P','A','H'])->nullable();
            $table->enum('day_21', ['P','A','H'])->nullable();
            $table->enum('day_22', ['P','A','H'])->nullable();
            $table->enum('day_23', ['P','A','H'])->nullable();
            $table->enum('day_24', ['P','A','H'])->nullable();
            $table->enum('day_25', ['P','A','H'])->nullable();
            $table->enum('day_26', ['P','A','H'])->nullable();
            $table->enum('day_27', ['P','A','H'])->nullable();
            $table->enum('day_28', ['P','A','H'])->nullable();
            $table->enum('day_29', ['P','A','H'])->nullable();
            $table->enum('day_30', ['P','A','H'])->nullable();
            $table->enum('day_31', ['P','A','H'])->nullable();
            $table->string('total');
            $table->string('total_present');
            $table->string('total_absent');
            $table->string('total_holiday');
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->timestamps();
            $table->softDeletes();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attendances');
    }
}
