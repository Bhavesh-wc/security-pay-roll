<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->timestamps();
            $table->softDeletes();
            $table->string('name');
            $table->string('company_code')->nullable();
            $table->string('email')->unique();
            $table->string('phone_no')->nullable();
            $table->string('contact_person_name')->nullable();
            $table->string('contact_person_phone_no')->nullable();
            $table->longText('address')->nullable();
            $table->string('gst_no')->nullable();
            $table->string('pf_no')->nullable();
            $table->string('esic')->nullable();
            $table->string('police_registration_number')->nullable();
            $table->string('shop_and_establishment')->nullable();
            $table->string('professional_tax')->nullable();
            $table->string('msme_registaration_no')->nullable();
            $table->string('welfare_fund')->nullable();
            $table->string('password');
            $table->enum('status', ['active','inactive'])->nullable();
            $table->string('profile_photo')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->rememberToken();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
};
