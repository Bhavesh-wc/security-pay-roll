<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCreditSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('credit_salaries', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('monthyear')->nullable();
            $table->foreignId('branch_id')->constrained('branches')->onDelete('restrict')->onUpdate('cascade');
            $table->string('cheque_no')->nullable();
            $table->string('account_no')->nullable();
            $table->foreignId('bank_id')->constrained('company_banks')->onDelete('restrict')->onUpdate('cascade');
            $table->string('bank_branch_name');
            $table->date('credit_date')->nullable();
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('credit_salaries');
    }
}
