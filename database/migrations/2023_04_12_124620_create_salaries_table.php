<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('salaries', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('branch_id')->constrained('branches')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('designation_id')->constrained('designations')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('department_id')->constrained('departments')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('grade_id')->constrained('grades')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('employee_id')->constrained('employees')->onDelete('restrict')->onUpdate('cascade');
            $table->string('monthyear')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->date('salary_date')->nullable();
            $table->integer('working_days')->nullable();
            $table->integer('no_of_days_work_done')->nullable();
            $table->integer('no_of_leave')->nullable();
            $table->decimal('basic_da', 10,2)->default(0);
            $table->decimal('extra_allowance', 10,2)->default(0);
            $table->decimal('hra', 10,2)->default(0);
            $table->decimal('bonus', 10,2)->default(0);
            $table->decimal('fix_salary', 10,2)->default(0);
            $table->decimal('sal_advance', 10,2)->default(0);
            $table->decimal('a', 10,2)->default(0);
            $table->decimal('b', 10,2)->default(0);
            $table->decimal('c', 10,2)->default(0);
            $table->decimal('ot_amount', 10,2)->default(0);
            $table->integer('ot_days')->nullable();
            $table->decimal('attendance_allowance', 10,2)->default(0);
            $table->decimal('dress', 10,2)->default(0);
            $table->decimal('mess', 10,2)->default(0);
            $table->decimal('pf', 10,2)->default(0);
            $table->decimal('profetional_tax', 10,2)->default(0);
            $table->decimal('basis_wages', 10,2)->default(0);
            $table->decimal('esic', 10,2)->default(0);
            $table->decimal('walfare_amount', 10,2)->default(0);
            $table->decimal('damage_amount', 10,2)->default(0);
            $table->decimal('fines', 10,2)->default(0);
            $table->decimal('total', 10,2)->default(0);
            $table->decimal('nettotal', 10,2)->default(0);
            $table->decimal('branch_ot_amount', 10,2)->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('salaries');
    }
}
