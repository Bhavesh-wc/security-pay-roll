<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBranchDesignationHistoriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_designation_histories', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('branch_id')->constrained('branches')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('designation_id')->constrained('designations')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('basic_fix', ['basic','fix']);
            $table->integer('basic_da')->default(0)->nullable();
            $table->integer('extra_allowance')->default(0)->nullable();
            $table->integer('hra')->default(0)->nullable();
            $table->integer('bonus')->default(0)->nullable();
            $table->integer('fix_salary')->default(0)->nullable();
            $table->string('monthyear');
            $table->string('month');
            $table->string('year');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branch_designation_histories');
    }
}
