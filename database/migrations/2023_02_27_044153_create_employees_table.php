<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employees', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->date('registartion_date');
            $table->integer('designation_id')->constrained('designations')->onDelete('restrict')->onUpdate('cascade');
            $table->string('first_name');
            $table->string('middle_name');
            $table->string('last_name');
            $table->string('full_name');
            $table->date('date_of_birth');
            $table->foreignId('country_id')->constrained('countries')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('state_id')->constrained('states')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('district_id')->constrained('districts')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('city_id')->constrained('cities')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('pincode_id')->constrained('pincodes')->onDelete('restrict')->onUpdate('cascade');
            $table->string('father_name')->nullable();
            $table->string('mother_name')->nullable();
            $table->string('spouse_name')->nullable();
            $table->string('visible_distinguishing_mark_1')->nullable();
            $table->string('visible_distinguishing_mark_2')->nullable();
            $table->string('birth_place')->nullable();
            $table->foreignId('mother_tongue_id')->constrained('mother_tongues')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('religion', ['hindu','muslim','christian','sikh']);
            $table->enum('marital_status', ['married','unmarried','seperate']);
            $table->string('height')->nullable();
            $table->string('weight')->nullable();
            $table->enum('blood_group', ['a+','a-','b+','b-','o+','o-','ab+','ab-']);
            $table->enum('sex', ['male','female']);
            $table->foreignId('qualification_id')->constrained('qualifications')->onDelete('restrict')->onUpdate('cascade');
            $table->longText('local_address');
            $table->longText('permanent_address');
            $table->string('local_phone_no')->nullable();
            $table->string('permanent_phone_no');
            $table->string('permanent_mobile_no')->nullable();
            $table->integer('salary')->default(0);
            $table->enum('experience', ['yes','no'])->nullable();
            $table->string('experience_company')->nullable();
            $table->date('police_varification_date')->nullable();
            $table->string('police_station_name')->nullable();
            $table->enum('citizen_of_india_by', ['indian birth','descent','registration','naturalization'])->nullable()->index();
            $table->enum('black_list', ['yes','no'])->nullable();
            $table->longText('black_list_reasone')->nullable();
            $table->string('election_card_no')->nullable();
            $table->string('driving_licenses_no')->nullable();
            $table->string('rto')->nullable();
            $table->foreignId('rto_state_id')->nullable();
            $table->string('passport_no')->nullable();
            $table->string('pancard_no')->nullable();
            $table->string('aadhar_card_no')->nullable();
            $table->string('reference_relation_contact_1')->nullable();
            $table->string('reference_relation_contact_2')->nullable();
            $table->string('reference_relation_contact_3')->nullable();
            $table->string('emergency_contact_name')->nullable();
            $table->string('emergency_relation')->nullable();
            $table->string('emergency_contact_no')->nullable();
            $table->string('nominee_1')->nullable();
            $table->date('birth_date_1')->nullable();
            $table->string('relation_1')->nullable();
            $table->foreignId('bank_id')->nullable()->constrained('banks')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('bank_branch_id')->nullable()->constrained('bank_branches')->onDelete('restrict')->onUpdate('cascade');
            $table->string('ifsc_code')->nullable();
            $table->string('bank_ac_no')->nullable();
            $table->foreignId('branch_id')->constrained('branches')->onDelete('restrict')->onUpdate('cascade');
            $table->date('interview_date')->nullable();
            $table->date('entry_date')->nullable();
            $table->foreignId('department_id')->nullable()->constrained('departments')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('grade_id')->nullable()->constrained('grades')->onDelete('restrict')->onUpdate('cascade');
            $table->string('senior_citizen')->nullable();
            $table->date('relieve_date')->nullable();
            $table->string('final_relieve_date')->nullable();
            $table->string('security_deposit')->nullable();
            $table->longText('deposit_detail')->nullable();
            $table->date('due_date')->nullable();
            $table->date('refund_date')->nullable();
            $table->longText('refund_detail')->nullable();
            $table->date('probation_date')->nullable();
            $table->date('confirmation_date')->nullable();
            $table->string('police_varification')->nullable();
            $table->string('leaving_certificate')->nullable();
            $table->string('bank_passbook')->nullable();
            $table->string('photo')->nullable();
            $table->string('document')->nullable();
            $table->string('signature')->nullable();
            $table->string('month')->nullable();
            $table->string('year')->nullable();
            $table->string('election_card')->nullable();
            $table->string('bank_form')->nullable();
            $table->string('monthyear')->nullable();
            $table->string('pf_ac_code')->nullable();
            $table->string('pf_uan_code')->nullable();
            $table->date('leave_date')->nullable();
            $table->date('rejoin_date')->nullable();
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('status', ['active','inactive']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employees');
    }
}
