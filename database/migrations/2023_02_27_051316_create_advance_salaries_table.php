<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateAdvanceSalariesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('advance_salaries', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('monthyear');
            $table->string('month');
            $table->string('year');
            $table->foreignId('branch_id')->constrained('branches')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('employee_id')->constrained('employees')->onDelete('restrict')->onUpdate('cascade');
            $table->decimal('wages_payable', 10, 2)->default(0);
            $table->date('date_of_advance');
            $table->decimal('amount')->nullable();
            $table->longText('purpose_of_advance');
            $table->longText('remarks');
            $table->integer('stock_category_id')->nullable();
            $table->integer('no_of_qty')->nullable();
            $table->enum('advance_for', ['Advance - 1','Advance - 2']);
            $table->foreignId('designation_id')->constrained('designations')->onDelete('restrict')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('advance_salaries');
    }
}
