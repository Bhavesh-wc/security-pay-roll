<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCompanyBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('company_banks', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->string('bank_name');
            $table->string('account_no');
            $table->string('bank_branch_name');
            $table->string('ifsc_code');
            $table->longText('address');
            $table->enum('status', ['active','inactive']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('company_banks');
    }
}
