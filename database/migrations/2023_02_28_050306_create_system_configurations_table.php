<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateSystemConfigurationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_configurations', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->string('logo');
            $table->string('Favicon');
            $table->string('title');
            $table->longText('address');
            $table->string('mobile');
            $table->string('email');
            $table->string('footer');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('system_configurations');
    }
}
