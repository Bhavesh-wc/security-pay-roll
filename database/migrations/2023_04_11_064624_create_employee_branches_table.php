<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateEmployeeBranchesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('employee_branches', function(Blueprint $table)
        {
            $table->increments('id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('branch_id')->constrained('branches')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('designation_id')->constrained('designations')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('employee_id')->constrained('employees')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('department_id')->constrained('departments')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('grade_id')->constrained('grades')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('basic_fix', ['basic','fix']);
            $table->integer('basic_da')->default(0)->nullable();
            $table->integer('extra_allowance')->default(0)->nullable();
            $table->integer('hra')->default(0)->nullable();
            $table->integer('bonus')->default(0)->nullable();
            $table->integer('fix_salary')->default(0)->nullable();
            $table->string('monthyear');
            $table->string('month');
            $table->string('year');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('employee_branches');
    }
}
