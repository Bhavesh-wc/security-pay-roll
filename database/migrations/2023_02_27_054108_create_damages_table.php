<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateDamagesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('damages', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('monthyear');
            $table->string('month');
            $table->string('year');
            $table->foreignId('branch_id')->constrained('branches')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('employee_id')->constrained('employees')->onDelete('restrict')->onUpdate('cascade');
            $table->longText('damage_details');
            $table->date('damage_date');
            $table->decimal('damage_amount', 10, 2)->default(0);
            $table->longText('employee_reason_for_deduction');
            $table->string('name_of_person_explanation_heard');
            $table->integer('total_installments');
            $table->date('installment_date');
            $table->decimal('installment_amount', 10, 2)->default(0);
            $table->longText('remarks');
            $table->foreignId('designation_id')->constrained('designations')->onDelete('restrict')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('damages');
    }
}
