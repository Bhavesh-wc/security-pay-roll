<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBranchBillsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('branch_bills', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->string('monthyear');
            $table->foreignId('branch_id')->constrained('branches')->onDelete('restrict')->onUpdate('cascade');
            $table->decimal('net_bill_amt', 10,2)->default(0);
            $table->decimal('bill_amount', 10,2)->default(0);
            $table->decimal('service_tax', 10,2)->default(0);
            $table->decimal('gross_amount', 10,2)->default(0);
            $table->decimal('service_charge', 10,2)->default(0);
            $table->decimal('pf_amt_12', 10,2)->default(0);
            $table->decimal('esi_amt', 10,2)->default(0);
            $table->decimal('leave_amt', 10,2)->default(0);
            $table->decimal('bonus_amt', 10,2)->default(0);
            $table->decimal('recover_payment', 10,2)->default(0);
            $table->decimal('professional_tax', 10,2)->default(0);
            $table->decimal('other_amt', 10,2)->default(0);
            $table->decimal('extra_exp', 10,2)->default(0);
            $table->decimal('tds', 10,2)->default(0);
            $table->decimal('net_amt', 10,2)->default(0);
            $table->decimal('gross_salary', 10,2)->default(0);
            $table->decimal('payroll_gross', 10,2)->default(0);
            $table->decimal('diff_amt', 10,2)->default(0);
            $table->decimal('salary_paid', 10,2)->default(0);
            $table->decimal('profit', 10,2)->default(0);
            $table->decimal('net_profit', 10,2)->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('branch_bills');
    }
}
