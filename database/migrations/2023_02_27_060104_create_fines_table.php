<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateFinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('fines', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('monthyear');
            $table->string('month');
            $table->string('year');
            $table->foreignId('branch_id')->constrained('branches')->onDelete('restrict')->onUpdate('cascade');
            $table->foreignId('employee_id')->constrained('employees')->onDelete('restrict')->onUpdate('cascade');
            $table->longText('act_for_which_fine_impossed');
            $table->date('date_of_offence');
            $table->longText('reasone_against_fine');
            $table->string('name_of_person_explanation_heard');
            $table->string('wages_payable');
            $table->decimal('fine_amount', 10, 2)->default(0);
            $table->date('fine_released_date');
            $table->decimal('fine_released_amount', 10, 2)->default(0);
            $table->longText('remarks');
            $table->foreignId('designation_id')->constrained('designations')->onDelete('restrict')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('fines');
    }
}
