<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateBanksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('banks', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('bank_name');
            $table->string('account_no')->nullable();
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('status', ['active','inactive']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('banks');
    }
}
