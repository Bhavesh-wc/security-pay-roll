<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateTaxManagersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tax_managers', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('monthyear')->unique()->nullable();
            $table->decimal('employee_pf', 10, 2)->default(0);
            $table->decimal('employee_admin', 10, 2)->default(0);
            $table->decimal('employer_pf', 10, 2)->default(0);
            $table->decimal('employer_fpf', 10, 2)->default(0);
            $table->decimal('employer_edli', 10, 2)->default(0);
            $table->decimal('employer_inepc', 10, 2)->default(0);
            $table->decimal('employee_esic', 10, 2)->default(0);
            $table->decimal('employer_esic', 10, 2)->default(0);
            $table->decimal('professional_tax')->default(0);
            $table->decimal('min_salary')->default(0);
            $table->decimal('welfare_fund', 10, 2)->default(0);
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('status', ['active','inactive']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('tax_managers');
    }
}
