<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateOverTimeDaysTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('over_time_days', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->foreignId('over_time_id')->constrained('over_times')->onDelete('restrict')->onUpdate('cascade');
            $table->date('over_time_date');
            $table->decimal('over_time_amount', 10, 2)->default(0);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('over_time_days');
    }
}
