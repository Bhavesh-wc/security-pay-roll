<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateCountriesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('countries', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->string('country_name');
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');
            $table->enum('status', ['active','inactive']);

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('countries');
    }
}
