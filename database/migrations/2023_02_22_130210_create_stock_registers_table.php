<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStockRegistersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('stock_registers', function(Blueprint $table)
        {
            $table->bigIncrements('id');
            $table->timestamps();
            $table->softDeletes();
            $table->foreignId('stock_category_id')->constrained('stock_categories')->onDelete('restrict')->onUpdate('cascade');
            $table->integer('stock_quantity');
            $table->date('stock_in_date');
            $table->string('balance_code')->nullable();
            $table->foreignId('company_id')->constrained('users')->onDelete('restrict')->onUpdate('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('stock_registers');
    }
}
