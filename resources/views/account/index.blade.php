@include('layouts.partials.header')
	<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
		<div class="d-flex flex-column flex-column-fluid">
			<div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
				<div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
					<div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
						<h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Account Settings</h1>
						<ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
							<li class="breadcrumb-item text-muted">
								<a href="{{route('home')}}" class="text-muted text-hover-primary">Home</a>
							</li>
							<li class="breadcrumb-item">
								<span class="bullet bg-gray-400 w-5px h-2px"></span>
							</li>
							<li class="breadcrumb-item text-muted">Account</li>
						</ul>
					</div>
				</div>
			</div>
			<div id="kt_app_content" class="app-content flex-column-fluid">
				<div id="kt_app_content_container" class="app-container container-xxl">
					@if(Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{!! \Session::get('success_message') !!}</strong>
                            <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
					<div class="card mb-5 mb-xl-10">
						<div class="card-header border-0 cursor-pointer" role="button" data-bs-toggle="collapse" data-bs-target="#kt_account_profile_details" aria-expanded="true" aria-controls="kt_account_profile_details">
							<div class="card-title m-0">
								<h3 class="fw-bold m-0">Profile Details</h3>
							</div>
						</div>
						<div id="kt_account_settings_profile_details" class="collapse show">
							<form  class="form" method="POST" action="{{ route('settings.update') }}" enctype="multipart/form-data">
					        @csrf
					        @method('PUT')
								<div class="card-body border-top p-9">
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Profile Image</label>
										<div class="col-lg-8">
											<div class="image-input image-input-outline {{ isset($info) && $info->avatar ? '' : 'image-input-empty' }}" data-kt-image-input="true" style="background-image: url({{ asset('admin/dist/assets/media/svg/avatars/blank.svg') }})">
												<div class="image-input-wrapper w-125px h-125px" style="background-image: {{ isset($info) && $info->avatar_url ? 'url('.asset($info->avatar_url).')' : 'none' }};"></div>
												<label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
													<i class="bi bi-pencil-fill fs-7"></i>
													<input type="file" name="avatar" accept=".png, .jpg, .jpeg" />
													<input type="hidden" name="avatar_remove" />
												</label>
												<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
													<i class="bi bi-x fs-2"></i>
												</span>
												<span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
													<i class="bi bi-x fs-2"></i>
												</span>
											</div>
											<div class="form-text">Allowed file types: png, jpg, jpeg.</div>
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label required fw-semibold fs-6">Name</label>
										<div class="col-lg-8">
											<div class="row">
												<!--begin::Col-->
												<div class="col-lg-8 fv-row">
													<input type="text" name="name" class="form-control form-control-lg form-control-solid" placeholder="Your Name" value="{{ old('name', auth()->user()->name ?? '') }}" />
												</div>
											</div>
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Company Code</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="company_code" class="form-control form-control-lg form-control-solid" placeholder="Company Code" value="{{ old('company_code', auth()->user()->company_code ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Contact Person Name</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="contact_person_name" class="form-control form-control-lg form-control-solid" placeholder="Contact Person Name" value="{{ old('contact_person_name', auth()->user()->contact_person_name ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">
											Contact Phone
										</label>
										<div class="col-lg-8 fv-row">
											<input type="number" name="contact_person_phone_no" class="form-control form-control-lg form-control-solid" placeholder="Phone number" value="{{ old('contact_person_phone_no', auth()->user()->contact_person_phone_no ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">GST No</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="gst_no" class="form-control form-control-lg form-control-solid" placeholder="GST No" value="{{ old('gst_no', auth()->user()->gst_no ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">PF No</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="pf_no" class="form-control form-control-lg form-control-solid" placeholder="PF No" value="{{ old('pf_no', auth()->user()->pf_no ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Esic</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="esic" class="form-control form-control-lg form-control-solid" placeholder="Esic" value="{{ old('esic', auth()->user()->esic ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Police Registration Number</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="police_registration_number" class="form-control form-control-lg form-control-solid" placeholder="Police Registration Number" value="{{ old('police_registration_number', auth()->user()->police_registration_number ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Shop And Establishment</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="shop_and_establishment" class="form-control form-control-lg form-control-solid" placeholder="Shop And Establishment" value="{{ old('shop_and_establishment', auth()->user()->shop_and_establishment ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Professional Tax</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="professional_tax" class="form-control form-control-lg form-control-solid" placeholder="Professional Tax" value="{{ old('professional_tax', auth()->user()->professional_tax ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">MSME Registaration No</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="msme_registaration_no" class="form-control form-control-lg form-control-solid" placeholder="MSME Registaration No" value="{{ old('msme_registaration_no', auth()->user()->msme_registaration_no ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Welfare Fund</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="welfare_fund" class="form-control form-control-lg form-control-solid" placeholder="Welfare Fund" value="{{ old('welfare_fund', auth()->user()->welfare_fund ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Email</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="email" class="form-control form-control-lg form-control-solid" placeholder="Email Address" value="{{ old('email', auth()->user()->email ?? '') }}" />
										</div>
									</div>
								</div>
								<div class="card-footer d-flex justify-content-end py-6 px-9">
									<button type="reset" class="btn btn-light btn-active-light-primary me-2">Discard</button>
									<button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
								</div>
							</form>
						</div>
					</div>
					
				</div>
			</div>
		</div>
	</div>
@include('layouts.partials.footer')
