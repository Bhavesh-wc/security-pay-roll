@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Reports</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">Welcome to Vir Security & Manpower Service Report</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-xxl">
                    <div class="row g-5 g-xl-10">
                        <div class="col-md-12 mb-xl-5">              
                            <div class="card card-flush h-xl-70">   
                                <div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-150px" style="background-color: #c9cd8c" data-bs-theme="light">
                                    <h3 class="card-title align-items-start flex-column text-dark pt-5">
                                        <span class="fw-bold fs-2x">Attendance</span>    
                                    </h3>
                                </div>
                                <div class="card-body mt-n20">
                                    <div class="mt-n20 position-relative">
                                        <div class="row g-3 g-lg-6" id="attendance">
                                            <a href="{{ route('import') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-4 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/attendance.png') }}" class="w-25px h-25px mb-2" alt="" />              
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Import Attendance (Excel)</span>
                                                    </div>
                                                </div>    
                                            </a>    
                                            <a href="{{ route('attendance_report') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/attendance.png') }}" class="w-25px h-25px mb-2" alt="" />                 
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Branchwise Attendance (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>   
                                            <a href="{{ route('export') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/attendance.png') }}" class="w-25px h-25px mb-2" alt="" />                 
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Branchwise Attendance (Excel)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('delete_attendance') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/attendance.png') }}" class="w-25px h-25px mb-2" alt="" />          
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Branchwise Attendance (Delete)</span>
                                                    </div>
                                                </div>    
                                            </a>       
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                    <div class="row g-5 g-xl-10">
                        <div class="col-md-12 mb-xl-5">              
                            <div class="card card-flush h-xl-70">   
                                <div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-150px" style="background-color: #e9c5bf" data-bs-theme="light">
                                    <h3 class="card-title align-items-start flex-column text-dark pt-5">
                                        <span class="fw-bold fs-2x">Salary</span>    
                                    </h3>
                                </div>
                                <div class="card-body mt-n20" id="salary">
                                    <div class="mt-n20 position-relative">
                                        <div class="row g-3 g-lg-6">
                                            <a href="{{ route('import_salary_excel') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-4 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                       <img src="{{ asset('/images/salary.png') }}" class="w-25px h-25px mb-2" alt="" />                
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Import Salary (Excel)</span>
                                                    </div>
                                                </div>    
                                            </a>    
                                            <a href="{{ route('branch_wise_salary') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/salary.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Branchwise Salary (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>   
                                            <a href="{{ route('export_salary_excel') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/salary.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Branchwise Salary (Excel)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('delete_salary') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/salary.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Branchwise Salary (Delete)</span>
                                                    </div>
                                                </div>    
                                            </a>   
                                            <a href="{{ route('employee_month_salary') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/salary.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Salary (CSV)</span>
                                                    </div>
                                                </div>    
                                            </a>   
                                            <a href="{{ route('male_female_salary') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/salary.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Male / Female Salary (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>     
                                            <a href="{{ route('wage_slip') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/salary.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Wage Slip (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>     
                                            <a href="{{ route('month_wage_slip') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/salary.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Month To Month Wage Slip (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>     
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                    <div class="row g-5 g-xl-10">
                        <div class="col-md-12 mb-xl-5">              
                            <div class="card card-flush h-xl-70">   
                                <div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-150px" style="background-color: #94bec1" data-bs-theme="light">
                                    <h3 class="card-title align-items-start flex-column text-dark pt-5">
                                        <span class="fw-bold fs-2x">Employee</span>    
                                    </h3>
                                </div>
                                <div class="card-body mt-n20" id="employee">
                                    <div class="mt-n20 position-relative">
                                        <div class="row g-3 g-lg-6">
                                            <a href="{{ route('employees.employee.leftemp') }}" class="col-4 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-4 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/employee.png') }}" class="w-25px h-25px mb-2" alt="" />                 
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Employee Left (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('upload_pf_code') }}" class="col-4 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/employee.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Import PF AC Code / PF UAN Code (Excel)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('appliction_form') }}" class="col-4 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/form.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Application Form (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>   
                                            <a href="{{ route('goverment_form') }}" class="col-4 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/form.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Goverment Form (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('icard') }}" class="col-4 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/id-card.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">I-card (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>   
                                            <a href="{{ route('identity_card_register') }}" class="col-4 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/id-card.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Identity Card Register (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>   
                                            <a href="{{ route('employed_by_contractor_register') }}" class="col-4 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/emp-contract.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Employed By Contractor (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('police_verification_register') }}" class="col-4 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/police-verification.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Police Verification (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a> 
                                            <a href="{{ route('police_verification_for_leave_register') }}" class="col-4 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/police-verification.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Police Verification For Leave (PDF and Excel)</span>
                                                    </div>
                                                </div>    
                                            </a> 
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                    <div class="row g-5 g-xl-10">
                        <div class="col-md-12 mb-xl-5">              
                            <div class="card card-flush h-xl-70">   
                                <div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-150px" style="background-color: #546370" data-bs-theme="light">
                                    <h3 class="card-title align-items-start flex-column text-white pt-5">
                                        <span class="fw-bold fs-2x">MISC</span>    
                                    </h3>
                                </div>
                                <div class="card-body mt-n20" id="misc">
                                    <div class="mt-n20 position-relative">
                                        <div class="row g-3 g-lg-6">
                                            <a href="{{ route('branchwise_profesional_tax') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-4 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/tax.png') }}" class="w-25px h-25px mb-2" alt="" />                 
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Profesional Tax (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>    
                                            <a href="{{ route('all_professional_tax_list') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/tax.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">All Professional Tax (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a> 
                                            <a href="{{ route('register_of_walfare_report') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/welfare.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Welfare (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('branch_wise_pf_statement') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/statement.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">PF Statement (PDF & Excel)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('bonus_report') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/bonus.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Bonus (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('bonus_report_monthly') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/bonus.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Month To Month Bonus (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('esic') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/esic.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">ESIC (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>
                                            <a href="{{ route('stock') }}" class="col-3 hover-scale">
                                                <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                    <div class="symbol symbol-30px me-5 mb-2">
                                                        <img src="{{ asset('/images/stock.png') }}" class="w-25px h-25px mb-2" alt="" />                  
                                                    </div>
                                                    <div class="m-0">
                                                        <span class="text-gray-700 fw-bolder d-block h3 lh-1 ls-n1 mb-1">Month To Month Stock (PDF)</span>
                                                    </div>
                                                </div>    
                                            </a>       
                                        </div>
                                    </div>
                                </div>    
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.partials.footer')