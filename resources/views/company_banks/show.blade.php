@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Company Bank' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('company_banks.company_bank.destroy', $companyBank->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('company_banks.company_bank.index') }}" class="btn btn-primary" title="Show All Company Bank">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('company_banks.company_bank.create') }}" class="btn btn-success" title="Create New Company Bank">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('company_banks.company_bank.edit', $companyBank->id ) }}" class="btn btn-primary" title="Edit Company Bank">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Company Bank" onclick="return confirm(&quot;Click Ok to delete Company Bank.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Bank Name</dt>
            <dd>{{ $companyBank->bank_name }}</dd>
            <dt>Account No</dt>
            <dd>{{ $companyBank->account_no }}</dd>
            <dt>Bank Branch Name</dt>
            <dd>{{ $companyBank->bank_branch_name }}</dd>
            <dt>Ifsc Code</dt>
            <dd>{{ $companyBank->ifsc_code }}</dd>
            <dt>Address</dt>
            <dd>{{ $companyBank->address }}</dd>
            <dt>Status</dt>
            <dd>{{ $companyBank->status }}</dd>

        </dl>

    </div>
</div>

@endsection