
<div class="d-flex flex-column gap-7 gap-lg-10">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('bank_name') ? 'has-error' : '' }}">
                        <label for="bank_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bank Name</label>
                        <div class="col-md-12">
                            <input class="form-control" name="bank_name" type="text" id="bank_name" value="{{ old('bank_name', optional($companyBank)->bank_name) }}" placeholder="Enter bank name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('account_no') ? 'has-error' : '' }}">
                        <label for="account_no" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Account No</label>
                        <div class="col-md-12">
                            <input class="form-control" name="account_no" type="number" id="account_no" value="{{ old('account_no', optional($companyBank)->account_no) }}" placeholder="Enter account no here...">
                            {!! $errors->first('account_no', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('bank_branch_name') ? 'has-error' : '' }}">
                        <label for="bank_branch_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bank Branch Name</label>
                        <div class="col-md-12">
                            <input class="form-control" name="bank_branch_name" type="text" id="bank_branch_name" value="{{ old('bank_branch_name', optional($companyBank)->bank_branch_name) }}" placeholder="Enter bank branch name here...">
                            {!! $errors->first('bank_branch_name', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('ifsc_code') ? 'has-error' : '' }}">
                        <label for="ifsc_code" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Ifsc Code</label>
                        <div class="col-md-12">
                            <input class="form-control" name="ifsc_code" type="text" id="ifsc_code" value="{{ old('ifsc_code', optional($companyBank)->ifsc_code) }}" placeholder="Enter ifsc code here...">
                            {!! $errors->first('ifsc_code', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                        <label for="address" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Address</label>
                        <div class="col-md-12">
                            <textarea class="form-control" name="address" rows="1" id="address" placeholder="Enter address here...">{{ old('address', optional($companyBank)->address) }}</textarea>
                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('status', optional($companyBank)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                                    @foreach (['active' => 'Active', 'inactive' => 'Inactive'] as $key => $text)
                                <option value="{{ $key }}" {{ old('status', optional($companyBank)->status) == $key ? 'selected' : '' }}>
                                    {{ $text }}
                                </option>
                            @endforeach
                        </select>
                        {!! $errors->first('status', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
