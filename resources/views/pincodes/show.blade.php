@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Pincode' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('pincodes.pincode.destroy', $pincode->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('pincodes.pincode.index') }}" class="btn btn-primary" title="Show All Pincode">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('pincodes.pincode.create') }}" class="btn btn-success" title="Create New Pincode">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('pincodes.pincode.edit', $pincode->id ) }}" class="btn btn-primary" title="Edit Pincode">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Pincode" onclick="return confirm(&quot;Click Ok to delete Pincode.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Pincode</dt>
            <dd>{{ $pincode->pincode }}</dd>
            <dt>City</dt>
            <dd>{{ optional($pincode->city)->city_name }}</dd>
            <dt>Status</dt>
            <dd>{{ $pincode->status }}</dd>

        </dl>

    </div>
</div>

@endsection