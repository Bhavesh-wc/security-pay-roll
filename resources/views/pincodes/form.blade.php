<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('pincode') ? 'has-error' : '' }}">
                        <label for="pincode" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Pincode</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="pincode" type="text" id="pincode" value="{{ old('pincode', optional($pincode)->pincode) }}" required="true" placeholder="Enter pincode here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
                        <label for="city_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">City</label>
                        <div class="col-md-12">
                            <select name="city_id" aria-label="Select a city" data-control="select2" data-placeholder="Select city" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('city_id', optional($pincode)->city_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select city</option>
                                @foreach ($cities as $key => $city)
                                    <option value="{{ $key }}" {{ old('city_id', optional($pincode)->city_id) == $key ? 'selected' : '' }}>
                                        {{ $city }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('status', optional($pincode)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                            @foreach (['active' => 'Active','inactive' => 'Inactive'] as $key => $text)
                                <option value="{{ $key }}" {{ old('status', optional($pincode)->status) == $key ? 'selected' : '' }}>
                                    {{ $text }}
                                </option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>