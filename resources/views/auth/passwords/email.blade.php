<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <link href="{{ asset('admin/dist/assets/css/all.css') }}" rel="stylesheet" type="text/css" />
  <link href="{{ asset('admin/dist/assets/css/all.min.css') }}" rel="stylesheet" type="text/css" />
  <title></title>
</head>

<style>
    .login-page
    {
        background-image: url('admin/dist/assets/media/auth/login-bg.png');
        background-size: cover;
        height: 100vh;
        width: 100%;
        padding: 80px 0px;
    }
    .login-left h2
    {
        font-size: 48px;
        line-height: 70px;
    }
    .login-left h2 span
    {
        font-weight: bold;
    }
    .login-left h5 
    {
        color: #D47823;
        font-size: 20px;
    }
    .login-left p
    {
        color: #808080;
        font-size: 20px;
    }
    .login-left img 
    {
        width: 100%;
    }
    .company-logo img 
    {
        width: 280px;
    }
    .input-box
    {
        position: relative;
    }
    .input-box input
    {
        border: 1px solid #DADADA;
        border-radius: 0px;
        height: 52px;
        padding-left: 50px;
        transition: all 1s ease;
    }
    .input-box input:focus
    {
        box-shadow: none;
        border-color: #D47823;
        padding-left: 20px;
    }
    .login-right a 
    {
        color: #D47823;
    }
    .input-box i 
    {
        position: absolute;
        left: 3%;
        top: 50px;
        color: #DADADA;
        font-size: 20px;
        transition: all 1s ease;
    }
    .input-box input:focus ~ i 
    {
        left: 95%;
        color: #507D46;
    }
    .login-form button 
    {
        height: 52px;
        border-radius: 0px;
        background-color: #507D46;
        color: white;
        font-weight: bold;
        width: 100%;
    }


    @media (max-width:992px){


.login-left h2
{
    font-size: 42px;
}
}

    @media (max-width:576px){

    .input-box input:focus ~ i 
    {
        left: 90%;
    }
    .login-left h2
    {
        font-size: 30px;
        line-height: 45px;
    }
    .login-page
    {
        padding: 30px 0px;
    }
    }
    
    </style>


<body>

<section class="login-page">
    <div class="container">
        <div class="row align-items-center">
            <div class="col-lg-6 mb-5 mb-lg-0">
                <div class="login-left pe-lg-5 pe-0">
                    <h2 class="mb-4"><span>Forgot Password ?</span></h2>
                    <p>Enter your email to reset your password.</p>
                    <img src="{{ asset('admin/dist/assets/media/logos/login.png') }}" alt="img" class="mt-5">
                </div>
            </div>
            <div class="col-lg-6">
                <div class="login-right ps-lg-5 ps-0">
                    <div class="company-logo text-center mb-5">
                        <img src="{{ asset('admin/dist/assets/media/logos/logo.png') }}" alt="img">
                    </div>
                    <form method="post" class="login-form" action="{{ route('password.email') }}">
                        @csrf
                        <div class="mb-4 input-box">
                            <label class="form-label">Enter Email Address</label>
                            <input type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus placeholder="Your email">
                            <i class="fas fa-envelope"></i>
                            @error('email')
                            <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                            @enderror
                        </div>
                        <div class="mb-4 d-flex justify-content-between text-uppercase">
                            <div class="form-check">   
                            </div>
                            <a href="{{ route('login') }}">Back to Login</a> 
                        </div>
                        <button type="submit" class="btn text-uppercase">Send verification link</button> 
                    </form>
                </div>
            </div>
        </div>
    </div>
</section>
</body>
</html>