<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                @if(empty($employee->id))
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('registartion_date') ? 'has-error' : '' }}">
                        <label for="registartion_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Registartion Date</label>
                        <input class="form-control" name="registartion_date" type="date" id="registartion_date" value="{{ now()->format('Y-m-d') }}" required="true" placeholder="Enter registartion date here...">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('branch_id') ? 'has-error' : '' }}">
                        <label for="branch_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                        <select class="form-select" aria-label="Select a Branch" data-control="select2" data-placeholder="Select branch" id="branch_id" name="branch_id">
                            <option value="" style="display: none;" {{ old('branch_id', optional($employee)->branch_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select branch</option>
                            @foreach ($branches as $key => $branch)
                            <option value="{{ $key }}" {{ old('branch_id', optional($employee)->branch_id) == $key ? 'selected' : '' }}>
                                {{ $branch }}
                            </option>
                            @endforeach
                        </select>
                        {!! $errors->first('branch_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('designation_id') ? 'has-error' : '' }}">
                        <label for="designation_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Designation</label>
                        <div class="d-flex">
                            <select id="designation_id" class="form-select" name="designation_id" data-placeholder="Select designation" data-control="select2" aria-label="Select a Designation" required="true">
                                <option value="" style="display: none;" {{ old('designation_id', optional($employee)->designation_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select designation</option>
                                @foreach ($designations as $key => $designation)
                                <option value="{{ $key }}" {{ old('designation_id', optional($employee)->designation_id) == $key ? 'selected' : '' }}>
                                    {{ $designation }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                @else
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('registartion_date') ? 'has-error' : '' }}">
                        <label for="registartion_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Registartion Date</label>
                        <input class="form-control" name="registartion_date" type="date" id="registartion_date" value="{{ old('registartion_date', optional($employee)->registartion_date) }}" required="true" placeholder="Enter registartion date here...">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('branch_id') ? 'has-error' : '' }}">
                        <label for="branch_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                        <select class="form-select" aria-label="Select a Branch" data-control="select2" data-placeholder="Select branch" id="branch_id" name="branch_id" disabled>
                            <option value="" style="display: none;" {{ old('branch_id', optional($employee)->branch_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select branch</option>
                            @foreach ($branches as $key => $branch)
                            <option value="{{ $key }}" {{ old('branch_id', optional($employee)->branch_id) == $key ? 'selected' : '' }}>
                                {{ $branch }}
                            </option>
                            @endforeach
                        </select>
                        {!! $errors->first('branch_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('designation_id') ? 'has-error' : '' }}">
                        <label for="designation_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Designation</label>
                        <div class="d-flex">
                            <select id="designation_id" class="form-select" name="designation_id" data-placeholder="Select designation" data-control="select2" aria-label="Select a Designation" required="true" disabled>
                                <option value="" style="display: none;" {{ old('designation_id', optional($employee)->designation_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select designation</option>
                                @foreach ($designations as $key => $designation)
                                <option value="{{ $key }}" {{ old('designation_id', optional($employee)->designation_id) == $key ? 'selected' : '' }}>
                                    {{ $designation }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                @endif
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('department_id') ? 'has-error' : '' }}">
                        <label for="department_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Department</label>
                        <div class="d-flex">
                            <select class="form-select" aria-label="Select a department" data-control="select2" data-placeholder="Select department" id="department_id" name="department_id">
                                <option value="" style="display: none;" {{ old('department_id', optional($employee)->department_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select department</option>
                                @foreach ($departments as $key => $department)
                                <option value="{{ $key }}" {{ old('department_id', optional($employee)->department_id) == $key ? 'selected' : '' }}>
                                    {{ $department }}
                                </option>
                                @endforeach
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#departmentModal">ADD</button>
                            {!! $errors->first('department_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group {{ $errors->has('grade_id') ? 'has-error' : '' }}">
                        <label for="grade_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Grade</label>
                        <div class="d-flex">
                            <select class="form-select" aria-label="Select a Grade" data-control="select2" data-placeholder="Select Grade" id="grade_id" name="grade_id">
                                <option value="" style="display: none;" {{ old('grade_id', optional($employee)->grade_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select grade</option>
                                @foreach ($grades as $key => $grade)
                                <option value="{{ $key }}" {{ old('grade_id', optional($employee)->grade_id) == $key ? 'selected' : '' }}>
                                    {{ $grade }}
                                </option>
                                @endforeach
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#gradeModal">ADD</button>
                            {!! $errors->first('grade_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('first_name') ? 'has-error' : '' }}">
                        <label for="first_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">First Name</label>
                        <input class="form-control" name="first_name" type="text" id="first-name" value="{{ old('first_name', optional($employee)->first_name) }}" required="true" placeholder="Enter first name here...">
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('middle_name') ? 'has-error' : '' }}">
                        <label for="middle_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Middle Name</label>
                        <input class="form-control" name="middle_name" type="text" id="middle-name" value="{{ old('middle_name', optional($employee)->middle_name) }}" required="true" placeholder="Enter middle name here...">
                        {!! $errors->first('middle_name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('last_name') ? 'has-error' : '' }}">
                        <label for="last_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Last Name</label>
                        <input class="form-control" name="last_name" type="text" id="last-name" value="{{ old('last_name', optional($employee)->last_name) }}" required="true" placeholder="Enter last name here...">
                        {!! $errors->first('last_name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('full_name') ? 'has-error' : '' }}">
                        <label for="full_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Full Name</label>
                        <input class="form-control" name="full_name" type="text" id="full-name" value="{{ old('full_name', optional($employee)->full_name) }}" placeholder="Enter full name here..." readonly>
                        {!! $errors->first('full_name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            @if(empty($employee->id))
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('date_of_birth') ? 'has-error' : '' }}">
                        <label for="date_of_birth" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Date Of Birth</label>
                        <input class="form-control" name="date_of_birth" type="date" id="date_of_birth" value="{{ old('date_of_birth', optional($employee)->date_of_birth) }}" required="true" placeholder="Enter date of birth here...">
                        {!! $errors->first('date_of_birth', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('country_id') ? 'has-error' : '' }}">
                        <label for="country_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Country</label>
                        <div class="d-flex">
                            <select class="form-select" id="country" name="country_id" data-control="select2" data-placeholder="Select Country" aria-label="Select a Country" required="true">
                                <option value="" style="display: none;" disabled selected>Select country</option>
                                @foreach($country as $list)
                                <option value="{{$list->id}}">{{$list->country_name}}</option>
                                @endforeach
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#countryModal">ADD</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('state_id') ? 'has-error' : '' }}">
                        <label for="state_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">State</label>
                        <div class="d-flex">
                            <select class="form-select" id="state" name="state_id" data-control="select2" aria-label="Select a State" data-placeholder="Select State">
                                <option value="{{$list->id}}">Select State</option>
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#stateModal">ADD</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('district_id') ? 'has-error' : '' }}">
                        <label for="district_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">District</label>
                        <div class="d-flex">
                            <select class="form-select" id="district" name="district_id" data-control="select2" aria-label="Select a Destrict" data-placeholder="Select destrict" required="true">
                                <option value="{{$list->id}}">Select District</option>
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#destrictModal">ADD</button>
                        </div>
                        {!! $errors->first('district_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
                        <label for="city_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">City</label>
                        <div class="d-flex">
                            <select class="form-select" id="city" name="city_id" data-control="select2" aria-label="Select a city" data-placeholder="Select city" required="true">
                                <option value="{{$list->id}}">Select City</option>
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#cityModal">ADD</button>
                        </div>
                        {!! $errors->first('city_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('pincode_id') ? 'has-error' : '' }}">
                        <label for="pincode_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Pincode</label>
                        <div class="d-flex">
                            <select class="form-select" id="pincode" name="pincode_id" data-control="select2" aria-label="Select a pincode" data-placeholder="Select pincode" required="true">
                                <option value="{{$list->id}}">Select Pincode</option>
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#pincodeModal">ADD</button>
                        </div>
                        {!! $errors->first('pincode_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            @else
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('date_of_birth') ? 'has-error' : '' }}">
                        <label for="date_of_birth" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Date Of Birth</label>
                        <input class="form-control" name="date_of_birth" type="date" id="date_of_birth" value="{{ old('date_of_birth', optional($employee)->date_of_birth) }}" required="true" placeholder="Enter date of birth here...">
                        {!! $errors->first('date_of_birth', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('country_id') ? 'has-error' : '' }}">
                        <label for="country_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Country</label>
                        <div class="d-flex">
                            <select class="form-select" id="country" name="country_id" data-control="select2" aria-label="Select a country" data-placeholder="Select country" required="true">
                                <option value="" style="display: none;" {{ old('country_id', optional($employee)->country_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select country</option>
                                @foreach ($countries as $key => $country)
                                <option value="{{ $key }}" {{ old('country_id', optional($employee)->country_id) == $key ? 'selected' : '' }}>
                                    {{ $country }}
                                </option>
                                @endforeach
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#countryModal">ADD</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('state_id') ? 'has-error' : '' }}">
                        <label for="state_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">State</label>
                        <div class="d-flex">
                            <select class="form-select" id="state" name="state_id" data-control="select2" data-placeholder="Select state" aria-label="Select a state" required="true">
                                <option value="" style="display: none;" {{ old('state_id', optional($employee)->state_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select state</option>
                                @foreach ($states as $key => $state)
                                <option value="{{ $key }}" {{ old('state_id', optional($employee)->state_id) == $key ? 'selected' : '' }}>
                                    {{ $state }}
                                </option>
                                @endforeach
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#stateModal">ADD</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('district_id') ? 'has-error' : '' }}">
                        <label for="district_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">District</label>
                        <div class="d-flex">
                            <select class="form-select" id="district" name="district_id" data-control="select2" data-placeholder="Select district" aria-label="Select a district" required="true">
                                <option value="" style="display: none;" {{ old('district_id', optional($employee)->district_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select district</option>
                                @foreach ($districts as $key => $district)
                                <option value="{{ $key }}" {{ old('district_id', optional($employee)->district_id) == $key ? 'selected' : '' }}>
                                    {{ $district }}
                                </option>
                                @endforeach
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#destrictModal">ADD</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
                        <label for="city_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">City</label>
                        <div class="d-flex">
                            <select class="form-select" aria-label="Select a city" data-control="select2" data-placeholder="Select city" id="city" name="city_id" required="true">
                                <option value="" style="display: none;" {{ old('city_id', optional($employee)->city_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select city</option>
                                @foreach ($cities as $key => $city)
                                <option value="{{ $key }}" {{ old('city_id', optional($employee)->city_id) == $key ? 'selected' : '' }}>
                                    {{ $city }}
                                </option>
                                @endforeach
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#cityModal">ADD</button>
                        </div>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('pincode_id') ? 'has-error' : '' }}">
                        <label for="pincode_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Pincode</label>
                        <div class="d-flex">
                            <select class="form-select" aria-label="Select a pincode" data-control="select2" data-placeholder="Select pincode" id="pincode" name="pincode_id" required="true">
                                <option value="" style="display: none;" {{ old('pincode_id', optional($employee)->pincode_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select pincode</option>
                                @foreach ($pincodes as $key => $pincode)
                                <option value="{{ $key }}" {{ old('pincode_id', optional($employee)->pincode_id) == $key ? 'selected' : '' }}>
                                    {{ $pincode }}
                                </option>
                                @endforeach
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#pincodeModal">ADD</button>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('father_name') ? 'has-error' : '' }}">
                        <label for="father_name" class="form-label fs-6 fw-bolder text-black-700 mb-3">Father Name</label>
                        <input class="form-control" name="father_name" type="text" id="father_name" value="{{ old('father_name', optional($employee)->father_name) }}" placeholder="Enter father name here...">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('mother_name') ? 'has-error' : '' }}">
                        <label for="mother_name" class="form-label fs-6 fw-bolder text-black-700 mb-3">Mother Name</label>
                        <input class="form-control" name="mother_name" type="text" id="mother_name" value="{{ old('mother_name', optional($employee)->mother_name) }}" placeholder="Enter mother name here...">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('spouse_name') ? 'has-error' : '' }}">
                        <label for="spouse_name" class="form-label fs-6 fw-bolder text-black-700 mb-3">Spouse Name</label>
                        <input class="form-control" name="spouse_name" type="text" id="spouse_name" value="{{ old('spouse_name', optional($employee)->spouse_name) }}" placeholder="Enter spouse name here...">
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('visible_distinguishing_mark_1') ? 'has-error' : '' }}">
                        <label for="visible_distinguishing_mark_1" class="form-label fs-6 fw-bolder text-black-700 mb-3">Visible Distinguishing Mark 1</label>
                        <input class="form-control" name="visible_distinguishing_mark_1" type="text" id="visible_distinguishing_mark_1" value="{{ old('visible_distinguishing_mark_1', optional($employee)->visible_distinguishing_mark_1) }}" placeholder="Enter visible distinguishing mark 1 here...">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('visible_distinguishing_mark_2') ? 'has-error' : '' }}">
                        <label for="visible_distinguishing_mark_2" class="form-label fs-6 fw-bolder text-black-700 mb-3">Visible Distinguishing Mark 2</label>
                        <input class="form-control" name="visible_distinguishing_mark_2" type="text" id="visible_distinguishing_mark_2" value="{{ old('visible_distinguishing_mark_2', optional($employee)->visible_distinguishing_mark_2) }}" placeholder="Enter visible distinguishing mark 2 here...">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('birth_place') ? 'has-error' : '' }}">
                        <label for="birth_place" class="form-label fs-6 fw-bolder text-black-700 mb-3">Birth Place</label>
                        <input class="form-control" name="birth_place" type="text" id="birth_place" value="{{ old('birth_place', optional($employee)->birth_place) }}" placeholder="Enter birth place here...">
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('mother_tongue_id') ? 'has-error' : '' }}">
                        <label for="mother_tongue_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Mother Tongue</label>
                        <div class="d-flex">
                            <select class="form-select" id="mother_tongue_id" name="mother_tongue_id" data-control="select2" required="true" data-placeholder="Select mother tongue" aria-label="Select a mother tougue">
                                <option value="" style="display: none;" {{ old('mother_tongue_id', optional($employee)->mother_tongue_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select mother tongue</option>
                                @foreach ($motherTongues as $key => $motherTongue)
                                <option value="{{ $key }}" {{ old('mother_tongue_id', optional($employee)->mother_tongue_id) == $key ? 'selected' : '' }}>
                                    {{ $motherTongue }}
                                </option>
                                @endforeach
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#mothertoungueModal">ADD</button>
                        </div>

                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('religion') ? 'has-error' : '' }}">
                        <label for="religion" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Religion</label>
                        <select class="form-select" aria-label="Select a Religion" data-control="select2" data-placeholder="Select religion" id="religion" name="religion" required="true">
                            <option value="" style="display: none;" {{ old('religion', optional($employee)->religion ?: '') == '' ? 'selected' : '' }} disabled selected>Enter religion here...</option>
                            @foreach (['hindu' => 'Hindu',
                            'muslim' => 'Muslim',
                            'christian' => 'Christian',
                            'sikh' => 'Sikh'] as $key => $text)
                            <option value="{{ $key }}" {{ old('religion', optional($employee)->religion) == $key ? 'selected' : '' }}>
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('marital_status') ? 'has-error' : '' }}">
                        <label for="marital_status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Marital Status</label>
                        <select class="form-select" aria-label="Select a marital status" data-control="select2" data-placeholder="Select marital status" id="marital_status" name="marital_status" required="true">
                            <option value="" style="display: none;" {{ old('marital_status', optional($employee)->marital_status ?: '') == '' ? 'selected' : '' }} disabled selected>Enter marital status here...</option>
                            @foreach (['married' => 'Married',
                            'unmarried' => 'Unmarried',
                            'seperate' => 'Seperate'] as $key => $text)
                            <option value="{{ $key }}" {{ old('marital_status', optional($employee)->marital_status) == $key ? 'selected' : '' }}>
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('height') ? 'has-error' : '' }}">
                        <label for="height" class="form-label fs-6 fw-bolder text-black-700 mb-3">Height</label>
                        <input class="form-control" name="height" type="number" id="height" value="{{ old('height', optional($employee)->height) }}" placeholder="Enter height here...">
                        {!! $errors->first('height', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('weight') ? 'has-error' : '' }}">
                        <label for="weight" class="form-label fs-6 fw-bolder text-black-700 mb-3">Weight</label>
                        <input class="form-control" name="weight" type="number" id="weight" value="{{ old('weight', optional($employee)->weight) }}" placeholder="Enter weight here...">
                        {!! $errors->first('weight', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('blood_group') ? 'has-error' : '' }}">
                        <label for="blood_group" class="form-label fs-6 fw-bolder text-black-700 mb-3">Blood Group</label>
                        <select class="form-select" aria-label="Select a blood group" data-control="select2" data-placeholder="Select blood group" id="blood_group" name="blood_group">
                            <option value="" style="display: none;" {{ old('blood_group', optional($employee)->blood_group ?: '') == '' ? 'selected' : '' }} disabled selected>Enter blood group here...</option>
                            @foreach (['a+' => 'A+',
                            'a-' => 'A-',
                            'b+' => 'B+',
                            'b-' => 'B-',
                            'o+' => 'O+',
                            'o-' => 'O-',
                            'ab+' => 'AB+',
                            'ab-' => 'AB-'] as $key => $text)
                            <option value="{{ $key }}" {{ old('blood_group', optional($employee)->blood_group) == $key ? 'selected' : '' }}>
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('sex') ? 'has-error' : '' }}">
                        <label for="sex" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Gender</label>
                        <select class="form-select" aria-label="Select a gender" data-control="select2" data-placeholder="Select gender" id="sex" name="sex" required="true">
                            <option value="" style="display: none;" {{ old('sex', optional($employee)->sex ?: '') == '' ? 'selected' : '' }} disabled selected>Enter sex here...</option>
                            @foreach (['male' => 'Male',
                            'female' => 'Female'] as $key => $text)
                            <option value="{{ $key }}" {{ old('sex', optional($employee)->sex) == $key ? 'selected' : '' }}>
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('qualification_id') ? 'has-error' : '' }}">
                        <label for="qualification_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Qualification</label>
                        <div class="d-flex">
                            <select class="form-select" id="qualification_id" name="qualification_id" data-placeholder="Select qualification" data-control="select2" aria-label="Select a qualification">
                                <option value="" style="display: none;" {{ old('qualification_id', optional($employee)->qualification_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select qualification</option>
                                @foreach ($qualifications as $key => $qualification)
                                <option value="{{ $key }}" {{ old('qualification_id', optional($employee)->qualification_id) == $key ? 'selected' : '' }}>
                                    {{ $qualification }}
                                </option>
                                @endforeach
                            </select>
                            <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#qualificationModal">ADD</button>
                        </div>
                    </div>
                </div>

                @if(empty($employee->id))
                <div class="col-md-4">
                    <div class="card-body pt-0">
                        <div class="form-group {{ $errors->has('entry_date') ? 'has-error' : '' }}">
                            <label for="entry_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Entry Date</label>
                            <input class="form-control" name="entry_date" type="date" id="entry_date" value="{{ old('entry_date', optional($employee)->entry_date) }}" placeholder="Enter entry date here...">
                            {!! $errors->first('entry_date', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
             @else
             <div class="col-md-4">
                    <div class="card-body pt-0">
                        <div class="form-group {{ $errors->has('entry_date') ? 'has-error' : '' }}">
                            <label for="entry_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Entry Date</label>
                            <input class="form-control" name="entry_date" type="date" id="entry_date" value="{{ old('entry_date', optional($employee)->entry_date) }}" placeholder="Enter entry date here..." readonly>
                            {!! $errors->first('entry_date', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            @endif      
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('local_address') ? 'has-error' : '' }}">
                        <label for="local_address" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Local Address</label>
                        <textarea class="form-control" name="local_address" cols="3" rows="2" id="local_address" required="true" placeholder="Enter local address here...">{{ old('local_address', optional($employee)->local_address) }}</textarea>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('permanent_address') ? 'has-error' : '' }}">
                        <label for="permanent_address" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Permanent Address</label>
                        <textarea class="form-control" name="permanent_address" cols="3" rows="2" id="permanent_address" required="true" placeholder="Enter permanent address here...">{{ old('permanent_address', optional($employee)->permanent_address) }}</textarea>
                        {!! $errors->first('permanent_address', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-check">
                        <input class="form-check-input" type="checkbox" value="" id="same_address_checkbox" onchange="copyAddress()">
                        <label class="form-check-label" for="flexCheckDefault"> Same as Local Address</label>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('local_phone_no') ? 'has-error' : '' }}">
                        <label for="local_phone_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Local Phone No</label>
                        <input class="form-control" name="local_phone_no" type="number" id="local_phone_no" value="{{ old('local_phone_no', optional($employee)->local_phone_no) }}" placeholder="Enter local phone no here...">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('permanent_phone_no') ? 'has-error' : '' }}">
                        <label for="permanent_phone_no" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Permanent Phone No</label>
                        <input class="form-control" name="permanent_phone_no" type="number" id="permanent_phone_no" value="{{ old('permanent_phone_no', optional($employee)->permanent_phone_no) }}" required="true" placeholder="Enter permanent phone no here...">
                    </div>
                </div>
                <div class="col-lg-4">
                    <div class="form-group {{ $errors->has('permanent_mobile_no') ? 'has-error' : '' }}">
                        <label for="permanent_mobile_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Permanent Mobile No</label>
                        <input class="form-control" name="permanent_mobile_no" type="number" id="permanent_mobile_no" value="{{ old('permanent_mobile_no', optional($employee)->permanent_mobile_no) }}" placeholder="Enter permanent mobile no here..." minlength="10" maxlength="10">
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('experience') ? 'has-error' : '' }}">
                        <label for="experience" class="form-label fs-6 fw-bolder text-black-700 mb-3">Experience</label>
                        <select id="experience" class="form-select" aria-label="Select a Experience" name="experience" data-control="select2" data-placeholder="Select Experience" onchange="toggleCompanyField()">
                            <option value="" style="display: none;" {{ old('experience', optional($employee)->experience ?: '') == '' ? 'selected' : '' }} disabled selected>select experience</option>
                            @foreach (['yes' => 'Yes',
                            'no' => 'No'] as $key => $text)
                            <option value="{{ $key }}" {{ old('experience', optional($employee)->experience) == $key ? 'selected' : '' }}>
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div id="companyField" style="{{ old('experience', optional($employee)->experience) == 'yes' ? 'display: block;' : 'display: none;' }}">
                        <label for="experience_company" class="form-label fs-6 fw-bolder text-black-700 mb-3">Experience Company</label>
                        <input class="form-control" name="experience_company" type="text" id="experience_company" value="{{ old('experience_company', optional($employee)->experience_company) }}" placeholder="Enter experience company here...">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    //   JavaScript code to automatically fill the full name added by Jaimin 15-march  -->

    // Get the input fields
    const firstName = document.getElementById('first-name');
    const middleName = document.getElementById('middle-name');
    const lastName = document.getElementById('last-name');
    const fullName = document.getElementById('full-name');

    // Add event listeners to the first name, middle name, and last name fields
    firstName.addEventListener('input', updateFullName);
    middleName.addEventListener('input', updateFullName);
    lastName.addEventListener('input', updateFullName);

    // Function to update the full name field
    function updateFullName() {
        const fullNameValue = firstName.value + ' ' + middleName.value + ' ' + lastName.value;
        fullName.value = fullNameValue;
    }


    //  local address is same as permanent address  added by Jaimin 15-march  -->

    function copyAddress() {
        var localAddress = document.getElementById("local_address").value;
        var permanentAddress = document.getElementById("permanent_address").value;

        if (document.getElementById("same_address_checkbox").checked) {
            document.getElementById("permanent_address").value = localAddress;
        } else {
            document.getElementById("permanent_address").value = '';
        }
    }
</script>


<!-- dependency dropdown  add by jaimin 16-march  -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $(document).ready(function() {
        $('#branch_id').on('change', function() {
            var idBranch = this.value;
            $("#designation_id").html('');
            $.ajax({
                url: "{{url('admin/fetch_designation')}}",
                type: "POST",
                data: {
                    branch_id: idBranch,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function(result) {
                    $('#designation_id').html('<option value="">Select Designation</option>');
                    $.each(result.data, function(key, value) {
                        console.log(value.designation_id);
                        $("#designation_id").append('<option value="' + value.designation_id + '">' + value.designation.designation_name + '</option>');
                    });
                }
            });
        });
    });
</script>



<script>
    jQuery(document).ready(function() {
        jQuery('#country').change(function() {
            let cid = jQuery(this).val();
            jQuery('#city').html('<option value="">Select City</option>')
            jQuery('#district').html('<option value="">Select District</option>')
            jQuery('#city').html('<option value="">Select city</option>')
            jQuery('#pincode').html('<option value="">Select Pincode</option>')
            jQuery.ajax({
                url: '/admin/fetch_state',
                type: 'post',
                data: 'cid=' + cid + '&_token={{csrf_token()}}',
                success: function(result) {
                    jQuery('#state').html(result)
                }
            });
        });

        jQuery('#state').change(function() {
            let sid = jQuery(this).val();
            jQuery('#city').html('<option value="">Select city</option>')
            jQuery('#pincode').html('<option value="">Select Pincode</option>')
            jQuery.ajax({
                url: '/admin/fetch_district',
                type: 'post',
                data: 'sid=' + sid + '&_token={{csrf_token()}}',
                success: function(result) {
                    jQuery('#district').html(result)
                }
            });
        });

        jQuery('#district').change(function() {
            let did = jQuery(this).val();
            jQuery('#pincode').html('<option value="">Select pincode</option>')
            jQuery.ajax({
                url: '/admin/fetch_city',
                type: 'post',
                data: 'did=' + did + '&_token={{csrf_token()}}',
                success: function(result) {
                    jQuery('#city').html(result)
                }
            });
        });

        jQuery('#city').change(function() {
            let cid = jQuery(this).val();
            jQuery.ajax({
                url: '/admin/fetch_pincode',
                type: 'post',
                data: 'cid=' + cid + '&_token={{csrf_token()}}',
                success: function(result) {
                    jQuery('#pincode').html(result)
                }
            });
        });

    });
</script>

<!-- if experience is yes then show the experience company js added by jaimin 17-march -->
<script>
    function toggleCompanyField() {
        const experienceValue = document.getElementById("experience").value;
        const companyField = document.getElementById("companyField");
        if (experienceValue === "yes") {
            companyField.style.display = "block";
        } else {
            companyField.style.display = "none";
        }
    }
</script>