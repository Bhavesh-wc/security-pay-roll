@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Employee Registration</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <!--begin::Item-->
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Employee Registration</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-xxl">
                    @if(Session::has('success_message'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{!! \Session::get('success_message') !!}</strong>
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <div class="card">
                        <div class="card-header border-0 pt-6">
                            <div class="card-title">
                                <div class="d-flex align-items-center position-relative my-1">
                                    <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                        <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                            <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                            <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor" />
                                        </svg>
                                    </span>
                                    <input type="text" data-kt-user-table-filter="search" class="form-control form-control-solid w-250px ps-14" placeholder="Search Employee" />
                                </div>
                            </div>
                            <div class="card-toolbar">
                                <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                    <a type="button" class="btn btn-sm btn-primary me-3" href="{{ route('employees.employee.create') }}">
                                        <span class="svg-icon svg-icon-2">
                                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor" />
                                                <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor" />
                                            </svg>
                                        </span>Add Employee</a>
                                    <a href="{{ route('all_employee') }}" class="btn btn-sm btn btn-success me-3">Export </a>
                                </div>
                                <div class="d-flex justify-content-end align-items-center d-none" data-kt-user-table-toolbar="selected">
                                    <div class="fw-bold me-5">
                                        <span class="me-2" data-kt-user-table-select="selected_count"></span>Selected
                                    </div>
                                    <button type="button" class="btn btn-danger" data-kt-user-table-select="delete_selected">Delete Selected</button>
                                </div>
                            </div>
                        </div>
                        <div class="card-body py-4" id="employee-table">
                            <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
                                <thead>
                                    <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                        <th class="min-w-125px">Emp Code</th>
                                        <th class="min-w-125px">Emp Image</th>
                                        <th class="min-w-125px">Full Name</th>
                                        <th class="min-w-125px">Branch</th>
                                        <th class="min-w-125px">Designation</th>
                                        <th data-sortable="false" class="min-w-125px">action</th>
                                        {{-- <th data-sortable="false" class="min-w-125px">Reports</th> --}}
                                        <th data-sortable="false" class="min-w-125px">PF Data</th>
                                    </tr>
                                </thead>
                                <tbody class="text-gray-600 fw-semibold">
                                    <?php $i = 1; ?>
                                    @foreach($employees as $employee)
                                    <tr>
                                        <td>{{ $employee->id }}</td>
                                        <td>
                                            <div class="d-flex align-items-center">
                                                <div class="symbol symbol-50px">
                                                    @if(!is_null($employee->photo))
                                                    <span class="symbol-label" style="background-image: url('{{ asset('storage/' . $employee->photo) }}');"></span>
                                                    @else
                                                    <span class="symbol-label" style="background-image:url('{{ asset('/images/userimage.jpg')}}');"></span>
                                                    @endif
                                                </div>
                                            </div>
                                        </td>
                                        <td>{{ $employee->full_name }}</td>
                                        <td>{{ optional($employee->branch)->name }}</td>
                                        <td>{{ optional($employee->designation)->designation_name }}</td>
                                        <td>
                                            <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Actions
                                                <span class="svg-icon svg-icon-5 m-0">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="currentColor" />
                                                    </svg>
                                                </span>
                                            </a>
                                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-200px py-4" data-kt-menu="true">
                                                <div class="menu-item px-3">
                                                    <a href="{{route('employees.employee.show', $employee->id ) }} " class="menu-link px-3">View</a>
                                                </div>
                                                <div class="menu-item px-3">
                                                    <a href="{{route('employees.employee.edit', $employee->id ) }} " class="menu-link px-3">Edit</a>
                                                </div>
                                                <div class="menu-item px-3">
                                                    <a href="{{route('employees.employee.editOther', $employee->id ) }}" class="menu-link px-3">Other Information</a>
                                                </div>
                                                <div class="menu-item px-3">
                                                    <a href="{{route('employees.employee.editcompanylegal', $employee->id ) }}" class="menu-link px-3">Comapany Legal</a>
                                                </div>
                                                <div class="menu-item px-3">
                                                    <a href="{{route('employees.employee.editdocument', $employee->id ) }}" class="menu-link px-3">Document</a>
                                                </div>
                                                <div class="menu-item px-3">
                                                    <a href="{{ route('employees.branch_transfer.create',$employee->id ) }}" class="menu-link px-3">Employee Transfer</a>
                                                </div>
                                                <!-- <div class="menu-item px-3">
                                                    <form method="POST" action="{!! route('employees.employee.destroy', $employee->id) !!}" accept-charset="UTF-8">
                                                        <input name="_method" value="DELETE" type="hidden">
                                                        {{ csrf_field() }}
                                                        <button type="submit" title="delete" class="btn btn-sm w-100 menu-link px-3" data-id="#" onclick="return confirm(&quot;Click Ok to delete Employee.&quot;)">
                                                            Delete
                                                        </button>
                                                    </form>
                                                </div> -->
                                                <div class="menu-item px-3">
                                                    <a href="{{route('employees.employee.leave_register', $employee->id ) }}" class="menu-link px-3">Print Leave Register</a>
                                                </div>
                                            </div>
                                        </td>
                                        {{-- <td>
                                            <a href="#" class="btn btn-light btn-active-light-primary btn-sm" data-kt-menu-trigger="click" data-kt-menu-placement="bottom-end">Reports
                                                <span class="svg-icon svg-icon-5 m-0">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <path d="M11.4343 12.7344L7.25 8.55005C6.83579 8.13583 6.16421 8.13584 5.75 8.55005C5.33579 8.96426 5.33579 9.63583 5.75 10.05L11.2929 15.5929C11.6834 15.9835 12.3166 15.9835 12.7071 15.5929L18.25 10.05C18.6642 9.63584 18.6642 8.96426 18.25 8.55005C17.8358 8.13584 17.1642 8.13584 16.75 8.55005L12.5657 12.7344C12.2533 13.0468 11.7467 13.0468 11.4343 12.7344Z" fill="currentColor" />
                                                    </svg>
                                                </span>
                                            </a>
                                            <div class="menu menu-sub menu-sub-dropdown menu-column menu-rounded menu-gray-600 menu-state-bg-light-primary fw-semibold fs-7 w-200px py-4" data-kt-menu="true">
                                                <div class="menu-item px-3">
                                                    <a href="{{route('employees.employee.leave_register', $employee->id ) }}" class="menu-link px-3">Leave Register</a>
                                                </div>
                                            </div>
                                        </td> --}}
                                        <td>
                                            <button type="button" value="{{ $employee->id }}" class="popUpBtn btn btn-primary editbtn btn-sm" data-modal="myModal{{$employee->id}}">PF CODE</button>
                                        </td> 
                                        <div id="myModal{{$employee->id}}" class="modal">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <h2>Employee PF & UAN A/C Code</h2>
                                                </div>
                                                <div class="modal-body">
                                                    <form action="{{ route('employees.employee.changepf',$employee->id)}}" method="get">
                                                        @csrf
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="card-body pt-2">
                                                                    <div class="form-group {{ $errors->has('pf_ac_code') ? 'has-error' : '' }}">
                                                                        <label for="pf_ac_code" class="form-label  fw-bolder text-black-700 mb-3">Employee Code *</label>
                                                                        <input class="form-control" name="pf_ac_code" type="text" id="pf_ac_code" value="{{ old('pf_ac_code', optional($employee)->id) }}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="card-body pt-2">
                                                                    <div class="form-group {{ $errors->has('pf_uan_code') ? 'has-error' : '' }}">
                                                                        <label for="pf_uan_code" class="form-label  fw-bolder text-black-700 mb-3">Full Name *</label>
                                                                        <input class="form-control" name="pf_uan_code" type="text" id="pf_uan_code" value="{{ old('pf_uan_code', optional($employee)->full_name) }}" readonly>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="row">
                                                            <div class="col-md-6">
                                                                <div class="card-body pt-1">
                                                                    <div class="form-group {{ $errors->has('pf_ac_code') ? 'has-error' : '' }}">
                                                                        <label for="pf_ac_code" class="form-label  fw-bolder text-black-700 mb-3">PF Account Code</label>
                                                                        <input class="form-control" name="pf_ac_code" type="text" id="pf_ac_code" value="{{ old('pf_ac_code', optional($employee)->pf_ac_code) }}" placeholder="Enter PF A/C Code.">
                                                                        {!! $errors->first('pf_ac_code', '<p class="help-block">:message</p>') !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6">
                                                                <div class="card-body pt-1">
                                                                    <div class="form-group {{ $errors->has('pf_uan_code') ? 'has-error' : '' }}">
                                                                        <label for="pf_uan_code" class="form-label  fw-bolder text-black-700 mb-3">PF UAN Code</label>
                                                                        <input class="form-control" name="pf_uan_code" type="text" id="pf_uan_code" value="{{ old('pf_uan_code', optional($employee)->pf_uan_code) }}" placeholder="Enter PF UAN Code">
                                                                        {!! $errors->first('pf_uan_code', '<p class="help-block">:message</p>') !!}
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="submit" class="btn btn-sm btn-secondary" data-bs-dismiss="modal">Close</button>
                                                            <button type="submit" class="btn btn-sm btn-primary">Save</button>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </tr>
                                    @endforeach
                                    
                                </tbody>
                            </table>
                            
{{ $employees->links() }}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>
@include('layouts.partials.footer')

<script src="{{ asset('admin/dist/assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/table.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/export-users.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/add.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/widgets.bundle.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/widgets.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/chat/chat.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/upgrade-plan.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/create-app.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/users-search.js') }}"></script>

<style type="text/css">
    /* The Modal (background) */
    .modal {
        display: none;
        /* Hidden by default */
        position: fixed;
        /* Stay in place */
        z-index: 1;
        /* Sit on top */
        padding-top: 150px;
        /* Location of the box */
        margin-left: 120px;
        left: 0;
        top: 0;
        width: 100%;
        /* Full width */
        height: 100%;
        /* Full height */
        overflow: auto;
        /* Enable scroll if needed */
        background-color: rgb(0, 0, 0);
        /* Fallback color */
        background-color: rgba(0, 0, 0, 0.4);
        /* Black w/ opacity */
    }

    /* Modal Content */
    .modal-content {
        position: relative;
        background-color: #fefefe;
        margin: auto;
        padding: 0;
        border: 1px solid #888;
        width: 50%;
        box-shadow: 0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19);
        -webkit-animation-name: animatetop;
        -webkit-animation-duration: 0.4s;
        animation-name: animatetop;
        animation-duration: 0.4s
    }

    /* Add Animation */
    @-webkit-keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }

        to {
            top: 0;
            opacity: 1
        }
    }

    @keyframes animatetop {
        from {
            top: -300px;
            opacity: 0
        }

        to {
            top: 0;
            opacity: 1
        }
    }

    /* The Close Button */
    .close {
        color: white;
        float: left;
        font-size: 28px;
        font-weight: bold;
    }

    .close:hover,
    .close:focus {
        color: white;
        text-decoration: none;
        cursor: pointer;
    }

    .modal-header {
        padding: 13px 13px;
        background-color: #FBFBFB;
        color: white;
        font-weight: bold;
        text-align: center;
    }

    .modal-body {
        padding: 2px 16px;
    }
</style>

<script type="text/javascript">
    $(document).ready(function() {


        $('.popUpBtn').on('click', function() {
            $('#' + $(this).data('modal')).css('display', 'block');
        })


        $('span.close').on('click', function() {
            $('.modal').css('display', 'none');
        })


        $(window).on('click', function(event) {
            if (jQuery.inArray(event.target, $('.modal')) != "-1") {
                $('.modal').css('display', 'none');
            }
        })

    })
</script>