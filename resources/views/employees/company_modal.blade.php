   <!-- Modal Bank -->
   <div class="modal fade" id="bankModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog">
           <div class="modal-content">
               <div class="modal-header">
                   <h3 class="modal-title" id="exampleModalLabel">Create New Bank</h3>
                   <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
               </div>
               <form id="form-bank">
                   <input type="hidden" id="hidden" name="hidden" value="hidden">
                   @csrf
                   <div class="modal-body" id="bank_add">
                       <div class="form-group-mb-3">
                           <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bank Name</label>
                           <input type="text" name="bank_name" class="form-control" required>
                       </div>
                       <div class="form-group-mb-3 mt-3">
                           <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                           <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                               <option value="" style="display: none;" disabled selected>Select Status</option>
                               @foreach (['active' => 'Active'] as $key => $text)
                               <option value="{{ $key }}">
                                   {{ $text }}
                               </option>
                               @endforeach
                           </select>
                       </div>
                       <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                           <button type="submit" class="btn btn-primary">Save</button>
                       </div>
                   </div>
                   <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="bank_success" style="display: none">
                       <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                           <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                               <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                   <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                   <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                   <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                               </svg>
                           </span>
                           <div class="d-flex flex-column text-light pe-0 text-center">
                               <h4 class="text-dark my-2 py-0">New Bank has been added.</h4>
                           </div>
                       </div>
                   </div>
               </form>
           </div>
       </div>
   </div>

   <!-- Modal Bank Branch -->
   <div class="modal fade" id="bankbranchModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog">
           <div class="modal-content">
               <div class="modal-header">
                   <h3 class="modal-title" id="exampleModalLabel">Create New Bank-Branch</h3>
                   <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
               </div>
               <form id="form-bankbranch">
                   <input type="hidden" id="hidden" name="hidden" value="hidden">
                   @csrf
                   <div class="modal-body" id="bankbranch_add">
                       <div class="form-group-mb-3">
                           <label for="bank_branch" class=" required form-label fs-6 fw-bolder text-black-700 mb-3">Bank Branch Name </label>
                           <input type="text" name="bank_branch_name" class="form-control" required>
                       </div>
                       <div class="form-group-mb-3 mt-3">
                           <div class="form-group {{ $errors->has('ifsc_code') ? 'has-error' : '' }}">
                               <label for="ifsc_code" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Ifsc Code</label>
                               <div class="col-md-12">
                                   <input class="form-control mb-2" name="ifsc_code" type="text" id="ifsc_code" value="{{ old('ifsc_code') }}" minlength="1" required="true" placeholder="Enter ifsc code here...">
                               </div>
                           </div>
                       </div>
                       <div class="form-group-mb-3 mt-3">
                           <div class="form-group {{ $errors->has('bank_id') ? 'has-error' : '' }}">
                               <label for="bank_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bank</label>
                               <div class="col-md-12">
                                   <select name="bank_id" id="bank-modal" aria-label="Select a Bank" data-control="select2" data-placeholder="Select Bank" class="form-select mb-2">
                                       <option value="" style="display: none;" {{ old('bank_id')}} disabled selected>Select Bank</option>
                                       @foreach ($banks as $key => $list)
                                       <option value="{{ $list->id }}">
                                           {{ $list->bank_name }}
                                       </option>
                                       @endforeach
                                   </select>
                               </div>
                           </div>
                       </div>
                       <div class="form-group-mb-3 mt-3">
                           <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                               <label for="address" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Address</label>
                               <div class="col-md-12">
                                   <textarea class="form-control mb-2" name="address" cols="2" rows="2" id="address" required="true" placeholder="Enter address here...">{{ old('address') }}</textarea>
                               </div>
                           </div>
                       </div>
                       <div class="form-group-mb-3 mt-3">
                           <label for="bank" class=" required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                           <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                               <option value="" style="display: none;" disabled selected>Select Status</option>
                               @foreach (['active' => 'Active'] as $key => $text)
                               <option value="{{ $key }}">
                                   {{ $text }}
                               </option>
                               @endforeach
                           </select>
                       </div>
                       <div class="modal-footer">
                           <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                           <button type="submit" class="btn btn-primary">Save</button>
                       </div>
                   </div>
                   <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="bankbranch_success" style="display: none">
                       <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                           <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                               <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                   <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                   <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                   <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                               </svg>
                           </span>
                           <div class="d-flex flex-column text-light pe-0 text-center">
                               <h4 class="text-dark my-2 py-0">New Bank-branch has been added.</h4>
                           </div>
                       </div>
                   </div>
               </form>
           </div>
       </div>
   </div>