@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_toolbar" class="app-toolbar  py-3 py-lg-6 ">
            <div id="kt_app_toolbar_container" class="app-container  container-xxl d-flex flex-stack ">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3 ">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">View Employee Details</h1>
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('employees.employee.index') }}" class="text-muted text-hover-primary">Index</a>
                        </li>
                    </ul>
                </div>
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a href="{{ route('employees.employee.index') }}" class="btn btn-sm fw-bold btn-primary" >List</a>
                </div>
            </div>
        </div>
        <div id="kt_app_content" class="app-content  flex-column-fluid ">
            <div id="kt_app_content_container" class="app-container  container-xxl ">             
                <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Personal Details</h3>
                        </div>
                        <a href="{{route('employees.employee.edit', $employee->id ) }}" class="btn btn-sm btn-primary align-self-center">Edit Personal Details</a>   
                    </div>
                    <div class="card-body p-9">
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Full Name</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->full_name }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Father Name</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->father_name }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Mother Name</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->mother_name }}</span>    
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Designation</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ optional($employee->designation)->designation_name }}
                                    </span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Branch</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->branch)->name }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Department</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->department)->department_name }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Grade</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->grade)->grade_name }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Registartion Date</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->registartion_date ? date('d-m-Y', strtotime($employee->registartion_date)): '' ; }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Date Of Birth</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->date_of_birth ? date('d-m-Y', strtotime($employee->date_of_birth)): '' ; }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Country</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->country)->country_name }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">State</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->state)->state_name }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">District</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->district)->district_name }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">City</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->city)->city_name }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Pincode</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->pincode)->pincode }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Spouse Name</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->spouse_name }}</span>    
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Visible Distinguishing Mark 1</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->visible_distinguishing_mark_1 }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Visible Distinguishing Mark 2</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->visible_distinguishing_mark_2 }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Birth Place</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $employee->birth_place }}</span>                         
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Mother Tongue</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->motherTongue)->language_name }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Religion</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->religion }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Marital Status</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ ucfirst($employee->marital_status) }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Height</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->height }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Weight</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->weight }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Blood Group</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ ucfirst($employee->blood_group) }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Gender</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ ucfirst($employee->sex) }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Sex</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->sex }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Qualification</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->qualification)->qualification_name }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Local Address</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->local_address }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Permanent Address</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->permanent_address }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Local Phone No</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->local_phone_no }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Permanent Phone No</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->permanent_phone_no }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Permanent Mobile No</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->permanent_mobile_no }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Salary</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->salary }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Experience</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ ucfirst($employee->experience) }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Experience Company</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->experience_company }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Other Information</h3>
                        </div>
                        <a href="{{route('employees.employee.editOther', $employee->id ) }}" class="btn btn-sm btn-primary align-self-center">Edit Other Information</a>   
                    </div>
                    <div class="card-body p-9">
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Police Varification Date</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->police_varification_date ? date('d-m-Y', strtotime($employee->police_varification_date)): '' ; }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Police Station Name</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->police_station_name }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Citizen Of India By</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ ucfirst($employee->citizen_of_india_by) }}</span>    
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Black List</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ ucfirst($employee->black_list) }}
                                    </span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Black List Reasone</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->black_list_reasone }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Election Card No</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->election_card_no }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Driving Licenses No</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->driving_licenses_no }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">RTO</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->rto }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">RTO State</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->rtoState)->state_name }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Passport No</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->passport_no }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Pancard No</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->pancard_no }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Aadhar Card No</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->aadhar_card_no }}</span>    
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">        
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Reference Relation Contact 1</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->reference_relation_contact_1 }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Reference Relation Contact 2</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $employee->reference_relation_contact_2 }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Reference Relation Contact 3</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->reference_relation_contact_3 }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Emergency Contact Name</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->emergency_contact_name }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Emergency Relation</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->emergency_relation }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Emergency Contact No</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->emergency_contact_no }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Nominee</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->nominee_1 }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Birth Date</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->birth_date_1 ? date('d-m-Y', strtotime($employee->birth_date_1)): '' ; }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Relation</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->relation_1 }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Comapany Legal</h3>
                        </div>
                        <a href="{{route('employees.employee.editcompanylegal', $employee->id ) }}" class="btn btn-sm btn-primary align-self-center">Edit Comapany Legal</a>   
                    </div>
                    <div class="card-body p-9">
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Bank</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->bank)->bank_name }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Bank Branch</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ optional($employee->bankBranch)->bank_branch_name }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">IFSC Code</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->ifsc_code }}</span>    
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Bank Ac No</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $employee->bank_ac_no }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Interview Date</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->interview_date ? date('d-m-Y', strtotime($employee->interview_date)): '' ; }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Entry Date</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->entry_date ? date('d-m-Y', strtotime($employee->entry_date)): '' ; }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Relieve Date</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->relieve_date ? date('d-m-Y', strtotime($employee->relieve_date)): '' ; }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Final Relieve Date</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->final_relieve_date ? date('d-m-Y', strtotime($employee->final_relieve_date)): '' ; }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Security Deposit</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->security_deposit }}</span>    
                                </div>
                            </div>  
                        </div>
                        <div class="row mb-7">   
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Deposit Detail</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->deposit_detail }}</span>    
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Due Date</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $employee->due_date ? date('d-m-Y', strtotime($employee->due_date)): '' ; }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Refund Date</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->refund_date ? date('d-m-Y', strtotime($employee->refund_date)): '' ; }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Refund Detail</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->refund_detail }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Probation Date</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->probation_date ? date('d-m-Y', strtotime($employee->probation_date)): '' ; }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Confirmation Date</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->confirmation_date ? date('d-m-Y', strtotime($employee->confirmation_date)): '' ; }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Senior Citizen</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $employee->senior_citizen }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
                    <div class="card-header cursor-pointer">
                        <!--begin::Card title-->
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Document</h3>
                        </div>
                        <a href="{{route('employees.employee.editdocument', $employee->id ) }}" class="btn btn-sm btn-primary align-self-center">Edit Document</a>   
                    </div>
                    <div class="card-body p-9">
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Police Varification</label>
                                <div>
                                    @if (isset($employee->police_varification) && !empty($employee->police_varification))    
                                        <a href="{{ asset('storage/' . $employee->police_varification) }}" target="_blank">Police Verification</a>
                                    @endif
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Leaving Certificate</label>
                                <div>      
                                    @if (isset($employee->leaving_certificate) && !empty($employee->leaving_certificate))          
                                        <a href="{{ asset('storage/' . $employee->leaving_certificate) }}" target="_blank">Leaving Certificate</a>   
                                    @endif
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Bank Passbook</label>
                                <div> 
                                    @if (isset($employee->bank_passbook) && !empty($employee->bank_passbook))                   
                                        <a href="{{ asset('storage/' . $employee->bank_passbook) }}" target="_blank">Bank Passbook</a> 
                                    @endif   
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Photo</label>
                                <div>
                                     @if (isset($employee->photo) && !empty($employee->photo))
                                        <a href="{{ asset('storage/' . $employee->photo) }}" target="_blank">Photo</a> 
                                    @endif                        
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Document</label>
                                <div>
                                    @if (isset($employee->document) && !empty($employee->document))
                                        <a href="{{ asset('storage/' . $employee->document) }}" target="_blank">document</a>
                                    @endif
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Signature</label>
                                <div>
                                    @if (isset($employee->signature) && !empty($employee->signature))
                                        <a href="{{ asset('storage/' . $employee->signature) }}" target="_blank">Signature</a>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(!$branchTransfers->isEmpty())
        <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
            <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                <div class="d-flex flex-column flex-column-fluid">
                    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Branch Transfer History</h1>
                            </div>
                        </div>
                    </div>
                    <div id="kt_app_content" class="app-content flex-column-fluid">
                        <div id="kt_app_content_container" class="app-container container-xxl">
                            @if(Session::has('success_message'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>{!! \Session::get('success_message') !!}</strong>
                                    <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <div class="card">
                                <div class="card-header border-0 pt-6">
                                    <div class="card-title">
                                        <div class="d-flex align-items-center position-relative my-1">
                                            <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor" />
                                                </svg>
                                            </span>
                                            <input type="text" data-kt-user-table-filter="search" class="form-control form-control-solid w-350px ps-14" placeholder="Search Branch Transfer History" />
                                        </div>
                                    </div>
                                    <div class="card-toolbar">
                                        <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                            <a type="button" class="btn btn-sm btn-primary me-3" href="{{ route('employees.branch_transfer.create',$employee->id ) }}">
                                                <span class="svg-icon svg-icon-2">
                                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                        <rect opacity="0.5" x="11.364" y="20.364" width="16" height="2" rx="1" transform="rotate(-90 11.364 20.364)" fill="currentColor" />
                                                        <rect x="4.36396" y="11.364" width="16" height="2" rx="1" fill="currentColor" />
                                                    </svg>
                                                </span>Add Employee Transfer</a>
                                        </div>
                                        <div class="d-flex justify-content-end align-items-center d-none" data-kt-user-table-toolbar="selected">
                                            <div class="fw-bold me-5">
                                            <span class="me-2" data-kt-user-table-select="selected_count"></span>Selected</div>
                                            <button type="button" class="btn btn-danger" data-kt-user-table-select="delete_selected">Delete Selected</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body py-4">
                                    <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
                                        <thead>
                                            <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                                <th class="min-w-125px">Branch</th>
                                                <th class="min-w-125px">Designation</th>
                                                <th class="min-w-125px">Transfer</th>
                                                <th class="min-w-125px">Updated Month</th>
                                                <th class="min-w-125px">Basic/Fix</th>
                                                <th class="min-w-125px">Basic + Da</th>
                                                <th class="min-w-125px">Extra Allowance</th>
                                                <th class="min-w-125px">Hra</th>
                                                <th class="min-w-125px">Bonus</th>
                                                <th class="min-w-125px">Fix Salary</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-gray-600 fw-semibold">
                                            <?php $i = 1; ?>
                                            @foreach($branchTransfers as $branchTransfer)
                                            <tr>
                                                <td>{{ optional($branchTransfer->branch)->name }}</td>
                                                <td>{{ optional($branchTransfer->designation)->designation_name }}</td>
                                                <td>{{ $branchTransfer->transfer }}</td>
                                                <td>{{ $branchTransfer->updated_month ? date('m-Y', strtotime($branchTransfer->updated_month)): '' ; }}</td>
                                                <td>{{ $branchTransfer->basic_fix }}</td>
                                                <td>{{ $branchTransfer->basic_da }}</td>
                                                <td>{{ $branchTransfer->extra_allowance }}</td>
                                                <td>{{ $branchTransfer->hra }}</td>
                                                <td>{{ $branchTransfer->bonus }}</td>
                                                <td>{{ $branchTransfer->fix_salary }}</td>
                                            </tr>  
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@include('layouts.partials.footer')

<script src="{{ asset('admin/dist/assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/table.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/export-users.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/add.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/widgets.bundle.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/widgets.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/chat/chat.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/upgrade-plan.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/create-app.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/users-search.js') }}"></script>

