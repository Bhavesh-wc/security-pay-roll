@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <form method="POST" action="{{ route('employees.employee.update', $employee->id) }}" id="edit_document_form" name="edit_document_form" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Document</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('employees.employee.index') }}" class="text-muted text-hover-primary">Index</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Document</li>
                        </ul>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{ route('employees.employee.index') }}" class="btn btn-sm btn-danger me-3">List</a>
                        <button type="submit" value="Update" class="btn btn-sm btn-primary me-3">
                            <span class="indicator-label">Save</span>
                        </button>
                    </div> 
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-xxl">
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                            <strong>{{ $error }}</strong>
                        @endforeach
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">    
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PUT">  
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="row">
                                    <div class="col-md-4">  
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('police_varification') ? 'has-error' : '' }}">
                                                <label for="police_varification" class="form-label fs-6 fw-bolder text-black-700 mb-3">Police Varification</label>
                                                <div class="col-md-10">
                                                @if (!isset($employee->police_varification) && empty($employee->police_varification))
                                                    <input class="hidden form-control mb-2" name="police_varification" type="file" id="police_varification" placeholder="Enter police verification here...">
                                                @else
                                                    <div class="image-input image-input-outline {{ isset($employee) && $employee->police_varification ? '' : 'image-input-empty' }}" data-kt-image-input="true" style="background-image: url({{ asset('admin/src/media/avatars/blank.png') }})">
                                                        <div class="image-input-wrapper w-300px h-300px" style="background-image: {{ isset($employee) && $employee->police_varification ? 'url('.asset('storage/'. $employee->police_varification).')' : 'none' }};"></div>
                                                        <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                            <i class="bi bi-pencil-fill fs-7"></i>
                                                            <input type="file" name="police_varification" id="police_varification" accept=".png, .jpg, .jpeg">
                                                            <input type="hidden" name="custom_delete_police_varification">
                                                        </label>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                    </div>
                                                @endif
                                                {!! $errors->first('police_varification', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">  
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('leaving_certificate') ? 'has-error' : '' }}">
                                                <label for="leaving_certificate" class="form-label fs-6 fw-bolder text-black-700 mb-3">Leaving Certificate</label>
                                                <div class="col-md-10">
                                                @if (!isset($employee->leaving_certificate) && empty($employee->leaving_certificate))
                                                    <input class="hidden form-control mb-2" name="leaving_certificate" type="file" id="leaving_certificate" placeholder="Enter leaving ertificate here...">
                                                @else
                                                    <div class="image-input image-input-outline {{ isset($employee) && $employee->leaving_certificate ? '' : 'image-input-empty' }}" data-kt-image-input="true" style="background-image: url({{ asset('admin/src/media/avatars/blank.png') }})">
                                                        <div class="image-input-wrapper w-300px h-300px" style="background-image: {{ isset($employee) && $employee->leaving_certificate ? 'url('.asset('storage/'. $employee->leaving_certificate).')' : 'none' }};"></div>
                                                        <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                            <i class="bi bi-pencil-fill fs-7"></i>
                                                            <input type="file" name="leaving_certificate" id="leaving_certificate" accept=".png, .jpg, .jpeg">
                                                            <input type="hidden" name="custom_delete_leaving_certificate">
                                                        </label>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                    </div>
                                                @endif
                                                {!! $errors->first('leaving_certificate', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">  
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('bank_passbook') ? 'has-error' : '' }}">
                                                <label for="bank_passbook" class="form-label fs-6 fw-bolder text-black-700 mb-3">Bank Passbook</label>
                                                <div class="col-md-10">
                                                @if (!isset($employee->bank_passbook) && empty($employee->bank_passbook))
                                                    <input class="hidden form-control mb-2" name="bank_passbook" type="file" id="bank_passbook" placeholder="Enter bank passbook here...">
                                                @else
                                                    <div class="image-input image-input-outline {{ isset($employee) && $employee->bank_passbook ? '' : 'image-input-empty' }}" data-kt-image-input="true" style="background-image: url({{ asset('admin/src/media/avatars/blank.png') }})">
                                                        <div class="image-input-wrapper w-300px h-300px" style="background-image: {{ isset($employee) && $employee->bank_passbook ? 'url('.asset('storage/'. $employee->bank_passbook).')' : 'none' }};"></div>
                                                        <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                            <i class="bi bi-pencil-fill fs-7"></i>
                                                            <input type="file" name="bank_passbook" id="bank_passbook" accept=".png, .jpg, .jpeg">
                                                            <input type="hidden" name="custom_delete_bank_passbook">
                                                        </label>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                    </div>
                                                @endif
                                                {!! $errors->first('bank_passbook', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('photo') ? 'has-error' : '' }}">
                                                <label for="photo" class="form-label fs-6 fw-bolder text-black-700 mb-3">Photos</label>
                                                <div class="col-md-10">
                                                @if (!isset($employee->photo) && empty($employee->photo))
                                                    <input class="hidden form-control mb-2" name="photo" type="file" id="photo" placeholder="Enter photo here...">
                                                @else
                                                    <div class="image-input image-input-outline {{ isset($employee) && $employee->photo ? '' : 'image-input-empty' }}" data-kt-image-input="true" style="background-image: url({{ asset('admin/src/media/avatars/blank.png') }})">
                                                        <div class="image-input-wrapper w-300px h-300px" style="background-image: {{ isset($employee) && $employee->photo ? 'url('.asset('storage/'. $employee->photo).')' : 'none' }};"></div>
                                                        <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                            <i class="bi bi-pencil-fill fs-7"></i>
                                                            <input type="file" name="photo" id="photo" accept=".png, .jpg, .jpeg">
                                                            <input type="hidden" name="custom_delete_photo">
                                                        </label>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                    </div>
                                                @endif
                                                {!! $errors->first('photo', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('document') ? 'has-error' : '' }}">
                                                <label for="document" class="form-label fs-6 fw-bolder text-black-700 mb-3">Document</label>
                                                <div class="col-md-10">
                                                @if (!isset($employee->document) && empty($employee->document))
                                                    <input class="hidden form-control mb-2" name="document" type="file" id="document" placeholder="Enter document here...">
                                                @else
                                                    <div class="image-input image-input-outline {{ isset($employee) && $employee->document ? '' : 'image-input-empty' }}" data-kt-image-input="true" style="background-image: url({{ asset('admin/src/media/avatars/blank.png') }})">
                                                        <div class="image-input-wrapper w-300px h-300px" style="background-image: {{ isset($employee) && $employee->document ? 'url('.asset('storage/'. $employee->document).')' : 'none' }};"></div>
                                                        <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                            <i class="bi bi-pencil-fill fs-7"></i>
                                                            <input type="file" name="document" id="document" accept=".png, .jpg, .jpeg">
                                                            <input type="hidden" name="custom_delete_document">
                                                        </label>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                    </div>
                                                @endif
                                                {!! $errors->first('document', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('signature') ? 'has-error' : '' }}">
                                                <label for="signature" class="form-label fs-6 fw-bolder text-black-700 mb-3">Signature</label>
                                                <div class="col-md-10">
                                                @if (!isset($employee->signature) && empty($employee->signature))
                                                    <input class="hidden form-control mb-2" name="signature" type="file" id="signature" placeholder="Enter signature here...">
                                                @else
                                                <div class="image-input image-input-outline {{ isset($employee) && $employee->signature ? '' : 'image-input-empty' }}" data-kt-image-input="true" style="background-image: url({{ asset('admin/src/media/avatars/blank.png') }})">
                                                    <div class="image-input-wrapper w-300px h-300px" style="background-image: {{ isset($employee) && $employee->signature ? 'url('.asset('storage/'. $employee->signature).')' : 'none' }};"></div>
                                                    <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                        <i class="bi bi-pencil-fill fs-7"></i>
                                                        <input type="file" name="signature" id="signature" accept=".png, .jpg, .jpeg">
                                                        <input type="hidden" name="custom_delete_signature">
                                                    </label>
                                                    <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                                        <i class="bi bi-x fs-2"></i>
                                                    </span>
                                                    <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                                        <i class="bi bi-x fs-2"></i>
                                                    </span>
                                                </div>
                                                @endif
                                                {!! $errors->first('signature', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>  
@include('layouts.partials.footer')
