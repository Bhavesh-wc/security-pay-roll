@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <form method="POST" action="{{ route('employees.employee.update', $employee->id) }}" id="edit_other_information_form" name="edit_other_information_form" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Other Information</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('employees.employee.index') }}" class="text-muted text-hover-primary">Index</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Other Information</li>
                        </ul>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{ route('employees.employee.index') }}" class="btn btn-sm btn-danger me-3">List</a>
                        <button type="submit" value="Update" class="btn btn-sm btn-primary me-3">
                            <span class="indicator-label">Save</span>
                        </button>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-xxl">
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                        <strong>{{ $error }}</strong>
                        @endforeach
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PUT">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('police_varification_date') ? 'has-error' : '' }}">
                                                <label for="police_varification_date" class="form-label fs-6 fw-bolder text-black-700 mb-3">Police Varification Date</label>
                                                <input class="form-control" name="police_varification_date" type="date" id="police_varification_date" value="{{ old('police_varification_date', optional($employee)->police_varification_date) }}" placeholder="Enter police varification date here...">
                                                {!! $errors->first('police_varification_date', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('police_station_name') ? 'has-error' : '' }}">
                                                <label for="police_station_name" class="form-label fs-6 fw-bolder text-black-700 mb-3">Police Station Name</label>
                                                <input class="form-control" name="police_station_name" type="text" id="police_station_name" value="{{ old('police_station_name', optional($employee)->police_station_name) }}" placeholder="Enter police station name here...">
                                                {!! $errors->first('police_station_name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('citizen_of_india_by') ? 'has-error' : '' }}">
                                                <label for="citizen_of_india_by" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Citizen Of India By</label>
                                                <select class="form-select" aria-label="Select a Citizen Of India By" data-control="select2" data-placeholder="Select Citizen Of India By" id="citizen_of_india_by" name="citizen_of_india_by">
                                                    <option value="" style="display: none;" {{ old('citizen_of_india_by', optional($employee)->citizen_of_india_by ?: '') == '' ? 'selected' : '' }} disabled selected>Select citizen of india by</option>
                                                    @foreach (['indian birth' => 'Indian Birth',
                                                    'descent' => 'Descent',
                                                    'registration' => 'Registration',
                                                    'naturalization' => 'Naturalization'
                                                    ] as $key => $text)
                                                    <option value="{{ $key }}" {{ old('citizen_of_india_by', optional($employee)->citizen_of_india_by) == $key ? 'selected' : '' }}>
                                                        {{ $text }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                                {!! $errors->first('citizen_of_india_by', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('black_list') ? 'has-error' : '' }}">
                                                <label for="black_list" class="form-label fs-6 fw-bolder text-black-700 mb-3">Black List</label>
                                                <select class="form-select" aria-label="Select a Black List" data-control="select2" data-placeholder="Select Black List" id="black_list" name="black_list">
                                                    <option value="" style="display: none;" {{ old('black_list', optional($employee)->black_list ?: '') == '' ? 'selected' : '' }} disabled selected>Select black list</option>
                                                    @foreach (['yes' => 'Yes',
                                                    'no' => 'No'] as $key => $text)
                                                    <option value="{{ $key }}" {{ old('black_list', optional($employee)->black_list) == $key ? 'selected' : '' }}>
                                                        {{ $text }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                                {!! $errors->first('black_list', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('black_list_reasone') ? 'has-error' : '' }}">
                                                <label for="black_list_reasone" class="form-label fs-6 fw-bolder text-black-700 mb-3">Black List Reasone</label>
                                                <textarea class="form-control" name="black_list_reasone" cols="50" rows="2" id="black_list_reasone" placeholder="Enter black list reasone here...">{{ old('black_list_reasone', optional($employee)->black_list_reasone) }}</textarea>
                                                {!! $errors->first('black_list_reasone', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('election_card_no') ? 'has-error' : '' }}">
                                                <label for="election_card_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Election Card No</label>
                                                <input class="form-control" name="election_card_no" type="text" id="election_card_no" value="{{ old('election_card_no', optional($employee)->election_card_no) }}" placeholder="Enter election card no here...">
                                                {!! $errors->first('election_card_no', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('driving_licenses_no') ? 'has-error' : '' }}">
                                                <label for="driving_licenses_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Driving Licenses No</label>
                                                <input class="form-control" name="driving_licenses_no" type="text" id="driving_licenses_no" value="{{ old('driving_licenses_no', optional($employee)->driving_licenses_no) }}" placeholder="Enter driving licenses no here...">
                                                {!! $errors->first('driving_licenses_no', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('rto') ? 'has-error' : '' }}">
                                                <label for="rto" class="form-label fs-6 fw-bolder text-black-700 mb-3">RTO</label>
                                                <input class="form-control" name="rto" type="text" id="rto" value="{{ old('rto', optional($employee)->rto) }}" placeholder="Enter rto here...">
                                                {!! $errors->first('rto', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('rto_state_id') ? 'has-error' : '' }}">
                                                <label for="rto_state_id" class="form-label fs-6 fw-bolder text-black-700 mb-3">Rto State</label>
                                                <select class="form-select" aria-label="Select a Rto State" data-control="select2" data-placeholder="Select Rto State" id="rto_state_id" name="rto_state_id">
                                                    <option value="" style="display: none;" {{ old('rto_state_id', optional($employee)->rto_state_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select rto state</option>
                                                    @foreach ($rtoStates as $key => $rtoState)
                                                    <option value="{{ $key }}" {{ old('rto_state_id', optional($employee)->rto_state_id) == $key ? 'selected' : '' }}>
                                                        {{ $rtoState }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                                {!! $errors->first('rto_state_id', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('passport_no') ? 'has-error' : '' }}">
                                                <label for="passport_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Passport No</label>
                                                <input class="form-control" name="passport_no" type="text" id="passport_no" value="{{ old('passport_no', optional($employee)->passport_no) }}" placeholder="Enter passport no here...">
                                                {!! $errors->first('passport_no', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('pancard_no') ? 'has-error' : '' }}">
                                                <label for="pancard_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Pancard No</label>
                                                <input class="form-control" name="pancard_no" type="text" id="pancard_no" value="{{ old('pancard_no', optional($employee)->pancard_no) }}" placeholder="Enter pancard no here...">
                                                {!! $errors->first('pancard_no', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('aadhar_card_no') ? 'has-error' : '' }}">
                                                <label for="aadhar_card_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Aadhar Card No</label>
                                                <input class="form-control" name="aadhar_card_no" type="number" id="aadhar_card_no" value="{{ old('aadhar_card_no', optional($employee)->aadhar_card_no) }}" placeholder="Enter aadhar card no here..." minlength="12" maxlength="16">
                                                {!! $errors->first('aadhar_card_no', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('reference_relation_contact_1') ? 'has-error' : '' }}">
                                                <label for="reference_relation_contact_1" class="form-label fs-6 fw-bolder text-black-700 mb-3">Reference Relation Contact 1</label>
                                                <input class="form-control" name="reference_relation_contact_1" type="text" id="reference_relation_contact_1" value="{{ old('reference_relation_contact_1', optional($employee)->reference_relation_contact_1) }}" placeholder="Enter reference relation contact 1 here...">
                                                {!! $errors->first('reference_relation_contact_1', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('reference_relation_contact_2') ? 'has-error' : '' }}">
                                                <label for="reference_relation_contact_2" class="form-label fs-6 fw-bolder text-black-700 mb-3">Reference Relation Contact 2</label>
                                                <input class="form-control" name="reference_relation_contact_2" type="text" id="reference_relation_contact_2" value="{{ old('reference_relation_contact_2', optional($employee)->reference_relation_contact_2) }}" placeholder="Enter reference relation contact 2 here...">
                                                {!! $errors->first('reference_relation_contact_2', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('reference_relation_contact_3') ? 'has-error' : '' }}">
                                                <label for="reference_relation_contact_3" class="form-label fs-6 fw-bolder text-black-700 mb-3">Reference Relation Contact 3</label>
                                                <input class="form-control" name="reference_relation_contact_3" type="text" id="reference_relation_contact_3" value="{{ old('reference_relation_contact_3', optional($employee)->reference_relation_contact_3) }}" placeholder="Enter reference relation contact 3 here...">
                                                {!! $errors->first('reference_relation_contact_3', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('emergency_contact_name') ? 'has-error' : '' }}">
                                                <label for="emergency_contact_name" class="form-label fs-6 fw-bolder text-black-700 mb-3">Emergency Contact Name</label>
                                                <input class="form-control" name="emergency_contact_name" type="text" id="emergency_contact_name" value="{{ old('emergency_contact_name', optional($employee)->emergency_contact_name) }}" placeholder="Enter emergency contact name here...">
                                                {!! $errors->first('emergency_contact_name', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('emergency_relation') ? 'has-error' : '' }}">
                                                <label for="emergency_relation" class="form-label fs-6 fw-bolder text-black-700 mb-3">Emergency Relation</label>
                                                <input class="form-control" name="emergency_relation" type="text" id="emergency_relation" value="{{ old('emergency_relation', optional($employee)->emergency_relation) }}" placeholder="Enter emergency relation here...">
                                                {!! $errors->first('emergency_relation', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('emergency_contact_no') ? 'has-error' : '' }}">
                                                <label for="emergency_contact_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Emergency Contact No</label>
                                                <input class="form-control" name="emergency_contact_no" type="number" id="emergency_contact_no" value="{{ old('emergency_contact_no', optional($employee)->emergency_contact_no) }}" placeholder="Enter emergency contact no here...">
                                                {!! $errors->first('emergency_contact_no', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('nominee_1') ? 'has-error' : '' }}">
                                                <label for="nominee_1" class="form-label fs-6 fw-bolder text-black-700 mb-3">Nominee</label>
                                                <input class="form-control" name="nominee_1" type="text" id="nominee_1" value="{{ old('nominee_1', optional($employee)->nominee_1) }}" placeholder="Enter nominee here...">
                                                {!! $errors->first('nominee_1', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('birth_date_1') ? 'has-error' : '' }}">
                                                <label for="birth_date_1" class="form-label fs-6 fw-bolder text-black-700 mb-3">Birth Date</label>
                                                <input class="form-control" name="birth_date_1" type="date" id="birth_date_1" value="{{ old('birth_date_1', optional($employee)->birth_date_1) }}" placeholder="Enter birth date here...">
                                                {!! $errors->first('birth_date_1', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('relation_1') ? 'has-error' : '' }}">
                                                <label for="relation_1" class="form-label fs-6 fw-bolder text-black-700 mb-3">Relation</label>
                                                <input class="form-control" name="relation_1" type="text" id="relation_1" value="{{ old('relation_1', optional($employee)->relation_1) }}" placeholder="Enter relation here...">
                                                {!! $errors->first('relation_1', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
    </form>
</div>
@include('layouts.partials.footer')