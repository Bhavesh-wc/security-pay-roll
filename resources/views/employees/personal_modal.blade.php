<!-- Mother Toungue Modal -->
<div class="modal fade" id="mothertoungueModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New Mother-toungue</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-mothertoungue">
                @csrf
                <input type="hidden" id="hidden" name="hidden" value="hidden">
                <div class="modal-body" id="mothertoungue_add">
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('language_name') ? 'has-error' : '' }}">
                            <label for="language_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Language Name</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="language_name" type="text" id="language_name" value="{{ old('language_name')}}" required="true" placeholder="Enter language name here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3 mt-3">
                        <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                            <option value="" style="display: none;" disabled selected>Select Status</option>
                            @foreach (['active' => 'Active'] as $key => $text)
                            <option value="{{ $key }}">
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary" id="saveBtn">Save</button>
                    </div>
                </div>
                <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="mothertoungue_success" style="display: none">
                    <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                        <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                            </svg>
                        </span>
                        <div class="d-flex flex-column text-light pe-0 text-center">
                            <h4 class="text-dark my-2 py-0">New MotherToungue has been added.</h4>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Qualification Modal -->
<div class="modal fade" id="qualificationModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New Qualification</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-qualification">
                @csrf
                <input type="hidden" id="hidden" name="hidden" value="hidden">
                <div class="modal-body" id="qualification_add">
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('qualification_name') ? 'has-error' : '' }}">
                            <label for="qualification_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Qualification Name</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="qualification_name" type="text" id="qualification_name" value="{{ old('qualification_name') }}" required="true" placeholder="Enter qualification name here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3 mt-3">
                        <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                            <option value="" style="display: none;" disabled selected>Select Status</option>
                            @foreach (['active' => 'Active'] as $key => $text)
                            <option value="{{ $key }}">
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="qualification_success" style="display: none">
                    <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                        <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                            </svg>
                        </span>
                        <div class="d-flex flex-column text-light pe-0 text-center">
                            <h4 class="text-dark my-2 py-0">New Qualification has been added.</h4>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Designation Modal -->
<div class="modal fade" id="designationModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New Designation</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-designation">
                @csrf
                <input type="hidden" id="hidden" name="hidden" value="hidden">
                <div class="modal-body" id="designation_add">
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('designation_name') ? 'has-error' : '' }}">
                            <label for="designation_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Designation Name</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="designation_name" type="text" id="designation_name" value="{{ old('designation_name') }}" required="true" placeholder="Enter designation name here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('category') ? 'has-error' : '' }}">
                            <label for="category" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Category</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="category" type="text" id="category" value="{{ old('category')}}" required="true" placeholder="Enter category here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3 mt-3">
                        <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                            <option value="" style="display: none;" disabled selected>Select Status</option>
                            @foreach (['active' => 'Active'] as $key => $text)
                            <option value="{{ $key }}">
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="designation_success" style="display: none">
                    <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                        <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                            </svg>
                        </span>
                        <div class="d-flex flex-column text-light pe-0 text-center">
                            <h4 class="text-dark my-2 py-0">New Designation has been added.</h4>
                        </div>
                    </div>
                </div>

            </form>
        </div>
    </div>
</div>
<!-- Country Modal -->
<div class="modal fade" id="countryModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New Country</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-country">
                @csrf
                <input type="hidden" id="hidden" name="hidden" value="hidden">
                <div class="modal-body" id="country_add">
                    <!-- <p class="text-success text-center">Country added successfully!</p> -->
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('country_name') ? 'has-error' : '' }}">
                            <label for="country_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Country Name</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="country_name" type="text" id="country_name" value="{{ old('country_name') }}" required="true" placeholder="Enter country name here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3 mt-3">
                        <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                            <option value="" style="display: none;" disabled selected>Select Status</option>
                            @foreach (['active' => 'Active'] as $key => $text)
                            <option value="{{ $key }}">
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="country_success" style="display: none">
                    <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                        <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                            </svg>
                        </span>
                        <div class="d-flex flex-column text-light pe-0 text-center">
                            <h4 class="text-dark my-2 py-0">New Country has been added.</h4>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- State Modal -->
<div class="modal fade" id="stateModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New State</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-state">
                @csrf
                <input type="hidden" id="hidden" name="hidden" value="hidden">
                <div class="modal-body" id="state_add">
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('state_name') ? 'has-error' : '' }}">
                            <label for="state_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">State Name</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="state_name" type="text" id="state_name" value="{{ old('state_name') }}" required="true" placeholder="Enter state name here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('country_id') ? 'has-error' : '' }}">
                            <label for="country_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Country</label>
                            <div class="col-md-12">
                                <select name="country_id" id="country-modal" aria-label="Select a Country" data-control="select2" data-placeholder="Select Country" class="form-select mb-2">
                                    <option value="" style="display: none;" {{ old('country_id') }} disabled selected>Select country</option>
                                    @foreach ($countries as $key => $country)
                                    <option value="{{ $key }}">
                                        {{ $country }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3 mt-3">
                        <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                            <option value="" style="display: none;" disabled selected>Select Status</option>
                            @foreach (['active' => 'Active'] as $key => $text)
                            <option value="{{ $key }}">
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="state_success" style="display: none">
                    <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                        <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                            </svg>
                        </span>
                        <div class="d-flex flex-column text-light pe-0 text-center">
                            <h4 class="text-dark my-2 py-0">New State has been added.</h4>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- District Modal -->
<div class="modal fade" id="destrictModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New District</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-district">
                @csrf
                <input type="hidden" id="hidden" name="hidden" value="hidden">
                <div class="modal-body" id="district_add">
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('district_name') ? 'has-error' : '' }}">
                            <label for="district_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">District Name</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="district_name" type="text" id="district_name" value="{{ old('district_name') }}" required="true" placeholder="Enter district name here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('state_id') ? 'has-error' : '' }}">
                            <label for="state_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">State</label>
                            <div class="col-md-12">
                                <select name="state_id" id="state-modal" aria-label="Select a State" data-control="select2" data-placeholder="Select State" class="form-select mb-2">
                                    <option value="" style="display: none;" {{ old('state_id')}} disabled selected>Select state</option>
                                    @foreach ($states as $key => $state)
                                    <option value="{{ $key }}">
                                        {{ $state }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3 mt-3">
                        <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                            <option value="" style="display: none;" disabled selected>Select Status</option>
                            @foreach (['active' => 'Active'] as $key => $text)
                            <option value="{{ $key }}">
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="district_success" style="display: none">
                    <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                        <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                            </svg>
                        </span>
                        <div class="d-flex flex-column text-light pe-0 text-center">
                            <h4 class="text-dark my-2 py-0">New District has been added.</h4>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- City Modal -->
<div class="modal fade" id="cityModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New City</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-city">
                @csrf
                <input type="hidden" id="hidden" name="hidden" value="hidden">
                <div class="modal-body" id="city_add">
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('city_name') ? 'has-error' : '' }}">
                            <label for="city_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">City Name</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="city_name" type="text" id="city_name" value="{{ old('city_name') }}" required="true" placeholder="Enter city name here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('district_id') ? 'has-error' : '' }}">
                            <label for="district_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">District</label>
                            <div class="col-md-12">
                                <select name="district_id" id="district-modal" aria-label="Select a District" data-control="select2" data-placeholder="Select District" class="form-select mb-2">
                                    <option value="" style="display: none;" {{ old('district_id') }} disabled selected>Select district</option>
                                    @foreach ($districts as $key => $district)
                                    <option value="{{ $key }}">
                                        {{ $district }}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3 mt-3">
                        <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                            <option value="" style="display: none;" disabled selected>Select Status</option>
                            @foreach (['active' => 'Active'] as $key => $text)
                            <option value="{{ $key }}">
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="city_success" style="display: none">
                    <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                        <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                            </svg>
                        </span>
                        <div class="d-flex flex-column text-light pe-0 text-center">
                            <h4 class="text-dark my-2 py-0">New City has been added.</h4>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Pincode Modal -->
<div class="modal fade" id="pincodeModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New Pincode</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-pincode">
                @csrf
                <input type="hidden" id="hidden" name="hidden" value="hidden">
                <div class="modal-body" id="pincode_add">
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('pincode') ? 'has-error' : '' }}">
                            <label for="pincode" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Pincode</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="pincode" type="text" id="pincode" value="{{ old('pincode') }}" required="true" placeholder="Enter pincode here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('city_id') ? 'has-error' : '' }}">
                            <label for="city_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">City</label>
                            <div class="col-md-12">
                                <select name="city_id" id="city-modal" aria-label="Select a City" data-control="select2" data-placeholder="Select City" class="form-select mb-2">
                                    <option value="" style="display: none;" {{ old('city_id')}} disabled selected>Select city</option>
                                    @foreach ($cities as $key => $city)
                                    <option value="{{ $key }}">
                                        {{ $city}}
                                    </option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3 mt-3">
                        <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                            <option value="" style="display: none;" disabled selected>Select Status</option>
                            @foreach (['active' => 'Active'] as $key => $text)
                            <option value="{{ $key }}">
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="pincode_success" style="display: none">
                    <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                        <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                            </svg>
                        </span>
                        <div class="d-flex flex-column text-light pe-0 text-center">
                            <h4 class="text-dark my-2 py-0">New Pincode has been added.</h4>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Grade Modal -->
<div class="modal fade" id="gradeModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New Grade</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-grade">
                <input type="hidden" id="hidden" name="hidden" value="hidden">
                @csrf
                <div class="modal-body" id="grade_add">
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('grade_name') ? 'has-error' : '' }}">
                            <label for="grade_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Grade Name</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="grade_name" type="text" id="grade_name" value="{{ old('grade_name') }}" required="true" placeholder="Enter grade name here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('garde_code') ? 'has-error' : '' }}">
                            <label for="garde_code" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Garde Code</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="garde_code" type="text" id="garde_code" value="{{ old('garde_code') }}" required="true" placeholder="Enter garde code here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3 mt-3">
                        <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                            <option value="" style="display: none;" disabled selected>Select Status</option>
                            @foreach (['active' => 'Active'] as $key => $text)
                            <option value="{{ $key }}">
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="grade_success" style="display: none">
                    <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                        <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                            </svg>
                        </span>
                        <div class="d-flex flex-column text-light pe-0 text-center">
                            <h4 class="text-dark my-2 py-0">New Grade has been added.</h4>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- Department Modal -->
<div class="modal fade" id="departmentModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h3 class="modal-title" id="exampleModalLabel">Create New Department</h3>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <form id="form-department">
                <input type="hidden" id="hidden" name="hidden" value="hidden">
                @csrf
                <div class="modal-body" id="department_add">
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('department_name') ? 'has-error' : '' }}">
                            <label for="department_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Department Name</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="department_name" type="text" id="department_name" value="{{ old('department_name') }}" required="true" placeholder="Enter department name here...">
                            </div>
                        </div>
                    </div>
                    <div class="form-group-mb-3">
                        <div class="form-group {{ $errors->has('department_code') ? 'has-error' : '' }}">
                            <label for="department_code" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Department Code</label>
                            <div class="col-md-12">
                                <input class="form-control mb-2" name="department_code" type="text" id="department_code" value="{{ old('department_code') }}" required="true" placeholder="Enter department code here...">
                            </div>
                        </div>

                    </div>
                    <div class="form-group-mb-3 mt-3">
                        <label for="bank" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select Status" class="form-select" required>
                            <option value="" style="display: none;" disabled selected>Select Status</option>
                            @foreach (['active' => 'Active'] as $key => $text)
                            <option value="{{ $key }}">
                                {{ $text }}
                            </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-primary">Save</button>
                    </div>
                </div>
                <div class="modal-body scroll-y px-10 px-lg-15 pt-5 pb-5" id="department_success" style="display: none">
                    <div class="alert alert-dismissible bg-light-dark border border-dashed border-dark d-flex flex-column flex-sm-row p-5 mb-10">
                        <span class="svg-icon svg-icon-2hx svg-icon-dark me-4 mb-5 mb-sm-0">
                            <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <rect opacity="0.3" x="2" y="2" width="20" height="20" rx="10" fill="currentColor" />
                                <path opacity="0.5" d="M12.4343 12.4343L10.75 10.75C10.3358 10.3358 9.66421 10.3358 9.25 10.75C8.83579 11.1642 8.83579 11.8358 9.25 12.25L12.2929 15.2929C12.6834 15.6834 13.3166 15.6834 13.7071 15.2929L19.25 9.75C19.6642 9.33579 19.6642 8.66421 19.25 8.25C18.8358 7.83579 18.1642 7.83579 17.75 8.25L13.5657 12.4343C13.2533 12.7467 12.7467 12.7467 12.4343 12.4343Z" fill="currentColor" />
                                <path d="M8.43431 12.4343L6.75 10.75C6.33579 10.3358 5.66421 10.3358 5.25 10.75C4.83579 11.1642 4.83579 11.8358 5.25 12.25L8.29289 15.2929C8.68342 15.6834 9.31658 15.6834 9.70711 15.2929L15.25 9.75C15.6642 9.33579 15.6642 8.66421 15.25 8.25C14.8358 7.83579 14.1642 7.83579 13.75 8.25L9.56569 12.4343C9.25327 12.7467 8.74673 12.7467 8.43431 12.4343Z" fill="currentColor" />
                            </svg>
                        </span>
                        <div class="d-flex flex-column text-light pe-0 text-center">
                            <h4 class="text-dark my-2 py-0">New Department has been added.</h4>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<!-- SCRIPT START -->

<!-- mothertoungue script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-mothertoungue').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('mother_tongues.mother_tongue.store') }}",
                data: $('#form-mothertoungue').serialize(),
                success: function(data) {
                    var dropdown = $('#mother_tongue_id');
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.language_name,
                        selected: true
                    }));

                    $("#mothertoungue_add").css('display', 'none');
                    $("#mothertoungue_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#mothertoungue_add").css('display', 'block');
                        $("#mothertoungue_success").css('display', 'none');
                    }, 3000);

                    // Hide modal popup
                    // $('#mothertoungueModal').modal('hide');
                    // Reset form fields
                    $('#form-mothertoungue')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- Quilification Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-qualification').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('qualifications.qualification.store') }}",
                data: $('#form-qualification').serialize(),
                success: function(data) {
                    var dropdown = $('#qualification_id');
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.qualification_name,
                        selected: true
                    }));

                    $("#qualification_add").css('display', 'none');
                    $("#qualification_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#qualification_add").css('display', 'block');
                        $("#qualification_success").css('display', 'none');
                    }, 3000);

                    // Hide modal popup
                    // $('#qualificationModal').modal('hide');
                    // Reset form fields
                    $('#form-qualification')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- Designation Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-designation').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('designations.designation.store') }}",
                data: $('#form-designation').serialize(),
                success: function(data) {
                    var dropdown = $('#designation_id');
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.designation_name,
                        selected: true

                    }));

                    $("#designation_add").css('display', 'none');
                    $("#designation_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#designation_add").css('display', 'block');
                        $("#designation_success").css('display', 'none');
                    }, 3000);

                    // Hide modal popup
                    // $('#designationModal').modal('hide');
                    // Reset form fields
                    $('#form-designation')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- Country Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-country').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('countries.country.store') }}",
                data: $('#form-country').serialize(),
                success: function(data) {
                    var dropdown = $('#country');
                    // var options = '';
                    var dropdownp = $('#country-modal');
                    // options += '<option value="' + data.id + '">' + data.country_name + '</option>';
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.country_name,
                        selected: true
                    }));
                    dropdownp.append($('<option>', {
                        value: data.id,
                        text: data.country_name,
                        selected: true
                    }));

                    $("#country_add").css('display', 'none');
                    $("#country_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#country_add").css('display', 'block');
                        $("#country_success").css('display', 'none');
                    }, 3000);

                    // Hide modal popup
                    // $('#countryModal').modal('hide');
                    // Reset form fields
                    $('#form-country')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- State Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-state').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('states.state.store') }}",
                data: $('#form-state').serialize(),
                success: function(data) {
                    var dropdown = $('#state');
                    var dropdownState = $('#state-modal');
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.state_name,
                        selected: true
                    }));

                    dropdownState.append($('<option>', {
                        value: data.id,
                        text: data.state_name
                    }));

                    $("#state_add").css('display', 'none');
                    $("#state_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#state_add").css('display', 'block');
                        $("#state_success").css('display', 'none');
                    }, 3000);

                    // Hide modal popup
                    // $('#stateModal').modal('hide');
                    // Reset form fields
                    $('#form-state')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- District Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-district').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('districts.district.store') }}",
                data: $('#form-district').serialize(),
                success: function(data) {
                    var dropdown = $('#district');
                    var dropdownDistrict = $('#district-modal');
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.district_name,
                        selected: true
                    }));
                    dropdownDistrict.append($('<option>', {
                        value: data.id,
                        text: data.district_name
                    }));

                    $("#district_add").css('display', 'none');
                    $("#district_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#district_add").css('display', 'block');
                        $("#district_success").css('display', 'none');
                    }, 3000);

                    // Hide modal popup
                    // $('#destrictModal').modal('hide');
                    // Reset form fields
                    $('#form-district')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- City Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-city').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('cities.city.store') }}",
                data: $('#form-city').serialize(),
                success: function(data) {
                    var dropdown = $('#city');
                    var dropdownCity = $('#city-modal');
                    // var options = '';
                    // options += '<option value="' + data.id + '">' + data.city_name + '</option>';
                    // $('#city').html(options);
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.city_name,
                        selected: true
                    }));
                    dropdownCity.append($('<option>', {
                        value: data.id,
                        text: data.city_name
                    }));

                    $("#city_add").css('display', 'none');
                    $("#city_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#city_add").css('display', 'block');
                        $("#city_success").css('display', 'none');
                    }, 3000);

                    // Hide modal popup
                    // $('#cityModal').modal('hide');
                    // Reset form fields
                    $('#form-city')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- pin code Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-pincode').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('pincodes.pincode.store') }}",
                data: $('#form-pincode').serialize(),
                success: function(data) {
                    var dropdown = $('#pincode');
                    var options = '';
                    options += '<option value="' + data.id + '">' + data.pincode + '</option>';
                    $('#pincode').html(options);

                    $("#pincode_add").css('display', 'none');
                    $("#pincode_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#pincode_add").css('display', 'block');
                        $("#pincode_success").css('display', 'none');
                    }, 3000);

                    // Hide modal popup
                    // $('#pincodeModal').modal('hide');
                    // Reset form fields
                    $('#form-pincode')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- Grade Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-grade').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('grades.grade.store') }}",
                data: $('#form-grade').serialize(),
                success: function(data) {
                    var dropdown = $('#grade_id');
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.grade_name,
                        selected: true
                    }));
                    $("#grade_add").css('display', 'none');
                    $("#grade_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#grade_add").css('display', 'block');
                        $("#grade_success").css('display', 'none');
                    }, 3000);
                    // Hide modal popup
                    // $('#gradeModal').modal('hide');
                    // Reset form fields
                    $('#form-grade')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- Department Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-department').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('departments.department.store') }}",
                data: $('#form-department').serialize(),
                success: function(data) {
                    var dropdown = $('#department_id');
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.department_name,
                        selected: true
                    }));

                    $("#department_add").css('display', 'none');
                    $("#department_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#department_add").css('display', 'block');
                        $("#department_success").css('display', 'none');
                    }, 3000);

                    // Hide modal popup
                    // $('#departmentModal').modal('hide');
                    // Reset form fields
                    $('#form-department')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- fix serchbox that not working in modal -->
<script>
    $(document).ready(function() {
        $("#country-modal").select2({
            dropdownParent: $("#stateModal")
        });
    });

    $(document).ready(function() {
        $("#state-modal").select2({
            dropdownParent: $("#destrictModal")
        });
    });

    $(document).ready(function() {
        $("#district-modal").select2({
            dropdownParent: $("#cityModal")
        });
    });

    $(document).ready(function() {
        $("#city-modal").select2({
            dropdownParent: $("#pincodeModal")
        });
    });
</script>