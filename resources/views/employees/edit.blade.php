@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <form method="POST" action="{{ route('employees.employee.update', $employee->id) }}" id="edit_employee_form" name="edit_employee_form" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Employee</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('employees.employee.index') }}" class="text-muted text-hover-primary">Index</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Edit</li>
                        </ul>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{ route('employees.employee.index') }}" class="btn btn-sm btn-danger me-3">List</a>
                        <button type="submit" value="Add" class="btn btn-sm btn-primary me-3">
                            <span class="indicator-label">Save</span>
                        </button>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-xxl">
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                        <strong>{{ $error }}</strong>
                        @endforeach
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PUT">
                        @include ('employees.form', [
                        'employee' => $employee,
                        ])

                    </div>
    </form>
    @include('employees.personal_modal', [
    'countries' => $countries,
    'states' => $states,
    'districts' => $districts,
    'cities' => $cities,
    'pincodes' => $pincodes,
    ])
</div>
</div>
</div>
</div>
@include('layouts.partials.footer')