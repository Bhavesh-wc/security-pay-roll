@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    @include('employees.company_modal')
    <form method="POST" action="{{ route('employees.employee.update', $employee->id) }}" id="edit_company_legal_form" name="edit_company_legal_form" accept-charset="UTF-8" class="form-horizontal" enctype="multipart/form-data">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Comapany Legal</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('employees.employee.index') }}" class="text-muted text-hover-primary">Index</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">company</li>
                        </ul>
                    </div>
                    <div class="d-flex justify-content-end">
                        <a href="{{ route('employees.employee.index') }}" class="btn btn-sm btn-danger me-3">List</a>
                        <button type="submit" value="Update" class="btn btn-sm btn-primary me-3">
                            <span class="indicator-label">Save</span>
                        </button>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content flex-column-fluid">
                <div id="kt_app_content_container" class="app-container container-xxl">
                    @if ($errors->any())
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        @foreach ($errors->all() as $error)
                        <strong>{{ $error }}</strong>
                        @endforeach
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    <div class="d-flex flex-column flex-row-fluid gap-7 gap-lg-10">
                        {{ csrf_field() }}
                        <input name="_method" type="hidden" value="PUT">
                        <div class="d-flex flex-column gap-7 gap-lg-10">
                            <div class="card card-flush py-4">
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('bank_id') ? 'has-error' : '' }}">
                                                <label for="bank_id" class="form-label fs-6 fw-bolder text-black-700 mb-3">Bank</label>
                                                <div class="d-flex">
                                                    <select class="form-select" aria-label="Select a Bank" data-control="select2" data-placeholder="Select bank" id="bank" name="bank_id">
                                                        <option value="" style="display: none;" {{ old('bank_id', optional($employee)->bank_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select Bank</option>
                                                        @foreach ($bank as $list)
                                                        <option value="{{$list->id}}" {{ old('bank_id', optional($employee)->bank_id) == $list->id ? 'selected' : '' }}>
                                                            {{$list->bank_name}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                    <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#bankModal">ADD</button>
                                                    {!! $errors->first('bank_id', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('bank_branch_id') ? 'has-error' : '' }}">
                                                <label for="bank_branch_id" class="form-label fs-6 fw-bolder text-black-700 mb-3">Bank Branch</label>
                                                <div class="d-flex">
                                                    <select class="form-select" aria-label="Select a Bank Branch" data-control="select2" data-placeholder="Select bank branch" id="bank_branch" name="bank_branch_id">
                                                        <option value="" style="display: none;" {{ old('bank_branch_id', optional($employee)->bank_branch_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select Bank Branch</option>
                                                        @foreach ($bankBranches as $list)
                                                        <option value="{{$list->id}}" {{ old('bank_branch_id', optional($employee)->bank_branch_id) == $list->id ? 'selected' : '' }}>
                                                            {{$list->bank_branch_name}}
                                                        </option>
                                                        @endforeach
                                                    </select>
                                                    <button class="btn btn-secondary ms-3" type="button" data-bs-toggle="modal" data-bs-target="#bankbranchModal">ADD</button>
                                                    {!! $errors->first('bank_branch_id', '<p class="help-block">:message</p>') !!}
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('ifsc_code') ? 'has-error' : '' }}">
                                                <label for="ifsc_code" class="form-label fs-6 fw-bolder text-black-700 mb-3">IFSC Code</label>

                                                <input class="form-control" name="ifsc_code" type="text" id="ifsc_codes" value="{{ old('ifsc_code', optional($employee)->ifsc_code) }}" placeholder="Enter IFSC code here...">
                                                
                                                {!! $errors->first('ifsc_code', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('bank_ac_no') ? 'has-error' : '' }}">
                                                <label for="bank_ac_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Bank Ac No</label>
                                                <input class="form-control" name="bank_ac_no" type="number" id="bank_ac_no" value="{{ old('bank_ac_no', optional($employee)->bank_ac_no) }}" placeholder="Enter bank ac no here...">
                                                {!! $errors->first('bank_ac_no', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('interview_date') ? 'has-error' : '' }}">
                                                <label for="interview_date" class="form-label fs-6 fw-bolder text-black-700 mb-3">Interview Date</label>
                                                <input class="form-control" name="interview_date" type="date" id="interview_date" value="{{ old('interview_date', optional($employee)->interview_date) }}" placeholder="Enter interview date here...">
                                                {!! $errors->first('interview_date', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('senior_citizen') ? 'has-error' : '' }}">
                                                <label for="senior_citizen" class="form-label fs-6 fw-bolder text-black-700 mb-3">Senior Citizen</label>
                                                <input class="form-control" name="senior_citizen" type="text" id="senior_citizen" value="{{ old('senior_citizen', optional($employee)->senior_citizen) }}" placeholder="Enter senior citizen here...">
                                                {!! $errors->first('senior_citizen', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                                                <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                                                <select class="form-select" aria-label="Select a marital status" data-control="select2" data-placeholder="Select marital status" id="status" name="status" required="true">
                                                    <option value="" style="display: none;" {{ old('status', optional($employee)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Enter status here...</option>
                                                    @foreach (['active' => 'Join',
                                                    'inactive' => 'Leave'] as $key => $text)
                                                    <option value="{{ $key }}" {{ old('status', optional($employee)->status) == $key ? 'selected' : '' }}>
                                                        {{ $text }}
                                                    </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('relieve_date') ? 'has-error' : '' }}">
                                                <label for="relieve_date" class="form-label fs-6 fw-bolder text-black-700 mb-3">Relieve Date</label>
                                                <input class="form-control" name="relieve_date" type="date" id="relieve_date" value="{{ old('relieve_date', optional($employee)->relieve_date) }}" placeholder="Enter relieve date here...">
                                                {!! $errors->first('relieve_date', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <!-- <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('final_relieve_date') ? 'has-error' : '' }}">
                                                <label for="final_relieve_date" class="form-label fs-6 fw-bolder text-black-700 mb-3">Final Relieve Date</label>
                                                <input class="form-control" name="final_relieve_date" type="date" id="final_relieve_date" value="{{ old('final_relieve_date', optional($employee)->final_relieve_date) }}" placeholder="Enter final relieve date here...">
                                                {!! $errors->first('final_relieve_date', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div> -->

                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('security_deposit') ? 'has-error' : '' }}">
                                                <label for="security_deposit" class="form-label fs-6 fw-bolder text-black-700 mb-3">Security Deposit</label>
                                                <input class="form-control" name="security_deposit" type="number" id="security_deposit" value="{{ old('security_deposit', optional($employee)->security_deposit) }}" placeholder="Enter security deposit here...">
                                                {!! $errors->first('security_deposit', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('deposit_detail') ? 'has-error' : '' }}">
                                                <label for="deposit_detail" class="form-label fs-6 fw-bolder text-black-700 mb-3">Deposit Detail</label>
                                                <textarea class="form-control" name="deposit_detail" cols="1" rows="1" id="deposit_detail" placeholder="Enter deposit detail here...">{{ old('deposit_detail', optional($employee)->deposit_detail) }}</textarea>
                                                {!! $errors->first('deposit_detail', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('refund_date') ? 'has-error' : '' }}">
                                                <label for="refund_date" class="form-label fs-6 fw-bolder text-black-700 mb-3">Refund Date</label>
                                                <input class="form-control" name="refund_date" type="date" id="refund_date" value="{{ old('refund_date', optional($employee)->refund_date) }}" placeholder="Enter refund date here...">
                                                {!! $errors->first('refund_date', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('refund_detail') ? 'has-error' : '' }}">
                                                <label for="refund_detail" class="form-label fs-6 fw-bolder text-black-700 mb-3">Refund Detail</label>
                                                <textarea class="form-control" name="refund_detail" cols="1" rows="1" id="refund_detail" placeholder="Enter refund detail here...">{{ old('refund_detail', optional($employee)->refund_detail) }}</textarea>
                                                {!! $errors->first('refund_detail', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('due_date') ? 'has-error' : '' }}">
                                                <label for="due_date" class="form-label fs-6 fw-bolder text-black-700 mb-3">Due Date</label>
                                                <input class="form-control" name="due_date" type="date" id="due_date" value="{{ old('due_date', optional($employee)->due_date) }}" placeholder="Enter due date here...">
                                                {!! $errors->first('due_date', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('probation_date') ? 'has-error' : '' }}">
                                                <label for="probation_date" class="form-label fs-6 fw-bolder text-black-700 mb-3">Probation Date</label>
                                                <input class="form-control" name="probation_date" type="date" id="probation_date" value="{{ old('probation_date', optional($employee)->probation_date) }}" placeholder="Enter probation date here...">
                                                {!! $errors->first('probation_date', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="card-body pt-0">
                                            <div class="form-group {{ $errors->has('confirmation_date') ? 'has-error' : '' }}">
                                                <label for="confirmation_date" class="form-label fs-6 fw-bolder text-black-700 mb-3">Confirmation Date</label>
                                                <input class="form-control" name="confirmation_date" type="date" id="confirmation_date" value="{{ old('confirmation_date', optional($employee)->confirmation_date) }}" placeholder="Enter confirmation date here...">
                                                {!! $errors->first('confirmation_date', '<p class="help-block">:message</p>') !!}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
<script>
    jQuery(document).ready(function() {
        jQuery('#bank').change(function() {
            let bid = jQuery(this).val();
            jQuery.ajax({
                url: '/admin/fetch_branch',
                type: 'post',
                data: 'bid=' + bid + '&_token={{csrf_token()}}',
                success: function(result) {
                    jQuery('#bank_branch').html(result)
                }
            });
        });

        jQuery('#bank_branch').change(function() {
            let bid = jQuery(this).val();
            jQuery.ajax({
                url: '/admin/fetch_ifsc_company_legal',
                type: 'post',
                data: 'bid=' + bid + '&_token={{csrf_token()}}',
                success: function(result) {
                    console.log(result.ifsc_code);
                    jQuery('#ifsc_codes').val(result.ifsc_code);
                }
            });
        });

    });
</script>

<!-- Bank Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-bank').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('banks.bank.store') }}",
                data: $('#form-bank').serialize(),
                success: function(data) {
                    var dropdown = $('#bank');
                    var dropdownbank = $('#bank-modal');
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.bank_name,
                        selected: true
                    }));
                    dropdownbank.append($('<option>', {
                        value: data.id,
                        text: data.bank_name
                    }));
                    $("#bank_add").css('display', 'none');
                    $("#bank_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#bank_add").css('display', 'block');
                        $("#bank_success").css('display', 'none');
                    }, 3000);

                    // Hide modal popup
                    // $('#bankModal').modal('hide');
                    // Reset form fields
                    $('#form-bank')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>

<!-- Bank Branch Script -->
<script>
    $(document).ready(function() {
        // Submit form data via AJAX
        $('#form-bankbranch').submit(function(e) {
            e.preventDefault();
            $.ajax({
                type: 'POST',
                url: "{{ route('bank_branches.bank_branch.store') }}",
                data: $('#form-bankbranch').serialize(),
                success: function(data) {
                    var dropdown = $('#bank_branch');
                    dropdown.append($('<option>', {
                        value: data.id,
                        text: data.bank_branch_name,
                        selected: true
                    }));
                    $("#bankbranch_add").css('display', 'none');
                    $("#bankbranch_success").css('display', 'block');
                    setTimeout(function() {
                        $('#skill_name').val('');
                        $("#bankbranch_add").css('display', 'block');
                        $("#bankbranch_success").css('display', 'none');
                    }, 3000);
                    // Hide modal popup
                    // $('#bankbranchModal').modal('hide');
                    // Reset form fields
                    $('#form-bankbranch')[0].reset();
                },
                error: function(data) {
                    console.log('Error:', data);
                }
            });
        });
    });
</script>


<script>
    $(document).ready(function() {
        $("#bank-modal").select2({
            dropdownParent: $("#bankbranchModal")
        });
    });
</script>

@include('layouts.partials.footer')