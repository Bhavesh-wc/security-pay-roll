
<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('stock_category_name') ? 'has-error' : '' }}">
                        <label for="stock_category_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Stock Category Name</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="stock_category_name" type="text" id="stock_category_name" value="{{ old('stock_category_name', optional($stockCategory)->stock_category_name) }}" required="true" placeholder="Enter stock category name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select">
                                <option value="" style="display: none;" {{ old('status', optional($stockCategory)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                                @foreach (['active' => 'Active','inactive' => 'Inactive'] as $key => $text)
                                <option value="{{ $key }}" {{ old('status', optional($stockCategory)->status) == $key ? 'selected' : '' }}>
                                    {{ $text }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>