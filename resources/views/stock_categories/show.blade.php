@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Stock Category' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('stock_categories.stock_category.destroy', $stockCategory->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('stock_categories.stock_category.index') }}" class="btn btn-primary" title="Show All Stock Category">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('stock_categories.stock_category.create') }}" class="btn btn-success" title="Create New Stock Category">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('stock_categories.stock_category.edit', $stockCategory->id ) }}" class="btn btn-primary" title="Edit Stock Category">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Stock Category" onclick="return confirm(&quot;Click Ok to delete Stock Category.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Stock Category Name</dt>
            <dd>{{ $stockCategory->stock_category_name }}</dd>
            <dt>Status</dt>
            <dd>{{ $stockCategory->status }}</dd>

        </dl>

    </div>
</div>

@endsection