<div class="d-flex flex-column gap-7 gap-lg-10">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
            <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" value="{{ old('monthyear', optional($taxManager)->monthyear) }}" minlength="1" required="true" placeholder="Enter Monthyear here..." required="true">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select">
                                <option value="" style="display: none;" {{ old('status', optional($taxManager)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                                    @foreach (['active' => 'Active', 'inactive' => 'Inactive'] as $key => $text)
                                <option value="{{ $key }}" {{ old('status', optional($taxManager)->status) == $key ? 'selected' : '' }}>
                                    {{ $text }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        <div class="row gx-10 mb-5">
            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('pf') ? 'has-error' : '' }}">
                    <label for="pf" class="required form-label fs-6 fw-bolder text-black-700 mb-3">PF(%)</label>
                    <div class="col-md-12">
                        <input class="form-control mb-2" name="employee_pf" type="number" id="pf" value="{{ old('pf', optional($taxManager)->pf) }}" required="true" placeholder="Enter pf here...">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('employee_admin') ? 'has-error' : '' }}">
                    <label for="employee_admin" class=" form-label fs-6 fw-bolder text-black-700 mb-3">Employee Admin(%)</label>
                    <div class="col-md-12">
                        <input class="form-control mb-2" name="employee_admin" type="number" id="employee_admin" value="0.5" step="any" placeholder="Enter employee admin...">
                    </div>
                </div>
            </div>
        </div>
            <div class="row gx-10 mb-5">
            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('employee_pf') ? 'has-error' : '' }}">
                    <label for="employee_pf" class=" form-label fs-6 fw-bolder text-black-700 mb-3">Employee PF(%)</label>
                    <div class="col-md-12">
                        <input class="form-control mb-2" name="employer_pf" type="number" id="employee_pf" value="3.67" step="any" placeholder="Enter employee pf...">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('employee_fpf') ? 'has-error' : '' }}">
                    <label for="employee_fpf" class=" form-label fs-6 fw-bolder text-black-700 mb-3">Employee FPF(%)</label>
                    <div class="col-md-12">
                        <input class="form-control mb-2" name="employer_fpf" type="number" id="employee_fpf" value="8.33" step="any"  placeholder="Enter employee fpf...">
                    </div>
                </div>
            </div>
        </div>
        <div class="row gx-10 mb-5">
            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('employee_edli') ? 'has-error' : '' }}">
                    <label for="employee_edli" class=" form-label fs-6 fw-bolder text-black-700 mb-3">Employee EDLI(%)</label>
                    <div class="col-md-12">
                        <input class="form-control mb-2" name="employer_edli" type="number" id="employee_edli" value="0.5" step="any" placeholder="Enter employee edli...">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('employee_inepc') ? 'has-error' : '' }}">
                    <label for="employee_inepc" class=" form-label fs-6 fw-bolder text-black-700 mb-3">Employee Inepc(%)</label>
                    <div class="col-md-12">
                        <input class="form-control mb-2" name="employer_inepc" type="number" id="employee_inepc" value="0.01" step="any" placeholder="Enter employee inepc...">
                    </div>
                </div>
            </div>
        </div>
        <div class="row gx-10 mb-5">
            <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('employee_esic') ? 'has-error' : '' }}">
                        <label for="esic" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee ESIC(%)</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="employee_esic" type="number" id="esic" value="{{ old('employee_esic', optional($taxManager)->employee_esic) }}" required="true" placeholder="Enter Employee ESIC here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('employer_esic') ? 'has-error' : '' }}">
                        <label for="esic" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employer ESIC(%)</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="employer_esic" type="number" id="esic" value="{{ old('employer_esic', optional($taxManager)->employer_esic) }}" required="true" placeholder="Enter Employer ESIC here...">
                        </div>
                    </div>
                </div>
            </div>

         <div class="row gx-10 mb-5">
              <div class="col-lg-6">
                <div class="form-group {{ $errors->has('professional_tax') ? 'has-error' : '' }}">
                    <label for="professional_tax" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Professional Tax(₹)</label>
                    <div class="col-md-12">
                        <input class="form-control mb-2" name="professional_tax" type="number" id="professional_tax" value="{{ old('professional_tax', optional($taxManager)->professional_tax) }}" minlength="1" required="true" placeholder="Enter professional tax here...">
                    </div>
                </div>
              </div>
             <div class="col-lg-6">
                <div class="form-group {{ $errors->has('min_salary') ? 'has-error' : '' }}">
                    <label for="min_salary" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Minimum Salary(₹)</label>
                    <div class="col-md-12">
                        <input class="form-control mb-2" name="min_salary" type="number" id="min_salary" value="{{ old('min_salary', optional($taxManager)->min_salary) }}" minlength="1" required="true" placeholder="Enter minimum salary here...">
                    </div>
                </div>
             </div>
        </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('welfare_fund') ? 'has-error' : '' }}">
                        <label for="welfare_fund" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Welfare Fund(₹)</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="welfare_fund" type="number" id="welfare_fund" value="{{ old('welfare_fund', optional($taxManager)->welfare_fund) }}" required="true" placeholder="Enter welfare fund here...">
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>