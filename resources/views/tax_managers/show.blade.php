@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Tax Manager' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('tax_managers.tax_manager.destroy', $taxManager->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('tax_managers.tax_manager.index') }}" class="btn btn-primary" title="Show All Tax Manager">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('tax_managers.tax_manager.create') }}" class="btn btn-success" title="Create New Tax Manager">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('tax_managers.tax_manager.edit', $taxManager->id ) }}" class="btn btn-primary" title="Edit Tax Manager">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Tax Manager" onclick="return confirm(&quot;Click Ok to delete Tax Manager.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Start Date</dt>
            <dd>{{ $taxManager->start_date }}</dd>
            <dt>End Date</dt>
            <dd>{{ $taxManager->end_date }}</dd>
            <dt>Pf</dt>
            <dd>{{ $taxManager->pf }}</dd>
            <dt>Esic</dt>
            <dd>{{ $taxManager->esic }}</dd>
            <dt>Professional Tax</dt>
            <dd>{{ $taxManager->professional_tax }}</dd>
            <dt>Welfare Fund</dt>
            <dd>{{ $taxManager->welfare_fund }}</dd>
            <dt>Status</dt>
            <dd>{{ $taxManager->status }}</dd>

        </dl>

    </div>
</div>

@endsection