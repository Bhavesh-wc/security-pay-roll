<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('district_name') ? 'has-error' : '' }}">
                        <label for="district_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">District Name</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="district_name" type="text" id="district_name" value="{{ old('district_name', optional($district)->district_name) }}" required="true" placeholder="Enter district name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('state_id') ? 'has-error' : '' }}">
                        <label for="state_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">State</label>
                        <div class="col-md-12">
                            <select name="state_id" id="state_id" aria-label="Select a state" data-control="select2" data-placeholder="Select state" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('state_id', optional($district)->state_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select state</option>
                                @foreach ($states as $key => $state)
                                    <option value="{{ $key }}" {{ old('state_id', optional($district)->state_id) == $key ? 'selected' : '' }}>
                                        {{ $state }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('status', optional($district)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                            @foreach (['active' => 'Active','inactive' => 'Inactive'] as $key => $text)
                                <option value="{{ $key }}" {{ old('status', optional($district)->status) == $key ? 'selected' : '' }}>
                                    {{ $text }}
                                </option>
                            @endforeach
                        </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>