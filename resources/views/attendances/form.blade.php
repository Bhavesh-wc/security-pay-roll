<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('branch_id') ? 'has-error' : '' }}">
                            <label for="branch_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                            <select id="branch_id" name="branch_id" class="form-select" data-control="select2" aria-label="Select a Branch" data-placeholder="Select branch" required="true">
                                <option value="" style="display: none;" {{ old('branch_id', optional($attendance)->branch_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select branch</option>
                                @foreach ($branches as $key => $branch)
                                <option value="{{ $key }}" {{ old('branch_id', optional($attendance)->branch_id) == $key ? 'selected' : '' }}>
                                    {{ $branch }}
                                </option>
                                @endforeach
                            </select>
                            {!! $errors->first('branch_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>

                    <div class="col-lg-6">
                        <div class="form-group {{ $errors->has('designation_id') ? 'has-error' : '' }}">
                            <label for="designation_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Designation</label>
                            <div class="d-flex">
                                <select id="designation_id" class="form-select" name="designation_id" data-placeholder="Select designation" data-control="select2" aria-label="Select a Designation" required="true">
                                    <option value="" style="display: none;" {{ old('designation_id', optional($attendance)->designation_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select designation</option>
                                    @foreach ($designations as $key => $designation)
                                    <option value="{{ $key }}" {{ old('designation_id', optional($attendance)->designation_id) == $key ? 'selected' : '' }}>
                                        {{ $designation }}
                                    </option>
                                    @endforeach    
                                </select>
                            </div>
                        </div>
                    </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('employee_id') ? 'has-error' : '' }}">
                        <label for="employee_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee</label>
                        <div class="col-md-12">
                            <select id="employee_id" class="form-select" name="employee_id" data-placeholder="Select employee" data-control="select2" aria-label="Select a Employee" required="true">
                                <option value="" style="display: none;" {{ old('employee_id', optional($attendance)->employee_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select employee</option>
                                @foreach ($employees as $key => $employee)
                                <option value="{{ $key }}" {{ old('employee_id', optional($attendance)->employee_id) == $key ? 'selected' : '' }}>
                                    {{ $employee }}
                                </option>
                                @endforeach
                            </select> 
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" value="{{ old('monthyear', optional($attendance)->monthyear) }}" minlength="1" required="true" placeholder="Enter Monthyear here..." required="true">
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row gx-10 mb-5">
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_1') ? 'has-error' : '' }}">
                        <label for="day_1_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 1</label>
                        <div class="col-md-12">
                            <label for="day_1_p" class="radio-inline">
                                <input id="day_1_p" class="" name="day_1" type="radio" value="P" checked="checked" {{ old('day_1', optional($attendance)->day_1) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_1_a" class="radio-inline">
                                <input id="day_1_a" class="" name="day_1" type="radio" value="A" {{ old('day_1', optional($attendance)->day_1) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_1_h" class="radio-inline">
                                <input id="day_1_h" class="" name="day_1" type="radio" value="H" {{ old('day_1', optional($attendance)->day_1) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_2') ? 'has-error' : '' }}">
                        <label for="day_2_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 2</label>
                        <div class="col-md-12">
                            <label for="day_2_p" class="radio-inline">
                                <input id="day_2_p" class="" name="day_2" type="radio" value="P" checked="checked" {{ old('day_2', optional($attendance)->day_2) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_2_a" class="radio-inline">
                                <input id="day_2_a" class="" name="day_2" type="radio" value="A" {{ old('day_2', optional($attendance)->day_2) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_2_h" class="radio-inline">
                                <input id="day_2_h" class="" name="day_2" type="radio" value="H" {{ old('day_2', optional($attendance)->day_2) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_3') ? 'has-error' : '' }}">
                        <label for="day_3_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 3</label>
                        <div class="col-md-12">
                            <label for="day_3_p" class="radio-inline">
                                <input id="day_3_p" class="" name="day_3" type="radio" value="P" checked="checked" {{ old('day_3', optional($attendance)->day_3) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_3_a" class="radio-inline">
                                <input id="day_3_a" class="" name="day_3" type="radio" value="A" {{ old('day_3', optional($attendance)->day_3) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_3_h" class="radio-inline">
                                <input id="day_3_h" class="" name="day_3" type="radio" value="H" {{ old('day_3', optional($attendance)->day_3) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_4') ? 'has-error' : '' }}">
                        <label for="day_4_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 4</label>
                        <div class="col-md-12">
                            <label for="day_4_p" class="radio-inline">
                                <input id="day_4_p" class="" name="day_4" type="radio" value="P" checked="checked" {{ old('day_4', optional($attendance)->day_4) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_4_a" class="radio-inline">
                                <input id="day_4_a" class="" name="day_4" type="radio" value="A" {{ old('day_4', optional($attendance)->day_4) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_4_h" class="radio-inline">
                                <input id="day_4_h" class="" name="day_4" type="radio" value="H" {{ old('day_4', optional($attendance)->day_4) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_5') ? 'has-error' : '' }}">
                        <label for="day_5_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 5</label>
                        <div class="col-md-12">
                            <label for="day_5_p" class="radio-inline">
                                <input id="day_5_p" class="" name="day_5" type="radio" value="P" checked="checked" {{ old('day_5', optional($attendance)->day_5) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_5_a" class="radio-inline">
                                <input id="day_5_a" class="" name="day_5" type="radio" value="A" {{ old('day_5', optional($attendance)->day_5) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_5_h" class="radio-inline">
                                <input id="day_5_h" class="" name="day_5" type="radio" value="H" {{ old('day_5', optional($attendance)->day_5) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_6') ? 'has-error' : '' }}">
                        <label for="day_6_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 6</label>
                        <div class="col-md-12">
                            <label for="day_6_p" class="radio-inline">
                                <input id="day_6_p" class="" name="day_6" type="radio" value="P" checked="checked" {{ old('day_6', optional($attendance)->day_6) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_6_a" class="radio-inline">
                                <input id="day_6_a" class="" name="day_6" type="radio" value="A" {{ old('day_6', optional($attendance)->day_6) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_6_h" class="radio-inline">
                                <input id="day_6_h" class="" name="day_6" type="radio" value="H" {{ old('day_6', optional($attendance)->day_6) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_7') ? 'has-error' : '' }}">
                        <label for="day_7_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 7</label>
                        <div class="col-md-12">
                            <label for="day_7_p" class="radio-inline">
                                <input id="day_7_p" class="" name="day_7" type="radio" value="P" checked="checked" {{ old('day_7', optional($attendance)->day_7) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_7_a" class="radio-inline">
                                <input id="day_7_a" class="" name="day_7" type="radio" value="A" {{ old('day_7', optional($attendance)->day_7) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_7_h" class="radio-inline">
                                <input id="day_7_h" class="" name="day_7" type="radio" value="H" {{ old('day_7', optional($attendance)->day_7) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_8') ? 'has-error' : '' }}">
                        <label for="day_8_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 8</label>
                        <div class="col-md-12">
                            <label for="day_8_p" class="radio-inline">
                                <input id="day_8_p" class="" name="day_8" type="radio" value="P" checked="checked" {{ old('day_8', optional($attendance)->day_8) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_8_a" class="radio-inline">
                                <input id="day_8_a" class="" name="day_8" type="radio" value="A" {{ old('day_8', optional($attendance)->day_8) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_8_h" class="radio-inline">
                                <input id="day_8_h" class="" name="day_8" type="radio" value="H" {{ old('day_8', optional($attendance)->day_8) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_9') ? 'has-error' : '' }}">
                        <label for="day_9_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 9</label>
                        <div class="col-md-12">
                            <label for="day_9_p" class="radio-inline">
                                <input id="day_9_p" class="" name="day_9" type="radio" value="P" checked="checked" {{ old('day_9', optional($attendance)->day_9) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_9_a" class="radio-inline">
                                <input id="day_9_a" class="" name="day_9" type="radio" value="A" {{ old('day_9', optional($attendance)->day_9) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_9_h" class="radio-inline">
                                <input id="day_9_h" class="" name="day_9" type="radio" value="H" {{ old('day_9', optional($attendance)->day_9) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_10') ? 'has-error' : '' }}">
                        <label for="day_10_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 10</label>
                        <div class="col-md-12">
                            <label for="day_10_p" class="radio-inline">
                                <input id="day_10_p" class="" name="day_10" type="radio" value="P" checked="checked" {{ old('day_10', optional($attendance)->day_10) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_10_a" class="radio-inline">
                                <input id="day_10_a" class="" name="day_10" type="radio" value="A" {{ old('day_10', optional($attendance)->day_10) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_10_h" class="radio-inline">
                                <input id="day_10_h" class="" name="day_10" type="radio" value="H" {{ old('day_10', optional($attendance)->day_10) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_11') ? 'has-error' : '' }}">
                        <label for="day_11_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 11</label>
                        <div class="col-md-12">
                            <label for="day_11_p" class="radio-inline">
                                <input id="day_11_p" class="" name="day_11" type="radio" value="P" checked="checked" {{ old('day_11', optional($attendance)->day_11) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_11_a" class="radio-inline">
                                <input id="day_11_a" class="" name="day_11" type="radio" value="A" {{ old('day_11', optional($attendance)->day_11) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_11_h" class="radio-inline">
                                <input id="day_11_h" class="" name="day_11" type="radio" value="H" {{ old('day_11', optional($attendance)->day_11) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_12') ? 'has-error' : '' }}">
                        <label for="day_12_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 12</label>
                        <div class="col-md-12">
                            <label for="day_12_p" class="radio-inline">
                                <input id="day_12_p" class="" name="day_12" type="radio" value="P" checked="checked" {{ old('day_12', optional($attendance)->day_12) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_12_a" class="radio-inline">
                                <input id="day_12_a" class="" name="day_12" type="radio" value="A" {{ old('day_12', optional($attendance)->day_12) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_12_h" class="radio-inline">
                                <input id="day_12_h" class="" name="day_12" type="radio" value="H" {{ old('day_12', optional($attendance)->day_12) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_13') ? 'has-error' : '' }}">
                        <label for="day_13_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 13</label>
                        <div class="col-md-12">
                            <label for="day_13_p" class="radio-inline">
                                <input id="day_13_p" class="" name="day_13" type="radio" value="P" checked="checked" {{ old('day_13', optional($attendance)->day_13) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_13_a" class="radio-inline">
                                <input id="day_13_a" class="" name="day_13" type="radio" value="A" {{ old('day_13', optional($attendance)->day_13) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_13_h" class="radio-inline">
                                <input id="day_13_h" class="" name="day_13" type="radio" value="H" {{ old('day_13', optional($attendance)->day_13) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_14') ? 'has-error' : '' }}">
                        <label for="day_14_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 14</label>
                        <div class="col-md-12">
                            <label for="day_14_p" class="radio-inline">
                                <input id="day_14_p" class="" name="day_14" type="radio" value="P" checked="checked" {{ old('day_14', optional($attendance)->day_14) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_14_a" class="radio-inline">
                                <input id="day_14_a" class="" name="day_14" type="radio" value="A" {{ old('day_14', optional($attendance)->day_14) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_14_h" class="radio-inline">
                                <input id="day_14_h" class="" name="day_14" type="radio" value="H" {{ old('day_14', optional($attendance)->day_14) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_15') ? 'has-error' : '' }}">
                        <label for="day_15_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 15</label>
                        <div class="col-md-12">
                            <label for="day_15_p" class="radio-inline">
                                <input id="day_15_p" class="" name="day_15" type="radio" value="P" checked="checked" {{ old('day_15', optional($attendance)->day_15) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_15_a" class="radio-inline">
                                <input id="day_15_a" class="" name="day_15" type="radio" value="A" {{ old('day_15', optional($attendance)->day_15) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_15_h" class="radio-inline">
                                <input id="day_15_h" class="" name="day_15" type="radio" value="H" {{ old('day_15', optional($attendance)->day_15) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_16') ? 'has-error' : '' }}">
                        <label for="day_16_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 16</label>
                        <div class="col-md-12">
                            <label for="day_16_p" class="radio-inline">
                                <input id="day_16_p" class="" name="day_16" type="radio" value="P" checked="checked" {{ old('day_16', optional($attendance)->day_16) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_16_a" class="radio-inline">
                                <input id="day_16_a" class="" name="day_16" type="radio" value="A" {{ old('day_16', optional($attendance)->day_16) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_16_h" class="radio-inline">
                                <input id="day_16_h" class="" name="day_16" type="radio" value="H" {{ old('day_16', optional($attendance)->day_16) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_17') ? 'has-error' : '' }}">
                        <label for="day_17_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 17</label>
                        <div class="col-md-12">
                            <label for="day_17_p" class="radio-inline">
                                <input id="day_17_p" class="" name="day_17" type="radio" value="P" checked="checked" {{ old('day_17', optional($attendance)->day_17) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_17_a" class="radio-inline">
                                <input id="day_17_a" class="" name="day_17" type="radio" value="A" {{ old('day_17', optional($attendance)->day_17) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_17_h" class="radio-inline">
                                <input id="day_17_h" class="" name="day_17" type="radio" value="H" {{ old('day_17', optional($attendance)->day_17) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_18') ? 'has-error' : '' }}">
                        <label for="day_18_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 18</label>
                        <div class="col-md-12">
                            <label for="day_18_p" class="radio-inline">
                                <input id="day_18_p" class="" name="day_18" type="radio" value="P" checked="checked" {{ old('day_18', optional($attendance)->day_18) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_18_a" class="radio-inline">
                                <input id="day_18_a" class="" name="day_18" type="radio" value="A" {{ old('day_18', optional($attendance)->day_18) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_18_h" class="radio-inline">
                                <input id="day_18_h" class="" name="day_18" type="radio" value="H" {{ old('day_18', optional($attendance)->day_18) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_19') ? 'has-error' : '' }}">
                        <label for="day_19_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 19</label>
                        <div class="col-md-12">
                            <label for="day_19_p" class="radio-inline">
                                <input id="day_19_p" class="" name="day_19" type="radio" value="P" checked="checked" {{ old('day_19', optional($attendance)->day_19) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_19_a" class="radio-inline">
                                <input id="day_19_a" class="" name="day_19" type="radio" value="A" {{ old('day_19', optional($attendance)->day_19) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_19_h" class="radio-inline">
                                <input id="day_19_h" class="" name="day_19" type="radio" value="H" {{ old('day_19', optional($attendance)->day_19) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_20') ? 'has-error' : '' }}">
                        <label for="day_20_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 20</label>
                        <div class="col-md-12">
                            <label for="day_20_p" class="radio-inline">
                                <input id="day_20_p" class="" name="day_20" type="radio" value="P" checked="checked" {{ old('day_20', optional($attendance)->day_20) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_20_a" class="radio-inline">
                                <input id="day_20_a" class="" name="day_20" type="radio" value="A" {{ old('day_20', optional($attendance)->day_20) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_20_h" class="radio-inline">
                                <input id="day_20_h" class="" name="day_20" type="radio" value="H" {{ old('day_20', optional($attendance)->day_20) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_21') ? 'has-error' : '' }}">
                        <label for="day_21_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 21</label>
                        <div class="col-md-12">
                            <label for="day_21_p" class="radio-inline">
                                <input id="day_21_p" class="" name="day_21" type="radio" value="P" checked="checked" {{ old('day_21', optional($attendance)->day_21) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_21_a" class="radio-inline">
                                <input id="day_21_a" class="" name="day_21" type="radio" value="A" {{ old('day_21', optional($attendance)->day_21) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_21_h" class="radio-inline">
                                <input id="day_21_h" class="" name="day_21" type="radio" value="H" {{ old('day_21', optional($attendance)->day_21) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_22') ? 'has-error' : '' }}">
                        <label for="day_22_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 22</label>
                        <div class="col-md-12">
                            <label for="day_22_p" class="radio-inline">
                                <input id="day_22_p" class="" name="day_22" type="radio" value="P" checked="checked" {{ old('day_22', optional($attendance)->day_22) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_22_a" class="radio-inline">
                                <input id="day_22_a" class="" name="day_22" type="radio" value="A" {{ old('day_22', optional($attendance)->day_22) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_22_h" class="radio-inline">
                                <input id="day_22_h" class="" name="day_22" type="radio" value="H" {{ old('day_22', optional($attendance)->day_22) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_23') ? 'has-error' : '' }}">
                        <label for="day_23_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 23</label>
                        <div class="col-md-12">
                            <label for="day_23_p" class="radio-inline">
                                <input id="day_23_p" class="" name="day_23" type="radio" value="P" checked="checked" {{ old('day_23', optional($attendance)->day_23) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_23_a" class="radio-inline">
                                <input id="day_23_a" class="" name="day_23" type="radio" value="A" {{ old('day_23', optional($attendance)->day_23) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_23_h" class="radio-inline">
                                <input id="day_23_h" class="" name="day_23" type="radio" value="H" {{ old('day_23', optional($attendance)->day_23) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_24') ? 'has-error' : '' }}">
                        <label for="day_24_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 24</label>
                        <div class="col-md-12">
                            <label for="day_24_p" class="radio-inline">
                                <input id="day_24_p" class="" name="day_24" type="radio" value="P" checked="checked" {{ old('day_24', optional($attendance)->day_24) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_24_a" class="radio-inline">
                                <input id="day_24_a" class="" name="day_24" type="radio" value="A" {{ old('day_24', optional($attendance)->day_24) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_24_h" class="radio-inline">
                                <input id="day_24_h" class="" name="day_24" type="radio" value="H" {{ old('day_24', optional($attendance)->day_24) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_25') ? 'has-error' : '' }}">
                        <label for="day_25_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 25</label>
                        <div class="col-md-12">
                            <label for="day_25_p" class="radio-inline">
                                <input id="day_25_p" class="" name="day_25" type="radio" value="P" checked="checked" {{ old('day_25', optional($attendance)->day_25) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_25_a" class="radio-inline">
                                <input id="day_25_a" class="" name="day_25" type="radio" value="A" {{ old('day_25', optional($attendance)->day_25) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_25_h" class="radio-inline">
                                <input id="day_25_h" class="" name="day_25" type="radio" value="H" {{ old('day_25', optional($attendance)->day_25) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_26') ? 'has-error' : '' }}">
                        <label for="day_26_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 26</label>
                        <div class="col-md-12">
                            <label for="day_26_p" class="radio-inline">
                                <input id="day_26_p" class="" name="day_26" type="radio" value="P" checked="checked" {{ old('day_26', optional($attendance)->day_26) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_26_a" class="radio-inline">
                                <input id="day_26_a" class="" name="day_26" type="radio" value="A" {{ old('day_26', optional($attendance)->day_26) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_26_h" class="radio-inline">
                                <input id="day_26_h" class="" name="day_26" type="radio" value="H" {{ old('day_26', optional($attendance)->day_26) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_27') ? 'has-error' : '' }}">
                        <label for="day_27_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 27</label>
                        <div class="col-md-12">
                            <label for="day_27_p" class="radio-inline">
                                <input id="day_27_p" class="" name="day_27" type="radio" value="P" checked="checked" {{ old('day_27', optional($attendance)->day_27) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_27_a" class="radio-inline">
                                <input id="day_27_a" class="" name="day_27" type="radio" value="A" {{ old('day_27', optional($attendance)->day_27) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_27_h" class="radio-inline">
                                <input id="day_27_h" class="" name="day_27" type="radio" value="H" {{ old('day_27', optional($attendance)->day_27) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_28') ? 'has-error' : '' }}">
                        <label for="day_28_p" class="radio-inline required form-label fs-6 fw-bolder text-black-700 mb-3">Day 28</label>
                        <div class="col-md-12">
                            <label for="day_28_p" class="radio-inline">
                                <input id="day_28_p" class="" name="day_28" type="radio" value="P" checked="checked" {{ old('day_28', optional($attendance)->day_28) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_28_a" class="radio-inline">
                                <input id="day_28_a" class="" name="day_28" type="radio" value="A" {{ old('day_28', optional($attendance)->day_28) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_28_h" class="radio-inline">
                                <input id="day_28_h" class="" name="day_28" type="radio" value="H" {{ old('day_28', optional($attendance)->day_28) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_29') ? 'has-error' : '' }}">
                        <label for="day_29_p" class="radio-inline form-label fs-6 fw-bolder text-black-700 mb-3">Day 29</label>
                        <div class="col-md-12">
                            <label for="day_29_p" class="radio-inline">
                                <input id="day_29_p" class="" name="day_29" type="radio" value="P" checked="checked" {{ old('day_29', optional($attendance)->day_29) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_29_a" class="radio-inline">
                                <input id="day_29_a" class="" name="day_29" type="radio" value="A" {{ old('day_29', optional($attendance)->day_29) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_29_h" class="radio-inline">
                                <input id="day_29_h" class="" name="day_29" type="radio" value="H" {{ old('day_29', optional($attendance)->day_29) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_30') ? 'has-error' : '' }}">
                        <label for="day_30_p" class="radio-inline form-label fs-6 fw-bolder text-black-700 mb-3">Day 30</label>
                        <div class="col-md-12">
                            <label for="day_30_p" class="radio-inline">
                                <input id="day_30_p" class="" name="day_30" type="radio" value="P" checked="checked" {{ old('day_30', optional($attendance)->day_30) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_30_a" class="radio-inline">
                                <input id="day_30_a" class="" name="day_30" type="radio" value="A" {{ old('day_30', optional($attendance)->day_30) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_30_h" class="radio-inline">
                                <input id="day_30_h" class="" name="day_30" type="radio" value="H" {{ old('day_30', optional($attendance)->day_30) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
                <div class="col-lg-3">
                    <div class="form-group {{ $errors->has('day_31') ? 'has-error' : '' }}">
                        <label for="day_31_p" class="radio-inline form-label fs-6 fw-bolder text-black-700 mb-3">Day 31</label>
                        <div class="col-md-12">
                            <label for="day_31_p" class="radio-inline">
                                <input id="day_31_p" class="" name="day_31" type="radio" value="P" checked="checked" {{ old('day_31', optional($attendance)->day_31) == 'P' ? 'checked' : '' }}>
                                Present
                            </label>
                            <label for="day_31_a" class="radio-inline">
                                <input id="day_31_a" class="" name="day_31" type="radio" value="A" {{ old('day_31', optional($attendance)->day_31) == 'A' ? 'checked' : '' }}>
                                Absent
                            </label>
                            <label for="day_31_h" class="radio-inline">
                                <input id="day_31_h" class="" name="day_31" type="radio" value="H" {{ old('day_31', optional($attendance)->day_31) == 'H' ? 'checked' : '' }}>
                                Holiday
                            </label>
                        </div>
                    </div>
                </div>
            </div> 
        </div>
    </div>
</div>


<!-- dependency dropdown  add by Rajiv  -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $(document).ready(function() {
        $('#branch_id').on('change', function() {
            var idBranch = this.value;
            $.ajax({
                url: "{{url('admin/fetch_branch_designation')}}",
                type: "POST",
                data: {
                    branch_id: idBranch,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function(result) {
                    $('#designation_id').html('<option value="">Select Designation</option>');
                    $.each(result.data, function(key, value) {
                        console.log(value);
                        $("#designation_id").append('<option value="' + value.designation_id + '">' + value.designation.designation_name + '</option>');
                    });
                }
            });
        });

        $('#designation_id').on('change', function() {
            var idDesignation = this.value;
            $.ajax({
                url: "{{url('admin/fetch_designation_employee   ')}}",
                type: "POST",
                data: {
                    designation_id: idDesignation,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function(result) {
                    $('#employee_id').html('<option value="">Select Designation</option>');
                    $.each(result.data, function(key, value) {
                        console.log(value);
                        $("#employee_id").append('<option value="' + value.employee_id + '">' + value.employee.full_name + '</option>');
                    });
                }
            });
        });
    });
</script>
