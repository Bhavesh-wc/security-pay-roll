@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Attendance' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('attendances.attendance.destroy', $attendance->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('attendances.attendance.index') }}" class="btn btn-primary" title="Show All Attendance">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('attendances.attendance.create') }}" class="btn btn-success" title="Create New Attendance">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('attendances.attendance.edit', $attendance->id ) }}" class="btn btn-primary" title="Edit Attendance">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Attendance" onclick="return confirm(&quot;Click Ok to delete Attendance.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Branch</dt>
            <dd>{{ optional($attendance->branch)->name }}</dd>
            <dt>Designation</dt>
            <dd>{{ optional($attendance->designation)->designation_name }}</dd>
            <dt>Employee</dt>
            <dd>{{ optional($attendance->employee)->full_name }}</dd>
            <dt>Monthyear</dt>
            <dd>{{ $attendance->monthyear }}</dd>
            <dt>Month</dt>
            <dd>{{ $attendance->month }}</dd>
            <dt>Year</dt>
            <dd>{{ $attendance->year }}</dd>
            <dt>Day 1</dt>
            <dd>{{ $attendance->day_1 }}</dd>
            <dt>Day 2</dt>
            <dd>{{ $attendance->day_2 }}</dd>
            <dt>Day 3</dt>
            <dd>{{ $attendance->day_3 }}</dd>
            <dt>Day 4</dt>
            <dd>{{ $attendance->day_4 }}</dd>
            <dt>Day 5</dt>
            <dd>{{ $attendance->day_5 }}</dd>
            <dt>Day 6</dt>
            <dd>{{ $attendance->day_6 }}</dd>
            <dt>Day 7</dt>
            <dd>{{ $attendance->day_7 }}</dd>
            <dt>Day 8</dt>
            <dd>{{ $attendance->day_8 }}</dd>
            <dt>Day 9</dt>
            <dd>{{ $attendance->day_9 }}</dd>
            <dt>Day 10</dt>
            <dd>{{ $attendance->day_10 }}</dd>
            <dt>Day 11</dt>
            <dd>{{ $attendance->day_11 }}</dd>
            <dt>Day 12</dt>
            <dd>{{ $attendance->day_12 }}</dd>
            <dt>Day 13</dt>
            <dd>{{ $attendance->day_13 }}</dd>
            <dt>Day 14</dt>
            <dd>{{ $attendance->day_14 }}</dd>
            <dt>Day 15</dt>
            <dd>{{ $attendance->day_15 }}</dd>
            <dt>Day 16</dt>
            <dd>{{ $attendance->day_16 }}</dd>
            <dt>Day 17</dt>
            <dd>{{ $attendance->day_17 }}</dd>
            <dt>Day 18</dt>
            <dd>{{ $attendance->day_18 }}</dd>
            <dt>Day 19</dt>
            <dd>{{ $attendance->day_19 }}</dd>
            <dt>Day 20</dt>
            <dd>{{ $attendance->day_20 }}</dd>
            <dt>Day 21</dt>
            <dd>{{ $attendance->day_21 }}</dd>
            <dt>Day 22</dt>
            <dd>{{ $attendance->day_22 }}</dd>
            <dt>Day 23</dt>
            <dd>{{ $attendance->day_23 }}</dd>
            <dt>Day 24</dt>
            <dd>{{ $attendance->day_24 }}</dd>
            <dt>Day 25</dt>
            <dd>{{ $attendance->day_25 }}</dd>
            <dt>Day 26</dt>
            <dd>{{ $attendance->day_26 }}</dd>
            <dt>Day 27</dt>
            <dd>{{ $attendance->day_27 }}</dd>
            <dt>Day 28</dt>
            <dd>{{ $attendance->day_28 }}</dd>
            <dt>Day 29</dt>
            <dd>{{ $attendance->day_29 }}</dd>
            <dt>Day 30</dt>
            <dd>{{ $attendance->day_30 }}</dd>
            <dt>Day 31</dt>
            <dd>{{ $attendance->day_31 }}</dd>
            <dt>Total</dt>
            <dd>{{ $attendance->total }}</dd>
            <dt>Total Present</dt>
            <dd>{{ $attendance->total_present }}</dd>
            <dt>Total Absent</dt>
            <dd>{{ $attendance->total_absent }}</dd>
            <dt>Total Holiday</dt>
            <dd>{{ $attendance->total_holiday }}</dd>

        </dl>

    </div>
</div>

@endsection