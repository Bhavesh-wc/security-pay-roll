<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label for="name" class=" required form-label fs-6 fw-bolder text-black-700 mb-3">Company Name</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="name" type="text" id="name" value="{{ old('name', optional($user)->name) }}" minlength="1" required="true" placeholder="Enter company name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('company_code') ? 'has-error' : '' }}">
                        <label for="company_code" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Company Code</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="company_code" type="text" id="company_code" value="{{ old('company_code', optional($user)->company_code) }}" minlength="1" required="true" placeholder="Enter company code here...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                        <label for="email" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Email</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="email" type="email" id="email" value="{{ old('email', optional($user)->email) }}" required="true" placeholder="Enter email here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('phone_no') ? 'has-error' : '' }}">
                        <label for="phone_no" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Phone No</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="phone_no" type="number" id="phone_no" value="{{ old('phone_no', optional($user)->phone_no) }}" minlength="10" maxlength="10" required="true" placeholder="Enter phone no here...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('contact_person_name') ? 'has-error' : '' }}">
                        <label for="contact_person_name" class="form-label fs-6 fw-bolder text-black-700 mb-3">Contact Person Name</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="contact_person_name" type="text" id="contact_person_name" value="{{ old('contact_person_name', optional($user)->contact_person_name) }}" placeholder="Enter contact person name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('contact_person_phone_no') ? 'has-error' : '' }}">
                        <label for="contact_person_phone_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Contact Person Phone No</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="contact_person_phone_no" type="number" id="contact_person_phone_no" value="{{ old('contact_person_phone_no', optional($user)->contact_person_phone_no) }}" placeholder="Enter contact person phone no here...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('gst_no') ? 'has-error' : '' }}">
                        <label for="gst_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">GST No</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="gst_no" type="text" id="gst_no" value="{{ old('gst_no', optional($user)->gst_no) }}" placeholder="Enter GST no here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('pf_no') ? 'has-error' : '' }}">
                        <label for="pf_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">PF No</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="pf_no" type="text" id="pf_no" value="{{ old('pf_no', optional($user)->pf_no) }}" placeholder="Enter PF no here...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('esic') ? 'has-error' : '' }}">
                        <label for="esic" class="form-label fs-6 fw-bolder text-black-700 mb-3">ESIC</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="esic" type="text" id="esic" value="{{ old('esic', optional($user)->esic) }}" placeholder="Enter ESIC here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('police_registration_number') ? 'has-error' : '' }}">
                        <label for="police_registration_number" class="form-label fs-6 fw-bolder text-black-700 mb-3">Police Registration Number</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="police_registration_number" type="text" id="police_registration_number" value="{{ old('police_registration_number', optional($user)->police_registration_number) }}" placeholder="Enter police registration number here...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('shop_and_establishment') ? 'has-error' : '' }}">
                        <label for="shop_and_establishment" class="form-label fs-6 fw-bolder text-black-700 mb-3">Shop & Establishment</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="shop_and_establishment" type="text" id="shop_and_establishment" value="{{ old('shop_and_establishment', optional($user)->shop_and_establishment) }}" placeholder="Enter shop and establishment here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('professional_tax') ? 'has-error' : '' }}">
                        <label for="professional_tax" class="form-label fs-6 fw-bolder text-black-700 mb-3">Professional Tax</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="professional_tax" type="text" id="professional_tax" value="{{ old('professional_tax', optional($user)->professional_tax) }}" placeholder="Enter professional tax here...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('msme_registaration_no') ? 'has-error' : '' }}">
                        <label for="msme_registaration_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">MSME Registaration No</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="msme_registaration_no" type="text" id="msme_registaration_no" value="{{ old('msme_registaration_no', optional($user)->msme_registaration_no) }}" placeholder="Enter MSME registaration no here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('welfare_fund') ? 'has-error' : '' }}">
                        <label for="welfare_fund" class="form-label fs-6 fw-bolder text-black-700 mb-3">Welfare Fund</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="welfare_fund" type="text" id="welfare_fund" value="{{ old('welfare_fund', optional($user)->welfare_fund) }}" placeholder="Enter welfare fund here...">
                        </div>
                    </div>
                </div>
            </div>
            @if(!isset($user->id))
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('password') ? 'has-error' : '' }}">
                        <label for="password" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Password</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="password" type="password" id="password" value="{{ old('password', optional($user)->password) }}" required="true" placeholder="Enter password Here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('password_confirmation') ? 'has-error' : '' }}">
                        <label for="password_confirmation" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Confirm Password</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="password_confirmation" type="password" id="password_confirmation" value="{{ old('password_confirmation', optional($user)->password_confirmation) }}" minlength="1" placeholder="Enter confirm password Here..." autocomplete="off" required>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('profile_photo') ? 'has-error' : '' }}">
                        <label for="profile_photo" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Profile Photo</label>
                        <div class="col-md-10">
                            <input class="form-control mb-2" name="profile_photo" type="file" id="profile_photo" value="{{ old('profile_photo', optional($user)->profile_photo) }}" placeholder="Enter profile photo here...">
                            @if (isset($user->profile_photo) && !empty($user->profile_photo))
                            <img src="{{asset('storage').'/'.$user->profile_photo}}" height="100px">
                            @endif
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-12">
                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                        <label for="address" class="form-label fs-6 fw-bolder text-black-700 mb-3">Address</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="address" cols="2" rows="3" id="address" placeholder="Enter address here...">{{ old('address', optional($user)->address) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>