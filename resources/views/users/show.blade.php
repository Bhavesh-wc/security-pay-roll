@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_toolbar" class="app-toolbar  py-3 py-lg-6 ">
            <div id="kt_app_toolbar_container" class="app-container  container-xxl d-flex flex-stack ">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3 ">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">View Company Details</h1>
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('users.user.index') }}" class="text-muted text-hover-primary">Index</a>
                        </li>
                    </ul>
                </div>
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a href="{{ route('users.user.index') }}" class="btn btn-sm fw-bold btn-primary">List</a>
                </div>
            </div>
        </div>
        <div id="kt_app_content" class="app-content  flex-column-fluid ">
            <div id="kt_app_content_container" class="app-container  container-xxl ">             
                <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
                    <div class="card-header cursor-pointer">
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Company Details</h3>
                        </div>
                        <a href="{{ route('users.user.edit', $user->id ) }}" class="btn btn-sm btn-primary align-self-center">Edit Company Details</a>   
                    </div>
                    <div class="card-body p-9">
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Company Name</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800">{{ $user->name }}</span>    
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Company Code</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800">{{ $user->company_code }}</span>    
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Email</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800">{{ $user->email }}</span>    
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Phone No</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $user->phone_no }}</span>                         
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Contact Person Name</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $user->contact_person_name }}</span>
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Contact Person Phone No</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $user->contact_person_phone_no }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Address</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $user->address }}</span>
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">GST No</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $user->gst_no }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">PF No</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $user->pf_no }}</span>
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">ESIC</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $user->esic }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Police Registration Number</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $user->police_registration_number }}</span>
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Shop & Establishment</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800">{{ $user->shop_and_establishment }}</span>    
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Professional Tax</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800">{{ $user->professional_tax }}</span>    
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">MSME Registaration No</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800">{{ $user->msme_registaration_no }}</span>    
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Welfare Fund</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $user->welfare_fund }}</span>                         
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Profile Photo</label>
                                <div>
                                    @if (isset($user->profile_photo) && !empty($user->profile_photo))
                                    <img src="{{asset('storage').'/'.$user->profile_photo}}" height="100px">
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.partials.footer')

