<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('city_name') ? 'has-error' : '' }}">
                        <label for="city_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">City Name</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="city_name" type="text" id="city_name" value="{{ old('city_name', optional($city)->city_name) }}" required="true" placeholder="Enter city name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('district_id') ? 'has-error' : '' }}">
                        <label for="district_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">District</label>
                        <div class="col-md-12">
                            <select name="district_id" aria-label="Select a district" data-control="select2" data-placeholder="Select district" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('district_id', optional($city)->district_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select district</option>
                                @foreach ($districts as $key => $district)
                                    <option value="{{ $key }}" {{ old('district_id', optional($city)->district_id) == $key ? 'selected' : '' }}>
                                        {{ $district }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('status', optional($city)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                                @foreach (['active' => 'Active','inactive' => 'Inactive'] as $key => $text)
                                    <option value="{{ $key }}" {{ old('status', optional($city)->status) == $key ? 'selected' : '' }}>
                                        {{ $text }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>