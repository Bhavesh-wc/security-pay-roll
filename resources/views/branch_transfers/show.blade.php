@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Branch Transfer' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('branch_transfers.branch_transfer.destroy', $branchTransfer->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('branch_transfers.branch_transfer.index') }}" class="btn btn-primary" title="Show All Branch Transfer">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('branch_transfers.branch_transfer.create') }}" class="btn btn-success" title="Create New Branch Transfer">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('branch_transfers.branch_transfer.edit', $branchTransfer->id ) }}" class="btn btn-primary" title="Edit Branch Transfer">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Branch Transfer" onclick="return confirm(&quot;Click Ok to delete Branch Transfer.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Branch</dt>
            <dd>{{ optional($branchTransfer->branch)->name }}</dd>
            <dt>Employee</dt>
            <dd>{{ optional($branchTransfer->employee)->full_name }}</dd>
            <dt>Designation</dt>
            <dd>{{ optional($branchTransfer->designation)->designation_name }}</dd>
            <dt>Department</dt>
            <dd>{{ optional($branchTransfer->department)->department_name }}</dd>
            <dt>Grade</dt>
            <dd>{{ optional($branchTransfer->grade)->grade_name }}</dd>
            <dt>Transfer</dt>
            <dd>{{ $branchTransfer->transfer }}</dd>
            <dt>Created Date</dt>
            <dd>{{ $branchTransfer->created_date }}</dd>
            <dt>Updated Month</dt>
            <dd>{{ $branchTransfer->updated_month }}</dd>
            <dt>Basic/Fix</dt>
            <dd>{{ $branchTransfer->basic_fix }}</dd>
            <dt>Basic + Da</dt>
            <dd>{{ $branchTransfer->basic_da }}</dd>
            <dt>Extra Allowance</dt>
            <dd>{{ $branchTransfer->extra_allowance }}</dd>
            <dt>Hra</dt>
            <dd>{{ $branchTransfer->hra }}</dd>
            <dt>Bonus</dt>
            <dd>{{ $branchTransfer->bonus }}</dd>
            <dt>Fix Salary</dt>
            <dd>{{ $branchTransfer->fix_salary }}</dd>
            <dt>Monthyear</dt>
            <dd>{{ $branchTransfer->monthyear }}</dd>
            <dt>Month</dt>
            <dd>{{ $branchTransfer->month }}</dd>
            <dt>Year</dt>
            <dd>{{ $branchTransfer->year }}</dd>

        </dl>

    </div>
</div>

@endsection