<input type="hidden" name="employee_id" value="{{ $oldemployees->id }}">
<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="oldemployee_id" class="form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                         <input class="form-control mb-2" name="oldemployee_id" type="text" id="oldemployee_id" value="{{ optional($oldemployees)->full_name }}" minlength="1" placeholder="Enter employee here..." readonly>
                     </div>
                 </div>
                 <div class="col-lg-6">
                    <div class="form-group">
                        <label for="oldbranch_id" class="form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                        <input class="form-control mb-2" name="oldbranch_id" type="text" id="oldbranch_id" value="{{ optional($oldemployees->branch)->name }}" minlength="1" placeholder="Enter branch here..." readonly>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group">
                        <label for="olddesignation_id" class="form-label fs-6 fw-bolder text-black-700 mb-3">Designation</label>
                        <input class="form-control mb-2" name="olddesignation_id" type="text" id="olddesignation_id" value="{{ optional($oldemployees->designation)->designation_name }}" minlength="1" placeholder="Enter designation here..." readonly>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('branch_id') ? 'has-error' : '' }}">
                        <label for="branch_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                        <select class="form-select" aria-label="Select a branch" data-control="select2" data-placeholder="Select branch" id="branch_id" name="branch_id">
                            <option value="" style="display: none;" {{ old('branch_id' ?: '') == '' ? 'selected' : '' }} disabled selected>Select branch</option>
                            @foreach ($branches as $key => $branch)
                            <option value="{{ $key }}" {{ old('branch_id') == $key ? 'selected' : '' }}>
                                {{ $branch }}
                            </option>
                            @endforeach
                        </select>
                        {!! $errors->first('branch_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('designation_id') ? 'has-error' : '' }}">
                        <label for="designation_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Designation</label>
                        <div class="d-flex">
                            <select id="designation_id" class="form-select" name="designation_id" data-placeholder="Select designation" data-control="select2" aria-label="Select a designation" required="true">
                                <option value="" style="display: none;" {{ old('designation_id' ?: '') == '' ? 'selected' : '' }} disabled selected>Select designation</option>
                                @foreach ($designations as $key => $designation)
                                <option value="{{ $key }}" {{ old('designation_id') == $key ? 'selected' : '' }}>
                                    {{ $designation }}
                                </option>
                                @endforeach
                            </select>
                        </div>

                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5"> 
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('department_id') ? 'has-error' : '' }}">
                        <label for="department_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Department</label>
                        <div class="col-md-12">
                            <select name="department_id" aria-label="Select a department" data-control="select2" data-placeholder="Select department" class="form-select">
                                <option value="" style="display: none;" {{ old('department_id', optional($branchTransfer)->department_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select department</option>
                                @foreach ($departments as $key => $department)
                                <option value="{{ $key }}" {{ old('department_id', optional($branchTransfer)->department_id) == $key ? 'selected' : '' }}>
                                    {{ $department }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('grade_id') ? 'has-error' : '' }}">
                        <label for="grade_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Grade</label>
                        <div class="col-md-12">
                            <select name="grade_id" aria-label="Select a grade" data-control="select2" data-placeholder="Select grade" class="form-select">
                                <option value="" style="display: none;" {{ old('grade_id', optional($branchTransfer)->grade_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select grade</option>
                                @foreach ($grades as $key => $grade)
                                <option value="{{ $key }}" {{ old('grade_id', optional($branchTransfer)->grade_id) == $key ? 'selected' : '' }}>
                                    {{ $grade }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5"> 
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('created_date') ? 'has-error' : '' }}">
                        <label for="created_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Created Date</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="created_date" type="date" id="created_date" value="{{ old('created_date', optional($branchTransfer)->created_date) }}" minlength="1" placeholder="Enter created date here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('updated_month') ? 'has-error' : '' }}">
                        <label for="updated_month" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Updated Month</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="updated_month" type="month" id="updated_month" value="{{ old('updated_month', optional($branchTransfer)->updated_month) }}" minlength="1" placeholder="Enter updated month here...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">   
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('basic_fix') ? 'has-error' : '' }}">
                        <label for="basic_fix" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Basic/Fix</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" type="text" id="basic_fix" name="basic_fix" placeholder="Enter basic/fix here..." readonly>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('fix_salary') ? 'has-error' : '' }}">
                        <label for="fix_salary" class="form-label fs-6 fw-bolder text-black-700 mb-3">Fix Salary</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" type="text" id="fix_salary" name="fix_salary" placeholder="Enter fix salary here..." readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('basic_da') ? 'has-error' : '' }}">
                        <label for="basic_da" class="form-label fs-6 fw-bolder text-black-700 mb-3">Basic + Da</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" type="text" id="basic_da" name="basic_da" placeholder="Enter basic + da here..." readonly>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('extra_allowance') ? 'has-error' : '' }}">
                        <label for="extra_allowance" class="form-label fs-6 fw-bolder text-black-700 mb-3">Extra Allowance</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" type="text" id="extra_allowance" name="extra_allowance" placeholder="Enter extra allowance here..." readonly>
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('hra') ? 'has-error' : '' }}">
                        <label for="hra" class="form-label fs-6 fw-bolder text-black-700 mb-3">HRA</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" type="text" id="hra" name="hra" placeholder="Enter HRA here..." readonly>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('bonus') ? 'has-error' : '' }}">
                        <label for="bonus" class="form-label fs-6 fw-bolder text-black-700 mb-3">Bonus</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" type="text" id="bonus" name="bonus" placeholder="Enter bonus here..." readonly>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>

    $(document).ready(function() {
        $('#branch_id').on('change', function() {
            var idBranch = this.value;
            $("#designation_id").html('');
            $.ajax({
                url: "{{url('admin/fetch_designation')}}",
                type: "POST",
                data: {
                    branch_id: idBranch,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function(result) {
                    $('#designation_id').html('<option value="">Select Designation</option>');
                    $.each(result.data, function(key, value) {
                        console.log(value.designation_id);
                        $("#designation_id").append('<option value="' + value.designation_id + '">' + value.designation.designation_name + '</option>');
                    });
                }
            });
        });

    $('#designation_id').on('change', function() {
            var idBranch = $('#branch_id').val();
            var idDesignation = this.value;
            $('#basic_fix').val('');
            $.ajax({
                url: "{{url('admin/fetch_designation_data')}}",
                type: "POST",
                data: {
                    branch_id: idBranch,
                    designation_id: idDesignation,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function(result) {
                    var data = result.data;
                    $('#basic_fix').val(data.basic_fix);
                    $('#fix_salary').val(data.fix_salary);
                    $('#basic_da').val(data.basic_da);
                    $('#extra_allowance').val(data.extra_allowance);
                    $('#hra').val(data.hra);
                    $('#bonus').val(data.bonus);
                }
            });
        });
    });
</script>

