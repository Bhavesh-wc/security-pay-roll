@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Branch Transfers</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('branch_transfers.branch_transfer.create') }}" class="btn btn-success" title="Create New Branch Transfer">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($branchTransfers) == 0)
            <div class="panel-body text-center">
                <h4>No Branch Transfers Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Company</th>
                            <th>Branch</th>
                            <th>Employee</th>
                            <th>Designation</th>
                            <th>Department</th>
                            <th>Grade</th>
                            <th>Transfer</th>
                            <th>Created Date</th>
                            <th>Updated Month</th>
                            <th>Basic/Fix</th>
                            <th>Basic + Da</th>
                            <th>Extra Allowance</th>
                            <th>Hra</th>
                            <th>Bonus</th>
                            <th>Fix Salary</th>
                            <th>Monthyear</th>
                            <th>Month</th>
                            <th>Year</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($branchTransfers as $branchTransfer)
                        <tr>
                            <td>{{ optional($branchTransfer->company)->username }}</td>
                            <td>{{ optional($branchTransfer->branch)->name }}</td>
                            <td>{{ optional($branchTransfer->employee)->full_name }}</td>
                            <td>{{ optional($branchTransfer->designation)->designation_name }}</td>
                            <td>{{ optional($branchTransfer->department)->department_name }}</td>
                            <td>{{ optional($branchTransfer->grade)->grade_name }}</td>
                            <td>{{ $branchTransfer->transfer }}</td>
                            <td>{{ $branchTransfer->created_date }}</td>
                            <td>{{ $branchTransfer->updated_month }}</td>
                            <td>{{ $branchTransfer->basic_fix }}</td>
                            <td>{{ $branchTransfer->basic_da }}</td>
                            <td>{{ $branchTransfer->extra_allowance }}</td>
                            <td>{{ $branchTransfer->hra }}</td>
                            <td>{{ $branchTransfer->bonus }}</td>
                            <td>{{ $branchTransfer->fix_salary }}</td>
                            <td>{{ $branchTransfer->monthyear }}</td>
                            <td>{{ $branchTransfer->month }}</td>
                            <td>{{ $branchTransfer->year }}</td>

                            <td>

                                <form method="POST" action="{!! route('branch_transfers.branch_transfer.destroy', $branchTransfer->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('branch_transfers.branch_transfer.show', $branchTransfer->id ) }}" class="btn btn-info" title="Show Branch Transfer">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('branch_transfers.branch_transfer.edit', $branchTransfer->id ) }}" class="btn btn-primary" title="Edit Branch Transfer">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <!-- <button type="submit" class="btn btn-danger" title="Delete Branch Transfer" onclick="return confirm(&quot;Click Ok to delete Branch Transfer.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button> -->
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $branchTransfers->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection