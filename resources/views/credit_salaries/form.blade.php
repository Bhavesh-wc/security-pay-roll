
<div class="mb-0">
    <div class="card card-flush py-4"> 
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">MonthYear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" value="{{ old('monthyear', optional($creditSalary)->monthyear) }}" required="true" placeholder="Enter monthyear here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('branch_id') ? 'has-error' : '' }}">
                        <label for="branch_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                        <div class="col-md-12">
                            <select name="branch_id" aria-label="Select a Branch" data-control="select2" data-placeholder="Select Branch" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('branch_id', optional($creditSalary)->branch_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select branch</option>
                                @foreach ($branches as $key => $branch)
                                    <option value="{{ $key }}" {{ old('branch_id', optional($creditSalary)->branch_id) == $key ? 'selected' : '' }}>
                                        {{ $branch }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>    
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('cheque_no') ? 'has-error' : '' }}">
                        <label for="cheque_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Cheque No</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="cheque_no" type="text" id="cheque_no" value="{{ old('cheque_no', optional($creditSalary)->cheque_no) }}" placeholder="Enter cheque no here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('company_bank_id') ? 'has-error' : '' }}">
                        <label for="company_bank_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bank</label>
                        <div class="col-md-12">
                            <select name="bank_id" id="bank" aria-label="Select a Bank" data-control="select2" data-placeholder="Select Bank" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('company_bank_id', optional($creditSalary)->company_bank_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select bank</option>
                                @foreach ($banks as $key => $bank)
                                    <option value="{{ $key }}" {{ old('company_bank_id', optional($creditSalary)->company_bank_id) == $key ? 'selected' : '' }}>
                                        {{ $bank }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div> 
            </div>
            <div class="row gx-10 mb-5">
            <div class="col-lg-6">
                <div class="form-group {{ $errors->has('bank_branch_id') ? 'has-error' : '' }}">
                    <label for="bank_branch_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bank Branch</label>
                    <div class="col-md-12">
                        <input class="form-control" name="bank_branch_name" type="text" id="cbranch" value="{{ old('bank_branch_name', optional($creditSalary)->bank_branch_name) }}" placeholder="Enter bank branch name here..." readonly>
                    </div>
                </div>
            </div> 
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('account_no') ? 'has-error' : '' }}">
                        <label for="ac_no" class="form-label fs-6 fw-bolder text-black-700 mb-3">Account No</label>
                        <div class="col-md-12">
                           <input class="form-control" name="account_no" type="text" id="account_no" value="{{ old('account_no', optional($creditSalary)->account_no) }}" placeholder="Enter bank ac no here..." readonly>
                        </div>
                    </div>
                </div>
            </div>
          
        </div>
    </div>
</div>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    jQuery(document).ready(function() {
        jQuery('#bank').change(function() {
            let bid = jQuery(this).val();
            jQuery.ajax({
                url: '/admin/fetch_branch',
                type: 'post',
                data: 'bid=' + bid + '&_token={{csrf_token()}}',
                success: function(result) {
                    jQuery('#bank_branch').html(result)
                }
            });
        });

        jQuery('#bank').change(function() {
            let bid = jQuery(this).val();
            jQuery.ajax({
                url: '/admin/fetch_account',
                type: 'post',
                data: 'bid=' + bid + '&_token={{csrf_token()}}',
                success: function(result) {
                    $('#account_no').val(result.account_no);
                }
            });
        });
        

        jQuery('#bank').change(function() {
            let bid = jQuery(this).val();
            jQuery.ajax({
                url: '/admin/fetch_cbranch',
                type: 'post',
                data: 'bid=' + bid + '&_token={{csrf_token()}}',
                success: function(result) {
                    $('#cbranch').val(result.cbranch);
                }
            });
        });

    });
</script>

