@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Credit Salary' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('credit_salaries.credit_salary.destroy', $creditSalary->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('credit_salaries.credit_salary.index') }}" class="btn btn-primary" title="Show All Credit Salary">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('credit_salaries.credit_salary.create') }}" class="btn btn-success" title="Create New Credit Salary">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('credit_salaries.credit_salary.edit', $creditSalary->id ) }}" class="btn btn-primary" title="Edit Credit Salary">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Credit Salary" onclick="return confirm(&quot;Click Ok to delete Credit Salary.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Monthyear</dt>
            <dd>{{ $creditSalary->monthyear }}</dd>
            <dt>Branch</dt>
            <dd>{{ optional($creditSalary->branch)->name }}</dd>
            <dt>Cheque No</dt>
            <dd>{{ $creditSalary->cheque_no }}</dd>
            <dt>Ac No</dt>
            <dd>{{ $creditSalary->ac_no }}</dd>
            <dt>Amount Credit</dt>
            <dd>{{ $creditSalary->amount_credit }}</dd>
            <dt>Bank</dt>
            <dd>{{ optional($creditSalary->bank)->bank_name }}</dd>
            <dt>Bank Branch</dt>
            <dd>{{ optional($creditSalary->bankBranch)->bank_branch_name }}</dd>
            <dt>Credit Date</dt>
            <dd>{{ $creditSalary->credit_date }}</dd>
            <dt>Company</dt>
            <dd>{{ optional($creditSalary->company)->id }}</dd>

        </dl>

    </div>
</div>

@endsection