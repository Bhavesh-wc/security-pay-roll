
<div class="mb-0">
    <div class="card card-flush py-4"> 
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('bank_branch_name') ? 'has-error' : '' }}">
                        <label for="bank_branch_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bank Branch Name</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="bank_branch_name" type="text" id="bank_branch_name" value="{{ old('bank_branch_name', optional($bankBranch)->bank_branch_name) }}" required="true" placeholder="Enter bank branch name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('ifsc_code') ? 'has-error' : '' }}">
                        <label for="ifsc_code" class="required form-label fs-6 fw-bolder text-black-700 mb-3">IFSC Code</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="ifsc_code" type="text" id="ifsc_code" value="{{ old('ifsc_code', optional($bankBranch)->ifsc_code) }}" minlength="1" required="true" placeholder="Enter IFSC code here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('bank_id') ? 'has-error' : '' }}">
                        <label for="bank_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bank</label>
                        <div class="col-md-12">
                            <select name="bank_id" aria-label="Select a bank" data-control="select2" data-placeholder="Select bank" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('bank_id', optional($bankBranch)->bank_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select Bank</option>
                                @foreach ($banks as $key => $bank)
                                <option value="{{ $key }}" {{ old('bank_id', optional($bankBranch)->bank_id) == $key ? 'selected' : '' }}> {{ $bank }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('status', optional($bankBranch)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                                @foreach (['active' => 'Active', 'inactive' => 'Inactive'] as $key => $text)
                                    <option value="{{ $key }}" {{ old('status', optional($bankBranch)->status) == $key ? 'selected' : '' }}>
                                        {{ $text }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                        <label for="address" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Address</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="address" cols="2" rows="2" id="address" required="true" placeholder="Enter address here...">{{ old('address', optional($bankBranch)->address) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>