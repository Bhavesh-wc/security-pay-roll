@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Bank Branch' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('bank_branches.bank_branch.destroy', $bankBranch->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('bank_branches.bank_branch.index') }}" class="btn btn-primary" title="Show All Bank Branch">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('bank_branches.bank_branch.create') }}" class="btn btn-success" title="Create New Bank Branch">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('bank_branches.bank_branch.edit', $bankBranch->id ) }}" class="btn btn-primary" title="Edit Bank Branch">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Bank Branch" onclick="return confirm(&quot;Click Ok to delete Bank Branch.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Bank Branch Name</dt>
            <dd>{{ $bankBranch->bank_branch_name }}</dd>
            <dt>Ifsc Code</dt>
            <dd>{{ $bankBranch->ifsc_code }}</dd>
            <dt>Address</dt>
            <dd>{{ $bankBranch->address }}</dd>
            <dt>Bank</dt>
            <dd>{{ optional($bankBranch->bank)->bank_name }}</dd>
            <dt>Status</dt>
            <dd>{{ $bankBranch->status }}</dd>

        </dl>

    </div>
</div>

@endsection