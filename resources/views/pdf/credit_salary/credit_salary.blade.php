<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>

</head>

<body>
    <table width="100%">
        <thead>
            <tr>
                <td valign="top" class="p-1 pb-2">
                    <p class="mb-0" style="font-size:10px">Ref No:</p>
                    <p class="mb-0" style="font-size:10px">Date: {{ date('d/m/Y h:i A', strtotime(now('Asia/Kolkata'))) }}</p>
                </td>
            </tr>
            <tr>
                <td valign="top" class="p-1 pb-2">
                    <p class="mb-0" style="font-size:10px">To,</p>
                    <p class="mb-0" style="font-size:10px">The Branch Manager</p>
                    <p class="mb-0" style="font-size:10px">{{$bankName}}</p>
                    <p class="mb-0" style="font-size:10px">{{ $bankAdreess }}</p>
                </td>
            </tr>
            <tr>
                <td valign="top" align="center" class="p-1">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Sub: Credit Salary payment Unit - {{ $branch_name }} {{ date('Y-M', strtotime($monthyear)) }}</h6>
                </td>
            </tr>
            <tr>
                <td valign="top" class="p-1">
                    <h6 class="mb-1" style="font-size:10px;font-weight: bold;">Dear Sir,</h6>
                    <p class="mb-0" style="font-size:10px">We Vir Security & Manpower Services Our Bank Ac No - {{ $accountNo }} Please Debit Rs - {{ $amountCredit }}=00 Ch No .{{$bankName}}
                        & Credit Salary Account Cradit Party Name as Under.</p>
                </td>
            </tr>
            <tr>
                <td class="pt-2"></td>
            </tr>

            <tr>
                <td>
                    <table width="100%" style="margin: 0px;padding: 0px;">
                        <tr>
                            <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Sr.No</h6>
                            </td>
                            <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Name</h6>
                            </td>
                            <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Ac No.</h6>
                            </td>
                            <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Bank Name</h6>
                            </td>
                            <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Branch Name</h6>
                            </td>
                            <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">IFSC Code.</h6>
                            </td>
                            <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Amount Credit</h6>
                            </td>
                        </tr>
                        @foreach($salaries as $list)
                        <tr>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $loop->iteration  }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $list->employee->full_name }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $list->employee->bank_ac_no }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $list->employee->bank->bank_name ?? 'null'}}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $list->employee->bankBranch->bank_branch_name ?? 'null' }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $list->employee->bankBranch->ifsc_code ?? 'null' }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $list->nettotal }}</p>
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="6" align="right" valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Amount Deposit</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $amountCredit }}</h6>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </thead>
    </table>

</body>

</html>