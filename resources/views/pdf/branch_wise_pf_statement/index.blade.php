@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <form action="{{route('branch_wise_pf_statement_pdf')}}" method="POST">
        @csrf
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Branch Wise PF Statement
                        </h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <!--begin::Item-->
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Export</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="post d-flex flex-column-fluid" id="kt_post">
                <div id="kt_content_container" class="container-xxl">
                    <div class="d-flex flex-column flex-lg-row">
                        <div class="flex-lg-row-fluid mb-10 mb-lg-0 me-lg-7 me-xl-10">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                            @endif
                            @if ($message = Session::get('fail'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <div class="card">
                                <div class="card-body p-12">
                                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                        <a href="{{ route('report.index')}} " class="btn btn-sm mt-5 mx-2 btn-primary">
                                            <span class="svg-icon svg-icon-1">Back</span>
                                        </a>
                                        <button type="submit" name="export_type" value="excel" class="btn btn-sm btn btn-success mt-5 mx-2" data-placement="top" data-toggle="tooltip">Export to Excel</button>
                                        <button type="submit" name="export_type" value="pdf" class="btn btn-sm btn btn-success mt-5 mx-2" data-placement="top" data-toggle="tooltip">Export to PDF</button>
                                    </div>
                                    
                                    <div class="d-flex flex-column align-items-start flex-xxl-row">
                                        <div class="mb-0 col-lg-12">
                                            <div class="row gx-10 mb-5">
                                                <div class="col-lg-6">
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Month Year *</label>
                                                    <input class="form-control" type="month" name="monthyear" id="monthyear" value="" required>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Branch</label>
                                                    <select name="branch_id" data-control="select2" class="form-control @error('branch_id') is-invalid @enderror form-select form-select-solid mb-5">
                                                        <option value="" selected disabled class="">Please Select Branch</option>
                                                        @foreach ($branch as $branch)
                                                        @if ($branch->status == 'active')
                                                        <option value="{{ trim($branch->id) }}">{{ $branch->name}}</option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                            <!-- <div class="row gx-10 mb-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group {{ $errors->has('employee_pf') ? 'has-error' : '' }}">
                                                        <label for="employee_pf" class=" form-label fs-6 fw-bolder text-black-700 mb-3">Employee PF(%)</label>
                                                        <div class="col-md-12">
                                                            <input class="form-control mb-2" name="employee_pf" type="number" id="employee_pf" value="3.67" step="any" placeholder="Enter employee pf...">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group {{ $errors->has('employee_fpf') ? 'has-error' : '' }}">
                                                        <label for="employee_fpf" class=" form-label fs-6 fw-bolder text-black-700 mb-3">Employee FPF(%)</label>
                                                        <div class="col-md-12">
                                                            <input class="form-control mb-2" name="employee_fpf" type="number" id="employee_fpf" value="8.33" step="any"  placeholder="Enter employee fpf...">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row gx-10 mb-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group {{ $errors->has('employee_edli') ? 'has-error' : '' }}">
                                                        <label for="employee_edli" class=" form-label fs-6 fw-bolder text-black-700 mb-3">Employee EDLI(%)</label>
                                                        <div class="col-md-12">
                                                            <input class="form-control mb-2" name="employee_edli" type="number" id="employee_edli" value="0.5" step="any" placeholder="Enter employee edli...">
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="form-group {{ $errors->has('employee_inepc') ? 'has-error' : '' }}">
                                                        <label for="employee_inepc" class=" form-label fs-6 fw-bolder text-black-700 mb-3">Employee Inepc(%)</label>
                                                        <div class="col-md-12">
                                                            <input class="form-control mb-2" name="employee_inepc" type="number" id="employee_inepc" value="0.01" step="any" placeholder="Enter employee inepc...">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="row gx-10 mb-5">
                                                <div class="col-lg-6">
                                                    <div class="form-group {{ $errors->has('employee_admin') ? 'has-error' : '' }}">
                                                        <label for="employee_admin" class=" form-label fs-6 fw-bolder text-black-700 mb-3">Employee Admin(%)</label>
                                                        <div class="col-md-12">
                                                            <input class="form-control mb-2" name="employee_admin" type="number" id="employee_admin" value="0.5" step="any" placeholder="Enter employee admin...">
                                                        </div>
                                                    </div>
                                                </div>
                                            </div> -->
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@include('layouts.partials.footer')
<script>
    $(document).ready(function() {
        $('#monthyear').on('change', function() {
            var monthyear = this.value;
            $("#branch_id").html('');
            $.ajax({
                url: "{{url('admin/fetch_branch_export')}}",
                type: "POST",
                data: {
                    monthyear: monthyear,
                    _token: '{{csrf_token()}}'
                },
                dataType: 'json',
                success: function(result) {
                    $('#branch_id').html('<option value="">Select Branch</option>');
                    $.each(result.attendances, function(key, value) {
                        $("#branch_id").append('<option value="' + value
                            .branch_id + '">' + value.branch_id + '</option>');
                    });
                }
            });
        });
    });
</script>