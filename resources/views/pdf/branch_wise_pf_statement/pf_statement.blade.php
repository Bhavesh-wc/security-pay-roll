<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
</head>
<body>
    <table width="100%">
        <thead>
            <tr>
                <td  colspan="12" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size:14px;font-weight: bold;">VIR SECURITY & MANPOWER SERVICE</h6>
                </td>
            </tr>
            <tr>
                <td colspan="5" valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Name And Address of Principal Employee:Vir Security & Manpower Service</h6>
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc Bank Palanpur Banaskantha</h6>
                </td>
                <td colspan="7" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Nature & Location of Work:{{ $branch_name }}</h6>
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">{{ $branch_address }}</h6>
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">PF STATEMENT FOR THE MONTH:{{ date('M-Y', strtotime($monthName)) }}</h6>
                    <h6 class="mb-0" style="font-size:10px;font-weight: bold;">PF Code:</h6>
                </td>
            </tr>
            <tr>
               
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">Sr.No.</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">PF A/C NO.</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">PF UAN NO</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">ECODE</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">NAME OF EMPLOYEE</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">PF BASIC</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">EMPLE P.F {{$employeePF}}%</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">Employer {{$empPF}}%</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">Employer F.P.F {{$empFPF}}%</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">EDLI {{$empEDLI}}%</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">INPEC {{$empINEPC}}%</h6>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">ADMIN {{$empADMIN}}%</h6>
                    </td>
                </tr>
                @foreach($salaries as $list)
                <tr>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{ $loop->iteration }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{ $list->employee->pf_ac_code}}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{ $list->employee->pf_uan_code}}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{ $list->id }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{ $list->employee->full_name}}</p>
                    </td>
                    <?php //dd($list); ?>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{ $list->basis_wages != '0.00' ? $list->basis_wages : $list->fix_salary }} </p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{ $list->pf }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{$list->totalEmployeePF}}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{$list->totalempFPF}}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{$list->totalempEDLI}}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{$list->totalempINEPC}}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{$list->totalempADMIN}}</p>
                    </td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="12"><p class="mb-0" style="font-size:9px">ADMIN CHARGES ON TOTAL WAGES RS: {{$a}} @ {{$empADMIN}}* IS RS: {{$f}}</p></td>
                </tr>
                
                <tr>
                    <td colspan="12" style="margin: 0px;padding: 0px;" class="pt-3">
                        <table width="40%">
                            <tr>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">A/C NO.</p>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">{{$b}}</p>
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">+{{$b}}</p>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">{{$k}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">A/C NO. 10.</p>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">{{$h}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">A/C NO. 21.</p>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">{{$c}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">A/C NO. 22.</p>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">{{$d}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">A/C NO. 2.</p>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">{{$f}}</p>
                                </td>
                            </tr>
                            <tr>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">Total</p>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <p class="mb-0" style="font-size: 9px;font-weight: bold;">{{$l}}</p>
                                </td>
                            </tr>
                            
                        </table>
                    </td>
                </tr>
        </thead>
    </table>

</body>
</html>