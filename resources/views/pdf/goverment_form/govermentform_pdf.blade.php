<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>
</head>

<body>
    <table width="100%">
        <tbody>
            <tr>
                <td class="p-1">
                    <table width="100%">
                        <tr>
                            <td width="20%" align="left">
                                <h5 class="mb-0" style="font-size: 14px;">Part IV-B</h5>
                            </td>
                            <td width="60%" align="center">
                                <h5 class="mb-0" style="font-size: 14px;">GUJARAT GOVERNMENT GAZETTE, EX., 18-7-1007</h5>
                            </td>
                            <td width="20%" align="center">
                                <h5 class="mb-0" style="font-size: 14px;">225-9</h5>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" class="p-2 mt-5">
                                <h6 class="mb-0" style="font-size: 14px;font-weight: bold;">(Left Hand Thumb Impression If Male And Rignt Hand Thumb Impression If Female)</h6>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <hr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="60%">
                                <table width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <h4 class="mb-0" style="font-size: 14px;text-decoration: underline;font-weight: bold;">FOR OFFICE USE ONLY</h4>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="20%" class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">File No.</h6>
                                        </td>
                                        <td width="80%" class="pt-3" style="border-bottom: 1px dotted black;font-size: 12px;">
                                            <p class="mb-0"></p>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td width="50%" class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Date Of Issue Of C&A Report</h6>
                                        </td>
                                        <td width="50%" class="pt-3" style="border-bottom: 1px dotted black;font-size: 12px;">
                                            <p class="mb-0"></p>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td width="60%" class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(Signature Of Police Station In charge)</h6>
                                        </td>
                                        <td width="40%" class="pt-3" style="border-bottom: 1px dotted black;font-size: 12px;">
                                            <p class="mb-0"></p>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td width="50%" class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Name Of Police Station</h6>
                                        </td>
                                        <td width="50%" class="pt-3" style="border-bottom: 1px dotted black;font-size: 12px;">
                                            <p class="mb-0">{{$employee->police_station_name}}</p>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td width="50%" class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Name Of Police District</h6>
                                        </td>
                                        <td width="50%" class="pt-3" style="border-bottom: 1px dashed black;font-size: 12px;">
                                            <p class="mb-0">{{$employee->district->district_name}}</p>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">* N.B Cancel Whatever Is Not Applicable.</h6>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="40%">
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td>
                    <hr>
                    <table width="100%">
                        <tr>
                            <td colspan="3" align="center">
                                <h4 class="mb-0" style="font-size: 14px;font-weight: bold;">FORM II </h4>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center">
                                <h4 class="mb-0" style="font-size: 14px;">(See Rule 4)</h4>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="3" align="center" class="pt-2 pb-2">
                                <h6 class="mb-0" style="font-size: 15px;font-weight: bold;">Form For Verification Of Character And Antecedents Of Security Guard And Supervisor</h6>
                            </td>
                        </tr>
                        <tr>
                            <td width="30%" class="pt-1">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Thumb Impression of The Applicant</h6>
                            </td>
                            <td width="50%" class="pt-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                            @if(!is_null($employee->photo))
                            <td width="20%" class="pt-1" align="right" rowspan="2"><img src="{{ asset('storage/' . $employee->photo)}}" width="100px"></td>
                            @else
                            <td width="20%" class="pt-1" align="right" rowspan="2"><img src="{{ asset('/images/userimage.jpg') }}" width="100px"></td>
                            @endif
                            
                        </tr>
                        <tr>
                            <td width="25%" class="pt-1">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Singnature of The Applicant</h6>
                            </td>
                            <td width="50%" class="pt-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="pt-3">
                    <table width="100%" style="border:1px solid black;">
                        <tr>
                            <td colspan="3" class="p-2 mt-3" align="center">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">For Official Use Only</h6>
                            </td>
                        </tr>
                        <tr style="border:1px solid black;">
                            <td width="20%" class="p-2" align="center" style="border-right:1px solid black;">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Form Number</h6>
                            </td>
                            <td width="60%" class="p-2" align="center" style="border-right:1px solid black;">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Name Of The Police Station Send For Police Verification</h6>
                            </td>
                            <td width="20%" class="p-2" align="center">
                                <h6 class="mb-0" style="font-size: 11px;font-weight: bold;">Date</h6>
                            </td>
                        </tr>
                        <tr style="border:1px solid black;">
                            <td width="20%" class="p-2" align="center" style="border-right:1px solid black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                            <td width="60%" class="p-2" align="center" style="border-right:1px solid black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                            <td width="20%" class="p-2" align="center">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="pt-2">
                    <table width="100%">
                        <tr>
                            <td width="20%">
                                <h6 class="pt-0 mb-0" style="font-size: 12px;font-weight: bold;">Fee Amount Rs.</h6>
                            </td>
                            <td width="35%" class="pt-3 pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Name Of Bank</h6>
                            </td>
                            <td width="35%" class="pt-3 pb-0" style="border-bottom: 1px dashed black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                        </tr>
                        <tr>
                            <td width="20%">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Date Of Issue</h6>
                            </td>
                            <td width="35%" class="pt-3" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                            <td width="5%"></td>
                            <td width="40%" class="pt-3"></td>
                        </tr>

                    </table>
                </td>

            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Please read the instructions carefully before filling the form. Please fill in BLOCK LETTERS.(CAUTION: Please
                        furnish correct information. Furnishing of incorrect information or suppression of any factual inforamtaion in
                        the form will render the candidate unsuitable for employment/engagement in the Private Agency)</h6>
                </td>
            </tr>
            <tr>
                <td class="pt-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">1. Name of applicant as should appear in the photo-identy card</h6>
                </td>
            </tr>
            <tr>
                <td class="pt-1 pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;">{{$employee->full_name}}</p>
                </td>
            </tr>
            <tr>
                <td class="pt-3">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">2. If you have ever changed your name, please indicate the previous name(s) in full</h6>
                </td>
            </tr>
            <tr>
                <td class="pt-1 pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <table width="100%">
                        <tr>
                            <td width="10%">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Sex</h6>
                            </td>
                            <td width="25%" class="pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;">{{$employee->sex}}</p>
                            </td>
                            <td width="15%"></td>
                            <td width="20%">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">4. Date Of Birth</h6>
                            </td>
                            <td width="30%" class="pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;">{{$employee->date_of_birth}}</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="pt-1 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">5. Place Of Birth Village / Town</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;">{{$employee->birth_place}}</p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">6. Father Full Name / Legal Guardian Full Name</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"> {{$employee->father_name}}</p>
                </td>
            </tr>
            <hr>
            <tr>
                <td class="p-1 mt-2">
                    <table width="100%">
                        <tr>
                            <td width="20%" align="center">
                                <h5 class="mb-0" style="font-size: 14px;">Part IV-B</h5>
                            </td>
                            <td width="60%" align="center">
                                <h5 class="mb-0" style="font-size: 14px;">GUJARAT GOVERNMENT GAZETTE, EX., 18-7-1007</h5>
                            </td>
                            <td width="20%" align="center">
                                <h5 class="mb-0" style="font-size: 14px;">225-9</h5>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="pt-4 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">7. Mother Full Name / Legal Guardian Full Name</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;">{{$employee->mother_name}}</p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">8. If Married, Full Name Of Spouse</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;">{{$employee->spouse_name}}</p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">9. Present Residential Address</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;">{{$employee->local_address}}</p>
                </td>
            </tr>

            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Telephone No./ Mobile No.</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;">{{$employee->permanent_mobile_no}}</p>
                </td>
            </tr>

            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">10. Please give the date since residing at the above mentioned address:</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">11. permanent Address</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;">{{$employee->permanent_address}}</p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">12. If you have not resided at the address given at COLUMN(9) confinuously for the last five year, please furnish
                        the other address with duration(s) resided. You should furnish additional photo copies of this form for each
                        additional place of stay during the last five year. Forms may be photo copies, but photograph and signature in
                        origional are required on each form.</h6>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <table width="100%">
                        <tr>
                            <td width="10%">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">From</h6>
                            </td>
                            <td width="25%" class="pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                            <td width="10%"></td>
                            <td width="10%">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">To</h6>
                            </td>
                            <td width="25%" class="pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                            <td width="20%"></td>
                        </tr>
                        <td colspan="6" class="pt-3 pb-0" style="border-bottom: 1px dotted black;">
                            <p class="mb-0" style="font-size: 12px;"></p>
                        </td>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">13. In case of stay abroad particulars of all places where you have resided for more than one year after attainig
                        the age of twenty-one years:</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <table width="100%">
                        <tr>
                            <td colspan="3">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">14. Other Details</h6>
                            </td>
                        </tr>
                        <tr>
                            <td width="5%" rowspan="8"></td>
                            <td colspan="2" class="pt-3">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(a) Educational Qualifications:</h6>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;">{{$employee->qualification->qualification_name}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="pt-3">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(b) Previous positions held if any along with name and address of employers:</h6>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0 pb-0" style="font-size: 12px;">{{ $employee->experience_company }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="pt-3">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(c) Reason for leaving last employment:</h6>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2" class="pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                        </tr>
                        <tr>
                            <td width="45%" class="pt-3">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(d) visible Distinguishing mark 1.</h6>
                            </td>
                            <td width="50%" class="pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;">{{$employee->visible_distinguishing_mark1}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td width="45%" class="pt-3">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(e) visible Distinguishing mark 2.</h6>
                            </td>
                            <td width="50%" class="pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;">{{$employee->visible_distinguishing_mark2}}</p>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">15. Are you working in Central Government / state Govt / PSU / Statutory Bodies Yes/No:</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">16. Are you a citizen of india by: Birth / Descent / Registration / Naturalization if you have ever possessed any
                        other citizenship, please indicate previous citizenship:</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">17. Have you at any time been convicted by a court in india for any criminal offence and sentedced to
                        imprisonment? if so, give name of the court, case number and offence.(attach copy of judgement):</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0 mb-1" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-2 pt-3">
                    <table width="100%">
                        <tr>
                            <td width="20%" align="center">
                                <h5 class="mb-0" style="font-size: 14px;">Part IV-B</h5>
                            </td>
                            <td width="60%" align="center">
                                <h5 class="mb-0" style="font-size: 14px;">GUJARAT GOVERNMENT GAZETTE, EX., 18-7-1007</h5>
                            </td>
                            <td width="20%" align="center">
                                <h5 class="mb-0" style="font-size: 14px;">225-9</h5>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">18. Are any criminal proccedings pending against you before a court in india? If so, give name of court, case
                        number and offence:</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">19. Has any court issued a warrant or summons for appearance or warrant for arrest or an order prohibiting
                        your departure from India ? if so, give name of court, case number and offence:</h6>
                </td>
            </tr>
            <tr>
                <td class="pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">20. self Declaration:</h6>
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">The information given by me in this form and enclosures is true and i am solety responsible for accurancy</h6>
                </td>
            </tr>
            <tr>
                <td class="pt-5 pb-2" align="right">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(Signature / T.I of applicant)</h6>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(Left Hand Thumb Impression if Male and Right Hand Thumb Impression if Female)</h6>
                </td>
            </tr>
            <tr>
                <td class="pt-1 pb-2">
                    <table width="100%">
                        <tr>
                            <td width="5%" rowspan="2"></td>
                            <td width="5%">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Date:</h6>
                            </td>
                            <td width="15%" class="pt-2 pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                            <td width="75%"></td>
                        </tr>
                        <tr>

                            <td width="5%">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Place:</h6>
                            </td>
                            <td width="15%" class="pt-2 pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;"></p>
                            </td>
                            <td width="75%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-2">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">21. Particulars of Person to be intimated in the even of death or accident:</h6>
                </td>
            </tr>
            <tr>
                <td class="pt-1 pb-2">
                    <table width="100%">
                        <tr>
                            <td width="5%" rowspan="3"></td>
                            <td width="10%" class="pt-2 pb-0">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Name:</h6>
                            </td>
                            <td width="85%" class="pt-2 pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;">{{$employee->full_name}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td width="10%" class="pt-2 pb-0">
                                <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Address::</h6>
                            </td>
                            <td width="85%" class="pt-2 pb-0" style="border-bottom: 1px dotted black;">
                                <p class="mb-0" style="font-size: 12px;">{{$employee->permanent_address}}</p>
                            </td>
                        </tr>
                        <tr>
                            <td colspan="2">
                                <table width="100%">
                                    <tr>
                                        <td width="15%" class="pt-2 pb-0">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Mobile / Tel. No:</h6>
                                        </td>
                                        <td width="85%" class="pt-2 pb-0" style="border-bottom: 1px dotted black;">
                                            <p class="mb-0" style="font-size: 12px;">{{$employee->permanent_mobile_no}}</p>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td class="pt-1">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">22. Enclosures:</h6>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-3 pb-0" style="border-bottom: 1px dotted black;">
                    <p class="mb-0" style="font-size: 12px;"></p>
                </td>
            </tr>
            <tr>
                <td class="pt-5 pb-2" align="right">
                    <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(Signature / T.I of applicant)</h6>
                </td>
            </tr>
            <tr>
                <td>
                    <table width="100%">
                        <tr>
                            <td width="60%">
                                <table width="100%">
                                    <tr>
                                        <td colspan="2">
                                            <h4 class="mb-0" style="font-size: 14px;text-decoration: underline;font-weight: bold;">FOR OFFICE USE ONLY</h4>
                                        </td>
                                    </tr>
                                    <hr>
                                    <tr>
                                        <td width="20%" class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">File No.</h6>
                                        </td>
                                        <td width="80%" class="pt-3" style="border-bottom: 1px dotted black;font-size: 12px;">
                                            <p class="mb-0"></p>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td width="50%" class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Date Of Issue Of C&A Report</h6>
                                        </td>
                                        <td width="50%" class="pt-3" style="border-bottom: 1px dotted black;font-size: 12px;">
                                            <p class="mb-0"></p>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td width="60%" class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(Signature Of Police Station In Character))</h6>
                                        </td>
                                        <td width="40%" class="pt-3"></td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td width="50%" class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Name Of Police Station</h6>
                                        </td>
                                        <td width="50%" class="pt-3" style="border-bottom: 1px dotted black;font-size: 12px;">
                                            <p class="mb-0"></p>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td width="50%" class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Name Of Police District</h6>
                                        </td>
                                        <td width="50%" class="pt-3" style="border-bottom: 1px dotted black;font-size: 12px;">
                                            <p class="mb-0"></p>
                                        </td>
                                    </tr>
                                </table>
                                <table width="100%">
                                    <tr>
                                        <td class="pt-3">
                                            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">* N.B Cancel Entries not applicable.</h6>
                                        </td>
                                    </tr>
                                </table>
                            </td>
                            <td width="40%"></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>