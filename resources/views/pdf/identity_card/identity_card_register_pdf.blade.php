<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
  <style>
    .last-td-border tr:last-child td
    {
        border-bottom: 0px !important;
    }
    .last-td-border tr td:first-child
    {
        border-left: 0px !important;
    }
    .last-td-border tr td:last-child
    {
        border-right: 0px !important;
    }
  </style>
</head> 

<body>
<table width="100%">
    <thead>
        <tr>
            <td colspan="2" align="center" class="p-1" style="border: 1px solid black;"><h5 class="mb-0" style="font-size:16px;font-weight: bold;">IDENTITY CARD REGISTER</h5></td>
        </tr>
        <tr>
            <td width="50%" class="p-1" valign="top" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Agency: Vir Security & Manpower Service</h6>
                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Address: 7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc Bank
                    Palanpur Banaskantha</h6>
            </td>
            <td width="50%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Principal Employer:{{ $branch_name }}</h6>
                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Address: {{ $branch_address }}</h6>
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Month: {{ date('M-Y', strtotime($toMonthName)) }}</h6>
            </td>
        </tr>
        <tr>
            <td colspan="2" style="padding: 0px;margin: 0px;border: 1px solid black;">
                <table width="100%" cellspacing="0" cellspadding="0" class="last-td-border">
                    <tr>
                        <td class="p-1" style="border: 1px solid black;border-left: 0px;border-top: 0px;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Sr.No.</h6></td>
                        <td class="p-1" style="border: 1px solid black;border-top: 0px;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Full Name & Address of the Worker</h6></td>
                        <td class="p-1" style="border: 1px solid black;border-top: 0px;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date of Birth of the worker</h6></td>
                        <td class="p-1" style="border: 1px solid black;border-top: 0px;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date of joining the Service of the Factory</h6></td>
                        <td class="p-1" style="border: 1px solid black;border-top: 0px;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Recent Passport Size Photograph of the Worker</h6></td>
                        <td class="p-1" style="border: 1px solid black;border-top: 0px;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Signature or Thumb Impression of the Worker</h6></td>
                        <td class="p-1" style="border: 1px solid black;border-top: 0px;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Signature of Manager or Authorised Agent</h6></td>
                        <td class="p-1" style="border: 1px solid black;border-top: 0px;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date of Issue</h6></td>
                        <td class="p-1" style="border: 1px solid black;border-top: 0px;border-right: 0px;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Remarks</h6></td>
                    </tr>
                    @foreach($data as $list)
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->id }}</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-3" style="font-size:10px">{{$list->full_name}}</p>
                            <p class="mb-0" style="font-size:10px">{{$list->permanent_address}}</p>
                        </td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ date('d-m-Y', strtotime($list->date_of_birth)) }}</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ date('d-m-Y', strtotime($list->entry_date)) }}</p></td>
                        @if(!is_null($list->photo))
                        <td class="p-1" align="center" style="border: 1px solid black;"><img src="{{ asset('storage/' . $list->photo)}}" width="80px"></td>
                        @else
                        <td class="p-1" align="center" style="border: 1px solid black;"><img src="{{ asset('/images/userimage.jpg')}}" width="80px"></td>
                        @endif
                        @if(!is_null($list->signature))
                        <td class="p-1" align="center" style="border: 1px solid black;"><img src="{{ asset('storage/' . $list->signature)}}" width="80px"></td>
                        @else
                        <td class="p-1" align="center" style="border: 1px solid black;"><img src="{{ asset('/images/signature.png')}}" width="80px"></td>
                        @endif
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px"></p></td>
                    </tr>
                    @endforeach
                </table>
            </td>
        </tr>
    </thead>
</table>

</body>
</html>