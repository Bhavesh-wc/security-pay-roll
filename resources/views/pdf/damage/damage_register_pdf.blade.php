<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
  <style>
    .last-td-border tr:last-child td
    {
        border-bottom: 0px !important;
    }
    .last-td-border tr td:first-child
    {
        border-left: 0px !important;
    }
    .last-td-border tr td:last-child
    {
        border-right: 0px !important;
    }
  </style>
</head> 

<body>

    <table width="100%">
        <thead>
            <tr>
                <td colspan="12" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-2" style="font-size:16px;font-weight: bold;">Register Of Deduction for Damage or Loss</h6>
                    <h6 class="mb-2" style="font-size:14px;font-weight: bold;">FORM XX</h6>
                    <h6 class="mb-0" style="font-size:10px;font-weight: bold;">[See Rule 78(2)(d)]</h6>
                </td>
            </tr>
            <tr>
                <td width="50%" colspan="6" valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Name of Contractor: of Contractor:Vir Security & Manpower Service</h6>
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Address of Contractor: 7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc
                        Bank Palanpur Banaskantha</h6>
                </td>
                <td width="50%" colspan="6" valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Name and Address of establishment under which contract is carried
                        on: {{ $data[0]->branch->name }}</h6>
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Address of the principal Employer: {{ $data[0]->branch->address }}</h6>
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Month: {{ date('M-Y', strtotime($data[0]->branch->monthyear)) }}</h6>
                </td>
            </tr>
            <tr>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Sr.No.</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name of the Worker</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Fathers/hasbands Name</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Designation/Nature of Employment</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Particulars of Damage or Loss</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date of Damage or Loss</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Whether workers showed cause against Deduction</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name of Person in Whose Presence Employees Explanation was heard</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Amount of Deduction Imposed</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">No of Installments</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date Recovery Recover Amount</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Remarks</h6>
                </td>
            </tr>
            <?php $sr = 1; ?>
            @foreach($data as $data)
            <tr>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $sr++; }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->employee->full_name }}<br/>{{ $data->employee_id }}</p></p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->employee->father_name }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->designation->designation_name }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->damage_details }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px"> {{ date('d-m-Y', strtotime($data->damage_date)) }} </p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->employee_reason_for_deduction }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->name_of_person_explanation_heard }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->damage_amount }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->total_installments }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ date('d-m-Y', strtotime($data->installment_date )) }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->remarks }}</p></td>
            </tr>
            @endforeach
        </thead>
    </table>

</body>
</html>