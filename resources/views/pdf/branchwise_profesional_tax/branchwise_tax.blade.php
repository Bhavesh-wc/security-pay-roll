<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>
</head>

<body>
    <table width="100%">
        <tbody>
            <tr>
                <td colspan="7" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 14px;">VIR SECURITY & MANPOWER SERVICE</h6>
                </td>
            </tr>
            <tr>
                <td colspan="3" width="55%" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;">PROF. TAX REGISTER FOR THE MONTH:{{ $monthName }}</h6>
                </td>
                <td colspan="4" width="45%" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;">BRANCH:{{ $branch_name }}</h6>
                </td>
            </tr>
            <tr>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;">Sr. No.</h6>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;">Employee Code</h6>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;">Name of Employee</h6>
                    <h6 class="mb-0" style="font-size: 8px;">Designation</h6>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;">Branch Name</h6>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;">Gross Salary</h6>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;">0</h6>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;">200</h6>
                </td>
            </tr>
            @foreach($employees as $list)
            @if($list->nettotal)
            <tr>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $loop->iteration }}</p>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{$list->id }}</p>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $list->full_name }}</p>
                    <p class="mb-0" style="font-size: 8px;">{{ $list->designation->designation_name }}</p>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $branch_name }}</p>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $list->nettotal }}</p>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $list->employee00 ?? '0' }}</p>
                </td>
                <td valign="top" class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $list->employee200 ?? '0' }}</p>
                </td>

            </tr>
            @endif
            @endforeach
            <tr>
                <td colspan="4" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;">Total:</h6>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px; font-weight:bold;">{{ $total }}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px; font-weight:bold;">00</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px; font-weight:bold;">{{ $total200 }}</p>
                </td>
            </tr>
        </tbody>
    </table>
</body>

</html>