<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
</head> 
<body>
  <table width="100%">
    <tbody>
      <tr>
        <td colspan="20" align="center" class="p-1" style="border: 1px solid black;">
            <h6 class="mb-1" style="font-size: 12px;">VIR SECURITY & MANPOWER SERVICE</h6>
            <h6 class="mb-0" style="font-size: 11px;">Branch Bill Detail</h6>
        </td>
      </tr>
      <tr>
        <td colspan="10" class="p-1" style="border: 1px solid black;">
          <h6 class="mb-1" style="font-size: 9px;">Branch : {{ $branch->name }}</h6>
          <h6 class="mb-0" style="font-size: 9px;">Branch Address: {{ $branch->address }}</h6>
        </td>
        <td colspan="10" class="p-1" style="border: 1px solid black;">
            <h6 class="mb-1" style="font-size: 9px;">Service Category : Security</h6>
            <h6 class="mb-0" style="font-size: 9px;">Month : {{ $monthyear }}</h6>
        </td>
      </tr>
      <tr>
        <td class="p-1" style="border: 1px solid black;">
          <h6 class="mb-0" style="font-size: 8px;font-weight: bold;font-weight: bold;">Sr. No.</h6>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Branch Name</h6>
        </td>
        <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Bill Amount Total</h6>
        </td>
        <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Service Tax</h6>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Net Bill Amount</h6>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Pf 13.61%</h6>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">ESIC 1.75%</h6>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Bonus</h6>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Leave</h6>
        </td>
        <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;font-weight: bold;">Service Charge</h6>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Gross Pay</h6>
          </td>
          <td class="p-1" style="border: 1px solid black;">
              <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Extra</h6>
          </td>
          <td class="p-1" style="border: 1px solid black;">
              <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Net Amt.</h6>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Salary Paid</h6>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Diff.</h6>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Tds</h6>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Profit</h6>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Recover Payment</h6>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Extra. Exp</h6>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Net Profit</h6>
          </td>
      </tr>
      <tr>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;">1</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;">{{ $branch->name }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;">{{ $data[0] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;">{{ $data[1] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;">{{ $data[2] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;">{{ $data[3] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;">{{ $data[4] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;">{{ $data[5] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;">{{ $data[6] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;">{{ $data[7] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;">{{ $data[8] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;">{{ $data[9] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;">{{ $data[10] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;">{{ $data[11] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;">{{ $data[12] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;">{{ $data[13] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;">{{ $data[14] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;">{{ $data[15] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
              <p class="mb-0" style="font-size: 8px;">{{ $data[16] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
              <p class="mb-0" style="font-size: 8px;">{{ $data[17] }}</p>
          </td>
      </tr>
      <tr>
        <td colspan="2" align="left" class="p-1" style="border: 1px solid black;">
          <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total:</h6>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[0] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[1] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[2] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
          <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[3] }}</p>
        </td>
        <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[4] }}</p>
        </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[5] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[6] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[7] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[8] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
              <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[9] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[10] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[11] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[12] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[13] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
              <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[14] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[15] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
            <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[16] }}</p>
          </td>
          <td class="p-1" style="border: 1px solid black;">
              <p class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $data[17] }}</p>
          </td>
      </tr>
    </tbody>
  </table>
</body>
</html>