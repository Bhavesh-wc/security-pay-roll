
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>

</head>

<body>
 
    <table width="100%">
        <thead>
            <tr>
                <td colspan="2" align="center" class="p-1 pb-3">
                    <h6 class="mb-0" style="font-size: 16px;font-weight: bold;">VIR SECURITY & MANPOWER SERVICE</h6>
                </td>
            </tr>
            <tr>
                <td width="50%" class="p-1 pb-2">
                    <h6 class="mb-0" style="font-size: 14px;font-weight: bold;">PROF. TAX REGISTER FOR THE MONTH: {{ $monthName }}</h6>
                </td>
                <td width="50%" class="p-1 pb-2">
                    <h6 class="mb-0" style="font-size: 14px;font-weight: bold;">BRANCH: ALL</h6>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2" class="m-0">
                    <table width="100%">
                        <tr>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Sr No.</h6>
                            </td>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Branch Name</h6>
                            </td>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Gross Salary</h6>
                            </td>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">00</h6>
                            </td>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">200</h6>
                            </td>
                        </tr>
                        <?php $counter = 1; ?>
                        @foreach($branches as $list )
                        @if($list->netTotal)
                        <tr>
                            <td valign="top" align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">{{ $counter }}</p>
                            </td>
                            <td valign="top" align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">{{ $list->name }}</p>
                            </td>
                            <td valign="top" align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">{{ $list->netTotal }}</p>
                            </td>
                            <td valign="top" align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">{{ $list->employee00 ?? '00' }}</p>
                            </td>
                            <td valign="top" align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">{{ $list->branch200 ?? '00' }}</p>
                            </td>
                        </tr>
                        <?php $counter++; ?>
                        @endif
                        @endforeach
                        <tr>
                            <td colspan="2" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Grand Total:</h6>
                            </td>
                            <td align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">{{$finalTotal}}</h6>
                            </td>
                            <td align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">00</h6>
                            </td>
                            <td align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">{{ $total200 }}</h6>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="p-2 pb-2">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">PROF. TAX SUMMARY</h6>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="m-0">
                    <table width="40%">
                        <tr>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Prof. Tax Slab</h6>
                            </td>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">No. Of Employee</h6>
                            </td>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Prof. Tax. Amount</h6>
                            </td>
                        </tr>
                        <tr>
                            <td align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">0</p>
                            </td>
                            <td align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">{{ $emp00 }}</p>
                            </td>
                            <td align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">0</p>
                            </td>
                        </tr>

                        <tr>
                            <td align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">200</p>
                            </td>
                            <td align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">{{ $emp200 }}</p>
                            </td>
                            <td align="center" class="p-1">
                                <p class="mb-0" style="font-size:10px">{{ $total200 }}</p>
                            </td>
                        </tr>
                        <tr>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Total:</h6>
                            </td>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">{{ $totalemployee }}</h6>
                            </td>
                            <td valign="top" align="center" class="p-1" style="border-top:1px solid black;border-bottom:1px solid black;">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">{{ $total200 }}</h6>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>