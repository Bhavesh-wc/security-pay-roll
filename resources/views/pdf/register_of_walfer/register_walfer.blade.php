<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>

</head>

<body>
    <table width="100%">
        <thead>
            <tr>
                <td colspan="6" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size:14px;font-weight: bold;">Register Of Walfare</h6>
                </td>
            </tr>
            <tr>
                <td colspan="3" width="50%" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name of Contractor: Vir Security & Manpower Service</h6>
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Address:7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc Bank
                        Palanpur Banaskantha</h6>
                </td>
                <td colspan="3" width="50%" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name of establishment under which contract is carried on: {{ $branch_name }}</h6>
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;"> {{ $branch_address }} </h6>
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Month: {{ date('M-Y', strtotime($monthName)) }}</h6>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Sr.No.</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Employee Code</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name of the Worker</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Branch</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Designation</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Walfare Amount</h6>
                </td>
            </tr>
            @foreach($salaries as $salary)
            <tr>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size:10px">{{ $loop->iteration }}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size:10px">{{ $salary->employee->id }}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size:10px">{{ $salary->employee->full_name }}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size:10px">{{ $branch_name }}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size:10px">{{ $salary->designation->designation_name }}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size:10px">{{ $salary->walfare_amount }}</p>
                </td>
            </tr>
            @endforeach

            <tr>
                <td colspan="5" class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size:10px">Total</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size:10px">{{ $totalWalferAmount }}</p>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>