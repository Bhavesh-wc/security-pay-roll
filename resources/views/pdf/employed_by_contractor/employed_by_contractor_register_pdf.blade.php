<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
  <style>
    .last-td-border tr:last-child td
    {
        border-bottom: 0px !important;
    }
    .last-td-border tr td:first-child
    {
        border-left: 0px !important;
    }
    .last-td-border tr td:last-child
    {
        border-right: 0px !important;
    }
  </style>
</head> 

<body>

    <table width="100%">
        <thead>
            <tr>
                <td colspan="11" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-2" style="font-size:16px;font-weight: bold;">Register Of Employed By Contractor</h6>
                    <h6 class="mb-2" style="font-size:14px;font-weight: bold;">FORM XIII</h6>
                    <h6 class="mb-0" style="font-size:10px;font-weight: bold;">(See Rule 75)</h6>
                </td>
            </tr>
            <tr>
                <td colspan="6" valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Name and Address of Contractor:Vir Security & Manpower Service</h6>
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Address of Contractor:7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc
                        Bank Palanpur Banaskantha</h6>
                </td>
                <td colspan="5" valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Name and Address of establishment under which contract is carried
                        on:{{ $branch_name }}</h6>
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Address of the principal Employer: {{ $branch_address }}</h6>
                    <h6 class="mb-2" style="font-size:10px;font-weight: bold;">Month: {{ $toMonthName }}</h6>
                </td>
            </tr>
            <tr>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Sr.No.</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name of the Worker</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Sex</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Fathers/hasbands Name</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Nature of Employment/Designation</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Permanent Home address of workers [village & Tehsil/taluka & District]</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date Of Commencement of Employment</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Signature or thumb impression of workers</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date of Termination Of employment</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Reasone for Termination</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Remarks</h6>
                </td>
            </tr>
            @foreach($data as $list)
            <tr>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->id }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->full_name}}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->sex }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->father_name }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ optional($list->designation)->designation_name }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$list->permanent_address}}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$list->entry_date}}</p></td>
                @if(!is_null($list->signature))
                <td valign="top" class="p-1" style="border: 1px solid black;"><img src="{{ asset('storage/' . $list->signature)}}" width="80px">
                @else
                <td valign="top" class="p-1" style="border: 1px solid black;"><img src="{{ asset('/images/signature.png')}}" width="80px">
                @endif
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->relieve_date }}</p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px"></p></td>
                <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px"></p></td>
            </tr>
            @endforeach
        </thead>
    </table>

</body>
</html>