@include('layouts.partials.header')

<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Employed By Contractor-Registration</h1>
                </div>
            </div>
        </div>
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                <div class="d-flex flex-column flex-lg-row">
                    <div class="flex-lg-row-fluid mb-10 mb-lg-0 me-lg-7 me-xl-10">
                        @if ($message = Session::get('success'))
                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                        @endif
                        <div class="card">
                            <div class="card-body p-12">
                                <form method="POST" action="{{ route('print_employed_by_contractor_register') }}" id="kt_invoice_form" enctype="multipart/form-data">
                                    @csrf
                                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                        <a href="{{ route('report.index')}}" class="btn btn-sm mt-5 mx-2 btn-primary">
                                            Back
                                        </a>
                                        <button type="submit" value="true" name="go_to_appform" data-placement="top" data-toggle="tooltip" title="Download" class="btn btn-sm btn btn-success mt-5">Export
                                        </button>
                                    </div>
                                    <div class="d-flex flex-column align-items-start flex-xxl-row">
                                        <div class="mb-0 col-lg-12">
                                            <div class="row gx-10 mb-5">
                                                <div class="col-lg-6">
                                                    <div class="card-body pt-0">
                                                        <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                                                            <label for="monthyear" class="form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                                                            <div class="col-md-12">
                                                                <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" id="monthyear_dropdown" value="{{ old('monthyear') }}" required="true" placeholder="Enter monthyear here...">
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="card-body pt-0">
                                                        <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Branch *</label>
                                                        <select id="branch_dropdown" name="branch_id" data-control="select2" class="form-control @error('branch_id') is-invalid @enderror form-select form-select-solid mb-5" required>
                                                            <option value="" selected disabled class="">Select Branch</option>
                                                            @foreach ($branches as $data)
                                                            <option value="{{ trim($data->id) }}">{{ $data->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="text-danger">
                                                            @error('branch_id')
                                                            *{{ $message }}
                                                            @enderror
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 d-flex justify-content-end ">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('input[name="monthyear"]').on('change', function(){
            var monthyear = $(this).val();
            $.ajax({
                url: '{{ route("get_employed_by_contractor_branches") }}',
                type: 'GET',
                data: { monthyear: monthyear },
                success: function(data){
                    $('select[name="branch_id"]').html('<option value="">Select Branch</option>');
                    $.each(data, function(key, value){
                        $('select[name="branch_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        });
    });
</script> -->
@include('layouts.partials.footer')