<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
  <style>
    @media print {
  body {
    margin: 0 !important;
    padding:0 !important;
    border:0 !important;
    outline:0 !important;
  }
  h6{
    line-height:0;
  }
}
  </style>
</head> 

<body style="margin: 0; padding: 0;" class="g-0">
@foreach($employees as $employee)
<div style="padding-bottom:5px;">
  <table width="100%" style="border: 1px solid black;">
    <tbody>
      <tr>
        <td width="50%">
            <table width="100%">
                <tr style="line-height: 10px;">
                    <td rowspan="2" width="25%" class="pt-0"><img src="images/Vir.jpg" width="70px"></td>
                </tr>
                <tr>
                    <td width="75%" class="p-1"><h6 class="mb-0" style="font-size:9px;">7, 1st Floor,Aroma Arecade, Opp. Joravar Palace,
                        Palanpur(B.K) Email : admin@virsecurity.in
                        Mo. 9712059999</h6></td>
                </tr>
                <tr>

                </tr>
            </table>
        </td>
        <td width="50%" rowspan="2" class="p-2 pt-0" style="border-left: 1px solid black;">
            <h6 style="font-size: 10px; ">1. Not permitted in premises without Identity Card.</h6>
            <h6 style="font-size: 10px;">2. If found this ICard please return to the office you have to
                produce your ICard when ever asked by authorities.</h6>
            <h6 style="font-size: 10px;">3. In the case of Lost the ICard the Employee has to the
                issuing Authority.</h6>
            <h6 style="font-size: 10px;">Office : 7,1st Floor,Aroma Arecade, Opp. Joravar Palace,
                Palanpur(B.K). Ph. 02742-261105</h6>
        </td>
    </tr>
    <tr>
        <td class="p-2" style="border-top: 1px solid black;">
            <table  width="100%">
                <tr>
                    <td width="20%"><h6 class="mb-0" style="font-size:10px; ">NAME</h6></td>
                    <td width="55%"><h6 class="mb-0" style="font-size:10px; ">: {{$employee->full_name}}</h6></td>
                    @if(!is_null($employee->photo))
                    <td rowspan="6" width="25%" align="right"><img src="{{ asset('storage/' . $employee->photo)}}" width="80px"></td>
                    @else
                    <td rowspan="6" width="25%" align="right"><img src="{{ asset('/images/userimage.jpg')}}" width="80px"></td>
                    @endif
                </tr>
                <tr>
                    <td width="20%"><h6 class="mb-0" style="font-size:10px; ">DESI</h6></td>
                    <td width="55%"><h6 class="mb-0" style="font-size:10px; ">: {{$employee->designation->designation_name}}</h6></td>
                </tr>
                <tr>
                    <td width="20%"><h6 class="mb-0" style="font-size:10px; ">D.O.B</h6></td>
                    <td width="55%"><h6 class="mb-0" style="font-size:10px; ">: {{$employee->date_of_birth}}</h6></td>
                </tr>
                <tr>
                    <td width="20%"><h6 class="mb-0" style="font-size:10px; ">EMP NO.</h6></td>
                    <td width="55%"><h6 class="mb-0" style="font-size:10px; ">: {{$employee->id}}</h6></td>
                </tr>
                <tr>
                    <td width="20%"><h6 class="mb-0" style="font-size:10px; ">ADDRESS</h6></td>
                    <td width="55%"><h6 class="mb-0" style="font-size:10px; ">: {{$employee->local_address}}</h6></td>
                </tr>
                <tr>
                    <td width="20%"><h6 class="mb-0" style="font-size:10px; ">PHONE</h6></td>
                    <td width="55%"><h6 class="mb-0" style="font-size:10px; ">: {{$employee->local_phone_no}}</h6></td>
                </tr>
            </table>
            <table width="100%">
                <tr>
                <td width="50%" class="pt-1 pb-2"><h6 class="mb-0" style="font-size:10px">Signature of Employees</h6></td>
                <td width="50%" class="pt-1 pb-2" align="right"><h6 class="mb-0" style="font-size:10px">Signature of Issuing Authority</h6></td>
                </tr>
            </table>
        </td>
    </tr>
    </tbody>
  </table>
  </div>
  @endforeach
  <br>
</body>
</html>