<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
  
</head> 

<body>
    <table width="100%">
        <tr>
            <td align="center" class="p-2" colspan="7"><h6 class="mb-0" style="font-size: 16px;font-weight: bold;">Stock History Reports</h6></td>
        </tr>
        <tr>
            <td class="py-2" colspan="7"><h6 class="mb-0" style="font-size: 14px;font-weight: bold;">Month/Year : {{ $monthyear }}</h6></td>
        </tr>
        <tr>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Sr.No.</h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Stock Name</h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Stock Quantity</h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Stock In Date</h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Balance Code (Cr/Dr) </h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Total Stock</h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Total Credit</h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Total Debit</h6></td>
        </tr>
      @foreach($data as $list)          
        <tr>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $loop->iteration }}</p></td>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->stockCategory->stock_category_name }}</p></td>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->stock_quantity }}</p></td>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->stock_in_date }}</p></td>

<td class="p-1" style="border: 1px solid black;">
    <p class="mb-0" style="font-size:10px">
        {{ $list->balance_code === 'cr' ? 'Credited' : ($list->balance_code === 'dr' ? 'Debited' : '') }}
    </p>
</td>

            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->stockCategory->stock_quantity  }}</p></td>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->totalCredit ?? '0'}}</p></td>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->totalDebit  ?? '0' }}</p></td>
        </tr>
      @endforeach  
       
        

    </table>

</body>
</html>