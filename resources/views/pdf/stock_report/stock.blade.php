<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>
 
</head>

<body>
    <table width="100%">
        <thead>
            <tr>
                <td colspan="5" align="center" class="p-1" style="border: 1px solid black;">
                    <h5 class="mb-1" style="font-size:14px;font-weight: bold;">Stock Report</h5>
                    <h5 class="mb-1" style="font-size:14px;font-weight: bold;">FORM XXII</h5>
                    <h6 class="mb-0" style="font-size:10px;font-weight: bold;">(See Rule 78(2)(a))</h6>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">Name of Contractor:Vir Security & Manpower Service</h6>
                    <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">Address Of Contractor:7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc
                        Bank Palanpur Banaskantha</h6>
                </td>
                <td colspan="2" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">Name of establishment under which contract is carried on:Banaskantha
                        District Co-Op. Milk Producers’ Union Ltd.</h6>
                    <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">Address of the principal Employer:Banas Dairy, P.B.No.20, Palanpur-385001
                        Gujarat</h6>
                    <h6 class="mb-0" style="font-size: 9px;font-weight: bold;"></h6>
                </td>
            </tr>
                
                        <tr>
                            <td width="6%" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Sr.No.</h6>
                            </td>
                            <td width="10%" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Date</h6>
                            </td>
                            <td width="15%" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Name of the Employee/Employee Code</h6>
                            </td>
                            <td width="10%" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Category</h6>
                            </td>
                            <td width="24%" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Deduct Stock</h6>
                            </td>
                        </tr>
                        @foreach($data as $list)
                        <tr>
                            <td class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $loop->iteration }}</p>
                            </td>
                            <td class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $list['date'] }}</p>
                            </td>
                            <td class="p-1" style="border: 1px solid black;">
                                <p class="mb-1" style="font-size:8px">{{ $list['employee_name'] }} - {{ $list['employee_code'] }}</p>
                            </td>
                            <td class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $list['category_name'] }}</p>
                            </td>
                            <td class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $list['added_stock'] }}</p>
                            </td>
                        </tr>
                        @endforeach
                        <tr>
                            <td colspan="4" align="right" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;">Total Stock :{{ $list['total_stock'] }}</h6>
                            </td>
                            <td class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-1" style="font-size: 8px;">Total Deduct Stock :{{$list['total_added_stock']}}</h6>
                                <h6 class="mb-0" style="font-size: 8px;">total Remaining Stock :{{ $total_remaining_stock }}</h6>
                            </td>
                        </tr>

            
        </thead>
    </table>

</body>

</html>