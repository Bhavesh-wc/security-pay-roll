@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <form action="{{route('stock_pdf')}}" method="POST">
        @csrf
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Stock Report</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <!--begin::Item-->
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Export</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div class="post d-flex flex-column-fluid" id="kt_post">
                <div id="kt_content_container" class="container-xxl">
                    @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ $message }}</strong>
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                    @endif
                    @if ($message = Session::get('fail'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>{{ $message }}</strong>
                            <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
                    <div class="d-flex flex-column flex-lg-row">
                        <div class="flex-lg-row-fluid mb-10 mb-lg-0 me-lg-7 me-xl-10">
                            <div class="card">
                                <div class="card-body p-12">
                                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base"> 
                                        <a href="{{ route('report.index')}} " class="btn btn-sm mt-5 mx-2 btn-primary">
                                            <span class="svg-icon svg-icon-1">Back</span>
                                        </a>
                                        <button type="submit" class="btn btn-sm btn btn-success mt-5"> Export </button>
                                    </div>
                                    
                                    <div class="d-flex flex-column align-items-start flex-xxl-row">
                                        <div class="mb-0 col-lg-12">
                                            <div class="row gx-10 mb-5">
                                                <div class="col-lg-6">
                                                    
                                                        <div class="form-group {{ $errors->has('startdate') ? 'has-error' : '' }}">
                                                            <label for="startdate" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Start Date</label>
                                                            <div class="input-group log-event" id="kt_td_picker_linked_1" data-td-target-input="nearest" data-td-target-toggle="nearest">
                                                                <input id="kt_td_picker_linked_1_input" type="text" name="startdate" data-toggle="datetimepicker" value="{{ old('startdate') }}" class="form-control" data-td-target="#kt_td_picker_linked_1" placeholder="Enter start date here..."/>
                                                                <span class="input-group-text" data-td-target="#kt_td_picker_linked_1" data-td-toggle="datetimepicker">
                                                                    <i class="fa fa-calendar"></i>
                                                                </span>
                                                            </div>
                                                        
                                                    </div>
                                                </div>
                                                <div class="col-lg-6">
                                                    
                                                        <div class="form-group {{ $errors->has('enddate') ? 'has-error' : '' }}">
                                                            <label for="enddate" class="required form-label fs-6 fw-bolder text-black-700 mb-3">End Date</label>
                                                            <div class="input-group log-event" id="kt_td_picker_linked_2" data-td-target-input="nearest" data-td-target-toggle="nearest">
                                                                <input id="kt_td_picker_linked_2_input" type="text" name="enddate" data-toggle="datetimepicker" value="{{ old('enddate') }}" class="form-control" data-td-target="#kt_td_picker_linked_2" placeholder="Enter end date here..."/>
                                                                <span class="input-group-text" data-td-target="#kt_td_picker_linked_2" data-td-toggle="datetimepicker">
                                                                    <i class="fa fa-calendar"></i>
                                                                </span>
                                                            </div>
                                                        </div>
                                                    
                                                </div>

                                                {{-- <div class="col-lg-6">
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Start Date *</label>
                                                    <input class="form-control" type="date" name="startdate" id="startdate" value="" required>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">End Date *</label>
                                                    <input class="form-control" type="date" name="enddate" id="enddate" value="" required>
                                                </div> --}}
                                            </div>
                                        </div>
                                    </div>

                                    <div class="d-flex flex-column align-items-start flex-xxl-row">
                                        <div class="mb-0 col-lg-12">
                                            <div class="row gx-10 mb-5">
                                                <div class="col-lg-6">
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Stock Category*</label>
                                                    <select name="category_id" data-control="select2" class="form-control @error('id') is-invalid @enderror form-select form-select-solid mb-5" required>
                                                        <option value="" selected disabled class="">Please Select Register Category</option>
                                                        @foreach ($categories as $category)
                                                        @if ($category->status == 'active')
                                                        <option value="{{ trim($category->id) }}">{{ $category->stock_category_name}}</option>
                                                        @endif
                                                        @endforeach
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>
</div>
@include('layouts.partials.footer')
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script>
        $(document).ready(function () {
            $('#monthyear').on('change', function () {
                var monthyear = this.value;
                $("#branch_id").html('');
                $.ajax({
                    url: "{{url('admin/fetch_branch_export')}}",
                    type: "POST",
                    data: {
                        monthyear: monthyear,
                        _token: '{{csrf_token()}}'
                    },
                    dataType: 'json',
                    success: function (result) {
                        $('#branch_id').html('<option value="">Select Branch</option>');
                        $.each(result.attendances, function (key, value) {
                            $("#branch_id").append('<option value="' + value
                                .branch_id + '">' + value.branch_id  + '</option>');
                        });
                    }
                });
            });
          
        });
    </script> -->