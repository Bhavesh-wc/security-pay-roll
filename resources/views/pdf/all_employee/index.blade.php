<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>

</head>

<body>
    <table width="100%">
        <tr>
            <td colspan="8" align="center" width="8%" class="p-2" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 14px;font-weight: bold;">Left Employees</h6>
            </td>
        </tr>
        <tr>
            <td valign="top" width="8%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Sr.No.</h6>
            </td>
            <td valign="top" width="10%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Registration Date.</h6>
            </td>
            <td valign="top" width="17%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Name of Supervisor/Guards</h6>
            </td>
            <td valign="top" width="17%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Father Name</h6>
            </td>
            <td valign="top" width="15%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Present Address & Phone No.</h6>
            </td>
            <td valign="top" width="10%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Date Of Joining/leave</h6>
            </td>

            <td valign="top" width="15%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Permanent Address</h6>
            </td>
            <td valign="top" width="8%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Bedge No.</h6>
            </td>
        </tr>
        @foreach($employees as $employee)
        <tr>
            <td valign="top" class="p-1" style="border: 1px solid black;">
                <p class="mb-0" style="font-size:8px">{{ $loop->iteration }}</p>
            </td>
            <td valign="top" class="p-1" style="border: 1px solid black;">
                <p class="mb-0" style="font-size:8px">{{ date('d-m-Y', strtotime($employee->registartion_date)) }}</p>
            </td>
            <td valign="top" class="p-1" style="border: 1px solid black;">
                <p class="mb-0" style="font-size:8px">{{$employee->full_name}}</p>
            </td>
            <td valign="top" class="p-1" style="border: 1px solid black;">
                <p class="mb-0" style="font-size:8px">{{$employee->father_name}}</p>
            </td>
            <td valign="top" class="p-1" style="border: 1px solid black;">
                <p class="mb-0" style="font-size:8px">{{$employee->local_address}}</p>
                <p class="mb-0" style="font-size:8px">{{$employee->local_phone_no}}</p>
            </td>
            <td valign="top" class="p-1" style="border: 1px solid black;">
                <p class="mb-0" style="font-size:8px">{{ $employee->entry_date ? date('d-m-Y', strtotime($employee->entry_date)): '' ; }}</br>{{ $employee->relieve_date ? date('d-m-Y', strtotime($employee->relieve_date)): '' ; }}</p>
            </td>
            <td valign="top" class="p-1" style="border: 1px solid black;">
                <p class="mb-0" style="font-size:8px">{{$employee->permanent_address}}</p>
                <p class="mb-0" style="font-size:8px">{{$employee->permanent_phone_no}}</p>
            </td>
            <td valign="top" class="p-1" style="border: 1px solid black;">
                <p class="mb-0" style="font-size:8px">{{$employee->id}}</p>
            </td>
        </tr>
        @endforeach
    </table>

</body>

</html>