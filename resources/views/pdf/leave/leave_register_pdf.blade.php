<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>

  <style>
    .table-no-border:first-child tr td
    {
        border-top: 0px !important;
    }
    .table-no-border:first-child tr td:first-child
    {
        border-left: 0px !important;
    }
    .table-no-border:first-child tr td:last-child
    {
        border-right: 0px !important;
    }
    .table-no-border tr:last-child td
    {
        border-bottom: 0px !important;
    }
  </style>

</head> 

<body>

  <table width="100%">
    <thead>
        <tr>
            <td colspan="3" align="center" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-2" style="font-size:14px;font-weight: bold;">Leave Register</h6>
                <h6 class="mb-2" style="font-size:12px;font-weight: bold;">FORM No 18</h6>
                <h6 class="mb-0" style="font-size:10px;font-weight: bold;">(See Rule 94)</h6>
            </td>
        </tr>
        <tr>
            <td width="33.33%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size:10px;font-weight: bold;">Branch: {{ $data->branch->name }}</h6>
                <h6 class="mb-0" style="font-size:10px;font-weight: bold;">Name: {{ $data->full_name }}</h6>
            </td>
            <td width="33.33%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size:10px;font-weight: bold;">Acount no: {{ $data->branch->name }}</h6>
                <h6 class="mb-0" style="font-size:10px;font-weight: bold;">Father Name: {{ $data->father_name }}</h6>
            </td>
            <td width="33.33%" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size:10px;font-weight: bold;">Part-1 if Adult:</h6>
                <h6 class="mb-0" style="font-size:10px;font-weight: bold;">Part-2 if Children:</h6>
            </td>
        </tr>
            <td colspan="3" align="left" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size:10px;font-weight: bold;">Register no: {{ $data->id }} Adult or Children Worker Register No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Job Entry Date: {{ date('d-m-Y', strtotime($data->entry_date)) }} Job Entry Year: {{ date('M-Y', strtotime($data->monthyear)) }}</h6>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-0" style="font-size:10px;font-weight: bold;">Insurance No:&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</h6>
            </td>
        </tr>
        <tr>
            <td colspan="3" style="border: 1px solid black;margin: 0px;padding: 0px;">
                <table width="100%" class="table-no-border">
                    <tr>
                        <td rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Month</h6>
                        </td>
                        <td colspan="4" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Working Day in Calender Year</h6>
                        </td>
                        <td rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Column 5 To 8 Addition</h6>
                        </td>
                        <td colspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Salary with Credited Leave</h6>
                        </td>
                        <td rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Column 10 To 11 Addition</h6>
                        </td>
                        <td rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">See Rule 79(8) With Salary Leave not accepted Why</h6>
                        </td>
                        <td rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Now Atfer 12 Month Leave Not Required</h6>
                        </td>
                        <td colspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Salary leave</h6>
                        </td>
                        <td rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Debit Salary</h6>
                        </td>
                        <td rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Basic Salary</h6>
                        </td>
                        <td rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Deduction of Mess Or Other</h6>
                        </td>
                        <td rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Of Column 17 and 18</h6>
                        </td>
                        <td colspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Transfer Employee</h6>
                        </td>
                        <td rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Remark</h6>
                        </td>
                    </tr>
                    <tr>
                        <td class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Working Day</h6>
                        </td>
                        <td class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Blank Day</h6>
                        </td>
                        <td class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Salary leave for Pregnance</h6>
                        </td>
                        <td class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Salary leave Accepted</h6>
                        </td>
                        <td class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Befor Year Total Salary Leave</h6>
                        </td>
                        <td class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Salary Day (see Rule (4))</h6>
                        </td>
                        <td class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">From</h6>
                        </td>
                        <td class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">To</h6>
                        </td>
                        <td class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Transfer Date</h6>
                        </td>
                        <td class="p-1" valign="top" style="border: 1px solid black;">
                            <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Salary leave Exchange in rupees</h6>
                        </td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">January</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">February</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">March</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">April</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">May</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">June</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">July</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">August</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">September</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">October</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">November</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">December</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                    <tr>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">Total</p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                        <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px"></p></td>
                    </tr>
                </table>
            </td>
        </tr>
      </thead>
  </table>

</body>
</html>
