<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
  <style>
    .last-td-border tr:last-child td
    {
        border-bottom: 0px !important;
    }
    .last-td-border tr td:first-child
    {
        border-left: 0px !important;
    }
    .last-td-border tr td:last-child
    {
        border-right: 0px !important;
    }
  </style>
</head> 

<body>

    <table width="100%">
        <thead>
            <tr>
                <td colspan="10" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-2" style="font-size:14px;font-weight: bold;">Register Of Advance</h6>
                    <h6 class="mb-2" style="font-size:14px;font-weight: bold;">FORM XXII</h6>
                    <h6 class="mb-0" style="font-size:10px;font-weight: bold;">(See Rule 78(2)(a))</h6>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="5" width="50%" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name of Contractor:Vir Security & Manpower Service</h6>
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Address Of Contractor:7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc
                        Bank Palanpur Banaskantha</h6>
                </td>
                <td colspan="5" width="50%" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name of establishment under which contract is carried on: {{ $branch_name }}</h6>
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Address of the principal Employer: {{ $branch_address }}</h6>
                        <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Month: {{ date('M-Y', strtotime($monthyear)) }}</h6>
                </td>
            </tr>
            <tr>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Sr.No.</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name of the Worker
                        Employee Code</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Father/
                        husband
                        Name</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name of
                        Employment/
                        Designation</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Wage Period
                        & Wages
                        Payable</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date & amount of advance given</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Purpose of Adavnce made</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">No. of Installments
                        by which
                        advance to be
                        paid</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date &
                        amount of
                        Repaid</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Remarks</h6>
                </td>
            </tr>
            <?php $sr = 1; ?>
            @foreach($data as $data)
            <tr>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $sr++; }}</p></td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->employee->full_name }}<br/>{{ $data->employee_id }}</p></p></td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->employee->father_name }}</p></td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->designation->designation_name }}</p></td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->wages_payable }}</p></td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-2" style="font-size:10px">{{ date('d-m-Y', strtotime($data->date_of_advance)) }}</p>
                    <p class="mb-0" style="font-size:10px">{{ $data->amount }}</p>      
                </td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->purpose_of_advance }}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->no_of_qty }}</p></td>
                <td class="p-1" style="border: 1px solid black;"></td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->remarks }}</p>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>


</body>
</html>