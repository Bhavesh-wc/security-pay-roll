<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>

</head> 

<body>
    <table width="100%">
        <thead>
            <tr>
                <td colspan="2" align="center" class="p-3" style="border-bottom: 1px solid black;">
                    <h6 class="mb-2" style="font-size: 16px;font-weight: bold;">VIR SECURITY & MANPOWER SERVICE</h6>
                    <h6 class="mb-0" style="font-size: 14px;font-weight: bold;">Aroma Arecade, Opp. Joravar Palace, Palanpur(B.K)</h6>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td width="20%" class="p-3"><p class="mb-0" style="font-size:12px;">Date:</p></td>
                <td width="80%" class="p-3"><p class="mb-0" style="font-size:12px;">{{ date('d-m-Y', strtotime($data->date_of_advance)) }}</p></td>
            </tr>
            <tr>
                <td colspan="2" align="center" class="p-2" bgcolor="black">
                    <h6 class="mb-0" style="font-size: 16px;font-weight: bold;color: white;">DEBIT VOUCHER</h6>
                </td>
            </tr>
            <tr>
                <td width="20%" class="p-2 pt-3"><p class="mb-0" style="font-size:12px;">EMPLOYEE NAME:</p></td>
                <td width="80%" class="p-2 pt-3"><p class="mb-0" style="font-size:12px;">{{ $data->employee->full_name }}</p></td>
            </tr>
            <tr>
                <td width="20%" class="p-2"><p class="mb-0" style="font-size:12px;">EMPLOYEE CODE:</p></td>
                <td width="80%" class="p-2"><p class="mb-0" style="font-size:12px;">{{ $data->employee_id }}</p></td>
            </tr>
            <tr>
                <td width="20%" class="p-2"><p class="mb-0" style="font-size:12px;">UNIT:</p></td>
                <td width="80%" class="p-2"><p class="mb-0" style="font-size:12px;">{{ $data->branch->name }}</p></td>
            </tr>
            <tr>
                <td colspan="2" class="pt-2">
                    <table width="100%">
                        <tr>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;font-weight: bold;">Sr. No.</p></td>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;font-weight: bold;">Payment Type</p></td>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;font-weight: bold;">Purpose of Advance</p></td>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;font-weight: bold;">Remarks</p></td>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;font-weight: bold;">Amount</p></td>
                        </tr>
                        <tr>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;">1</p></td>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;">{{ $data->advance_for }}</p></td>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;">{{ $data->purpose_of_advance }}</p></td>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;">{{ $data->remarks }}</p></td>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;">{{ $data->amount }}</p></td>
                        </tr>
                        <tr>
                            <td colspan="4" align="right" class="p-2" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;font-weight: bold;">Total:</p></td>
                            <td class="p-2" align="center" style="border: 1px solid black;"><p class="mb-0" style="font-size:12px;">{{ $data->amount }}</p></td>
                        </tr>
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="pt-5">
                    <table width="100%">
                        <tr>
                            <td width="33.33%" class="p-2" align="center"><p class="mb-0" style="font-size:10px;font-weight: bold;">Cashier</p></td>
                            <td width="33.33%" class="p-2" align="center"><p class="mb-0" style="font-size:10px;font-weight: bold;">Authorised Person</p></td>
                            <td width="33.33%" class="p-2" align="center"><p class="mb-0" style="font-size:10px;font-weight: bold;">Employee Signature</p></td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>



    </table>


</body>
</html>