<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>

</head>

<body>
    <table width="100%">
       <tr>
            <td colspan="14" class="p-1" align="center" style="border: 1px solid black;">
            <h5 class="mb-0" style="font-size: 12px;font-weight: bold;">FORM NO.19 WAGE SLIP(See Rule 78(b))</h5>
            </td>
        </tr>
        <tr>
            <td colspan="14" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">Vir Security & Manpower Service</h6>
                <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc Bank Palanpur Banaskantha</h6>
            </td>
        </tr>
        <tr>
            <td valign="top" colspan="2" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">Month Year:</h6>
                <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">Branch Name:</h6>
                <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">Branch Address:</h6>
            </td>
            <td colspan="12" class="p-1" style="border: 1px solid black;">
                <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">{{ date('M-Y', strtotime($monthName)) }}</h6>
                <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">{{ $branch_name }}</h6>
                <h6 class="mb-0" style="font-size: 9px;font-weight: bold;">{{ $branch_address }}</h6>
            </td>
        </tr>
         <tr>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Month</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Working Days</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Worker(M)</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Worker(F)</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Worker</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Days(M)</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Days(F)</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Days</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total PF & ESI(M)</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total PF & ESI(F)</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total PF & ESI</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Gross Salary(M)</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Gross Salary(F)</h6></td>
                <td align="center" class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total Gross Salary</h6></td>
          </tr>


                    <tr>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ date('M-Y', strtotime($monthName)) }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $working_day }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalMale }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalFemale }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalWorker }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalMaleDays }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalFemaleDays }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalDays }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalPfEsicMale }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalpfEsicFemale }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalPfEsic }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalSalaryMale }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalSalaryFemale }}</p></td>
                        <td align="center" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:8px">{{ $totalGrossSalary }}</p></td>
                    </tr>
    </table>

</body>

</html>