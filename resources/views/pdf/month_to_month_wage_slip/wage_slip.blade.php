<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>

</head>

<body>

    <table width="100%">
        <thead>
            <tr>
                <td colspan="2" align="center" class="p-1 pb-3">
                    <h6 class="mb-1" style="font-size: 14px;font-weight: bold;">FORM NO. 19 WAGE SLIP(See rule 78(b))</h6>
                    <h6 class="mb-1" style="font-size: 13px;font-weight: bold;">Name & Address Of The Contractor:Vir Security & Manpower Service</h6>
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc Bank Palanpur Banaskantha</h6>
                </td>
            </tr>
            <tr>
                <td width="50%" valign="top" class="p-1">
                    <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">NAME OF PRINCIPAL EMPLOYER:{{ $branch_name }}</h6>
                    <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">NATURE AND LOCATION OF WORK:{{ $branch_address }}.</h6>
                    <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">DEPARTMENT : {{ $departmentName }}</h6>
                    <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">UAN :{{ $pfUanCode }}</h6>
                    <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">EMP. CODE : {{ $employee_id }}</h6>
                </td>
                <td width="50%" valign="top" class="p-1">
                    <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">NAME OF WORKMAN:{{ $employeeName }}</h6>
                    <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">P.F A/C No.:{{ $pfAccountCode }}</h6>
                    <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">ESI No.: {{ $esic }} </h6>
                    <h6 class="mb-1" style="font-size: 9px;font-weight: bold;">DESIGNATION : {{ $designationName }}</h6>
                </td>
            </tr>
        </thead>
        <tbody>
            <tr>
                <td colspan="2" class="pt-2 m-0">
                    <table width="100%">
                        <tr>
                            <td width="8%" valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Month</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">No. of
                                    Days
                                    worked</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Daily rate</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">O.T Days</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Basic</h6>
                                <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">DA</h6>
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Basic +
                                    DA</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total of
                                    Wages</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">A</h6>
                                <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">B</h6>
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">C</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">OT Amount</h6>
                                <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">ATT ALL</h6>
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Bonus</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">P.F</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">ESIC</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Prof. Tax</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Salary Adv.</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Other Dedu.</h6>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Net. Amount</h6>
                            </td>
                        </tr>
                        @foreach($salaries as $salary)
                        <tr>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ date('M-Y', strtotime($salary->monthyear)) }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $salary->no_of_days_work_done }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">Y</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $salary->ot_days }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $salary->basic_da }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $salary->basis_wages }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-1" style="font-size:8px">{{ $salary->a }}</p>
                                <p class="mb-1" style="font-size:8px">{{ $salary->b }}</p>
                                <p class="mb-0" style="font-size:8px">{{ $salary->c }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-1" style="font-size:8px">{{ $salary->ot_amount }}</p>
                                <p class="mb-0" style="font-size:8px">{{ $salary->bonus }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $salary->total }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $salary->pf }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $salary->esic }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $salary->profetional_tax }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $salary->sal_advance }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">0</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:8px">{{ $salary->nettotal }}</p>
                            </td>
                        </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
            <tr>
                <td colspan="2" class="pt-3 mb-0">
                    <table width="100%">
                        <tr>
                            <td width="33.33%" align="center" class="p-1">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Authorised Signature</h6>
                            </td>
                            <td width="33.33%" align="center" class="p-1">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Manager</h6>
                            </td>
                            <td width="33.33%" align="center" class="p-1">
                                <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Siganature Of Employee</h6>
                            </td>
                        </tr>
                    </table>
                </td>
            </tr>
        </tbody>
    </table>

</body>

</html>