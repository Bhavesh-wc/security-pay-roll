<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
  
</head> 

<body>
    <table width="100%">
        <tr>
            <td align="center" class="p-2" colspan="6"><h6 class="mb-0" style="font-size: 16px;font-weight: bold;">Branch Wise Employee Leave History</h6></td>
        </tr>
        <tr>
            <td class="py-2" colspan="6"><h6 class="mb-0" style="font-size: 14px;font-weight: bold;">Month/Year : {{ $monthyear ?? " " }}  </h6></td>
        </tr>
        <tr>
            <td class="py-2" colspan="6"><h6 class="mb-0" style="font-size: 14px;font-weight: bold;">Branch : {{ $branchName ?? " " }} </h6></td>
        </tr>
        <tr>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Sr.No.</h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Employee Name </h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Company Name</h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Branch Name</h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Join Date</h6></td>
            <td class="p-1" style="border: 1px solid black;"><h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Leave Date</h6></td>
        </tr>   
        @foreach($data as $list)     
        <tr>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $loop->iteration }}</p></td>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->employee->full_name }}</p></td>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->company->name }}</p></td>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->branch->name }}</p></td>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->join_date ?? "  -  " }}</p></td>
            <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $list->leave_date ?? "  -  " }}</p></td>
        </tr>
        @endforeach
    </table>
</body>
</html>