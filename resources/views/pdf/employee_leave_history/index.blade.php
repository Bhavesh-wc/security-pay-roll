@include('layouts.partials.header')

<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Employee Leave History</h1>
                </div>
            </div>
        </div> 
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ $message }}</strong>
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if ($message = Session::get('fail'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ $message }}</strong>
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="d-flex flex-column flex-lg-row">
                    <div class="flex-lg-row-fluid mb-10 mb-lg-0 me-lg-7 me-xl-10">
                        <div class="card">
                            <div class="card-body p-12">
                                <form method="POST" action="{{ route('print_employee_leave_histories_register') }}" id="kt_invoice_form" enctype="multipart/form-data">
                                    @csrf
                                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                        <a href="{{ route('report.index')}}" class="btn btn-sm mt-5 mx-2 btn-primary">
                                            Back
                                        </a>
                                        <button type="submit" name="export_type" value="pdf" class="btn btn-sm btn btn-success mt-5 mx-2" data-placement="top" data-toggle="tooltip">Export to PDF</button>
                                    </div>
                                    <div class="d-flex flex-column align-items-start flex-xxl-row">
                                        <div class="mb-0 col-lg-12">
                                            <div class="row gx-10 mb-5">
                                                <div class="col-lg-6">
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">Month Year *</label>
                                                    <input class="form-control" type="month" name="monthyear" id="monthyear" value="" required>
                                                </div>
                                                <div class="col-lg-6">
                                                    <div class="card-body pt-0">
                                                        <label class="required form-label fs-6 fw-bolder text-gray-700 mb-3">Branch</label>
                                                        <select id="branch_dropdown" name="branch_id" data-control="select2" data-placeholder="Select Branch" class="form-control @error('branch_id') is-invalid @enderror form-select mb-5" required>
                                                            <option value="" selected disabled class="">Select Branch</option>
                                                            @foreach ($branches as $data)
                                                            <option value="{{ trim($data->id) }}">{{ $data->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="text-danger">
                                                            @error('branch_id')
                                                            *{{ $message }}
                                                            @enderror
                                                        </span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('layouts.partials.footer')