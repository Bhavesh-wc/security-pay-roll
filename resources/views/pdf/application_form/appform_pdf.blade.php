<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
  <title>Application Form</title>
  <style>
    body {
      font-size: 14px;

    }

    p {
      font-size: 80%;
      font-weight: bold;
    }

    .form-top {
      padding: 30px;
      border-bottom: 1px solid black;
    }

    .form-logo {
      width: 30%;
    }

    .form-header {
      width: 70%;
    }

    .form-logo img {
      width: 200px;
    }

    .form-sixfiled {
      width: 75%;
    }

    .form-photo {
      width: 25%;
    }

    .form-row {
      padding: 5px 0px;
    }

    .filed-label {
      font-weight: bold;
      margin-right: 25px;
      max-width: 225px;
      width: 100%;
    }

    .output-filed {
      border-bottom: 1px solid black;
      width: calc(100% - 250px);
    }

    #heading {
      margin-top: 40px;
    }

    .left {
      padding-left: 10px;
    }
  </style>
</head>

<body>
  <div class="wrapper">
    <table class="w-100" cellspadding="10">
      <tbody>
        <tr style="border-bottom: 1px solid black;">
          <td width="30%" style="padding: 10px 30px 20px 30px;"><img src="{{ asset('/images/veer.png') }}" width="150px" alt="Veer-Security"></td>
          <td width="70%" align="center" style="padding: 10px 30px 20px 30px;">
            <p class="h5 font-weight-bold mb-2">VIR SECURITY & MANPOWER SERVICE</p>
            <p class="font-weight-bold mb-0">1st Floor,Aroma Arecade, Opp. Joravar Palace, Palanpur(B.K)</p>
            <p class="font-weight-bold mb-0">Email : admin@virsecurity.in Mo. 9712059999</p>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table class="w-100">
              <tr>
                <td width="70%" id="heading" style="padding-left: 180px;">
                  <p class="h5 font-weight-bold mb-2 ">APPLICATION FOR EMPLOYEE</p>
                </td>
                <td width="30%" align="center">
                  <p class="h5 font-weight-bold mb-2"> {{ $employee->id }}</p>
                </td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table class="w-100">
              <tr>
                <td width="80%">
                  <table width="100%">
                    <tr>
                      <td width="30%" style="padding-right: 25px; padding-top: 5px;">POSITION APPLIED FOR</td>
                      <td width="70%" style="border-bottom: 1px solid black;">{{$employee->designation->designation_name}}</td>
                    </tr>
                    <tr>
                      <td width="30%" style="padding-right: 25px;">Name</td>
                      <td width="70%" style="border-bottom: 1px solid black;">{{$employee->full_name}}</td>
                    </tr>
                    <tr>
                      <td width="30%" style="padding-right: 25px;">D.O.B</td>
                      <td width="70%" style="border-bottom: 1px solid black">{{ date('d-m-Y', strtotime($employee->date_of_birth)) }}</td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <table width="100%">
                          <tr>
                            <td width="20%" style="padding-top:10px; ">FATHER NAME</td>
                            <td width="30%" style="border-bottom: 1px solid black;">{{$employee->father_name}}</td>
                            <td width="20%" style="padding-top: 5px; ">MOTHER NAME</td>
                            <td width="30%" style="border-bottom: 1px solid black;">{{$employee->mother_name}}</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <table width="100%">
                          <tr>
                            <td width="20%" style="padding-top: 5px;">PANCARD NO.</td>
                            <td width="30%" style="border-bottom: 1px solid black;">{{$employee->pancard_no}}</td>
                            <td width="20%" style="padding-top: 5px;">AADHARCARD NO.</td>
                            <td width="30%" style="border-bottom: 1px solid black;">{{$employee->aadhar_card_no}}</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                    <tr>
                      <td colspan="2">
                        <table width="100%">
                          <tr>
                            <td width="20%" style="padding-top: 10px;" class="left">MOTHER TONGUE</td>
                            <td width="30%" style="border-bottom: 1px solid black;">{{$employee->MotherTongue->language_name}}</td>
                            <td width="20%" style="padding-top: 5px;" class="left">RELIGION.</td>
                            <td width="30%" style="border-bottom: 1px solid black;">{{$employee->religion}}</td>
                          </tr>
                        </table>
                      </td>
                    </tr>
                  </table>
                </td>
                 @if(!is_null($employee->photo))
                  <td width="20%" class="pt-1" align="right" rowspan="2"><img src="{{ asset('storage/' . $employee->photo)}}" width="100px"></td>
                  @else
                  <td width="20%" class="pt-1" align="right" rowspan="2"><img src="{{ asset('/images/userimage.jpg') }}" width="100px"></td>
                  @endif
              </tr>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="15%" style="padding-top: 5px;">MARITAL STATUS.</td>
                <td width="15%" style="border-bottom: 1px solid black;">{{$employee->marital_status}}</td>
                <td width="10%" style="padding-top: 5px;">HEIGHT.</td>
                <td width="10%" style="border-bottom: 1px solid black;">{{$employee->height}}</td>
                <td width="10%" style="padding-top: 5px;">WEIGHT.</td>
                <td width="10%" style="border-bottom: 1px solid black;">{{$employee->weight}}</td>
                <td width="15%" style="padding-top: 5px;">BLOOD GROUP.</td>
                <td width="15%" style="border-bottom: 1px solid black;">{{$employee->blood_group}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="35%" style="padding-top: 5px;">VISIBLE DISTINGUISHING MARK.</td>
                <td width="65%" style="border-bottom: 1px solid black;">{{$employee->visible_distinguishing_mark1}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">LOCAL ADDRESS.</td>
                <td width="80%" style="border-bottom: 1px solid black;">{{$employee->local_address}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">PHONE.</td>
                <td width="80%" style="border-bottom: 1px solid black;">{{$employee->local_phone_no}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">BIRTH PLACE.</td>
                <td width="80%" style="border-bottom: 1px solid black;">{{$employee->birth_place}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="25%" style="padding-top: 5px;">PERMANENT ADDRESS.</td>
                <td width="75%" style="border-bottom: 1px solid black;">{{$employee->permanent_address}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">PHONE.</td>
                <td width="80%" style="border-bottom: 1px solid black;">{{$employee->permanent_phone_no}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">QUALIFICATION.</td>
                <td width="80%" style="border-bottom: 1px solid black;">{{$employee->qualification->qualification_name}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">EXPERIENCE.</td>
                <td width="80%" style="border-bottom: 1px solid black;">{{$employee->experience_company}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">REFERENCE.</td>
                <td width="80%" style="border-bottom: 1px solid black;">{{$employee->reference_relation_contact_1}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">SPOUSE NAME.</td>
                <td width="80%" style="border-bottom: 1px solid black;">{{$employee->spouse_name}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="30%" style="padding-top: 5px;">POLICE VERIFICATION CERTIFICATE.</td>
                <td width="70%" style="border-bottom: 1px solid black;">{{$employee->police_varification}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2" style="border-bottom: 1px solid black; padding-bottom:30px">
            <table width="100%">
              <tr>
                <td width="10%" style="padding-top: 5px;">DATE.</td>
                <td width="25%" style="border-bottom: 1px solid black;">{{ date('d-m-Y', strtotime($employee->registartion_date)) }}</td>
                <td width="30%" style="padding-top: 5px;">SIGNATURE OF APPLICANT.</td>
                <td width="35%" style="border-bottom: 1px solid black;">{{$employee->signature}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2" align="center" style="padding-top:10px">
            <p class="h5 font-weight-bold mb-2">FOR OFFICE USE ONLY</p>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="25%" style="padding-top: 5px;">DATE OF INTERVIEW:</td>
                <td width="75%" style="border-bottom: 1px solid black;">#</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">BRANCH NAME:</td>
                <td width="80%" style="border-bottom: 1px solid black;">{{$employee->branch->name}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">DATE OF JOINING:</td>
                <td width="80%" style="border-bottom: 1px solid black;">{{$employee->entry_date}}</td>
              </tr>
            </table>
          </td>
        </tr>
        <tr>
          <td colspan="2">
            <table width="100%">
              <tr>
                <td width="20%" style="padding-top: 5px;">DATE</td>
                <td width="30%" style="border-bottom: 1px solid black;">#</td>
                <td width="20%" style="padding-top: 5px;">SIGNATURE</td>
                <td width="30%" style="border-bottom: 1px solid black;">#</td>
              </tr>
            </table>
          </td>
        </tr>
    </table>
    </tbody>
    </table>
  </div>
</body>

</html>