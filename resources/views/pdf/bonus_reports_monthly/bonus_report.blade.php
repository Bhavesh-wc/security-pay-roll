<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>




</head>

<style>
    .sec-table tr:first-child th:first-child {
        border-left: 0px !important;
    }

    .sec-table tr:first-child th {
        border-top: 0px !important;
    }

    .sec-table tr td:first-child {
        border-left: 0px !important;
    }

    .sec-table tr td:last-child {
        border-right: 0px !important;
    }

    .sec-table tr:last-child td {
        border-bottom: 0px !important;
    }
</style>

<body>

    <table width="100%">
        <thead>
            <tr>
                <td colspan="2" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size:14px;font-weight: bold;">FORM NO C [Rulw 4d]</h6>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-1" style="font-size:9px;font-weight: bold;">Name Of Contractor:Vir Security & Manpower Service</h6>
                    <h6 class="mb-0" style="font-size:9px;font-weight: bold;">Address Of Contractor:7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc Bank Palanpur Banaskantha</h6>
                </td>
            </tr>
            <tr>
                <td colspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size:9px;font-weight: bold;">Register To Be Maintined As Per Rule 4(d) Of The Payment Of Bonus Rule 1995 Bonus Paid To The Accounting Year Ending On The No. Of Working Day In Year.</h6>
                </td>
            </tr>
            <tr>
                <td width="50%" valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size:9px;font-weight: bold;">Branch Name: {{ $branch_name }}</h6>
                </td>
                <td width="50%" valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size:9px;font-weight: bold;">Month-Year: {{ $fromMonthName }} TO {{ $toMonthName }}</h6>
                </td>
            </tr>
        </thead>

        <tbody>
            <tr>
                <td colspan="2" style="padding: 0px;border: 1px solid black;">
                    <table width="100%" class="sec-table">
                        <thead>
                            <tr>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Sr.No</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Name of Employee And His Fathers Name</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Wheather He Has Completed Is Year Of Age At The Begining Accounting year?</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Designation</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">No.Of Days Worked In The Year</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Total Salary Of Wages In Respect Of The Accounting Year</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Amount Of Bonus Pyable Under Section 10 to 11 As The Case May Be</h6>
                                </th>
                                <th colspan="3" align="center" valign="middle" class="p-1" style="border: 1px solid black;text-align: center;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Deduction</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Total Sum De ducted (col.9-10-11)</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Net Amount Payable (col.8-12)</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Amount Actually Paid</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Date On Which Paid</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Signature Thumb Impression Of The Employer</h6>
                                </th>
                                <th rowspan="2" valign="top" class="p-1" style="">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Remarkr</h6>
                                </th>
                            </tr>
                            <tr>
                                <th valign="top" class="p-1" style="border: 1px solid black;border-bottom: 0px !important;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Puja Or Other Customer Bonus Paid During The Accounting Year</h6>
                                </th>
                                <th valign="top" class="p-1" style="border: 1px solid black;border-bottom: 0px !important;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Intoriam Bonus Or Bonus Paid In Advance</h6>
                                </th>
                                <th valign="top" class="p-1" style="border: 1px solid black;border-bottom: 0px !important;">
                                    <h6 class="mb-0" style="font-size:8px;font-weight: bold;">Deduction Account Of Financial Lots In Any Casual By Misconduct Of The Employer</h6>
                                </th>

                            </tr>
                        </thead>
                        @foreach($employees as $employee)
                        @if(count($employee->salaries) > 0)
                        <tr>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px">{{ $loop->iteration }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-1" style="font-size:9px">{{ $employee->full_name }}</p>
                                <p class="mb-0" style="font-size:9px">{{ $employee->father_name }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px">{{ $fromMonthName }} TO {{ $toMonthName }}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px">{{$employee->designation_name}} </p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px">{{$employee->total_working_days}}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px">{{$employee->total_basis_wages}}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px">{{$employee->total_bonus}}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px"></p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px"></p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px"></p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px"></p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px">-</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px">{{$employee->total_paid_amount}}</p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px"></p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px"></p>
                            </td>
                            <td valign="top" class="p-1" style="border: 1px solid black;">
                                <p class="mb-0" style="font-size:9px">737601141</p>
                                <p class="mb-0" style="font-size:9px">000312</p>
                            </td>
                        </tr>
                        @endif
                        @endforeach
                    </table>
                </td>
            </tr>
        </tbody>

    </table>

</body>

</html>