<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
  <style>
    .last-td-border tr:last-child td
    {
        border-bottom: 0px !important;
    }
    .last-td-border tr td:first-child
    {
        border-left: 0px !important;
    }
    .last-td-border tr td:last-child
    {
        border-right: 0px !important;
    }
  </style>
</head> 

<body>
    <table width="100%">
        <thead>
            <tr>
                <td colspan="8" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size:10px;font-weight: bold;">Form VIII</h6>
                    <h6 class="mb-0" style="font-size:10px;font-weight: bold;">(See Rule 14)</h6>
                    <h6 class="mb-0" style="font-size:10px;font-weight: bold;">Register Of Particulars</h6>
                    <h6 class="mb-0" style="font-size:10px;font-weight: bold;">(Part-II Private Security Guards And Supervisor)</h6>
                </td>
            </tr>
            <tr>
                <td colspan="4" width="50%" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Agency: Vir Security & Manpower Service</h6>
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Address: 7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc Bank
                        Palanpur Banaskantha</h6>
                </td>
                <td colspan="4" width="50%" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Principal Employer: {{ $data[0]->branch->name }}</h6>
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Address: {{ $data[0]->branch->address }}</h6>
                        <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date: {{ date('d-m-Y', strtotime($startdate)) }} TO {{ date('d-m-Y', strtotime($enddate)) }}</h6>
                </td>
            </tr>
            <tr>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Sr.No.</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Name Of Supervisor/Guards</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Father Name</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Present Address & Phone No.</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Date Of Joining/Leaving</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Permanent Address</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Photograph</h6>
                </td>
                <td class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Bedge No.</h6>
                </td>
            </tr>
            <?php $sr = 1; ?>
            @foreach($data as $data)
            <tr>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $sr++; }}</p></td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->full_name }}</p></td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->father_name }}</p></td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size:10px">{{ $data->local_address }}</p>
                    <p class="mb-3" style="font-size:10px">{{ $data->local_phone_no }}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ date('d-m-Y', strtotime($data->entry_date)) }}<br />{{ date('d-m-Y', strtotime($data->relieve_date)) }}</p></td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size:10px">{{ $data->permanent_address }}</p>
                    <p class="mb-3" style="font-size:10px">Phone : {{ $data->permanent_phone_no }}</p>
                </td>
                @if(!is_null($data->photo))
                    <td class="p-1" align="center" style="border: 1px solid black;"><img src="{{ 'storage/' . $data->photo }}" width="80px"></td>
                @else
                    <td class="p-1" align="center" style="border: 1px solid black;"><img src="{{ asset('/images/user.jpg') }}" width="80px"></td>
                @endif
                <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{ $data->id }}</p></td>
            </tr>
            @endforeach
        </thead>
    </table>

</body>
</html>