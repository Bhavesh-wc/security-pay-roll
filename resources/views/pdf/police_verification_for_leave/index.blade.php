@include('layouts.partials.header')

<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Police Verification For Leave Report</h1>
                </div>
            </div>
        </div> 
        <div class="post d-flex flex-column-fluid" id="kt_post">
            <div id="kt_content_container" class="container-xxl">
                @if ($message = Session::get('success'))
                    <div class="alert alert-success alert-dismissible fade show" role="alert">
                        <strong>{{ $message }}</strong>
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                @if ($message = Session::get('fail'))
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong>{{ $message }}</strong>
                        <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                    </div>
                @endif
                <div class="d-flex flex-column flex-lg-row">
                    <div class="flex-lg-row-fluid mb-10 mb-lg-0 me-lg-7 me-xl-10">
                        <div class="card">
                            <div class="card-body p-12">
                                <form method="POST" action="{{ route('print_police_verification_for_leave_register') }}" id="kt_invoice_form" enctype="multipart/form-data">
                                    @csrf
                                    <div class="d-flex justify-content-end" data-kt-user-table-toolbar="base">
                                        <a href="{{ route('report.index')}}" class="btn btn-sm mt-5 mx-2 btn-primary">
                                            Back
                                        </a>
                                        <button type="submit" name="export_type" value="excel" class="btn btn-sm btn btn-success mt-5 mx-2" data-placement="top" data-toggle="tooltip">Export to Excel</button>
                                        <button type="submit" name="export_type" value="pdf" class="btn btn-sm btn btn-success mt-5 mx-2" data-placement="top" data-toggle="tooltip">Export to PDF</button>
                                        {{-- <button type="submit" value="true" name="go_to_appform" data-placement="top" data-toggle="tooltip" title="Download" class="btn btn-sm btn btn-success mt-5">Export
                                        </button> --}}
                                    </div>
                                    <div class="d-flex flex-column align-items-start flex-xxl-row">
                                        <div class="mb-0 col-lg-12">
                                            <div class="row gx-10 mb-5">
                                                    <div class="col-lg-6">
                                                        <div class="card-body pt-0">
                                                            <div class="form-group {{ $errors->has('start_date') ? 'has-error' : '' }}">
                                                                <label for="start_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Start Date</label>
                                                                <div class="input-group log-event" id="kt_td_picker_linked_1" data-td-target-input="nearest" data-td-target-toggle="nearest">
                                                                    <input id="kt_td_picker_linked_1_input" type="text" name="start_date" data-toggle="datetimepicker" value="{{ old('start_date') }}" class="form-control" data-td-target="#kt_td_picker_linked_1" placeholder="Enter start date here..."/>
                                                                    <span class="input-group-text" data-td-target="#kt_td_picker_linked_1" data-td-toggle="datetimepicker">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <div class="card-body pt-0">
                                                            <div class="form-group {{ $errors->has('end_date') ? 'has-error' : '' }}">
                                                                <label for="end_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">End Date</label>
                                                                <div class="input-group log-event" id="kt_td_picker_linked_2" data-td-target-input="nearest" data-td-target-toggle="nearest">
                                                                    <input id="kt_td_picker_linked_2_input" type="text" name="end_date" data-toggle="datetimepicker" value="{{ old('end_date') }}" class="form-control" data-td-target="#kt_td_picker_linked_2" placeholder="Enter end date here..."/>
                                                                    <span class="input-group-text" data-td-target="#kt_td_picker_linked_2" data-td-toggle="datetimepicker">
                                                                        <i class="fa fa-calendar"></i>
                                                                    </span>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                <div class="col-lg-6">
                                                    <div class="card-body pt-0">
                                                        <label class="required form-label fs-6 fw-bolder text-gray-700 mb-3">Branch</label>
                                                        <select id="branch_dropdown" name="branch_id" data-control="select2" data-placeholder="Select Branch" class="form-control @error('branch_id') is-invalid @enderror form-select mb-5" required>
                                                            <option value="" selected disabled class="">Select Branch</option>
                                                            @foreach ($branches as $data)
                                                            <option value="{{ trim($data->id) }}">{{ $data->name}}</option>
                                                            @endforeach
                                                        </select>
                                                        <span class="text-danger">
                                                            @error('branch_id')
                                                            *{{ $message }}
                                                            @enderror
                                                        </span>
                                                    </div>
                                                </div>
                                                <div class="col-lg-12 d-flex justify-content-end ">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>            
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.4/jquery.min.js"></script>
<script>
    $(document).ready(function(){
        $('select[name="status"]').on('change', function(){
            var status = $(this).val();
            var startdate = $('#start_date').val();
            var enddate = $('#end_date').val();
            $.ajax({
                url: '{{ route("get_police_verification_for_leave_branches") }}',
                type: 'GET',
                data: { status: status, startdate: startdate, enddate:enddate },
                success: function(data){
                    $('select[name="branch_id"]').html('<option value="">Select Branch</option>');
                    $.each(data, function(key, value){
                        $('select[name="branch_id"]').append('<option value="'+ key +'">'+ value +'</option>');
                    });
                }
            });
        });
    });
</script>
@include('layouts.partials.footer')