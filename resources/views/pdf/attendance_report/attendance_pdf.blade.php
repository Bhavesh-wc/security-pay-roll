<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
  <title></title>
  <style>
    .table-no-border:first-child tr td
    {
        border-top: 0px !important;
    }
    .table-no-border:first-child tr td:first-child
    {
        border-left: 0px !important;
    }
    .table-no-border:first-child tr td:last-child
    {
        border-right: 0px !important;
    }
    .table-no-border tr:last-child td
    {
        border-bottom: 0px !important;
    }
  </style>
</head> 

<body>
  <table width="100%">
    <tr>
        <td colspan="3" class="p-1" align="center" style="border: 1px solid black;">
            <h4 class="mb-3" style="font-size: 12px;font-weight: bold;">MUSTER ROLL</h4>
            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Form XV</h6>
            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">(See Rule-78-2)(a)</h6>
        </td>
    </tr>
    <tr>
        <td width="40%" class="p-1" align="center" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Name of Contractor:</h6>
            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Vir Security & Manpower Service</h6>
        </td>
        <td width="20%"rowspan="2" width="40%" class="p-1" align="center" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Month:-{{$monthName}}</h6>
        </td>
        <td width="40%" class="p-1" align="center" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Name of establishment in under</h6>
            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">which contract is carried on:{{ $branch_name }}</h6>
        </td>
    </tr>
    <tr>
        <td class="p-1" align="center" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Address of contractor:7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc
                Bank Palanpur Banaskantha</h6>
        </td>
        <td class="p-1" align="center" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">Address of Principal Employer:{{$branch_address}}</h6>
        </td>
    </tr>
    <tr>
        <td colspan="3" class="p-1" align="center" style="border: 1px solid black;">
            <h6 class="mb-0" style="font-size: 12px;font-weight: bold;">{{$branch_name}} MONTH OF {{$monthName}}</h6>
        </td>
    </tr>
    <tr>
        <td colspan="3" style="border: 1px solid black;margin: 0px;padding: 0px;">
            <table width="100%" class="table-no-border">
                <tr>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">Sr No.</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">Name Of Workman</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">Designation</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">1</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">2</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">3</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">4</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">5</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">6</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">7</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">8</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">9</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">10</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">11</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">12</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">13</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">14</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">15</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">16</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">17</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">18</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">19</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">20</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">21</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">22</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">23</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">24</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">25</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">26</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">27</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">28</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">29</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">30</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">31</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">Present</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">Absent</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">Holiday</p></td>
                </tr>
                @foreach ($employee as $emp)
                <tr>

                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->id}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->employee->full_name}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->designation->designation_name}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_1}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_2}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_3}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_4}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_5}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_6}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_7}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_8}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_9}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_10}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_11}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_12}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_13}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_14}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_15}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_16}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_17}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_18}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_19}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_20}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_21}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_22}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_23}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_24}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_25}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_26}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_27}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_28}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_29}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_30}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->day_31}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->total_present}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->total_absent}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$emp->total_holiday}}</p></td>
                </tr>
                @endforeach
                <tr>
                    <td colspan="34" align="right" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">Total</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$present}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$absent}}</p></td>
                    <td class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:10px">{{$holiday}}</p></td>
                </tr>

            </table>
        </td>
    </tr>
   

  </table>
</body>
