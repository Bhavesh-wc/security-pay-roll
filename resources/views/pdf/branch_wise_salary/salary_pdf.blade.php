<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>

</head>

<body>

    <table width="100%">
        <thead>
             <tr>
                <td  colspan="8" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size:12px;font-weight: bold;">FORM NO. 17</h6>
                </td>
                <td colspan="7" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size:12px;font-weight: bold;">See Rule 78(2)(3)</h6>
                </td>
            </tr>
            <tr>
            <td colspan="8" valign="top" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-1" style="font-size:9px;font-weight: bold;">Name Of Contractor:Vir Security & Manpower Service</h6>
                    <h6 class="mb-0" style="font-size:9px;font-weight: bold;">Address Of Contractor:7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc Bank Palanpur Banaskantha</h6>
                </td>
                <td colspan="7" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-1" style="font-size:9px;font-weight: bold;">Name Establishment In/Under Which Contract Is Carried On:{{ $branch_name }}</h6>
                    <h6 class="mb-1" style="font-size:9px;font-weight: bold;">Address Of Establishment:{{ $branch_address }}</h6>
                    <h6 class="mb-0" style="font-size:9px;font-weight: bold;">Month: {{$monthName}}</h6>
                </td>
            </tr>

            <tr>
               
               <td valign="top" rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Sr.No.</h6>
               </td>
               <td valign="top" rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Name of Employee</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Designation</h6>
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Department</h6>
               </td>
               <td valign="top" rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Emp Code</h6>
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">SalaryMonth</h6>
               </td>
               <td valign="top" rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Working Days</h6>
               </td>
               <td valign="top" rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">No of Days Work Done</h6>
               </td>
               <td valign="top" rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Basic + DA</h6>
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Extra ALL.</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">FIX SALARY.</h6>
               </td>
               <td valign="top" align="center" colspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Amount</h6>
               </td>
               <td valign="top" rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">OT Amount</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">ATT ALL</h6>
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Allowance</h6>
               </td>
               <td valign="top" rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Total</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Branch Bonus</h6>
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total + BranchBonus</h6>
               </td>
               <td valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Other Deduction</h6>
               </td>
               <td valign="top" align="center" colspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Deduction of Any</h6>
               </td>
               <td valign="top" rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Net Amount Paid</h6>
               </td>
               <td valign="top" rowspan="2" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Bank Name</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Bank Branch</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">IFSC Code</h6>
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Bank AC. No.</h6>
               </td>
       </tr>
           <tr>
               <td valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Basis Wages</h6>
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Extar ALL. Total</h6>
               </td>
               <td valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Medical Allowance</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">HRA</h6>
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">C</h6>
               </td>
               <td valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Damage/ Loss</h6>
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Fines</h6>
               </td>
               <td valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">P.F</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Prof. Tax</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">E.S.I</h6>
                   <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Wal. Fund</h6>
               </td>
               <td valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Sal Adv.</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Advance</h6>
                   <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">Mess</h6>
               </td>
           </tr>
            @foreach($salaries as $salary)

            <tr>
                    <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:9px">{{ $loop->iteration }}</p></td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-1" style="font-size:9px">{{ $salary->employee->full_name }}</p>
                        <p class="mb-0" style="font-size:9px">{{ $salary->designation->designation_name }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-1" style="font-size:9px">{{ $salary->employee->id }}</p>
                        <p class="mb-0" style="font-size:9px">{{ $monthName }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:9px">{{ $salary->working_days }}</p></td>
                    <td valign="top" class="p-1" style="border: 1px solid black;"><p class="mb-0" style="font-size:9px">{{ $salary->no_of_days_work_done }}</p></td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-1" style="font-size:9px">{{ $salary->basic_da }}</p>
                        <p class="mb-1" style="font-size:9px">{{ $salary->extra_allowance }}</p>
                        <p class="mb-1" style="font-size:9px">{{ $salary->fix_salary }}</p>
                        <p class="mb-0" style="font-size:9px">0</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-1" style="font-size:9px">{{ $salary->basis_wages }}</p>
                        <p class="mb-0" style="font-size:9px">0.00</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-1" style="font-size:9px">0.00</p>
                        <p class="mb-1" style="font-size:9px">{{ $salary->hra }}</p>
                        <p class="mb-0" style="font-size:9px">{{ $salary->c }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-1" style="font-size:9px">{{ $salary->ot_amount }}</p>
                        <p class="mb-1" style="font-size:9px">0.00</p>
                        <p class="mb-0" style="font-size:9px">0.00</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-1" style="font-size:9px">{{$salary->total}}</p>
                        <p class="mb-1" style="font-size:9px">{{$salary->bonus}}</p>
                        <?php $sum = $salary->total + $salary->bonus; ?>
                        <p class="mb-0" style="font-size:9px">{{ $sum }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-1" style="font-size:9px">{{ $salary->damage_amount }}</p>
                        <p class="mb-0" style="font-size:9px">{{ $salary->fines }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-1" style="font-size:9px">{{ $salary->pf }}</p>
                        <p class="mb-1" style="font-size:9px">{{ $salary->profetional_tax }}</p>
                        <p class="mb-1" style="font-size:9px">{{ $salary->esic }}</p>
                        <p class="mb-1" style="font-size:9px">{{ $salary->walfare_amount }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                   <p class="mb-1" style="font-size:9px">{{ $salary->sal_advance }}</p>
                    <p class="mb-1" style="font-size:9px">0.00</p>
                    <p class="mb-0" style="font-size:9px">{{ $salary->mess }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                        <p class="mb-0" style="font-size:9px">{{ $salary->nettotal }}</p>
                    </td>
                    <td valign="top" class="p-1" style="border: 1px solid black;">
                   <p class="mb-1" style="font-size:9px">{{ $salary->employee->bank_id ? $salary->employee->bank->bank_name : '' }}</p>
                    <p class="mb-1" style="font-size:9px">{{ $salary->employee->bank_branch_id ? $salary->employee->bankBranch->bank_branch_name : '' }}</p>
                    <p class="mb-1" style="font-size:9px">{{ $salary->employee->ifsc_code ? $salary->employee->bankBranch->ifsc_code : ''}}</p>
                    <p class="mb-0" style="font-size:9px">{{ $salary->employee->bank_ac_no ? $salary->employee->bank_ac_no : ''}}</p>
                    </td>
                </tr>

            @endforeach       

                        <tr>
                                <td colspan="3" valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $branch_name }} Staff TOTAL</h6>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $workingday }}</h6>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">{{$workingdayDone}}</h6>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">             
                                <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $basicDa }}</h6>
                                <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $extraAllowance }}</h6>
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $fixSalary }}</h6>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $basicWages }}</h6>
                                <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">0</h6>
                               </td>
                                 <td valign="top" class="p-1" style="border: 1px solid black;">
                                 <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">0</h6>
                                 <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $hra }}</h6>
                                 <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $c }}</h6>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $otAmount }}</h6>
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">0</h6>
                                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">0</h6>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $total }}</h6>
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $branchBonus }}</h6>
                                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $totalBranchBonus }}</h6>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $damage }}</h6>
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $fine}}</h6>
                            
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{$pf}}</h6>
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $professionalTax }}</h6>
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $esic }}</h6>
                                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">{{ $walferFund }}</h6>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $salaryAdvance }}</h6>
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">0</h6>
                                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">{{$mess}}</h6>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                    <h6 class="mb-1" style="font-size: 8px;font-weight: bold;">{{ $netAmountPaid }}</h6>
                                </td>
                                <td valign="top" class="p-1" style="border: 1px solid black;">
                                   
                                </td>
                            </tr>

                            <tr>
                                <td colspan="15" class="pt-3"></td>
                            </tr>
                            <tr>
                                <td colspan="5" valign="top" class="p-1">
                                    <h6 class="mb-1" style="font-size: 10px;font-weight: bold;">Prepared By</h6>
                                </td>
                                <td colspan="3" valign="top" class="p-1">
                                    <h6 class="mb-1" style="font-size: 10px;font-weight: bold;">Checked By</h6>
                                </td>
                                <td colspan="3" valign="top" class="p-1">
                                    <h6 class="mb-1" style="font-size: 10px;font-weight: bold;">Accountant</h6>
                                </td>
                                <td colspan="4" valign="top" class="p-1">
                                    <h6 class="mb-1" style="font-size: 10px;font-weight: bold;">Authorised Signature</h6>
                                </td>
                            </tr>
          

        </thead>
    </table>

</body>

</html>