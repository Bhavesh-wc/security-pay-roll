<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet">
    <title></title>
</head>

<body>
    <table width="100%">
        <tbody>
            <tr>
                <td colspan="8" align="center" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 14px;">ESIC Report Month of {{ $monthyear }}</h6>
                </td>
            </tr>
            <tr>
                <td colspan="3" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 9px;">Name Of Contractor:Vir Security & Manpower Service</h6>
                    <h6 class="mb-0" style="font-size: 9px;">Address Of Contractor:7 1st Floor Aroma Arcade Opp Joraver Palace Nr Hdfc Bank Palanpur Banaskantha</h6>
                </td>
                <td colspan="5" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 9px;">Branch Name: {{ $branch_name }}</h6>
                    <h6 class="mb-0" style="font-size: 9px;">Year: {{ $monthyear }}</h6>
                </td>
            </tr>
            
            <tr style="border: 1px solid black;">
                <td class="p-1" align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Sr. No.</h6>
                </td>
                <td class=" p-1" align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">ESIC NO</h6>
                </td>
                <td class=" p-1" align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Employee Code</h6>
                </td>
                <td class=" p-1" align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Name of Employee</h6>
                </td>
                <td class=" p-1" align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">BASIC + DA/Fix Salary</h6>
                </td>
                <td class=" p-1" align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">ESIC - 1.75%</h6>
                </td>
                <td class=" p-1" align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">ESIC - 4.75%</h6>
                </td>
                <td class=" p-1" align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 8px;font-weight: bold;">Total</h6>
                </td>
            </tr>
            ?>
            @foreach($salary as $list)
            <tr>
                <td align=" center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{$loop->iteration }}</p>
                </td>
                <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">0</p>
                </td>
                <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $list->employee->id }}</p>
                </td>
                <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $list->employee->full_name }}</p>
                </td>
                <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $list->basic_da }} / {{ $list->fix_salary }}</p>
                </td>

                <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $list->esic1 }} / {{ $list->esic3 }}</p>
                </td>
                <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $list->esic2 }} / {{ $list->esic4 }}</p>
                </td>
                <td align="center" valign="top" class="p-1" valign="top" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 8px;">{{ $list->totalEsicaaa }} </p>
                </td>

            </tr>
            @endforeach

            <tr>
                <td colspan="4" align="right" class="p-1" style="border: 1px solid black;">
                    <h6 class="mb-0" style="font-size: 10px;font-weight: bold;">Total:</h6>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 10px;font-weight: bold;">{{$totalBasic}}//{{$totalFix}}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 10px;font-weight: bold;">{{$totalEsic1}}/{{$totalEsic3}}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 10px;font-weight: bold;">{{ $totalEsic2}}/{{ $totalEsic4 }}</p>
                </td>
                <td class="p-1" style="border: 1px solid black;">
                    <p class="mb-0" style="font-size: 10px;font-weight: bold;">{{$totalFinal}}</p>
                </td>
            </tr>

        </tbody>
    </table>








</body>

</html>