<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('grade_name') ? 'has-error' : '' }}">
                        <label for="grade_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Grade Name</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="grade_name" type="text" id="grade_name" value="{{ old('grade_name', optional($grade)->grade_name) }}" required="true" placeholder="Enter grade name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('garde_code') ? 'has-error' : '' }}">
                        <label for="garde_code" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Garde Code</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="garde_code" type="text" id="garde_code" value="{{ old('garde_code', optional($grade)->garde_code) }}" required="true" placeholder="Enter garde code here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('status', optional($grade)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                                    @foreach (['active' => 'Active', 'inactive' => 'Inactive'] as $key => $text)
                                        <option value="{{ $key }}" {{ old('status', optional($grade)->status) == $key ? 'selected' : '' }}>
                                            {{ $text }}
                                        </option>
                                    @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
    