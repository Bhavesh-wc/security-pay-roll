<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('country_name') ? 'has-error' : '' }}">
                        <label for="country_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Country Name</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="country_name" type="text" id="country_name" value="{{ old('country_name', optional($country)->country_name) }}" required="true" placeholder="Enter country name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('status', optional($country)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                                @foreach (['active' => 'Active', 'inactive' => 'Inactive'] as $key => $text)
                                    <option value="{{ $key }}" {{ old('status', optional($country)->status) == $key ? 'selected' : '' }}>
                                        {{ $text }}
                                    </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
       