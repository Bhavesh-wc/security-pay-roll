

<div class="d-flex flex-column gap-7 gap-lg-12">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="form-group {{ $errors->has('logo') ? 'has-error' : '' }}">
                <label for="Logo" class="form-label fs-6 fw-bolder text-black-700 mb-3">Logo</label>
                <div class="col-md-12">
                    <input class="form-control mb-2" name="logo" type="file" id="logo" value="{{ old('logo', optional($systemConfiguration)->logo) }}" placeholder="Enter logo here...">  
                </div>
                <img src="{{asset('storage').'/'.$systemConfiguration->logo}}" height="100px">
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="form-group {{ $errors->has('Favicon') ? 'has-error' : '' }}">
                <label for="Favicon" class="form-label fs-6 fw-bolder text-black-700 mb-3">Favicon</label>
                <div class="col-md-12">
                    <input class="form-control mb-2" name="Favicon" type="file" id="Favicon" value="{{ old('Favicon', optional($systemConfiguration)->Favicon) }}" placeholder="Enter Favicon here...">  
                </div>
                <img src="{{asset('storage').'/'.$systemConfiguration->Favicon}}" height="100px">
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="form-group {{ $errors->has('title') ? 'has-error' : '' }}">
                <label for="title" class="form-label fs-6 fw-bolder text-black-700 mb-3">Title</label>
                <div class="col-md-12">
                    <input class="form-control mb-2" name="title" type="text" id="title" value="{{ old('title', optional($systemConfiguration)->title) }}" minlength="1" required="true" placeholder="Enter title here...">
                    {!! $errors->first('title', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                <label for="address" class="form-label fs-6 fw-bolder text-black-700 mb-3">Address</label>
                <div class="col-md-12">
                    <textarea class="form-control mb-2" name="address" cols="50" rows="10" id="address" required="true" placeholder="Enter address here...">{{ old('address', optional($systemConfiguration)->address) }}</textarea>
                    {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="form-group {{ $errors->has('mobile') ? 'has-error' : '' }}">
                <label for="mobile" class="form-label fs-6 fw-bolder text-black-700 mb-3">Mobile</label>
                <div class="col-md-12">
                    <input class="form-control mb-2" name="mobile" type="text" id="mobile" value="{{ old('mobile', optional($systemConfiguration)->mobile) }}" minlength="1" required="true" placeholder="Enter mobile here...">
                    {!! $errors->first('mobile', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="form-group {{ $errors->has('email') ? 'has-error' : '' }}">
                <label for="email" class="form-label fs-6 fw-bolder text-black-700 mb-3">Email</label>
                <div class="col-md-12">
                    <input class="form-control mb-2" name="email" type="email" id="email" value="{{ old('email', optional($systemConfiguration)->email) }}" required="true" placeholder="Enter email here...">
                    {!! $errors->first('email', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
        <div class="card-body pt-0">
            <div class="form-group {{ $errors->has('footer') ? 'has-error' : '' }}">
                <label for="footer" class="form-label fs-6 fw-bolder text-black-700 mb-3">Footer</label>
                <div class="col-md-12">
                    <input class="form-control mb-2" name="footer" type="text" id="footer" value="{{ old('footer', optional($systemConfiguration)->footer) }}" minlength="1" required="true" placeholder="Enter footer here...">
                    {!! $errors->first('footer', '<p class="help-block">:message</p>') !!}
                </div>
            </div>
        </div>
    </div>
</div>

