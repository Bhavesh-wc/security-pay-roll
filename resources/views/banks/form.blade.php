
<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('bank_name') ? 'has-error' : '' }}">
                        <label for="bank_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bank Name</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="bank_name" type="text" id="bank_name" value="{{ old('bank_name', optional($bank)->bank_name) }}" minlength="1" required="true" placeholder="Enter bank name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('account_no') ? 'has-error' : '' }}">
                        <label for="account_no" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Account No</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="account_no" type="number" id="account_no" value="{{ old('account_no', optional($bank)->account_no) }}" minlength="1" required="true" placeholder="Enter bank account no here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select">
                                <option value="" style="display: none;" {{ old('status', optional($bank)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                                    @foreach (['active' => 'Active', 'inactive' => 'Inactive'] as $key => $text)
                                <option value="{{ $key }}" {{ old('status', optional($bank)->status) == $key ? 'selected' : '' }}>
                                    {{ $text }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>