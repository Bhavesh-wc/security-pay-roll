<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_toolbar" class="app-toolbar  py-3 py-lg-6 ">
            <div id="kt_app_toolbar_container" class="app-container  container-xxl d-flex flex-stack ">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3 ">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Dashboard</h1>
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">Welcome to Vir Security & Manpower Service</li>
                    </ul>
                </div>
            </div>
        </div>
        <div id="kt_app_content" class="app-content  flex-column-fluid ">
            <div id="kt_app_content_container" class="app-container  container-xxl ">
                <div class="row g-5 g-xl-10">
                    <div class="col-md-4 mb-xl-5">              
                        <div class="card card-flush h-xl-70">   
                            <div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-200px" style="background-image: url({{ asset('/images/img-75-dark.jpg') }})" data-bs-theme="light">
                                <h3 class="card-title align-items-start flex-column text-white pt-5">
                                    <span class="fw-bold fs-2x mb-2">Company</span>
                                    <div class="fs-4 text-white">
                                        <span class="opacity-75">You have Total</span>
                                        <span class="position-relative d-inline-block">
                                            @php $count = DB::table('users')->count(); @endphp
                                            <a href="{{ route('users.user.index') }}" class="link-white opacity-75-hover fw-bold d-block mb-1">{{$count}} companies</a>
                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
                                        </span>
                                    </div>    
                                </h3>
                            </div>
                            <div class="card-body mt-n20">
                                <div class="mt-n20 position-relative">
                                    <div class="row g-3 g-lg-6">
                                        <div class="col-6">
                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-2">
                                                   <img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
                                                </div>
                                                <div class="m-0">
                                                    @php $count = DB::table('users')->where('status','=','active')->count(); @endphp
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{$count}}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">Active</span>
                                                </div>
                                            </div>    
                                        </div>    
                                        <div class="col-6">
                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-2">
                                                    <img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />                  
                                                </div>
                                                <div class="m-0">
                                                    @php $count = DB::table('users')->where('status','=','inactive')->count(); @endphp
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{$count}}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">Inactive</span>
                                                </div>
                                            </div>    
                                        </div>          
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="col-md-4 mb-xl-5">              
                        <div class="card card-flush h-xl-70">   
                            <div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-200px" style="background-image: url({{ asset('/images/img-75.jpg') }})" data-bs-theme="light">
                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
                                    <span class="fw-bold fs-2x mb-2">Employee</span>
                                    <div class="fs-4 text-dark">
                                        <span class="opacity-75">You have Total</span>
                                        <span class="position-relative d-inline-block">
                                            @php $count = DB::table('employees')->count(); @endphp
                                            <a href="{{ route('employees.employee.index') }}" class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}} employees</a>
                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
                                        </span>
                                    </div>    
                                </h3>
                            </div>
                            <div class="card-body mt-n20">
                                <div class="mt-n20 position-relative">
                                    <div class="row g-3 g-lg-6">
                                        <div class="col-6">
                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-2">
                                                    <img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />
                                                </div>
                                                <div class="m-0">
                                                    @php $count = DB::table('employees')->where('status','=','active')->count(); @endphp
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{$count}}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">Active</span>
                                                </div>
                                            </div>    
                                        </div>    
                                        <div class="col-6">
                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-2">
                                                    <img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />
                                                </div>
                                                <div class="m-0">
                                                    @php $count = DB::table('employees')->where('status','=','inactive')->count(); @endphp
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{$count}}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">Inactive</span>
                                                </div>
                                            </div>    
                                        </div>          
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>
                    <div class="col-md-4 mb-xl-5">              
                        <div class="card card-flush h-xl-70">   
                            <div class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-200px" style="background-image: url({{ asset('/images/img-75-dark.jpg') }})" data-bs-theme="light">
                                <h3 class="card-title align-items-start flex-column text-white pt-5">
                                    <span class="fw-bold fs-2x mb-2">Branch</span>
                                    <div class="fs-4 text-white">
                                        <span class="opacity-75">You have Total</span>
                                        <span class="position-relative d-inline-block">
                                            @php $count = DB::table('branches')->count(); @endphp
                                            <a href="{{ route('branches.branch.index') }}" class="link-white opacity-75-hover fw-bold d-block mb-1">{{$count}} branches</a>
                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
                                        </span>
                                    </div>    
                                </h3>
                            </div>
                            <div class="card-body mt-n20">
                                <div class="mt-n20 position-relative">
                                    <div class="row g-3 g-lg-6">
                                        <div class="col-6">
                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-2">
                                                    <img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />
                                                </div>
                                                <div class="m-0">
                                                    @php $count = DB::table('branches')->where('status','=','active')->count(); @endphp
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{$count}}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">Active</span>
                                                </div>
                                            </div>    
                                        </div>    
                                        <div class="col-6">
                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-6 py-5">
                                                <div class="symbol symbol-30px me-5 mb-2">
                                                <img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />              
                                                </div>
                                                <div class="m-0">
                                                    @php $count = DB::table('branches')->where('status','=','inactive')->count(); @endphp
                                                    <span class="text-gray-700 fw-bolder d-block fs-2qx lh-1 ls-n1 mb-1">{{$count}}</span>
                                                    <span class="text-gray-500 fw-semibold fs-6">Inactive</span>
                                                </div>
                                            </div>    
                                        </div>          
                                    </div>
                                </div>
                            </div>    
                        </div>
                    </div>  
                </div>
                <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
                    <div class="col-xl-6">
                        <div class="card card-flush h-md-100">
                            <div class="card-header pt-7">
                                <h3 class="card-title align-items-start flex-column">           
                                    <span class="card-label fw-bold">Employee</span>
                                </h3>
                                <div class="card-toolbar">   
                                    <a href="{{ route('employees.employee.create') }}" class="btn btn-sm text-hover-primary btn-light">Create</a>
                                </div>
                            </div>
                            <div class="card-body pt-6">
                                <div class="table-responsive">
                                    <table class="table table-row-dashed align-middle gs-0 gy-3 my-0">
                                        <thead>
                                            <tr class="fs-7 fw-bold text-gray-400 border-bottom-0">                               
                                                <th class="p-0 pb-3 min-w-175px text-start">Employee</th>
                                                <th class="p-0 pb-3 min-w-100px text-end">Branch</th>
                                                <th class="p-0 pb-3 min-w-100px text-end">Designation</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($employees as $employee)
                                            <tr>                            
                                                <td>
                                                    <div class="d-flex align-items-center"> 
                                                        <div class="symbol symbol-50px me-3">    
                                                            @if(!is_null($employee->photo))
                                                        <span class="symbol-label" style="background-image: url('{{ asset('storage/' . $employee->photo) }}');"></span>
                                                        @else
                                                        <span class="symbol-label" style="background-image:url('{{ asset('/images/userimage.jpg')}}');"></span>
                                                        @endif                         
                                                        </div>
                                                        <div class="d-flex justify-content-start flex-column">
                                                            <a href="{{route('employees.employee.show', $employee->id ) }}" class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6">{{$employee->full_name}}</a>
                                                        </div>
                                                    </div>                                
                                                </td> 
                                                <td class="text-end pe-0">
                                                    <span class="text-gray-600 fw-bold fs-6">{{ optional($employee->branch)->name }}
                                                    </span>                                
                                                </td>
                                                <td class="text-end pe-0">
                                                    <span class="text-gray-600 fw-bold fs-6">{{ optional($employee->designation)->designation_name }}
                                                    </span>                                
                                                </td>
                                            </tr>  
                                            @endforeach                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="card card-flush h-md-100">
                            <div class="card-header pt-7">
                                <h3 class="card-title align-items-start flex-column">           
                                    <span class="card-label fw-bold">Company</span>
                                </h3>
                                <div class="card-toolbar">   
                                    <a href="{{ route('users.user.create') }}" class="btn btn-sm text-hover-primary btn-light">Create</a>
                                </div>
                            </div>
                            <div class="card-body pt-6">
                                <div class="table-responsive">
                                    <table class="table table-row-dashed align-middle gs-0 gy-3 my-0">
                                        <thead>
                                            <tr class="fs-7 fw-bold text-gray-400 border-bottom-0">                               
                                                <th class="p-0 pb-3 min-w-175px text-start">Comapny Name</th>
                                                <th class="p-0 pb-3 min-w-100px text-end">Email</th>
                                                <th class="p-0 pb-3 min-w-100px text-end">Contact Person</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($users as $user)
                                            <tr>                            
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div class="d-flex justify-content-start flex-column">
                                                            <a href="{{route('users.user.show', $user->id ) }}" class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6">{{$user->name}}</a>
                                                        </div>
                                                    </div>                                
                                                </td> 
                                                <td class="text-end pe-0">
                                                    <span class="text-gray-600 fw-bold fs-6">{{ $user->email }}
                                                    </span>                                
                                                </td>
                                                <td class="text-end pe-0">
                                                    <span class="text-gray-600 fw-bold fs-6">{{ $user->contact_person_name }}
                                                    </span>                                
                                                </td>
                                            </tr>  
                                            @endforeach                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
                    <div class="col-xl-6">
                        <div class="card card-flush h-md-100">
                            <div class="card-header pt-7">
                                <h3 class="card-title align-items-start flex-column">           
                                    <span class="card-label fw-bold">Branch</span>
                                </h3>
                                <div class="card-toolbar">   
                                    <a href="{{ route('branches.branch.create') }}" class="btn btn-sm text-hover-primary btn-light">Create</a>
                                </div>
                            </div>
                            <div class="card-body pt-6">
                                <div class="table-responsive">
                                    <table class="table table-row-dashed align-middle gs-0 gy-3 my-0">
                                        <thead>
                                            <tr class="fs-7 fw-bold text-gray-400 border-bottom-0">                               
                                                <th class="p-0 pb-3 min-w-175px text-start">Branch Name</th>
                                                <th class="p-0 pb-3 min-w-100px text-end">Comapny</th>
                                                <th class="p-0 pb-3 min-w-100px text-end">status</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach($branches as $branch)
                                            <tr>                            
                                                <td>
                                                    <div class="d-flex align-items-center">
                                                        <div class="d-flex justify-content-start flex-column">
                                                            <a href="{{route('branches.branch.show', $branch->id ) }}" class="text-gray-800 fw-bold text-hover-primary mb-1 fs-6">{{$branch->name}}</a>
                                                        </div>
                                                    </div>                                
                                                </td> 
                                                <td class="text-end pe-0">
                                                    <span class="text-gray-600 fw-bold fs-6">{{ optional($branch->company)->name }}
                                                    </span>                                
                                                </td>
                                                <td class="text-end pe-0">
                                                    @if($branch->status == 'active')
                                                        <div class="badge badge-light-success fw-bold">Active</div>
                                                    @else
                                                    <div class="badge badge-light-danger fw-bold">Inactive</div>
                                                    @endif
                                                </td>
                                            </tr>  
                                            @endforeach                   
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>                
    </div>
</div>