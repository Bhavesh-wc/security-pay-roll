<div class="app-sidebar-menu overflow-hidden flex-column-fluid">
    <!--begin::Sidebar-->
    <div id="kt_app_sidebar" class="app-sidebar flex-column" data-kt-drawer="true" data-kt-drawer-name="app-sidebar" data-kt-drawer-activate="{default: true, lg: false}" data-kt-drawer-overlay="true" data-kt-drawer-width="225px" data-kt-drawer-direction="start" data-kt-drawer-toggle="#kt_app_sidebar_mobile_toggle">
        <!--begin::Logo-->
        <div class="app-sidebar-logo px-6" id="kt_app_sidebar_logo">
            <a href="{{ route('home')}}">
                <img alt="Logo" src="{{asset('storage').'/'.$Config->logo}}" class="h-25px app-sidebar-logo-default" />
                <img alt="Logo" src="{{asset('storage').'/'.$Config->logo}}" class="h-20px app-sidebar-logo-minimize" />
            </a>
            <div id="kt_app_sidebar_toggle" class="app-sidebar-toggle btn btn-icon btn-shadow btn-sm btn-color-muted btn-active-color-primary body-bg h-30px w-30px position-absolute top-50 start-100 translate-middle rotate" data-kt-toggle="true" data-kt-toggle-state="active" data-kt-toggle-target="body" data-kt-toggle-name="app-sidebar-minimize">
                <span class="svg-icon svg-icon-2 rotate-180">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.5" d="M14.2657 11.4343L18.45 7.25C18.8642 6.83579 18.8642 6.16421 18.45 5.75C18.0358 5.33579 17.3642 5.33579 16.95 5.75L11.4071 11.2929C11.0166 11.6834 11.0166 12.3166 11.4071 12.7071L16.95 18.25C17.3642 18.6642 18.0358 18.6642 18.45 18.25C18.8642 17.8358 18.8642 17.1642 18.45 16.75L14.2657 12.5657C13.9533 12.2533 13.9533 11.7467 14.2657 11.4343Z" fill="currentColor" />
                        <path d="M8.2657 11.4343L12.45 7.25C12.8642 6.83579 12.8642 6.16421 12.45 5.75C12.0358 5.33579 11.3642 5.33579 10.95 5.75L5.40712 11.2929C5.01659 11.6834 5.01659 12.3166 5.40712 12.7071L10.95 18.25C11.3642 18.6642 12.0358 18.6642 12.45 18.25C12.8642 17.8358 12.8642 17.1642 12.45 16.75L8.2657 12.5657C7.95328 12.2533 7.95328 11.7467 8.2657 11.4343Z" fill="currentColor" />
                    </svg>
                </span>
            </div>
        </div>
        <div class="app-sidebar-menu overflow-hidden flex-column-fluid">
            <div id="kt_app_sidebar_menu_wrapper" class="app-sidebar-wrapper hover-scroll-overlay-y my-5" data-kt-scroll="true" data-kt-scroll-activate="true" data-kt-scroll-height="auto" data-kt-scroll-dependencies="#kt_app_sidebar_logo, #kt_app_sidebar_footer" data-kt-scroll-wrappers="#kt_app_sidebar_menu" data-kt-scroll-offset="5px" data-kt-scroll-save-state="true">
                <div class="menu menu-column menu-rounded menu-sub-indention px-3" id="#kt_app_sidebar_menu" data-kt-menu="true" data-kt-menu-expand="false">
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*home*') ? 'active' : ''}}" href="{{ route('home')}}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="{{ asset('/images/sidebar/dashboardw.svg') }}" class="w-20px h-20px" alt="" />
                                </span>
                            </span>
                            <span class="menu-title">Dashboard</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*users*') ? 'active' : ''}}" href="{{ route('users.user.index')}}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="{{ asset('/images/sidebar/company-w.png') }}" class="w-20px h-20px" alt="" />
                                </span>
                            </span>
                            <span class="menu-title">Company</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*/branches*') ? 'active' : ''}}" href="{{ route('branches.branch.index')}}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="{{ asset('/images/sidebar/branch-w.png') }}" class="w-20px h-20px" alt="" />
                                </span>
                            </span>
                            <span class="menu-title">Branch</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*employees*') ? 'active' : ''}}" href="{{ route('employees.employee.index')}}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="{{ asset('/images/sidebar/employee-w.png') }}" class="w-20px h-20px" alt="" />
                                </span>
                            </span>
                            <span class="menu-title">Employee</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <!--begin:Menu link-->
                        <a class="menu-link {{ request()->is('*advance_salaries*') ? 'active' : ''}}" href="{{ route('advance_salaries.advance_salary.index')}}">
                        <span class="menu-icon">
                        <span class="svg-icon svg-icon-2">
                            <img src="{{ asset('/images/sidebar/emp-salary-w.png') }}" class="w-20px h-20px" alt="" />
                        </span>
                        </span>
                            <span class="menu-title">Advance Salary</span>
                        </a>
                        <!--end:Menu link-->
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*attendances*') ? 'active' : ''}}" href="{{ route('attendances.attendance.index')}}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="{{ asset('/images/sidebar/attendance-w.png') }}" class="w-20px h-20px" alt="" />
                                </span>
                            </span>
                            <span class="menu-title">Attendance</span>
                        </a>
                    </div>
                    <!-- <div class="menu-item">
                        <a class="menu-link" href="#">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                        <rect x="2" y="2" width="9" height="9" rx="2" fill="currentColor" />
                                        <rect opacity="0.3" x="13" y="2" width="9" height="9" rx="2" fill="currentColor" />
                                        <rect opacity="0.3" x="13" y="13" width="9" height="9" rx="2" fill="currentColor" />
                                        <rect opacity="0.3" x="2" y="13" width="9" height="9" rx="2" fill="currentColor" />
                                    </svg>
                                </span>
                            </span>
                            <span class="menu-title">Employee Transfer</span>
                        </a>
                    </div> -->
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*/salaries*') ? 'active' : ''}}"  href="{{ route('salaries.salary.index')}}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="{{ asset('/images/sidebar/emp-salary-w.png') }}" class="w-20px h-20px" alt="" />
                                </span>
                            </span>

                            <span class="menu-title">Employee Salary</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*credit*') ? 'active' : ''}}" href="{{ route('credit_salaries.credit_salary.index') }}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="{{ asset('/images/sidebar/credit-salary.png') }}" class="w-20px h-20px" alt="" />
                                </span>
                            </span>
                            <span class="menu-title">Credit Salary</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*branch_bills*') ? 'active' : ''}}" href="{{route('branch_bills.branch_bill.index')}}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="{{ asset('/images/sidebar/branch-bill-w.png') }}" class="w-20px h-20px" alt="" />
                                </span>
                            </span>
                            <span class="menu-title">Branch Bill</span>
                        </a>
                    </div>
                    <div class="menu-item">
                        <a class="menu-link {{ request()->is('*stock_registers*') ? 'active' : ''}}" href="{{route('stock_registers.stock_register.index')}}">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="{{ asset('/images/sidebar/stock_register-w.png') }}" class="w-20px h-20px" alt="" />
                                </span>
                            </span>
                            <span class="menu-title">Stock Register</span>
                        </a>
                    </div>
                    <div class="menu-item pt-5">
                        <div class="menu-content">
                            <span class="menu-heading fw-bold text-uppercase fs-7">Master</span>
                        </div>
                    </div>
                    <div data-kt-menu-trigger="click" class="menu-item menu-accordion {{ request()->is('*banks*') || request()->is('*bank_branches*') || request()->is('*departments*') || request()->is('*designations*') || request()->is('*qualifications*') || request()->is('*mother_tongues*') || request()->is('*grades*') || request()->is('*countries*') || request()->is('*states*') || request()->is('*districts*') || request()->is('*cities*') || request()->is('*pincodes*') || request()->is('*stock_categories*') || request()->is('*tax_managers*') || request()->is('*advance_salaries*') || request()->is('*over_times*') || request()->is('*damages*') || request()->is('*masters*') ||request()->is('*fines*') ? 'hover show' : ''}}">
                        <!--begin:Menu link-->
                        <span class="menu-link">
                            <span class="menu-icon">
                                <span class="svg-icon svg-icon-2">
                                    <img src="{{ asset('/images/sidebar/master.png') }}" class="w-20px h-20px" alt="" />
                                </span>
                            </span>
                            <span class="menu-title">Master</span>
                            <span class="menu-arrow"></span>
                        </span>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*masters*') ? 'active' : ''}}" href="{{ route('masters.master.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Masters</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*banks*') ? 'active' : ''}}" href="{{ route('banks.bank.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Banks</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*company*') ? 'active' : ''}}" href="{{ route('company_banks.company_bank.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Company Bank</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>

                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*bank_branches*') ? 'active' : ''}}" href="{{ route('bank_branches.bank_branch.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Bank Branch</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*departments*') ? 'active' : ''}}" href="{{ route('departments.department.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Department</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*designations*') ? 'active' : ''}}" href="{{ route('designations.designation.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Designation</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*qualifications*') ? 'active' : ''}}" href="{{ route('qualifications.qualification.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Qualification</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*mother_tongues*') ? 'active' : ''}}" href="{{ route('mother_tongues.mother_tongue.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Mother Tongue</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*grades*') ? 'active' : ''}}" href="{{ route('grades.grade.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Grade</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*countries*') ? 'active' : ''}}" href="{{ route('countries.country.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Country</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*states*') ? 'active' : ''}}" href="{{ route('states.state.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">State</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*districts*') ? 'active' : ''}}" href="{{ route('districts.district.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">District</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*cities*') ? 'active' : ''}}" href="{{ route('cities.city.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">City</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*pincodes*') ? 'active' : ''}}" href="{{ route('pincodes.pincode.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Pincode</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*stock_categories*') ? 'active' : ''}}" href="{{ route('stock_categories.stock_category.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Stock Category</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*tax_managers*') ? 'active' : ''}}" href="{{ route('tax_managers.tax_manager.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Tax Manager</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                           
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*over_times*') ? 'active' : ''}}" href="{{ route('over_times.over_time.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Overtime</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*damages*') ? 'active' : ''}}" href="{{ route('damages.damage.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Damage & Loss</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                        <div class="menu-sub menu-sub-accordion">
                            <!--begin:Menu item-->
                            <div class="menu-item">
                                <!--begin:Menu link-->
                                <a class="menu-link {{ request()->is('*fines*') ? 'active' : ''}}" href="{{ route('fines.fine.index')}}">
                                    <span class="menu-bullet">
                                        <span class="bullet bullet-dot"></span>
                                    </span>
                                    <span class="menu-title">Fine</span>
                                </a>
                                <!--end:Menu link-->
                            </div>
                        </div>
                    </div>
                </div>
                <!--end::Menu-->
            </div>
            <!--end::Menu wrapper-->
        </div>
        <div class="app-sidebar-footer side-bottom1 flex-column-auto pt-2 px-6" id="kt_app_sidebar_footer">
            <a href="{{ route('report.index')}}" class="{{ request()->is('*report*') || request()->is('*import*') || request()->is('*attendance_report*') || request()->is('*import_salary_excel*') || request()->is('*export_salary_excel*') || request()->is('*branch_wise_salary*') || request()->is('*export*') || request()->is('*delete_salary*') || request()->is('*delete_attendance*') || request()->is('*employee_month_salary*') || request()->is('*wage_slip*') || request()->is('*month_to_month_wage_slip*') || request()->is('*appliction_form*') || request()->is('*goverment_form*') || request()->is('*icard*') || request()->is('*branchwise_profesional_tax*') || request()->is('*all_professional_tax_list*') || request()->is('*branch_wise_pf_statement*') || request()->is('*branchwise_pf_excel*') || request()->is('*all_pf_excel*') || request()->is('*identity_card_register*') || request()->is('*employed_by_contractor_register*') || request()->is('*register_of_overtime*') || request()->is('*walfers*') || request()->is('*upload_pf_code*') || request()->is('*register_of_walfare_report*') || request()->is('*police_verification_register*') || request()->is('*police_verification_report_excel*') || request()->is('*police_verification_for_leave_register*') || request()->is('*bonus_report*') || request()->is('*esic*') || request()->is('*bonus_report_monthly*') || request()->is('*stock*') || request()->is('*male_female_salary*') ? 'active' : ''}} btn btn-flex flex-center btn-custom btn-primary overflow-hidden text-nowrap px-0 h-40px w-100" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss-="click">
                <span class="btn-label">Reports</span>
                <span class="svg-icon btn-icon svg-icon-2 m-0">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z" fill="currentColor" />
                        <rect x="7" y="17" width="6" height="2" rx="1" fill="currentColor" />
                        <rect x="7" y="12" width="10" height="2" rx="1" fill="currentColor" />
                        <rect x="7" y="7" width="6" height="2" rx="1" fill="currentColor" />
                        <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="currentColor" />
                    </svg>
                </span>
            </a>
        </div>
        <div class="app-sidebar-footer side-bottom2 flex-column-auto pt-2 pb-6 px-6" id="kt_app_sidebar_footer">
            <a href="{{ route('system_configurations.system_configuration.edit', '1') }}" class="{{ request()->is('*system_configurations*') ? 'active' : ''}} btn btn-flex flex-center btn-custom btn-primary overflow-hidden text-nowrap px-0 h-40px w-100" data-bs-toggle="tooltip" data-bs-trigger="hover" data-bs-dismiss-="click">
                <span class="btn-label">System Configuration</span>
                <span class="svg-icon btn-icon svg-icon-2 m-0">
                    <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                        <path opacity="0.3" d="M19 22H5C4.4 22 4 21.6 4 21V3C4 2.4 4.4 2 5 2H14L20 8V21C20 21.6 19.6 22 19 22ZM12.5 18C12.5 17.4 12.6 17.5 12 17.5H8.5C7.9 17.5 8 17.4 8 18C8 18.6 7.9 18.5 8.5 18.5L12 18C12.6 18 12.5 18.6 12.5 18ZM16.5 13C16.5 12.4 16.6 12.5 16 12.5H8.5C7.9 12.5 8 12.4 8 13C8 13.6 7.9 13.5 8.5 13.5H15.5C16.1 13.5 16.5 13.6 16.5 13ZM12.5 8C12.5 7.4 12.6 7.5 12 7.5H8C7.4 7.5 7.5 7.4 7.5 8C7.5 8.6 7.4 8.5 8 8.5H12C12.6 8.5 12.5 8.6 12.5 8Z" fill="currentColor" />
                        <rect x="7" y="17" width="6" height="2" rx="1" fill="currentColor" />
                        <rect x="7" y="12" width="10" height="2" rx="1" fill="currentColor" />
                        <rect x="7" y="7" width="6" height="2" rx="1" fill="currentColor" />
                        <path d="M15 8H20L14 2V7C14 7.6 14.4 8 15 8Z" fill="currentColor" />
                    </svg>
                </span>
            </a>
        </div>
    </div>
</div>