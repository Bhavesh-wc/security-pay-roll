@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Stock Register' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('stock_registers.stock_register.destroy', $stockRegister->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('stock_registers.stock_register.index') }}" class="btn btn-primary" title="Show All Stock Register">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('stock_registers.stock_register.create') }}" class="btn btn-success" title="Create New Stock Register">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('stock_registers.stock_register.edit', $stockRegister->id ) }}" class="btn btn-primary" title="Edit Stock Register">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Stock Register" onclick="return confirm(&quot;Click Ok to delete Stock Register.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Stock Category</dt>
            <dd>{{ optional($stockRegister->stockCategory)->stock_category_name }}</dd>
            <dt>Stock Quantity</dt>
            <dd>{{ $stockRegister->stock_quantity }}</dd>
            <dt>Stock In Date</dt>
            <dd>{{ $stockRegister->stock_in_date }}</dd>

        </dl>

    </div>
</div>

@endsection