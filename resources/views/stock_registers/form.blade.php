<div class="mb-0">
    <div class="card card-flush py-4"> 
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
        <div class="form-group {{ $errors->has('stock_category_id') ? 'has-error' : '' }}">
            <label for="stock_category_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Stock Category</label>
            <div class="col-md-12">
                <select class="form-select" aria-label="Select a stock category" data-control="select2" data-placeholder="Select stock category" id="stock_category_id" name="stock_category_id" required="true">
                        <option value="" style="display: none;" {{ old('stock_category_id', optional($stockRegister)->stock_category_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select stock category</option>
                    @foreach ($stockCategories as $key => $stockCategory)
                        <option value="{{ $key }}" {{ old('stock_category_id', optional($stockRegister)->stock_category_id) == $key ? 'selected' : '' }}>
                            {{ $stockCategory }}
                        </option>
                    @endforeach
                </select>
                
                {!! $errors->first('stock_category_id', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
   </div>
   <div class="col-lg-6">
        <div class="form-group {{ $errors->has('stock_quantity') ? 'has-error' : '' }}">
            <label for="stock_quantity" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Stock Quantity</label>
            <div class="col-md-12">
                <input class="form-control" name="stock_quantity" type="text" id="stock_quantity" value="{{ old('stock_quantity', optional($stockRegister)->stock_quantity) }}" required="true" placeholder="Enter stock quantity here...">
                {!! $errors->first('stock_quantity', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
   </div>
   <div class="col-lg-6 pt-5">
        <div class="form-group {{ $errors->has('stock_in_date') ? 'has-error' : '' }}">
            <label for="stock_in_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Stock In Date</label>
            <div class="col-md-12">
                <input class="form-control" name="stock_in_date" type="date" id="stock_in_date" value="{{ old('stock_in_date', optional($stockRegister)->stock_in_date) }}" required="true" placeholder="Enter stock in date here...">
                {!! $errors->first('stock_in_date', '<p class="help-block">:message</p>') !!}
            </div>
        </div>
  </div>
      </div>
        </div>
    </div>
</div>