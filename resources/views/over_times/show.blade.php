@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Over Time' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('over_times.over_time.destroy', $overTime->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('over_times.over_time.index') }}" class="btn btn-primary" title="Show All Over Time">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('over_times.over_time.create') }}" class="btn btn-success" title="Create New Over Time">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('over_times.over_time.edit', $overTime->id ) }}" class="btn btn-primary" title="Edit Over Time">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Over Time" onclick="return confirm(&quot;Click Ok to delete Over Time.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Monthyear</dt>
            <dd>{{ $overTime->monthyear }}</dd>
            <dt>Branch</dt>
            <dd>{{ optional($overTime->branch)->name }}</dd>
            <dt>Employee</dt>
            <dd>{{ optional($overTime->employee)->full_name }}</dd>
            <dt>Total Overtime Amount</dt>
            <dd>{{ $overTime->total_overtime_amount }}</dd>
            <dt>Total Overtime Days</dt>
            <dd>{{ $overTime->total_overtime_days }}</dd>
            <dt>Date Overtime Paid</dt>
            <dd>{{ $overTime->date_overtime_paid }}</dd>
            <dt>Remarks</dt>
            <dd>{{ $overTime->remarks }}</dd>

        </dl>

    </div>
</div>

@endsection