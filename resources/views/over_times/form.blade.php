<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            @if(empty($overTime->id))
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" value="{{ old('monthyear', optional($overTime)->monthyear) }}" required="true" placeholder="Enter monthyear here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                    <select id="branch_dropdown" name="branch_id" data-control="select2" class="form-control @error('branch_id') is-invalid @enderror form-select mb-2 mb-5" required>
                        <option value="" selected disabled class="">Select branch</option>
                        @foreach ($branches as $data)
                        <option value="{{ trim($data->id) }}">{{ $data->name}}</option>
                        @endforeach
                    </select>
                    <span class="text-danger">
                        @error('branch_id')
                        *{{ $message }}
                        @enderror
                    </span>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee</label>
                    <select name="employee_id[]" id="employee_dropdown" class="form-select mb-2" data-control="select2" data-placeholder="Select employee" data-allow-clear="true" multiple="multiple" required>
                    </select>
                </div>
            </div>
            @else
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear"class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="text" id="monthyear" value="{{ $overTime->monthyear }}" placeholder="Enter monthyear here..." readonly>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                    <input class="form-control mb-2" name="branch_id" type="text" id="monthyear" value="{{ $overTime->branch_id }}" hidden>
                    <input class="form-control mb-2" name="branch" type="text" id="monthyear" value="{{ old('branch_id', optional($overTime)->branch->name) }}" readonly/>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee</label>
                    <input class="form-control mb-2" name="employee_id" type="text" id="employee_id" value="{{ $overTime->employee_id }}" hidden>
                    <input class="form-control mb-2" name="employee" type="text" id="employee_id" value="{{ old('employee_id', optional($overTime)->employee->full_name) }}" readonly>
                </div>
            </div>
            @endif
            <div class="row gx-10 mb-5">
                <div id="kt_docs_repeater_advanced">
                    <!--begin::Form group-->
                    <div class="form-group">
                        <div data-repeater-list="overtime">
                            @if(old('overtime'))
                                @foreach(old('overtime') as $index => $item)
                                <div data-repeater-item class="kt-repeater-row">
                                    <div class="form-group row mb-5">
                                        <div class="col-md-5">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Over Time Date:</label>
                                            <input class="form-control" name="overtime[{{ $index }}][over_time_date]" value="{{ $item['over_time_date'] }}" data-kt-repeater="datepicker" placeholder="Enter over time date here..." />
                                        </div>
                                        <div class="col-md-5">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Over Time Amount:</label>
                                            <input class="form-control"name="overtime[{{ $index }}][over_time_amount]" value="{{ $item['over_time_amount'] }}" type="number" placeholder="Enter over time amount here..." />
                                        </div>
                                        <div class="col-md-2">
                                            <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-light-danger mt-3 mt-md-9 kt-repeater-delete">
                                                <i class="la la-trash-o fs-3"></i>Delete
                                            </a>
                                        </div>
                                    </div>
                                </div>  
                                @endforeach
                            @elseif(isset($overTime))
                                @foreach($overTime->overTimeDays as $index => $item)
                                <div data-repeater-item class="kt-repeater-row">
                                    <div class="form-group row mb-5">
                                        <div class="col-md-5">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Over Time Date:</label>
                                            <input class="form-control" name="overtime[{{ $index }}][over_time_date]" value="{{ $item->over_time_date }}" data-kt-repeater="datepicker" placeholder="Enter over time date here..." />
                                        </div>
                                        <div class="col-md-5">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Over Time Amount:</label>
                                            <input class="form-control" name="overtime[{{ $index }}][over_time_amount]" value="{{ $item->over_time_amount }}" type="number" placeholder="Enter over time amount here..." />
                                        </div>
                                        <div class="col-md-2 text-end">
                                            <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-danger mt-3 mt-md-9 kt-repeater-delete">
                                                <i class="la la-trash-o fs-3"></i>Delete
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @else
                                <div data-repeater-item class="kt-repeater-row">
                                    <div class="form-group row mb-5">
                                        <div class="col-md-5">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Over Time Date:</label>
                                            <input class="form-control" name="overtime[0][over_time_date]" data-kt-repeater="datepicker" placeholder="Enter over time date here..." />
                                        </div>
                                        <div class="col-md-5">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Over Time Amount:</label>
                                            <input class="form-control" name="overtime[0][over_time_amount]" type="number" placeholder="Enter over time amount here..." />
                                        </div>
                                        <div class="col-md-2 text-end">
                                            <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-3 mt-md-9 kt-repeater-delete">
                                                <i class="la la-trash-o fs-3"></i>Delete
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            @endif
                        </div>
                    </div>
                    <!--end::Form group-->

                    <!--begin::Form group-->
                    <div class="form-group">
                        <a href="javascript:;" data-repeater-create class="btn btn-light-primary">
                            <i class="la la-plus"></i>Add
                        </a>
                    </div>
                    <!--end::Form group-->
                </div>
            </div>

            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('date_overtime_paid') ? 'has-error' : '' }}">
                        <label for="date_overtime_paid" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Date Overtime Paid</label>
                        <div class="col-md-12">
                            <input class="form-control" name="date_overtime_paid" type="date" id="date_overtime_paid" value="{{ old('date_overtime_paid', optional($overTime)->date_overtime_paid) }}" required="true" placeholder="Enter date overtime paid here...">
                            {!! $errors->first('date_overtime_paid', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('remarks') ? 'has-error' : '' }}">
                        <label for="remarks" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Remarks</label>
                        <div class="col-md-12">
                            <textarea class="form-control" name="remarks" cols="1" rows="1" id="remarks" required="true" placeholder="Enter remarks here...">{{ old('remarks', optional($overTime)->remarks) }}</textarea>
                            {!! $errors->first('remarks', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
<script>
    $(document).ready(function () {
    $("#branch_dropdown").on("change", function () {
        var idBranch = this.value;
        $("#employee_dropdown").html("");
        $.ajax({
            url: "{{url('admin/fetch_employee')}}",
            type: "POST",
            data: {
                branch_id: idBranch,
                _token: "{{csrf_token()}}",
            },
            dataType: "json",
            success: function (result) {
                $.each(result.Employees, function (key, value) {
                    $("#employee_dropdown").append(
                        '<option value="' +
                            value.id +
                            '">' +
                            value.full_name +
                            "</option>"
                    );
                });
            },
        });
    });
});
</script>