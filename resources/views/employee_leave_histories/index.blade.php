@extends('layouts.app')

@section('content')

    @if(Session::has('success_message'))
        <div class="alert alert-success">
            <span class="glyphicon glyphicon-ok"></span>
            {!! session('success_message') !!}

            <button type="button" class="close" data-dismiss="alert" aria-label="close">
                <span aria-hidden="true">&times;</span>
            </button>

        </div>
    @endif

    <div class="panel panel-default">

        <div class="panel-heading clearfix">

            <div class="pull-left">
                <h4 class="mt-5 mb-5">Employee Leave Histories</h4>
            </div>

            <div class="btn-group btn-group-sm pull-right" role="group">
                <a href="{{ route('employee_leave_histories.employee_leave_history.create') }}" class="btn btn-success" title="Create New Employee Leave History">
                    <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                </a>
            </div>

        </div>
        
        @if(count($employeeLeaveHistories) == 0)
            <div class="panel-body text-center">
                <h4>No Employee Leave Histories Available.</h4>
            </div>
        @else
        <div class="panel-body panel-body-with-table">
            <div class="table-responsive">

                <table class="table table-striped ">
                    <thead>
                        <tr>
                            <th>Employee</th>
                            <th>Company</th>
                            <th>Join Date</th>
                            <th>Leave Date</th>

                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($employeeLeaveHistories as $employeeLeaveHistory)
                        <tr>
                            <td>{{ optional($employeeLeaveHistory->employee)->full_name }}</td>
                            <td>{{ optional($employeeLeaveHistory->company)->username }}</td>
                            <td>{{ $employeeLeaveHistory->join_date }}</td>
                            <td>{{ $employeeLeaveHistory->leave_date }}</td>

                            <td>

                                <form method="POST" action="{!! route('employee_leave_histories.employee_leave_history.destroy', $employeeLeaveHistory->id) !!}" accept-charset="UTF-8">
                                <input name="_method" value="DELETE" type="hidden">
                                {{ csrf_field() }}

                                    <div class="btn-group btn-group-xs pull-right" role="group">
                                        <a href="{{ route('employee_leave_histories.employee_leave_history.show', $employeeLeaveHistory->id ) }}" class="btn btn-info" title="Show Employee Leave History">
                                            <span class="glyphicon glyphicon-open" aria-hidden="true"></span>
                                        </a>
                                        <a href="{{ route('employee_leave_histories.employee_leave_history.edit', $employeeLeaveHistory->id ) }}" class="btn btn-primary" title="Edit Employee Leave History">
                                            <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                                        </a>

                                        <button type="submit" class="btn btn-danger" title="Delete Employee Leave History" onclick="return confirm(&quot;Click Ok to delete Employee Leave History.&quot;)">
                                            <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                                        </button>
                                    </div>

                                </form>
                                
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

            </div>
        </div>

        <div class="panel-footer">
            {!! $employeeLeaveHistories->render() !!}
        </div>
        
        @endif
    
    </div>
@endsection