
<div class="form-group {{ $errors->has('employee_id') ? 'has-error' : '' }}">
    <label for="employee_id" class="col-md-2 control-label">Employee</label>
    <div class="col-md-10">
        <select class="form-control" id="employee_id" name="employee_id" required="true">
        	    <option value="" style="display: none;" {{ old('employee_id', optional($employeeLeaveHistory)->employee_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select employee</option>
        	@foreach ($employees as $key => $employee)
			    <option value="{{ $key }}" {{ old('employee_id', optional($employeeLeaveHistory)->employee_id) == $key ? 'selected' : '' }}>
			    	{{ $employee }}
			    </option>
			@endforeach
        </select>
        
        {!! $errors->first('employee_id', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('join_date') ? 'has-error' : '' }}">
    <label for="join_date" class="col-md-2 control-label">Join Date</label>
    <div class="col-md-10">
        <input class="form-control" name="join_date" type="date" id="join_date" value="{{ old('join_date', optional($employeeLeaveHistory)->join_date) }}" required="true" placeholder="Enter join date here...">
        {!! $errors->first('join_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>

<div class="form-group {{ $errors->has('leave_date') ? 'has-error' : '' }}">
    <label for="leave_date" class="col-md-2 control-label">Leave Date</label>
    <div class="col-md-10">
        <input class="form-control" name="leave_date" type="date" id="leave_date" value="{{ old('leave_date', optional($employeeLeaveHistory)->leave_date) }}" placeholder="Enter leave date here...">
        {!! $errors->first('leave_date', '<p class="help-block">:message</p>') !!}
    </div>
</div>

