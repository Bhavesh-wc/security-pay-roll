@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Employee Leave History' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('employee_leave_histories.employee_leave_history.destroy', $employeeLeaveHistory->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('employee_leave_histories.employee_leave_history.index') }}" class="btn btn-primary" title="Show All Employee Leave History">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('employee_leave_histories.employee_leave_history.create') }}" class="btn btn-success" title="Create New Employee Leave History">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('employee_leave_histories.employee_leave_history.edit', $employeeLeaveHistory->id ) }}" class="btn btn-primary" title="Edit Employee Leave History">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Employee Leave History" onclick="return confirm(&quot;Click Ok to delete Employee Leave History.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Employee</dt>
            <dd>{{ optional($employeeLeaveHistory->employee)->full_name }}</dd>
            <dt>Join Date</dt>
            <dd>{{ $employeeLeaveHistory->join_date }}</dd>
            <dt>Leave Date</dt>
            <dd>{{ $employeeLeaveHistory->leave_date }}</dd>

        </dl>

    </div>
</div>

@endsection