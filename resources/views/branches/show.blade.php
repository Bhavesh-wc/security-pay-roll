@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_toolbar" class="app-toolbar  py-3 py-lg-6 ">
            <div id="kt_app_toolbar_container" class="app-container  container-xxl d-flex flex-stack ">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3 ">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">View Branch Details</h1>
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('branches.branch.index') }}" class="text-muted text-hover-primary">List</a>
                        </li>
                    </ul>
                </div>
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <a href="{{ route('branches.branch.index') }}" class="btn btn-sm fw-bold btn-primary">List</a>
                </div>
            </div>
        </div>
        <div id="kt_app_content" class="app-content  flex-column-fluid ">
            <div id="kt_app_content_container" class="app-container  container-xxl ">             
                <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
                    <div class="card-header cursor-pointer">
                        <div class="card-title m-0">
                            <h3 class="fw-bold m-0">Branch Details</h3>
                        </div>
                        <a href="{{ route('branches.branch.edit', $branch->id ) }}" class="btn btn-sm btn-primary align-self-center">Edit Branch Details</a>   
                    </div>
                    <div class="card-body p-9">
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Name</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800">{{ $branch->name }}</span>    
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Status</label>
                                <div>
                                    @if($branch->status == "active")
                                    <span class="fw-bold fs-6 text-gray-800 me-2">Active</span>
                                    @else
                                    <span class="fw-bold fs-6 text-gray-800 me-2">In-Active</span>
                                    @endif
                                </div>
                            </div>     
                        </div>
                        @foreach ($branchDesignation as $branchDesignation)
                            <div class="row mb-7">
                                <div class="col-6">
                                    <label class="col-lg-4 fw-semibold text-muted mb-2">Designation</label>
                                    <div>                    
                                        <span class="fw-bold fs-6 text-gray-800">{{ $branchDesignation->designation->designation_name }}</span>    
                                    </div>
                                </div>
                                <div class="col-6">
                                    <label class="col-lg-4 fw-semibold text-muted mb-2">Basic / Fix</label>
                                    <div> 
                                    @if($branchDesignation->basic_fix == "basic")                   
                                        <span class="fw-bold fs-6 text-gray-800">Basic</span>  
                                    @else($branchDesignation->basic_fix == "fix")  
                                        <span class="fw-bold fs-6 text-gray-800">Fix</span> 
                                    @endif
                                    </div>
                                </div>
                            </div>
                            <div class="row mb-7">
                                @if($branchDesignation->basic_fix == 'basic')
                                <div class="col-3">
                                    <label class="col-lg-4 fw-semibold text-muted mb-2">Basic + DA</label>
                                    <div>                    
                                        <span class="fw-bold fs-6 text-gray-800">{{ $branchDesignation->basic_da }}</span>    
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label class="col-lg-4 fw-semibold text-muted mb-2">Extra Allowance</label>
                                    <div>                    
                                        <span class="fw-bold fs-6 text-gray-800">{{ $branchDesignation->extra_allowance }}</span>    
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label class="col-lg-4 fw-semibold text-muted mb-2">HRA</label>
                                    <div>                    
                                        <span class="fw-bold fs-6 text-gray-800">{{ $branchDesignation->hra }}</span>    
                                    </div>
                                </div>
                                <div class="col-3">
                                    <label class="col-lg-4 fw-semibold text-muted mb-2">Bonus</label>
                                    <div>                    
                                        <span class="fw-bold fs-6 text-gray-800">{{ $branchDesignation->bonus }}</span>    
                                    </div>
                                </div>
                                @else($branchDesignation->basic_fix == 'fix')
                                <div class="col-6">
                                    <label class="col-lg-4 fw-semibold text-muted mb-2">Fix Salary</label>
                                    <div>                    
                                        <span class="fw-bold fs-6 text-gray-800">{{ $branchDesignation->fix_salary }}</span>    
                                    </div>
                                </div>
                                @endif
                            </div>
                        @endforeach                
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Over Time Amount</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $branch->over_time_amount }}</span>
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Monthyear</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $branch->monthyear ? date('M-Y', strtotime($branch->monthyear)): '' ; }}</span>
                                </div>
                            </div>
                        </div>
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Month</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $branch->month }}</span>
                                </div>
                            </div>
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Year</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $branch->year }}</span>
                                </div>
                            </div>
                        </div>
                        @foreach ($branchWorkOrder as $branchWorkOrder)
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Work Start Date</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $branchWorkOrder->work_start_date ? date('d-m-Y', strtotime($branchWorkOrder->work_start_date)): '' ; }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Work End Date</label>
                                <div>
                                    <span class="fw-bold fs-6 text-gray-800 me-2">{{ $branchWorkOrder->work_expire_date ? date('d-m-Y', strtotime($branchWorkOrder->work_expire_date)): '' ; }}</span>
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Work Order</label>
                                <div>
                                    @if (isset($branchWorkOrder->work_order) && !empty($branchWorkOrder->work_order))
                                    <img src="{{asset('storage').'/'.$branchWorkOrder->work_order}}" height="100px">
                                    @endif
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="row mb-7">
                            <div class="col-6">
                                <label class="col-lg-4 fw-semibold text-muted mb-2">Address</label>
                                <div>                    
                                    <span class="fw-bold fs-6 text-gray-800">{{ $branch->address }}</span>    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @if(!$branchDesignationHistory->isEmpty())
        <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
            <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
                <div class="d-flex flex-column flex-column-fluid">
                    <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                        <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                            <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                                <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Branch Designation History</h1>
                            </div>
                        </div>
                    </div>
                    <div id="kt_app_content" class="app-content flex-column-fluid">
                        <div id="kt_app_content_container" class="app-container container-xxl">
                            @if(Session::has('success_message'))
                                <div class="alert alert-success alert-dismissible fade show" role="alert">
                                    <strong>{!! \Session::get('success_message') !!}</strong>
                                    <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <div class="card">
                                <div class="card-header border-0 pt-6">
                                    <div class="card-title">
                                        <div class="d-flex align-items-center position-relative my-1">
                                            <span class="svg-icon svg-icon-1 position-absolute ms-6">
                                                <svg width="24" height="24" viewBox="0 0 24 24" fill="none" xmlns="http://www.w3.org/2000/svg">
                                                    <rect opacity="0.5" x="17.0365" y="15.1223" width="8.15546" height="2" rx="1" transform="rotate(45 17.0365 15.1223)" fill="currentColor" />
                                                    <path d="M11 19C6.55556 19 3 15.4444 3 11C3 6.55556 6.55556 3 11 3C15.4444 3 19 6.55556 19 11C19 15.4444 15.4444 19 11 19ZM11 5C7.53333 5 5 7.53333 5 11C5 14.4667 7.53333 17 11 17C14.4667 17 17 14.4667 17 11C17 7.53333 14.4667 5 11 5Z" fill="currentColor" />
                                                </svg>
                                            </span>
                                            <input type="text" data-kt-user-table-filter="search" class="form-control form-control-solid w-350px ps-14" placeholder="Search Branch Designation History" />
                                        </div>
                                    </div>
                                    <div class="card-toolbar">
                                        <div class="d-flex justify-content-end align-items-center d-none" data-kt-user-table-toolbar="selected">
                                            <div class="fw-bold me-5">
                                            <span class="me-2" data-kt-user-table-select="selected_count"></span>Selected</div>
                                            <button type="button" class="btn btn-danger" data-kt-user-table-select="delete_selected">Delete Selected</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body py-4">
                                    <table class="table align-middle table-row-dashed fs-6 gy-5" id="kt_table_users">
                                        <thead>
                                            <tr class="text-start text-muted fw-bold fs-7 text-uppercase gs-0">
                                                <th class="min-w-125px">designation</th>
                                                <th class="min-w-125px">Basic / Fix</th>
                                                <th class="min-w-125px">Basic + Da</th>
                                                <th class="min-w-125px">Extra Allowance</th>
                                                <th class="min-w-125px">HRA</th>
                                                <th class="min-w-125px">Bonus</th>
                                                <th class="min-w-125px">Fix Salary</th>
                                                <th class="min-w-125px">Month Year</th>
                                            </tr>
                                        </thead>
                                        <tbody class="text-gray-600 fw-semibold">
                                            <?php $i = 1; ?>
                                            @foreach($branchDesignationHistory as $branchDesignationHistory)
                                            <tr>
                                                <td>{{ $branchDesignationHistory->designation->designation_name }}</td>
                                                <td>{{ $branchDesignationHistory->basic_fix }}</td>
                                                <td>{{ $branchDesignationHistory->basic_da }}</td>
                                                <td>{{ $branchDesignationHistory->extra_allowance }}</td>
                                                <td>{{ $branchDesignationHistory->hra }}</td>
                                                <td>{{ $branchDesignationHistory->bonus }}</td>
                                                <td>{{ $branchDesignationHistory->fix_salary }}</td>
                                                <td>{{ date('m-Y', strtotime($branchDesignationHistory->monthyear)) }}</td>
                                            </tr>  
                                            @endforeach
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        @endif
    </div>
</div>
@include('layouts.partials.footer')

<script src="{{ asset('admin/dist/assets/plugins/custom/datatables/datatables.bundle.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/table.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/export-users.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/user-management/users/list/add.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/widgets.bundle.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/widgets.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/apps/chat/chat.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/upgrade-plan.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/create-app.js') }}"></script>
<script src="{{ asset('admin/dist/assets/js/custom/utilities/modals/users-search.js') }}"></script>