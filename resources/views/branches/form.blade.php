<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('name') ? 'has-error' : '' }}">
                        <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Name</label>
                        <input class="form-control mb-2" name="name" type="text" id="name" value="{{ old('name', optional($branch)->name) }}" placeholder="Enter name here...">
                        {!! $errors->first('name', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <select class="form-select mb-2" id="status" name="status" aria-label="Select a Status" data-control="select2" data-placeholder="Select status">
                            <option value="" style="display: none;" {{ old('status', optional($branch)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select status</option>
                            @foreach (['active' => 'Active',
                                'inactive' => 'Inactive'] as $key => $text)
                                <option value="{{ $key }}" {{ old('status', optional($branch)->status) == $key ? 'selected' : '' }}>
                                    {{ $text }}
                                </option>
                            @endforeach
                        </select>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div id="kt_docs_repeater_advanced">
                    <!--begin::Form group-->
                    <div class="form-group">
                        <div data-repeater-list="branchdesignation" class="kt-docs-repeater-advanced">
                            @if(old('branchdesignation'))
                                @foreach(old('branchdesignation') as $index => $item)
                                <div data-repeater-item class="kt-repeater-row border-top pt-5">
                                    <div class="form-group row mb-5 ">
                                        <div class="col-md-5">
                                            <div class="form-group {{ $errors->has('designation_id') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Designation</label>
                                                
                                                    <select class="form-select mb-2" name="branchdesignation[{{ $index }}][designation_id]" aria-label="Select a designation required" data-kt-repeater="select2" data-placeholder="Select designation required" onchange="updateDropdowns()">
                                                            <option value="" style="display: none;" {{ ($item['designation_id'] ?: '') == '' ? 'selected' : '' }} disabled selected>select designation required</option>
                                                        @foreach ($designations as $key => $text)
                                                            <option value="{{ $key }}" {{ ($item['designation_id']) == $key ? 'selected' : '' }}>
                                                                {{ $text }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    
                                                    {!! $errors->first('designation_id', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group {{ $errors->has('basic_fix') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Basic / Fix</label>
                                               
                                                    <select class="form-select mb-2 basic_fix" name="branchdesignation[{{ $index }}][basic_fix]" aria-label="Select a basic fix required" data-kt-repeater="select2" data-placeholder="Select basic fix required" >
                                                            <option value="" style="display: none;" {{ ($item['basic_fix'] ?: '') == '' ? 'selected' : '' }} disabled selected>select basic fix required</option>
                                                        @foreach (['basic' => 'Basic',
                                            'fix' => 'Fix'] as $key => $text)
                                                            <option value="{{ $key }}" {{ ($item['basic_fix']) == $key ? 'selected' : '' }}>
                                                                {{ $text }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    
                                                    {!! $errors->first('basic_fix', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-end">
                                            <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-danger mt-3 mt-md-9 kt-repeater-delete">
                                                <i class="la la-trash-o fs-3"></i>Delete
                                            </a>
                                        </div>
                                    </div>
                                    <div id="basic_show_hide" class="row mb-5 basic_show_hide">
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('basic_da') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Basic + DA(₹)</label>
                                                
                                                    <input class="form-control mb-2" type="number" name="branchdesignation[{{ $index }}][basic_da]" id="basic_da" value="{{ $item['basic_da'] }}" placeholder="Enter basic + da here...">
                                                    {!! $errors->first('basic_da', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('extra_allowance') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Extra Allowance(₹)</label>
                                                
                                                    <input class="form-control mb-2" type="number" name="branchdesignation[{{ $index }}][extra_allowance]" id="extra_allowance" value="{{ $item['extra_allowance'] }}" placeholder="Enter extra allowance here...">
                                                    {!! $errors->first('extra_allowance', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('hra') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">HRA(₹)</label>
                                                
                                                    <input class="form-control mb-2" type="number" name="branchdesignation[{{ $index }}][hra]" id="hra" value="{{ $item['hra'] }}" placeholder="Enter hra here...">
                                                    {!! $errors->first('hra', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('bonus') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bonus(₹)</label>
                                                
                                                    <input class="form-control mb-2" type="number" name="branchdesignation[{{ $index }}][bonus]" id="bonus" value="{{ $item['bonus'] }}" placeholder="Enter bonus here...">
                                                    {!! $errors->first('bonus', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div id="fix_show_hide" class="row mb-5 fix_show_hide">
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('fix_salary') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Fix Salary(₹)</label>
                                                
                                                    <input class="form-control mb-2" type="number" name="branchdesignation[{{ $index }}][fix_salary]" id="fix_salary" value="{{ $item['fix_salary'] }}" placeholder="Enter fix salary here...">
                                                    {!! $errors->first('fix_salary', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @elseif(isset($branch))
                                @foreach($branch->branchDesignation as $index => $item)
                                <div data-repeater-item class="kt-repeater-row border-top pt-5">
                                    <input class="form-control mb-2 d-none" name="branchdesignation[{{ $index }}][branchdesignation_id]" value="{{ $item->id }}" id="branchdesignation_id" placeholder="Enter work start date here...">
                                    <div class="form-group row mb-5">
                                        <div class="col-md-5">
                                            <div class="form-group {{ $errors->has('designation_id') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Designation</label>
                                                
                                                    <select class="form-select mb-2" name="branchdesignation[{{ $index }}][designation_id]" aria-label="Select a designation required" data-kt-repeater="select2" data-placeholder="Select designation required" onchange="updateDropdowns()">
                                                            <option value="" style="display: none;" {{ ($item->designation_id ?: '') == '' ? 'selected' : '' }} disabled selected>select designation required</option>
                                                        @foreach ($designations as $key => $text)
                                                            <option value="{{ $key }}" {{ ($item->designation_id) == $key ? 'selected' : '' }}>
                                                                {{ $text }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    
                                                    {!! $errors->first('designation_id', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-5">
                                            <div class="form-group {{ $errors->has('basic_fix') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Basic / Fix</label>
                                                
                                                    <select class="form-select mb-2 basic_fix" name="branchdesignation[{{ $index }}][basic_fix]" aria-label="Select a basic fix required" data-kt-repeater="select2" data-placeholder="Select basic fix required" >
                                                            <option value="" style="display: none;" {{ ($item->basic_fix ?: '') == '' ? 'selected' : '' }} disabled selected>select basic fix required</option>
                                                        @foreach (['basic' => 'Basic',
                                            'fix' => 'Fix'] as $key => $text)
                                                            <option value="{{ $key }}" {{ ($item->basic_fix) == $key ? 'selected' : '' }}>
                                                                {{ $text }}
                                                            </option>
                                                        @endforeach
                                                    </select>
                                                    
                                                    {!! $errors->first('basic_fix', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        
                                        <div class="col-md-2 text-end">
                                            <a href="javascript:;" data-repeater-delete data-id="{{ $item->id }}" class="btn btn-danger mt-3 mt-md-9 kt-repeater-delete">
                                                <i class="la la-trash-o fs-3"></i>Delete
                                            </a>
                                        </div>
                                    </div>
                                    <div id="basic_show_hide" class="row mb-5 basic_show_hide">
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('basic_da') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Basic + DA(₹)</label>
                                                
                                                    <input class="form-control mb-2" type="number" name="branchdesignation[{{ $index }}][basic_da]" id="basic_da" value="{{ $item->basic_da }}" placeholder="Enter basic + da here...">
                                                    {!! $errors->first('basic_da', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('extra_allowance') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Extra Allowance(₹)</label>
                                                
                                                    <input class="form-control mb-2" type="number" name="branchdesignation[{{ $index }}][extra_allowance]" id="extra_allowance" value="{{ $item->extra_allowance }}" placeholder="Enter extra allowance here...">
                                                    {!! $errors->first('extra_allowance', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('hra') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">HRA(₹)</label>
                                                
                                                    <input class="form-control mb-2" type="number" name="branchdesignation[{{ $index }}][hra]" id="hra" value="{{ $item->hra }}" placeholder="Enter hra here...">
                                                    {!! $errors->first('hra', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('bonus') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bonus(₹)</label>
                                                
                                                    <input class="form-control mb-2" type="number" name="branchdesignation[{{ $index }}][bonus]" id="bonus" value="{{ $item->bonus }}" placeholder="Enter bonus here...">
                                                    {!! $errors->first('bonus', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                    </div>
                                    <div id="fix_show_hide" class="row mb-5 fix_show_hide">
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('fix_salary') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Fix Salary(₹)</label>
                                                
                                                    <input class="form-control mb-2" type="number" name="branchdesignation[{{ $index }}][fix_salary]" id="fix_salary" value="{{ $item->fix_salary }}" placeholder="Enter fix salary here...">
                                                    {!! $errors->first('fix_salary', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @else
                            <div data-repeater-item class="kt-repeater-row border-top pt-5">
                                <div class="form-group row mb-5">
                                    <div class="col-md-5">
                                        <div class="form-group {{ $errors->has('designation_id') ? 'has-error' : '' }}">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Designation</label>
                                            
                                                <select class="form-select mb-2" name="branchdesignation[0][designation_id]" aria-label="Select a designation required" data-kt-repeater="select2" data-placeholder="Select designation required" onchange="updateDropdowns()">
                                                        <option value="" style="display: none;" disabled selected>select designation required</option>
                                                    @foreach ($designations as $key => $text)
                                                        <option value="{{ $key }}">
                                                            {{ $text }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                
                                                {!! $errors->first('designation_id', '<p class="help-block">:message</p>') !!}
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-5">
                                        <div class="form-group {{ $errors->has('basic_fix') ? 'has-error' : '' }}">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Basic / Fix</label>
                                            
                                                <select class="form-select mb-2 basic_fix" name="branchdesignation[0][basic_fix]" aria-label="Select a basic fix required" data-kt-repeater="select2" data-placeholder="Select basic fix required" >
                                                        <option value="" style="display: none;" disabled selected>select basic fix required</option>
                                                    @foreach (['basic' => 'Basic',
                                        'fix' => 'Fix'] as $key => $text)
                                                        <option value="{{ $key }}">
                                                            {{ $text }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                
                                                {!! $errors->first('basic_fix', '<p class="help-block">:message</p>') !!}
                                            
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-2 text-end">
                                        <a href="javascript:;" data-repeater-delete class="btn  btn-danger mt-3 mt-md-9 kt-repeater-delete d-flex justify-content-center">
                                            <i class="la la-trash-o fs-3"></i>Delete
                                        </a>
                                    </div>
                                </div>
                                <div id="basic_show_hide" class="row mb-5 basic_show_hide">
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('basic_da') ? 'has-error' : '' }}">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Basic + DA(₹)</label>
                                            
                                                <input class="form-control mb-2" type="number" name="branchdesignation[0][basic_da]" id="basic_da" placeholder="Enter basic + da here...">
                                                {!! $errors->first('basic_da', '<p class="help-block">:message</p>') !!}
                                           
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('extra_allowance') ? 'has-error' : '' }}">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Extra Allowance(₹)</label>
                                            
                                                <input class="form-control mb-2" type="number" name="branchdesignation[0][extra_allowance]" id="extra_allowance" placeholder="Enter extra allowance here...">
                                                {!! $errors->first('extra_allowance', '<p class="help-block">:message</p>') !!}
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('hra') ? 'has-error' : '' }}">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">HRA(₹)</label>
                                            
                                                <input class="form-control mb-2" type="number" name="branchdesignation[0][hra]" id="hra" placeholder="Enter hra here...">
                                                {!! $errors->first('hra', '<p class="help-block">:message</p>') !!}
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('bonus') ? 'has-error' : '' }}">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bonus(₹)</label>
                                            
                                                <input class="form-control mb-2" type="number" name="branchdesignation[0][bonus]" id="bonus" placeholder="Enter bonus here...">
                                                {!! $errors->first('bonus', '<p class="help-block">:message</p>') !!}
                                            
                                        </div>
                                    </div>
                                </div>
                                <div id="fix_show_hide" class="row mb-5 fix_show_hide">
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('fix_salary') ? 'has-error' : '' }}">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Fix Salary(₹)</label>
                                            
                                                <input class="form-control mb-2" type="number" name="branchdesignation[0][fix_salary]" id="fix_salary" placeholder="Enter fix salary here...">
                                                {!! $errors->first('fix_salary', '<p class="help-block">:message</p>') !!}
                                            
                                        </div>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <!--end::Form group-->

                    <!--begin::Form group-->
                    <div class="form-group">
                        <a href="javascript:;" data-repeater-create class="btn btn-primary">
                            <i class="la la-plus"></i>Add
                        </a>
                    </div>
                    <!--end::Form group-->
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('over_time_amount') ? 'has-error' : '' }}">
                        <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Over Time Amount(₹)</label>
                        
                            <input class="form-control mb-2" name="over_time_amount" type="number" id="over_time_amount" value="{{ old('over_time_amount', optional($branch)->over_time_amount) }}" placeholder="Enter over time amount here in (₹)...">
                            {!! $errors->first('over_time_amount', '<p class="help-block">:message</p>') !!}
                        
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        
                            <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" value="{{ old('monthyear', optional($branch)->monthyear) }}" placeholder="Enter monthyear here...">
                            {!! $errors->first('monthyear', '<p class="help-block">:message</p>') !!}
                        
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-12">
                    <div class="form-group {{ $errors->has('address') ? 'has-error' : '' }}">
                        <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Address</label>
                        
                            <textarea class="form-control mb-2" name="address" cols="2" rows="1" id="address" placeholder="Enter address here...">{{ old('address', optional($branch)->address) }}</textarea>
                            {!! $errors->first('address', '<p class="help-block">:message</p>') !!}
                        
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div id="kt_docs_repeater_advanced_1">
                    <div class="form-group">
                        <div data-repeater-list="workorder">
                            @if(old('workorder'))
                                @foreach(old('workorder') as $index => $item)
                                <div data-repeater-item class="kt-repeater-row border-top pt-5">
                                    <div class="form-group row mb-5">
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('work_start_date') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Work Start Date</label>
                                               
                                                    <input class="form-control mb-2" name="workorder[{{ $index }}][work_start_date]" value="{{ $item['work_start_date'] }}"  data-kt-repeater="datepicker" id="work_start_date" placeholder="Enter work start date here...">
                                                    {!! $errors->first('work_start_date', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('work_expire_date') ? 'has-error' : '' }}">
                                                <label class="form-label fs-6 fw-bolder text-black-700 mb-3">Work Expire Date</label>
                                               
                                                    <input class="form-control mb-2" name="workorder[{{ $index }}][work_expire_date]" value="{{ $item['work_expire_date'] }}" data-kt-repeater="datepicker" id="work_expire_date" placeholder="Enter work expire date here...">
                                                    {!! $errors->first('work_expire_date', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group {{ $errors->has('work_order') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Work Order</label>
                                               
                                                    @if (!isset($item['work_order']) && empty($item['work_order']))
                                                        <input class="hidden form-control mb-2" name="workorder[{{ $index }}][work_order]" type="file" id="work_order" placeholder="Enter workorder here...">
                                                    @else
                                                        <div class="image-input image-input-outline {{ isset($item) && $item['work_order'] ? '' : 'image-input-empty' }}" data-kt-image-input="true" style="background-image: url({{ asset('admin/src/media/avatars/blank.png') }})">
                                                            <div class="image-input-wrapper w-100px h-150px" style="background-image: {{ isset($item) && $item['work_order'] ? 'url('.asset('storage/'. $item['work_order']).')' : 'none' }};"></div>
                                                            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                                <i class="bi bi-pencil-fill fs-7"></i>
                                                                <input type="file" name="workorder[{{ $index }}][work_order]" id="work_order" accept=".png, .jpg, .jpeg">
                                                                <input type="hidden" name="custom_delete_work_order">
                                                            </label>
                                                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                                                <i class="bi bi-x fs-2"></i>
                                                            </span>
                                                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                                                <i class="bi bi-x fs-2"></i>
                                                            </span>
                                                        </div>
                                                    @endif
                                                    {!! $errors->first('work_order', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-end">
                                            <a href="javascript:;" data-repeater-delete class="btn btn-sm btn-danger mt-3 mt-md-8 kt-repeater-delete d-flex justify-content-center ">
                                                <i class="la la-trash-o"></i>Delete
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @elseif(isset($branch))
                                @foreach($branch->branchWorkOrder as $index => $item)
                                <div data-repeater-item class="kt-repeater-row border-top pt-5">
                                    <input class="form-control mb-2 d-none" name="workorder[{{ $index }}][workorder_id]" value="{{ $item->id }}" id="workorder_id" placeholder="Enter work start date here...">
                                    <div class="form-group row mb-5">
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('work_start_date') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Work Start Date</label>
                                                
                                                    <input class="form-control mb-2" name="workorder[{{ $index }}][work_start_date]" value="{{ $item->work_start_date }}"  data-kt-repeater="datepicker" id="work_start_date" placeholder="Enter work start date here...">
                                                    {!! $errors->first('work_start_date', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-3">
                                            <div class="form-group {{ $errors->has('work_expire_date') ? 'has-error' : '' }}">
                                                <label class="form-label fs-6 fw-bolder text-black-700 mb-3">Work Expire Date</label>
                                               
                                                    <input class="form-control mb-2" name="workorder[{{ $index }}][work_expire_date]" value="{{ $item->work_expire_date }}" data-kt-repeater="datepicker" id="work_expire_date" placeholder="Enter work expire date here...">
                                                    {!! $errors->first('work_expire_date', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-4">
                                            <div class="form-group {{ $errors->has('work_order') ? 'has-error' : '' }}">
                                                <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Work Order</label>
                                                
                                                    @if (!isset($item->work_order) && empty($item->work_order))
                                                        
                                                        <input class="hidden form-control mb-2" name="workorder[{{ $index }}][work_order]" type="file" id="work_order" placeholder="Enter workorder here...">
                                                    @else
                                                        <div class="image-input image-input-outline {{ isset($item) && $item->work_order ? '' : 'image-input-empty' }}" data-kt-image-input="true" style="background-image: url({{ asset('admin/src/media/avatars/blank.png') }})">
                                                            <div class="image-input-wrapper w-150px h-150px" style="background-image: {{ isset($item) && $item->work_order ? 'url('.asset('storage/'. $item->work_order).')' : 'none' }};"></div>
                                                            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                                <i class="bi bi-pencil-fill fs-7"></i>
                                                                <input type="file" name="workorder[{{ $index }}][work_order]" id="work_order" accept=".png, .jpg, .jpeg">
                                                                <input type="hidden" name="custom_delete_work_order">
                                                            </label>
                                                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                                                <i class="bi bi-x fs-2"></i>
                                                            </span>
                                                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                                                <i class="bi bi-x fs-2"></i>
                                                            </span>
                                                        </div>
                                                    @endif
                                                    {!! $errors->first('work_order', '<p class="help-block">:message</p>') !!}
                                                
                                            </div>
                                        </div>
                                        <div class="col-md-2 text-end">
                                            <a href="javascript:;" data-repeater-delete="" class="btn  btn-danger mt-3 mt-md-9 kt-repeater-delete d-flex justify-content-center">
                                                <i class="la la-trash-o fs-3"></i>Delete
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                @endforeach
                            @else
                            <div data-repeater-item class="kt-repeater-row border-top pt-5">
                                <div class="form-group row mb-5">
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('work_start_date') ? 'has-error' : '' }}">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Work Start Date</label>
                                            
                                                <input class="form-control mb-2" name="workorder[0][work_start_date]" data-kt-repeater="datepicker" id="work_start_date" value="{{ old('work_start_date', optional($branch)->work_start_date) }}" placeholder="Enter work start date here...">
                                                {!! $errors->first('work_start_date', '<p class="help-block">:message</p>') !!}
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="form-group {{ $errors->has('work_expire_date') ? 'has-error' : '' }}">
                                            <label class="form-label fs-6 fw-bolder text-black-700 mb-3">Work Expire Date</label>
                                            
                                                <input class="form-control mb-2" name="workorder[0][work_expire_date]" data-kt-repeater="datepicker" id="work_expire_date" value="{{ old('work_expire_date', optional($branch)->work_expire_date) }}" placeholder="Enter work expire date here...">
                                                {!! $errors->first('work_expire_date', '<p class="help-block">:message</p>') !!}
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-4">
                                        <div class="form-group {{ $errors->has('work_order') ? 'has-error' : '' }}">
                                            <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Work Order</label>
                                            
                                                @if (!isset($branch->work_order) && empty($branch->work_order))
                                                    
                                                    <input class="hidden form-control mb-2" name="workorder[0][work_order]" type="file" id="work_order" placeholder="Enter workorder here...">
                                                @else
                                                    <div class="image-input image-input-outline {{ isset($branch) && $branch->work_order ? '' : 'image-input-empty' }}" data-kt-image-input="true" style="background-image: url({{ asset('admin/src/media/avatars/blank.png') }})">
                                                        <div class="image-input-wrapper w-150px h-150px" style="background-image: {{ isset($branch) && $branch->work_order ? 'url('.asset('storage/'. $branch->work_order).')' : 'none' }};"></div>
                                                        <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" title="Change avatar">
                                                            <i class="bi bi-pencil-fill fs-7"></i>
                                                            <input class="form-control" type="file" name="workorder[0][work_order]" id="work_order" accept=".png, .jpg, .jpeg">
                                                            <input type="hidden" name="custom_delete_work_order">
                                                        </label>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" title="Cancel avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                        <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" title="Remove avatar">
                                                            <i class="bi bi-x fs-2"></i>
                                                        </span>
                                                    </div>
                                                @endif
                                                {!! $errors->first('work_order', '<p class="help-block">:message</p>') !!}
                                            
                                        </div>
                                    </div>
                                    <div class="col-md-2 text-end">
                                        <a href="javascript:;" data-repeater-delete="" class="btn  btn-danger mt-3 mt-md-9 kt-repeater-delete d-flex justify-content-center">
                                            <i class="la la-trash-o fs-3"></i>Delete
                                        </a>
                                    </div>
                                </div>
                            </div>
                            @endif
                        </div>
                    </div>
                    <div class="form-group mt-5">
                        <a href="javascript:;" data-repeater-create class="btn btn-primary">
                            <i class="la la-plus"></i>Add
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
<script>
    $(document).ready(function () {
        $('#kt_docs_repeater_advanced [data-repeater-delete]').click(function (){
        var itemId = $(this).data('id');
        var row = $(this).closest('[data-repeater-item]');
        $.ajax({
            type: 'POST',
            url: '/admin/branches/branchdesignation/' + itemId,
            data: {_method: 'DELETE', _token: '{{ csrf_token() }}'},
            success: function () {
                row.remove();
            }
        });
    });
}); 
    $(document).ready(function () {
        $('#kt_docs_repeater_advanced_1 [data-repeater-delete]').click(function () {
        var itemId = $(this).data('id');
        var row = $(this).closest('[data-repeater-item]');
        $.ajax({
            type: 'POST',
            url: '/admin/branches/workorder/' + itemId,
            data: {_method: 'DELETE', _token: '{{ csrf_token() }}'},
            success: function () {
                row.remove();
            }
        });
    });
});
</script>