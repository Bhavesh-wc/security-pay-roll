@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Damage' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('damages.damage.destroy', $damage->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('damages.damage.index') }}" class="btn btn-primary" title="Show All Damage">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('damages.damage.create') }}" class="btn btn-success" title="Create New Damage">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('damages.damage.edit', $damage->id ) }}" class="btn btn-primary" title="Edit Damage">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Damage" onclick="return confirm(&quot;Click Ok to delete Damage.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Monthyear</dt>
            <dd>{{ $damage->monthyear }}</dd>
            <dt>Branch</dt>
            <dd>{{ optional($damage->branch)->name }}</dd>
            <dt>Employee</dt>
            <dd>{{ optional($damage->employee)->full_name }}</dd>
            <dt>Damage Details</dt>
            <dd>{{ $damage->damage_details }}</dd>
            <dt>Damage Date</dt>
            <dd>{{ $damage->damage_date }}</dd>
            <dt>Damage Amount</dt>
            <dd>{{ $damage->damage_amount }}</dd>
            <dt>Employee Reason For Deduction</dt>
            <dd>{{ $damage->employee_reason_for_deduction }}</dd>
            <dt>Name Of Person Explanation Heard</dt>
            <dd>{{ $damage->name_of_person_explanation_heard }}</dd>
            <dt>Total Installments</dt>
            <dd>{{ $damage->total_installments }}</dd>
            <dt>Installment Date</dt>
            <dd>{{ $damage->installment_date }}</dd>
            <dt>Installment Amount</dt>
            <dd>{{ $damage->installment_amount }}</dd>
            <dt>Remarks</dt>
            <dd>{{ $damage->remarks }}</dd>

        </dl>

    </div>
</div>

@endsection