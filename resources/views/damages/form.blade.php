<div class="mb-0">
    <div class="card card-flush py-4"> 
        <div class="card-body pt-0">
            @if(empty($damage->id))
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" value="{{ old('monthyear', optional($damage)->monthyear) }}" required="true" placeholder="Enter monthyear here...">
                        </div>
                    </div>          
                </div>
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                    <select id="branch_dropdown" name="branch_id" data-control="select2" class="form-control @error('branch_id') is-invalid @enderror form-select mb-2 mb-5" required>
                        <option value="" selected disabled class="">Select branch</option>
                        @foreach ($branches as $data)
                        <option value="{{ trim($data->id) }}">{{ $data->name}}</option>
                        @endforeach
                    </select>
                    <span class="text-danger">
                        @error('branch_id')
                        *{{ $message }}
                        @enderror
                    </span>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee</label>
                    <select name="employee_id[]" id="employee_dropdown" class="form-select mb-2" data-control="select2" data-placeholder="Select employee" data-allow-clear="true" multiple="multiple" required>
                    </select>         
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('damage_details') ? 'has-error' : '' }}">
                        <label for="damage_details" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Damage Details</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="damage_details" cols="1" rows="1" id="damage_details" required="true" placeholder="Enter damage details here...">{{ old('damage_details', optional($damage)->damage_details) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="text" id="monthyear" value="{{ $damage->monthyear }}" placeholder="Enter monthyear here..." readonly>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                    <input class="form-control mb-2" name="branch_id" type="text" id="branch_id" value="{{ $damage->branch_id }}" hidden>
                    <input class="form-control mb-2" name="branch_id" type="text" id="branch_id" value="{{ old('branch_id', optional($damage)->branch->name) }}" readonly>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee</label>
                    <input class="form-control mb-2" name="employee_id" type="text" id="employee_id" value="{{ $damage->employee_id }}" hidden>
                    <input class="form-control mb-2" name="employee" type="text" id="employee_id" value="{{ old('employee_id', optional($damage)->employee->full_name) }}" readonly>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('damage_details') ? 'has-error' : '' }}">
                        <label for="damage_details" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Damage Details</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="damage_details" cols="1" rows="1" id="damage_details" required="true" placeholder="Enter damage details here...">{{ old('damage_details', optional($damage)->damage_details) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('damage_date') ? 'has-error' : '' }}">
                        <label for="damage_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Damage Date</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="damage_date" type="date" id="damage_date" value="{{ old('damage_date', optional($damage)->damage_date) }}" required="true" placeholder="Enter damage date here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('damage_amount') ? 'has-error' : '' }}">
                        <label for="damage_amount" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Damage Amount</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="damage_amount" type="number" id="damage_amount" value="{{ old('damage_amount', optional($damage)->damage_amount) }}" required="true" placeholder="Enter damage amount here...">
                        </div>       
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('employee_reason_for_deduction') ? 'has-error' : '' }}">
                        <label for="employee_reason_for_deduction" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee Reason For Deduction</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="employee_reason_for_deduction" cols="1" rows="1" id="employee_reason_for_deduction" required="true" placeholder="Enter employee reason for deduction here...">{{ old('employee_reason_for_deduction', optional($damage)->employee_reason_for_deduction) }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('name_of_person_explanation_heard') ? 'has-error' : '' }}">
                        <label for="name_of_person_explanation_heard" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Name of Person Explanation Heard</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="name_of_person_explanation_heard" type="text" id="name_of_person_explanation_heard" value="{{ old('name_of_person_explanation_heard', optional($damage)->name_of_person_explanation_heard) }}" required="true" placeholder="Enter name of person explanation heard here...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('total_installments') ? 'has-error' : '' }}">
                        <label for="total_installments" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Total Installments</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="total_installments" type="number" id="total_installments" value="{{ old('total_installments', optional($damage)->total_installments) }}" required="true" placeholder="Enter total installments here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('installment_date') ? 'has-error' : '' }}">
                        <label for="installment_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Installment Date</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="installment_date" type="date" id="installment_date" value="{{ old('installment_date', optional($damage)->installment_date) }}" required="true" placeholder="Enter first installment date here...">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('installment_amount') ? 'has-error' : '' }}">
                        <label for="installment_amount" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Installment Amount</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="installment_amount" type="number" id="installment_amount" value="{{ old('installment_amount', optional($damage)->installment_amount) }}" required="true" placeholder="Enter first installment amount here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('remarks') ? 'has-error' : '' }}">
                        <label for="remarks" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Remarks</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="remarks" cols="1" rows="1" id="remarks" required="true" placeholder="Enter remarks here...">{{ old('remarks', optional($damage)->remarks) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
<script>
    $(document).ready(function () {
    $("#branch_dropdown").on("change", function () {
        var idBranch = this.value;
        $("#employee_dropdown").html("");
        $.ajax({
            url: "{{url('admin/fetch_employee')}}",
            type: "POST",
            data: {
                branch_id: idBranch,
                _token: "{{csrf_token()}}",
            },
            dataType: "json",
            success: function (result) {
                $.each(result.Employees, function (key, value) {
                    $("#employee_dropdown").append(
                        '<option value="' +
                            value.id +
                            '">' +
                            value.full_name +
                            "</option>"
                    );
                });
            },
        });
    });
});
</script>