@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="d-flex flex-column flex-column-fluid">
        <div id="kt_app_toolbar" class="app-toolbar  py-3 py-lg-6 ">
            <div id="kt_app_toolbar_container" class="app-container  container-xxl d-flex flex-stack ">
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3 ">
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">View Salary Details</h1>
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                        </li>
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('salaries.salary.index') }}" class="text-muted text-hover-primary">Index</a>
                        </li>
                    </ul>
                </div>
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <form method="POST" action="{!! route('salaries.salary.destroy', $salary->id) !!}" accept-charset="UTF-8">
                        <input name="_method" value="DELETE" type="hidden">
                        {{ csrf_field() }}
                                <a href="{{ route('salaries.salary.index') }}" class="btn btn-sm fw-bold btn-primary" title="Show All Salary">
                                    Show
                                </a>
            
                                <a href="{{ route('salaries.salary.create') }}" class="btn btn-sm fw-bold btn-success" title="Create New Salary">
                                    Create
                                </a>
                                
                                {{-- <a href="{{ route('salaries.salary.edit', $salary->id ) }}" class="btn btn-sm fw-bold btn-primary" title="Edit Salary">
                                    Edit
                                </a> --}}
            
                                <button type="submit" class="btn btn-sm fw-bold btn-danger" title="Delete Salary" onclick="return confirm(&quot;Click Ok to delete Salary.?&quot;)">
                                    Delete
                                </button>
                        </form>
                </div>
            </div>
        </div>
        <div id="kt_app_content" class="app-content  flex-column-fluid ">
            <div id="kt_app_content_container" class="app-container  container-xxl ">             
                <div class="card mb-5 mb-xl-10" id="kt_profile_details_view">
                    <div class="card-body p-9">
                        <div class="row mb-7">
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Branch</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ optional($salary->branch)->name }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Designation</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ optional($salary->designation)->designation_name }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Department</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ optional($salary->department)->department_name }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Grade</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ optional($salary->grade)->grade_name }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Employee</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ optional($salary->employee)->full_name }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Monthyear</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->monthyear ? date('M-Y', strtotime($salary->monthyear)): '' ; }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Month</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->month }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Year</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->year }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Salary Date</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->salary_date ? date('d-m-Y', strtotime($salary->salary_date)): '' ; }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Working Days</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->working_days }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">No Of Days Work Done</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->no_of_days_work_done }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">No Of Leave</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->no_of_leave }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Basic + Da</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->basic_da }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Extra Allowance</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->extra_allowance }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Hra</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->hra }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Bonus</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->bonus }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Fix Salary</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->fix_salary }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Sal Advance</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->sal_advance }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">A</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->a }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">B</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->b }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">C</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->c }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Ot Amount</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->ot_amount }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Ot Days</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->ot_days }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Attendance Allowance</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->attendance_allowance }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Dress</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->dress }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Mess</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->mess }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Pf</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->pf }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Profetional Tax</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->profetional_tax }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Basis Wages</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->basis_wages }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Esic</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->esic }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Walfare Amount</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->walfare_amount }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Damage Amount</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->damage_amount }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Fines</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->fines }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Total</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->total }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Nettotal</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->nettotal }}</span>                         
                                </div>
                            </div>
                            <div class="col-4">
                                <label class="fw-semibold text-muted mb-2">Branch Ot Amount</label>
                                <div>
                                    <span class="fw-semibold text-gray-800 fs-6">{{ $salary->branch_ot_amount }}</span>                         
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('layouts.partials.footer')