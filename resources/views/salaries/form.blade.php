
<div class="d-flex flex-column gap-7 gap-lg-10">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
            <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('branch_id') ? 'has-error' : '' }}">
                        <label for="branch_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                        <div class="col-md-12">
                            <select id="branch_dropdown" name="branch_id" class="form-select" data-control="select2" aria-label="Select a Branch" data-placeholder="Select branch" required="true">
                                    <option value="" style="display: none;" {{ old('branch_id', optional($salary)->branch_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select branch</option>
                                @foreach ($branches as $key => $branch)
                                    <option value="{{ $key }}" {{ old('branch_id', optional($salary)->branch_id) == $key ? 'selected' : '' }}>
                                        {{ $branch }}
                                    </option>
                                @endforeach
                            </select> 
                            {!! $errors->first('branch_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('employee_id') ? 'has-error' : '' }}">
                        <label for="employee_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee</label>
                        <div class="col-md-12">
                            <select class="form-select" id="employee_dropdown" data-control="select2" name="employee_id" data-placeholder="Select employee" data-control="select2" aria-label="Select a Employee" required="true">
                                    <option value="" style="display: none;"  {{ old('employee_id', optional($salary)->employee_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select employee</option>
                                @foreach ($employees as $key => $employee)
                                    <option value="{{ $key }}" {{ old('employee_id', optional($salary)->employee_id) == $key ? 'selected' : '' }}>
                                        {{ $employee }}
                                    </option>
                                @endforeach
                            </select>
                            {!! $errors->first('employee_id', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" value="{{ old('monthyear', optional($salary)->monthyear) }}" placeholder="Enter monthyear here...">
                            {!! $errors->first('monthyear', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('bonus') ? 'has-error' : '' }}">
                        <label for="bonus" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bonus</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="bonus" type="number" id="bonus" value="{{ old('bonus', optional($salary)->bonus) }}" min="-99999999" max="99999999" required="true" placeholder="Enter bonus here..." step="any">
                            {!! $errors->first('bonus', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('a') ? 'has-error' : '' }}">
                        <label for="a" class="required form-label fs-6 fw-bolder text-black-700 mb-3">A</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="a" type="text" id="a" value="{{ old('a', optional($salary)->a) }}" min="-99999999" max="99999999" required="true" placeholder="Enter a here..." step="any">
                            {!! $errors->first('a', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('b') ? 'has-error' : '' }}">
                        <label for="b" class="required form-label fs-6 fw-bolder text-black-700 mb-3">B</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="b" type="text" id="b" value="{{ old('b', optional($salary)->b) }}" min="-99999999" max="99999999" required="true" placeholder="Enter b here..." step="any">
                            {!! $errors->first('b', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('c') ? 'has-error' : '' }}">
                        <label for="c" class="required form-label fs-6 fw-bolder text-black-700 mb-3">C</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="c" type="text" id="c" value="{{ old('c', optional($salary)->c) }}" min="-99999999" max="99999999" required="true" placeholder="Enter c here..." step="any">
                            {!! $errors->first('c', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('attendance_allowance') ? 'has-error' : '' }}">
                        <label for="attendance_allowance" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Attendance Allowance</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="attendance_allowance" type="text" id="attendance_allowance" value="{{ old('attendance_allowance', optional($salary)->attendance_allowance) }}" min="-99999999" max="99999999" required="true" placeholder="Enter attendance allowance here..." step="any">
                            {!! $errors->first('attendance_allowance', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('dress') ? 'has-error' : '' }}">
                        <label for="dress" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Dress</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="dress" type="text" id="dress" value="{{ old('dress', optional($salary)->dress) }}" min="-99999999" max="99999999" required="true" placeholder="Enter dress here..." step="any">
                            {!! $errors->first('dress', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('mess') ? 'has-error' : '' }}">
                        <label for="mess" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Mess</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="mess" type="text" id="mess" value="{{ old('mess', optional($salary)->mess) }}" min="-99999999" max="99999999" required="true" placeholder="Enter mess here..." step="any">
                            {!! $errors->first('mess', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $(document).ready(function() {
        $("#branch_dropdown").on("change", function() {
            var idBranch = this.value;
            $("#employee_dropdown").html("");
            $.ajax({
                url: "{{url('admin/fetch_branch_employee')}}",
                type: "POST",
                data: {
                    branch_id: idBranch,
                    _token: "{{csrf_token()}}",
                },
                dataType: "json",
                success: function(result) {
                    $('#employee_dropdown').html('<option value="">Select Designation</option>');
                    $.each(result.data, function(key, value) {
                        console.log(value);
                        $("#employee_dropdown").append('<option value="' + value.employee_id + '">' + value.employee.full_name + '</option>');
                    });

                    // $.each(result.Employees, function(key, value) {
                    //     $("#employee_dropdown").append(
                    //         '<option value="' +
                    //         value.employee_id +
                    //         '">' +
                    //         value.employee.full_name +
                    //         "</option>"
                    //     );
                    // });
                },
            });
        });
    });
</script>