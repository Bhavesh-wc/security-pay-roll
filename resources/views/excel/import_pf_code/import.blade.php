@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <form action="{{ route('upload_pf_code_import') }}" method="POST" enctype="multipart/form-data">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Upload Excel For Pf AC Code/ PF UAN Code</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <!--begin::Item-->
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Import</li>
                        </ul>
                    </div>
                </div>
            </div>

            <div class="post d-flex flex-column-fluid" id="kt_post">
                <div id="kt_content_container" class="container-xxl">
                    <div class="d-flex flex-column flex-lg-row">
                        <div class="flex-lg-row-fluid mb-10 mb-lg-0 me-lg-7 me-xl-10">
                            @if ($message = Session::get('success'))
                            <div class="alert alert-success alert-dismissible fade show" role="alert">
                                <strong>{{ $message }}</strong>
                                <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                            </div>
                            @endif
                            @if ($message = Session::get('fail'))
                                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                    <strong>{{ $message }}</strong>
                                    <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                                </div>
                            @endif
                            <div class="card">
                                <div class="card-body p-12">
                                    <div class="d-flex justify-content-between mb-5">
                                        <div class="d-block">
                                            <a href="{{ route('download-pfcode-sheet') }}" class="btn btn-sm btn-outline btn-outline-dashed btn-outline-success btn-active-light-success">
                                            Download Sample Sheet
                                            </a>
                                        </div>
                                        <div class="d-block" data-kt-user-table-toolbar="base">
                                            <a href="{{ route('report.index')}} " class="btn btn-sm mt-5 mx-2 btn-primary">
                                                <span class="svg-icon svg-icon-1">Back</span>
                                            </a>
                                            <button class="btn btn-sm btn btn-success mt-5" type="submit">Import</button>
                                        </div>
                                    </div>
                                    
                                    @csrf
                                    <div class="d-flex flex-column align-items-start flex-xxl-row">
                                        <div class="mb-0 col-lg-12">
                                            <div class="row gx-10 mb-5">
                                                <div class="col-lg-6">
                                                    <label class="form-label fs-6 fw-bolder text-gray-700 mb-3">File*</label>
                                                    <input type="file" name="file" class="form-control">
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@include('layouts.partials.footer')