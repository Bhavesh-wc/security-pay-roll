@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Fine' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('fines.fine.destroy', $fine->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('fines.fine.index') }}" class="btn btn-primary" title="Show All Fine">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('fines.fine.create') }}" class="btn btn-success" title="Create New Fine">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('fines.fine.edit', $fine->id ) }}" class="btn btn-primary" title="Edit Fine">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Fine" onclick="return confirm(&quot;Click Ok to delete Fine.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Monthyear</dt>
            <dd>{{ $fine->monthyear }}</dd>
            <dt>Branch</dt>
            <dd>{{ optional($fine->branch)->name }}</dd>
            <dt>Employee</dt>
            <dd>{{ optional($fine->employee)->full_name }}</dd>
            <dt>Act For Which Fine Impossed</dt>
            <dd>{{ $fine->act_for_which_fine_impossed }}</dd>
            <dt>Date Of Offence</dt>
            <dd>{{ $fine->date_of_offence }}</dd>
            <dt>Reasone Against Fine</dt>
            <dd>{{ $fine->reasone_against_fine }}</dd>
            <dt>Name Of Person Explanation Heard</dt>
            <dd>{{ $fine->name_of_person_explanation_heard }}</dd>
            <dt>Wages Payable</dt>
            <dd>{{ $fine->wages_payable }}</dd>
            <dt>Fine Amount</dt>
            <dd>{{ $fine->fine_amount }}</dd>
            <dt>Fine Released Date</dt>
            <dd>{{ $fine->fine_released_date }}</dd>
            <dt>Fine Released Amount</dt>
            <dd>{{ $fine->fine_released_amount }}</dd>
            <dt>Remarks</dt>
            <dd>{{ $fine->remarks }}</dd>

        </dl>

    </div>
</div>

@endsection