<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            @if(empty($fine->id))
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" value="{{ old('monthyear', optional($fine)->monthyear) }}" required="true" placeholder="Enter monthyear here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                    <select id="branch_dropdown" name="branch_id" data-control="select2" class="form-control @error('branch_id') is-invalid @enderror form-select mb-2 mb-5" required>
                        <option value="" selected disabled class="">Select branch</option>
                        @foreach ($branches as $data)
                        <option value="{{ trim($data->id) }}">{{ $data->name}}</option>
                        @endforeach
                    </select>
                    <span class="text-danger">
                        @error('branch_id')
                        *{{ $message }}
                        @enderror
                    </span>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee</label>
                    <select name="employee_id[]" id="employee_dropdown" class="form-select mb-2" data-control="select2" data-placeholder="Select employee" data-allow-clear="true" multiple="multiple" required>
                    </select>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('act_for_which_fine_impossed') ? 'has-error' : '' }}">
                        <label for="act_for_which_fine_impossed" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Act For Which Fine Impossed</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="act_for_which_fine_impossed" cols="1" rows="1" id="act_for_which_fine_impossed" required="true" placeholder="Enter act for which fine impossed here...">{{ old('act_for_which_fine_impossed', optional($fine)->act_for_which_fine_impossed) }}</textarea>
                            {!! $errors->first('act_for_which_fine_impossed', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="text" id="monthyear" value="{{ $fine->monthyear }}" placeholder="Enter monthyear here..." readonly>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                    <input class="form-control mb-2" name="branch_id" type="text" id="branch_id" value="{{ $fine->branch_id }}" hidden>
                    <input class="form-control mb-2" name="branch_id" type="text" id="branch_id" value="{{ old('branch_id', optional($fine)->branch->name) }}" readonly>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee</label>
                    <input class="form-control mb-2" name="employee_id" type="text" id="employee_id" value="{{ $fine->employee_id }}" hidden>
                    <input class="form-control mb-2" name="employee" type="text" id="employee_id" value="{{ old('employee_id', optional($fine)->employee->full_name) }}" readonly>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('act_for_which_fine_impossed') ? 'has-error' : '' }}">
                        <label for="act_for_which_fine_impossed" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Act For Which Fine Impossed</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="act_for_which_fine_impossed" cols="1" rows="1" id="act_for_which_fine_impossed" required="true" placeholder="Enter act for which fine impossed here...">{{ old('act_for_which_fine_impossed', optional($fine)->act_for_which_fine_impossed) }}</textarea>
                            {!! $errors->first('act_for_which_fine_impossed', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            @endif
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('reasone_against_fine') ? 'has-error' : '' }}">
                        <label for="reasone_against_fine" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Reasone Against Fine</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="reasone_against_fine" cols="1" rows="1" id="reasone_against_fine" required="true" placeholder="Enter reasone against fine here...">{{ old('reasone_against_fine', optional($fine)->reasone_against_fine) }}</textarea>
                            {!! $errors->first('reasone_against_fine', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('date_of_offence') ? 'has-error' : '' }}">
                        <label for="date_of_offence" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Date of Offence</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="date_of_offence" type="date" id="date_of_offence" value="{{ old('date_of_offence', optional($fine)->date_of_offence) }}" required="true" placeholder="Enter date of offence here...">
                            {!! $errors->first('date_of_offence', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('name_of_person_explanation_heard') ? 'has-error' : '' }}">
                        <label for="name_of_person_explanation_heard" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Name Of Person Explanation Heard</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="name_of_person_explanation_heard" type="text" id="name_of_person_explanation_heard" value="{{ old('name_of_person_explanation_heard', optional($fine)->name_of_person_explanation_heard) }}" required="true" placeholder="Enter name of person explanation heard here...">
                            {!! $errors->first('name_of_person_explanation_heard', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('wages_payable') ? 'has-error' : '' }}">
                        <label for="wages_payable" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Wages Payable</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="wages_payable" type="text" id="wages_payable" value="{{ old('wages_payable', optional($fine)->wages_payable) }}" required="true" placeholder="Enter wages payable here...">
                            {!! $errors->first('wages_payable', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('fine_amount') ? 'has-error' : '' }}">
                        <label for="fine_amount" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Fine Amount</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="fine_amount" type="number" id="fine_amount" value="{{ old('fine_amount', optional($fine)->fine_amount) }}" required="true" placeholder="Enter fine amount here...">
                            {!! $errors->first('fine_amount', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('fine_released_date') ? 'has-error' : '' }}">
                        <label for="fine_released_date" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Fine Released Date</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="fine_released_date" type="date" id="fine_released_date" value="{{ old('fine_released_date', optional($fine)->fine_released_date) }}" required="true" placeholder="Enter fine released date here...">
                            {!! $errors->first('fine_released_date', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div> 
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('fine_released_amount') ? 'has-error' : '' }}">
                        <label for="fine_released_amount" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Fine Released Amount</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="fine_released_amount" type="number" id="fine_released_amount" value="{{ old('fine_released_amount', optional($fine)->fine_released_amount) }}" required="true" placeholder="Enter fine released amount here...">
                            {!! $errors->first('fine_released_amount', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div> 
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('remarks') ? 'has-error' : '' }}">
                        <label for="remarks" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Remarks</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="remarks" cols="1" rows="1" id="remarks" required="true" placeholder="Enter remarks here...">{{ old('remarks', optional($fine)->remarks) }}</textarea>
                            {!! $errors->first('remarks', '<p class="help-block">:message</p>') !!}
                        </div>
                    </div>
                </div> 
            </div>
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js" ></script>
<script>
    $(document).ready(function () {
    $("#branch_dropdown").on("change", function () {
        var idBranch = this.value;
        $("#employee_dropdown").html("");
        $.ajax({
            url: "{{url('admin/fetch_employee')}}",
            type: "POST",
            data: {
                branch_id: idBranch,
                _token: "{{csrf_token()}}",
            },
            dataType: "json",
            success: function (result) {
                $.each(result.Employees, function (key, value) {
                    $("#employee_dropdown").append(
                        '<option value="' +
                            value.id +
                            '">' +
                            value.full_name +
                            "</option>"
                    );
                });
            },
        });
    });
});
</script>