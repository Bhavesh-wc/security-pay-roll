@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Branch Bill' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('branch_bills.branch_bill.destroy', $branchBill->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('branch_bills.branch_bill.index') }}" class="btn btn-primary" title="Show All Branch Bill">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('branch_bills.branch_bill.create') }}" class="btn btn-success" title="Create New Branch Bill">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('branch_bills.branch_bill.edit', $branchBill->id ) }}" class="btn btn-primary" title="Edit Branch Bill">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Branch Bill" onclick="return confirm(&quot;Click Ok to delete Branch Bill.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Monthyear</dt>
            <dd>{{ $branchBill->monthyear }}</dd>
            <dt>Branch</dt>
            <dd>{{ optional($branchBill->branch)->name }}</dd>
            <dt>Net Bill Amt</dt>
            <dd>{{ $branchBill->net_bill_amt }}</dd>
            <dt>Bill Amount</dt>
            <dd>{{ $branchBill->bill_amount }}</dd>
            <dt>Service Tax</dt>
            <dd>{{ $branchBill->service_tax }}</dd>
            <dt>Gross Amount</dt>
            <dd>{{ $branchBill->gross_amount }}</dd>
            <dt>Service Charge</dt>
            <dd>{{ $branchBill->service_charge }}</dd>
            <dt>Pf Amt 12</dt>
            <dd>{{ $branchBill->pf_amt_12 }}</dd>
            <dt>Esi Amt</dt>
            <dd>{{ $branchBill->esi_amt }}</dd>
            <dt>Leave Amt</dt>
            <dd>{{ $branchBill->leave_amt }}</dd>
            <dt>Bonus Amt</dt>
            <dd>{{ $branchBill->bonus_amt }}</dd>
            <dt>Recover Payment</dt>
            <dd>{{ $branchBill->recover_payment }}</dd>
            <dt>Professional Tax</dt>
            <dd>{{ $branchBill->professional_tax }}</dd>
            <dt>Other Amt</dt>
            <dd>{{ $branchBill->other_amt }}</dd>
            <dt>Extra Exp</dt>
            <dd>{{ $branchBill->extra_exp }}</dd>
            <dt>Tds</dt>
            <dd>{{ $branchBill->tds }}</dd>
            <dt>Net Amt</dt>
            <dd>{{ $branchBill->net_amt }}</dd>
            <dt>Gross Salary</dt>
            <dd>{{ $branchBill->gross_salary }}</dd>
            <dt>Payroll Gross</dt>
            <dd>{{ $branchBill->payroll_gross }}</dd>
            <dt>Diff Amt</dt>
            <dd>{{ $branchBill->diff_amt }}</dd>
            <dt>Salary Paid</dt>
            <dd>{{ $branchBill->salary_paid }}</dd>
            <dt>Profit</dt>
            <dd>{{ $branchBill->profit }}</dd>
            <dt>Net Profit</dt>
            <dd>{{ $branchBill->net_profit }}</dd>

        </dl>

    </div>
</div>

@endsection