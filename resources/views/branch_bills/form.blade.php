<div class="mb-0">
    <div class="card card-flush py-4"> 
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                            <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" value="{{ old('monthyear', optional($branchBill)->monthyear) }}" minlength="1" required="true" placeholder="Enter monthyear here...">
                            {!! $errors->first('monthyear', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('branch_id') ? 'has-error' : '' }}">
                        <label for="branch_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                        <select class="form-select" aria-label="Select a Branch" data-control="select2" data-placeholder="Select branch" id="branch_id" name="branch_id">
                            <option value="" style="display: none;" {{ old('branch_id', optional($branchBill)->branch_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select branch</option>
                            @foreach ($branches as $key => $branch)
                            <option value="{{ $key }}" {{ old('branch_id', optional($branchBill)->branch_id) == $key ? 'selected' : '' }}>
                                {{ $branch }}
                            </option>
                            @endforeach
                        </select>
                        {!! $errors->first('branch_id', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('bill_amount') ? 'has-error' : '' }}">
                        <label for="bill_amount" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Bill Amount</label>
                            <input class="form-control mb-2" name="bill_amount" type="number" id="bill_amount" value="{{ old('bill_amount', optional($branchBill)->bill_amount) }}" placeholder="Enter bill amount here...">
                            {!! $errors->first('bill_amount', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('service_tax') ? 'has-error' : '' }}">
                        <label for="service_tax" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Service Tax</label>
                            <input class="form-control mb-2" name="service_tax" type="number" id="service_tax" value="{{ old('service_tax', optional($branchBill)->service_tax) }}" placeholder="Enter service tax here...">
                            {!! $errors->first('service_tax', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('recover_payment') ? 'has-error' : '' }}">
                        <label for="recover_payment" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Recover Payment</label>
                            <input class="form-control mb-2" name="recover_payment" type="number" id="recover_payment" value="{{ old('recover_payment', optional($branchBill)->recover_payment) }}" placeholder="Enter recover payment here...">
                            {!! $errors->first('recover_payment', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('other_amt') ? 'has-error' : '' }}">
                        <label for="other_amt" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Other Amt</label>
                            <input class="form-control mb-2" name="other_amt" type="number" id="other_amt" value="{{ old('other_amt', optional($branchBill)->other_amt) }}" placeholder="Enter other amt here...">
                            {!! $errors->first('other_amt', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('extra_exp') ? 'has-error' : '' }}">
                        <label for="extra_exp" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Extra Exp</label>
                            <input class="form-control mb-2" name="extra_exp" type="number" id="extra_exp" value="{{ old('extra_exp', optional($branchBill)->extra_exp) }}" placeholder="Enter extra exp here...">
                            {!! $errors->first('extra_exp', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('service_charge') ? 'has-error' : '' }}">
                        <label for="service_charge" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Service Charge</label>
                            <input class="form-control mb-2" name="service_charge" type="number" id="service_charge" value="{{ old('service_charge', optional($branchBill)->service_charge) }}" placeholder="Enter service charge here...">
                            {!! $errors->first('service_charge', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('tds') ? 'has-error' : '' }}">
                        <label for="tds" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Tds</label>
                            <input class="form-control mb-2" name="tds" type="number" id="tds" value="{{ old('tds', optional($branchBill)->tds) }}" placeholder="Enter tds here...">
                            {!! $errors->first('tds', '<p class="help-block">:message</p>') !!}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>