<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            @if(empty($advanceSalary->id))
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="month" id="monthyear" value="{{ old('monthyear', optional($advanceSalary)->monthyear) }}" required="true" placeholder="Enter monthyear here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label for="branch_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch</label>
                    <select id="branch_dropdown" name="branch_id" data-control="select2" class="form-control @error('branch_id') is-invalid @enderror form-select mb-2 mb-5">
                        <option value="" selected disabled class="">Select branch</option>
                        @foreach ($branches as $data)
                        <option value="{{ trim($data->id) }}">{{ $data->name}}</option>
                        @endforeach
                    </select>
                    <span class="text-danger">
                        @error('branch_id')
                        *{{ $message }}
                        @enderror
                    </span>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <label for="employee_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee</label>
                    <select id="employee_dropdown" name="employee_id[]" data-control="select2" class="form-control @error('employee_id') is-invalid @enderror form-select mb-2 mb-5" data-placeholder="Select employee" multiple="multiple">

                    </select>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('wages_payable') ? 'has-error' : '' }}">
                        <label for="wages_payable" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Wages Payable</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="wages_payable" type="number" id="wages_payable" value="{{ old('wages_payable', optional($advanceSalary)->wages_payable) }}" required="true" placeholder="Enter wages payable here...">
                        </div>
                    </div>
                </div>
            </div>
            @else
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('monthyear') ? 'has-error' : '' }}">
                        <label for="monthyear" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Monthyear</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="monthyear" type="text" id="monthyear" value="{{ $advanceSalary->monthyear }}" placeholder="Enter monthyear here..." readonly>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Branch </label>
                    <input class="form-control mb-2" name="branch_id" type="text" id="branch_id" value="{{ old('branch_id', optional($advanceSalary)->branch->name) }}" disabled>
                </div>
            </div>
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <label class="required form-label fs-6 fw-bolder text-black-700 mb-3">Employee </label>
                    <input class="form-control mb-2" name="employee_id" type="text" id="employee_id" value="{{ $advanceSalary->employee_id }}" hidden>
                    <input class="form-control mb-2" name="employee" type="text" id="employee_id" value="{{ old('employee_id', optional($advanceSalary)->employee->full_name) }}" readonly>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('wages_payable') ? 'has-error' : '' }}">
                        <label for="wages_payable" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Wages Payable</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="wages_payable" type="number" id="wages_payable" value="{{ old('wages_payable', optional($advanceSalary)->wages_payable) }}" required="true" placeholder="Enter wages payable here...">
                        </div>
                    </div>
                </div>
            </div>
            @endif

            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('purpose_of_advance') ? 'has-error' : '' }}">
                        <label for="purpose_of_advance" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Purpose Of Advance</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="purpose_of_advance" cols="3" rows="2" id="purpose_of_advance" required="true" placeholder="Enter purpose of advance here...">{{ old('purpose_of_advance', optional($advanceSalary)->purpose_of_advance) }}</textarea>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('remarks') ? 'has-error' : '' }}">
                        <label for="remarks" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Remarks</label>
                        <div class="col-md-12">
                            <textarea class="form-control mb-2" name="remarks" cols="3" rows="2" id="remarks" required="true" placeholder="Enter remarks here...">{{ old('remarks', optional($advanceSalary)->remarks) }}</textarea>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row gx-10 mb-5">
            @if(empty($advanceSalary->id))
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('date_of_advance') ? 'has-error' : '' }}">
                        <label for="date_of_advance" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Date Of Advance</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="date_of_advance" type="date" id="date_of_advance" value="{{ now()->format('Y-m-d') }}" required="true" placeholder="Enter date of advance here...">
                        </div>
                    </div>
                </div>
                @else
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('date_of_advance') ? 'has-error' : '' }}">
                        <label for="date_of_advance" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Date Of Advance</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="date_of_advance" type="date" id="date_of_advance" value="{{ old('date_of_advance', optional($advanceSalary)->date_of_advance) }}" required="true" placeholder="Enter date of advance here...">
                        </div>
                    </div>
                </div>
                @endif
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('advance_for') ? 'has-error' : '' }}">
                        <label for="advance_for" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Advance For</label>
                        <div class="col-md-12">
                            <select name="advance_for" id="advance_for" aria-label="Select a advance for" data-control="select2" data-placeholder="Select advance for" class="form-select" required="true" onchange="toggleCompanyField()">
                                <option value="" style="display: none;" {{ old('advance_for', optional($advanceSalary)->advance_for ?: '') == '' ? 'selected' : '' }} disabled selected>Select advance for</option>
                                @foreach (['Advance - 1' => 'Advance - 1','Advance - 2' => 'Advance - 2'] as $key => $text)
                                <option value="{{ $key }}" {{ old('advance_for', optional($advanceSalary)->advance_for) == $key ? 'selected' : '' }}>
                                    {{ $text }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>      

            <div class="row gx-10 mb-5" id="advance2Fields" >
                <div class="col-lg-6 mb-3">
                    <div class="form-group {{ $errors->has('stock_category_id') ? 'has-error' : '' }}">
                        <label for="stock_category_id" class="form-label fs-6 fw-bolder text-black-700 mb-3">Stock Category</label>
                        <div class="col-md-12">
                            <select name="stock_category_id" aria-label="Select a stock category" data-control="select2" data-placeholder="Select stock category" class="form-select">
                                <option value="" style="display: none;" {{ old('stock_category_id', optional($advanceSalary)->stock_category_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select stock category</option>
                                @foreach ($stockCategories as $key => $stockCategory)
                                <option value="{{ $key }}" {{ old('stock_category_id', optional($advanceSalary)->stock_category_id) == $key ? 'selected' : '' }}>
                                    {{ $stockCategory }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('no_of_qty') ? 'has-error' : '' }}">
                        <label for="no_of_qty" class="form-label fs-6 fw-bolder text-black-700 mb-3">No Of Qty</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="no_of_qty" type="number" id="no_of_qty" value="{{ old('no_of_qty', optional($advanceSalary)->no_of_qty) }}" placeholder="Enter no of qty here...">
                        </div>
                    </div>
                </div>
            </div>

            <div class="row gx-10 mb-5">
               <div class="col-lg-6" id="amountField">
                    <div class="form-group {{ $errors->has('amount') ? 'has-error' : '' }}">
                        <label for="amount" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Amount</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="amount" type="number" id="amount" value="{{ old('amount', optional($advanceSalary)->amount) }}" required="true" placeholder="Enter amount here...">
                        </div>
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</div>

<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

<script>
    $(document).ready(function() {
        $("#branch_dropdown").on("change", function() {
            var idBranch = this.value;
            $("#employee_dropdown").html("");
            $.ajax({
                url: "{{url('admin/fetch_employee')}}",
                type: "POST",
                data: {
                    branch_id: idBranch,
                    _token: "{{csrf_token()}}",
                },
                dataType: "json",
                success: function(result) {
                    $.each(result.Employees, function(key, value) {
                        $("#employee_dropdown").append(
                            '<option value="' +
                            value.id +
                            '">' +
                            value.full_name +
                            "</option>"
                        );
                    });
                },
            });
        });
    });
</script>

<!-- <script>
    function toggleCompanyField() {
        const selectedValue = document.getElementById("advance_for").value;
        const amountField = document.getElementById("amountField");
        const advance2Fields = document.getElementById("advance2Fields");
        if (selectedValue === 'Advance - 1') {
            amountField.style.display = 'block';
            advance2Fields.style.display = 'none';
        } else if (selectedValue === 'Advance - 2') {
            amountField.style.display = 'none';
            advance2Fields.style.display = 'block';
        }
    }
</script> -->




