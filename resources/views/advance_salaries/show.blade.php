@extends('layouts.app')

@section('content')

<div class="panel panel-default">
    <div class="panel-heading clearfix">

        <span class="pull-left">
            <h4 class="mt-5 mb-5">{{ isset($title) ? $title : 'Advance Salary' }}</h4>
        </span>

        <div class="pull-right">

            <form method="POST" action="{!! route('advance_salaries.advance_salary.destroy', $advanceSalary->id) !!}" accept-charset="UTF-8">
            <input name="_method" value="DELETE" type="hidden">
            {{ csrf_field() }}
                <div class="btn-group btn-group-sm" role="group">
                    <a href="{{ route('advance_salaries.advance_salary.index') }}" class="btn btn-primary" title="Show All Advance Salary">
                        <span class="glyphicon glyphicon-th-list" aria-hidden="true"></span>
                    </a>

                    <a href="{{ route('advance_salaries.advance_salary.create') }}" class="btn btn-success" title="Create New Advance Salary">
                        <span class="glyphicon glyphicon-plus" aria-hidden="true"></span>
                    </a>
                    
                    <a href="{{ route('advance_salaries.advance_salary.edit', $advanceSalary->id ) }}" class="btn btn-primary" title="Edit Advance Salary">
                        <span class="glyphicon glyphicon-pencil" aria-hidden="true"></span>
                    </a>

                    <button type="submit" class="btn btn-danger" title="Delete Advance Salary" onclick="return confirm(&quot;Click Ok to delete Advance Salary.?&quot;)">
                        <span class="glyphicon glyphicon-trash" aria-hidden="true"></span>
                    </button>
                </div>
            </form>

        </div>

    </div>

    <div class="panel-body">
        <dl class="dl-horizontal">
            <dt>Monthyear</dt>
            <dd>{{ $advanceSalary->monthyear }}</dd>
            <dt>Branch</dt>
            <dd>{{ optional($advanceSalary->branch)->name }}</dd>
            <dt>Employee</dt>
            <dd>{{ optional($advanceSalary->employee)->full_name }}</dd>
            <dt>Wages Payable</dt>
            <dd>{{ $advanceSalary->wages_payable }}</dd>
            <dt>Date Of Advance</dt>
            <dd>{{ $advanceSalary->date_of_advance }}</dd>
            <dt>Amount</dt>
            <dd>{{ $advanceSalary->amount }}</dd>
            <dt>Purpose Of Advance</dt>
            <dd>{{ $advanceSalary->purpose_of_advance }}</dd>
            <dt>Remarks</dt>
            <dd>{{ $advanceSalary->remarks }}</dd>
            <dt>Stock Category</dt>
            <dd>{{ optional($advanceSalary->stockCategory)->stock_category_name }}</dd>
            <dt>No Of Qty</dt>
            <dd>{{ $advanceSalary->no_of_qty }}</dd>
            <dt>Advance For</dt>
            <dd>{{ $advanceSalary->advance_for }}</dd>

        </dl>

    </div>
</div>

@endsection