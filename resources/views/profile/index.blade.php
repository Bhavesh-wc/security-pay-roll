@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Profile Management</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                            <li class="breadcrumb-item text-muted">Profile</li>
                        </ul>
                    </div>
                </div>
            </div>
			<div id="kt_app_content" class="app-content  flex-column-fluid ">
		    	<div id="kt_app_content_container" class="app-container  container-xxl ">
		    		@if(Session::has('success_message'))
                        <div class="alert alert-success alert-dismissible fade show" role="alert">
                            <strong>{!! \Session::get('success_message') !!}</strong>
                            <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif

					@if(Session::has('error'))
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            <strong>{!! \Session::get('error') !!}</strong>
                            <button type="button" class="btn-close btn-sm" data-bs-dismiss="alert" aria-label="Close"></button>
                        </div>
                    @endif
					<div class="card mb-5 mb-xl-10">
		    			<div class="card-body pt-9 pb-0">
				        	<div class="d-flex flex-wrap flex-sm-nowrap">
					            <div class="me-7 mb-4">
					                <div class="symbol symbol-100px symbol-lg-160px symbol-fixed position-relative">
					                    <img src="{{asset('storage').'/'.auth()->user()->profile_photo}}" alt="image">
					                    <div class="position-absolute translate-middle bottom-0 start-100 mb-6 bg-success rounded-circle border border-4 border-body h-20px w-20px"></div>
					                </div>
					            </div>
			            		<div class="flex-grow-1">
				                	<div class="d-flex justify-content-between align-items-start flex-wrap mb-2">
			                    		<div class="d-flex flex-column">
				                       		<div class="d-flex align-items-center mb-2">
			                            		<a href="#" class="text-gray-900 text-hover-primary fs-2 fw-bold me-1">{{ Auth::user()->name }}</a>
			                         		</div>
			                        		<div class="d-flex flex-wrap fw-semibold fs-6 mb-4 pe-2">
			                        			<a href="#" class="d-flex align-items-center text-gray-400 text-hover-primary mb-2">{{ Auth::user()->email }}</a>
			                        		</div>
			                        	</div>
			               			</div>
			            		</div>
			        		</div>
			        		<ul class="nav nav-stretch nav-line-tabs nav-line-tabs-2x border-transparent fs-5 fw-bold">
			        			<li class="nav-item mt-2">
				                    <a class="nav-link text-active-primary ms-0 me-10 py-5 active" href="#vir_profile_update">
				                        Profile Update</a>
				                </li>
				                <li class="nav-item mt-2">
				                    <a class="nav-link text-active-primary ms-0 me-10 py-5 " href="#change_password">
				                        Change Password</a>
				                </li>		                
			               	</ul>
		    			</div>
					</div>
					<div class="card mb-5 mb-xl-10">
		    			<div id="vir_profile_update" role="tabpanel">
		        			<form id="vir_profile_update" method="POST" action="{{ route('profile.update', $user['id']) }}" enctype="multipart/form-data">
		        				@csrf
					        	@method('PUT')
		            			<div class="card-body border-top p-9">
					                <div class="row mb-6">
					                    <label class="col-lg-4 col-form-label fw-semibold fs-6">Profile Image</label>
					                    <div class="col-lg-8">
					                        <div class="image-input image-input-outline" data-kt-image-input="true"  style="background-image: url({{ asset('admin/src/media/avatars/blank.png') }})">
					                            <div class="image-input-wrapper w-125px h-125px" style="background-image: {{ isset($user) && auth()->user()->profile_photo ? 'url('.asset('storage/'. auth()->user()->profile_photo).')' : 'none' }};"></div>
					                            <label class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="change" data-bs-toggle="tooltip" aria-label="Change avatar" data-bs-original-title="Change avatar" data-kt-initialized="1">
					                                <i class="ki-duotone ki-pencil fs-7"><span class="path1"></span><span class="path2"></span></i>
					                                <input type="file" name="profile_photo" accept=".png, .jpg, .jpeg">
					                                <input type="hidden" name="custom_delete_profile_photo">
					                            </label>
					                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="cancel" data-bs-toggle="tooltip" aria-label="Cancel avatar" data-bs-original-title="Cancel avatar" data-kt-initialized="1">
					                                <i class="ki-duotone ki-cross fs-2"><span class="path1"></span><span class="path2"></span></i>                            </span>
					                            <span class="btn btn-icon btn-circle btn-active-color-primary w-25px h-25px bg-body shadow" data-kt-image-input-action="remove" data-bs-toggle="tooltip" aria-label="Remove avatar" data-bs-original-title="Remove avatar" data-kt-initialized="1">
					                                <i class="ki-duotone ki-cross fs-2"><span class="path1"></span><span class="path2"></span></i>                            </span>
					                       	</div>
					                        <div class="form-text">Allowed file types:  png, jpg, jpeg.</div>
					                    </div>
					                </div>
					                <div class="row mb-6">
										<label class="col-lg-4 col-form-label required fw-semibold fs-6">Name</label>
										<div class="col-lg-8">
											<div class="row">
												<!--begin::Col-->
												<div class="col-lg-8 fv-row">
													<input type="text" name="name" class="form-control form-control-lg form-control-solid" placeholder="Your Name" value="{{ old('name', auth()->user()->name ?? '') }}" />
												</div>
											</div>
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Email</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="email" class="form-control form-control-lg form-control-solid" placeholder="Email Address" value="{{ old('email', auth()->user()->email ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Company Code</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="company_code" class="form-control form-control-lg form-control-solid" placeholder="Company Code" value="{{ old('company_code', auth()->user()->company_code ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Contact Person Name</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="contact_person_name" class="form-control form-control-lg form-control-solid" placeholder="Contact Person Name" value="{{ old('contact_person_name', auth()->user()->contact_person_name ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">
											Contact Phone
										</label>
										<div class="col-lg-8 fv-row">
											<input type="number" name="contact_person_phone_no" class="form-control form-control-lg form-control-solid" placeholder="Phone number" value="{{ old('contact_person_phone_no', auth()->user()->contact_person_phone_no ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">GST No</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="gst_no" class="form-control form-control-lg form-control-solid" placeholder="GST No" value="{{ old('gst_no', auth()->user()->gst_no ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">PF No</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="pf_no" class="form-control form-control-lg form-control-solid" placeholder="PF No" value="{{ old('pf_no', auth()->user()->pf_no ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Esic</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="esic" class="form-control form-control-lg form-control-solid" placeholder="Esic" value="{{ old('esic', auth()->user()->esic ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Police Registration Number</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="police_registration_number" class="form-control form-control-lg form-control-solid" placeholder="Police Registration Number" value="{{ old('police_registration_number', auth()->user()->police_registration_number ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Shop And Establishment</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="shop_and_establishment" class="form-control form-control-lg form-control-solid" placeholder="Shop And Establishment" value="{{ old('shop_and_establishment', auth()->user()->shop_and_establishment ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Professional Tax</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="professional_tax" class="form-control form-control-lg form-control-solid" placeholder="Professional Tax" value="{{ old('professional_tax', auth()->user()->professional_tax ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">MSME Registaration No</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="msme_registaration_no" class="form-control form-control-lg form-control-solid" placeholder="MSME Registaration No" value="{{ old('msme_registaration_no', auth()->user()->msme_registaration_no ?? '') }}" />
										</div>
									</div>
									<div class="row mb-6">
										<label class="col-lg-4 col-form-label fw-semibold fs-6">Welfare Fund</label>
										<div class="col-lg-8 fv-row">
											<input type="text" name="welfare_fund" class="form-control form-control-lg form-control-solid" placeholder="Welfare Fund" value="{{ old('welfare_fund', auth()->user()->welfare_fund ?? '') }}" />
										</div>
									</div>
		            			</div>
					            <div class="card-footer d-flex justify-content-end py-6 px-9">
					                <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
					            </div>
		        				<input type="hidden">
		        			</form>
		    			</div>
					</div>
					<div class="card mb-5 mb-xl-10">
		    			<div class="card-header border-0 cursor-pointer">
					        <div class="card-title m-0">
					            <h3 class="fw-bold m-0">Change Passowrd</h3>
					        </div>
		    			</div>
		    			<div id="change_password" role="tabpanel">
		        			<form method="POST" action="{{ route('profile.change', $user['id']) }}">
		        				@csrf
		            			<div class="card-body border-top p-9">
					                <div class="row mb-6">
					                    <label class="col-lg-4 col-form-label required fw-semibold fs-6">New Password</label>
					                    <div class="col-lg-8 fv-row fv-plugins-icon-container">
					                    	<input type="password" name="password" value="" class="form-control form-control-lg form-control-solid"placeholder="Password" required />
					                    		<span class="text-danger">
						                            @error('password')
						                              *{{ $message }}
						                            @enderror
					                          	</span>
					                	</div>
					                </div>
					                <div class="row mb-6">
					                    <label class="col-lg-4 col-form-label required fw-semibold fs-6">Confirm Password</label>
					                    <div class="col-lg-8 fv-row fv-plugins-icon-container">
					                    	<input type="password" name="confirm-password" value="" class="form-control form-control-lg form-control-solid" placeholder="Confirm-Password" />
					                    		<span class="text-danger">
						                            @error('password')
						                              *{{ $message }}
						                            @enderror
					                          	</span>
					                	</div>
					                </div>
		            			</div>
					            <div class="card-footer d-flex justify-content-end py-6 px-9">
					                <button type="submit" class="btn btn-sm btn-primary">Save Changes</button>
					            </div>
		        				<input type="hidden">
		        			</form>
		    			</div>
					</div>
		        </div>
		    </div>
		</div>
	</div>
</div>
@include('layouts.partials.footer')
