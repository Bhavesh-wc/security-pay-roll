
<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('walfare_amount') ? 'has-error' : '' }}">
                        <label for="walfare_amount" class="form-label fs-6 fw-bolder text-black-700 mb-3">Welfare Amount</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="walfare_amount" type="number" id="walfare_amount" value="{{ old('walfare_amount', optional($walfer)->walfare_amount) }}" placeholder="Enter Walfer Amount here...">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>