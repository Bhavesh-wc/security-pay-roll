@include('layouts.partials.header')
<div class="app-main flex-column flex-row-fluid" id="kt_app_main">
    <div class="app-main flex-column flex-row-fluid" id="kt_app_main">
        <div class="d-flex flex-column flex-column-fluid">
            <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
                <div id="kt_app_toolbar_container" class="app-container container-xxl d-flex flex-stack">
                    <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                        <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">Masters List</h1>
                        <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                            <li class="breadcrumb-item text-muted">
                                <a href="{{ route('home')}}" class="text-muted text-hover-primary">Home</a>
                            </li>
                            <li class="breadcrumb-item">
                                <span class="bullet bg-gray-400 w-5px h-2px"></span>
                            </li>
                           <li class="breadcrumb-item text-muted">Masters</li>
                        </ul>
                    </div>
                </div>
            </div>
            <div id="kt_app_content" class="app-content  flex-column-fluid ">
            	<div id="kt_app_content_container" class="app-container  container-xxl ">
	                <div class="row g-5 g-xl-10">
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('banks.bank.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #EBEDC6" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Bank</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('banks')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('banks')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />                  
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('banks')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('bank_branches.bank_branch.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #E1EAF3" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Bank-Branch</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('bank_branches')->count(); @endphp
	                                            <span  class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('bank_branches')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('bank_branches')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('departments.department.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #F5DDD9" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Department</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('departments')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('departments')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('departments')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('designations.designation.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #BBEFF3" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Designation</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('designations')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('designations')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('designations')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('qualifications.qualification.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #BBEFF3" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Qualification</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('qualifications')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('qualifications')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('qualifications')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('mother_tongues.mother_tongue.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #F5DDD9" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Mother-Tongue</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('mother_tongues')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('mother_tongues')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('mother_tongues')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('grades.grade.index') }}"  class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #E1EAF3" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Grade</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('grades')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('grades')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('grades')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                </div>
	                <div class="row g-5 g-xl-10">
	                	<div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('countries.country.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #EBEDC6" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Country</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('countries')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('countries')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('countries')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('states.state.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #E1EAF3" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">State</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('states')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('states')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('states')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('districts.district.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #F5DDD9" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">District</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('districts')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('districts')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('districts')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('cities.city.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #BBEFF3" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">City</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('cities')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('cities')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('cities')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('pincodes.pincode.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #BBEFF3" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Pincode</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('pincodes')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('pincodes')->where('status','=','active')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>    
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('pincodes')->where('status','=','inactive')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Inactive</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
						<div class="col-md-3 mb-xl-5">              
							<div class="card card-flush h-xl-100">   
								<a href="{{ route('stock_categories.stock_category.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #E1EAF3" data-bs-theme="light">
									<h3 class="card-title align-items-start flex-column text-dark pt-5">
										<span class="fw-bold fs-1x">Stock Category</span>
										<div class="fs-4 text-dark mt-2">
											<span class="opacity-75">Total :</span>
											<span class="position-relative d-inline-block">
												@php $count = DB::table('stock_categories')->count(); @endphp
												<span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
												<span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
											</span>
										</div>    
									</h3>
								</a>
								<div class="card-body mt-n20">
									<div class="mt-n20 position-relative">
										<div class="row g-3 g-lg-6">
											<div class="col-6">
												<div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
													<div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
													<div class="m-0">
														@php $count = DB::table('stock_categories')->where('status','=','active')->count(); @endphp
														<span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
														<span class="text-gray-500 fw-semibold fs-7">Active</span>
													</div>
												</div>    
											</div>    
											<div class="col-6">
												<div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
													<div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
													<div class="m-0">
														@php $count = DB::table('stock_categories')->where('status','=','inactive')->count(); @endphp
														<span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
														<span class="text-gray-500 fw-semibold fs-7">Inactive</span>
													</div>
												</div>    
											</div>          
										</div>
									</div>
								</div>    
							</div>
						</div>
						<div class="col-md-3 mb-xl-5">              
							<div class="card card-flush h-xl-100">   
								<a href="{{ route('tax_managers.tax_manager.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #F5DDD9" data-bs-theme="light">
									<h3 class="card-title align-items-start flex-column text-dark pt-5">
										<span class="fw-bold fs-1x">Tax-Manager</span>
										<div class="fs-4 text-dark mt-2">
											<span class="opacity-75">Total :</span>
											<span class="position-relative d-inline-block">
												@php $count = DB::table('tax_managers')->count(); @endphp
												<span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
												<span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
											</span>
										</div>    
									</h3>
								</a>
								<div class="card-body mt-n20">
									<div class="mt-n20 position-relative">
										<div class="row g-3 g-lg-6">
											<div class="col-6">
												<div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
													<div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
													<div class="m-0">
														@php $count = DB::table('tax_managers')->where('status','=','active')->count(); @endphp
														<span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
														<span class="text-gray-500 fw-semibold fs-7">Active</span>
													</div>
												</div>    
											</div>    
											<div class="col-6">
												<div class="bg-gray-100 bg-opacity-70 rounded-2 px-4 py-5">
													<div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/inactive.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
													<div class="m-0">
														@php $count = DB::table('tax_managers')->where('status','=','inactive')->count(); @endphp
														<span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
														<span class="text-gray-500 fw-semibold fs-7">Inactive</span>
													</div>
												</div>    
											</div>          
										</div>
									</div>
								</div>    
							</div>
						</div>
						<div class="col-md-3 mb-xl-5">              
							<div class="card card-flush h-xl-100">   
								<a href="{{ route('walfers.walfer.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #EBEDC6" data-bs-theme="light">
									<h3 class="card-title align-items-start flex-column text-dark pt-5">
										<span class="fw-bold fs-1x">Welfare Fund</span>
										<div class="fs-4 text-dark mt-2">
											<span class="opacity-75">Total :</span>
											<span class="position-relative d-inline-block">
												@php $count = DB::table('walfers')->count(); @endphp
												<span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
												<span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
											</span>
										</div>    
									</h3>
								</a>
								<div class="card-body mt-n20">
									<div class="mt-n20 position-relative">
										<div class="row g-3 g-lg-6">
											<div class="col-6">
												<div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
													<div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
													<div class="m-0">
														@php $count = DB::table('walfers')->count(); @endphp
														<span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
														<span class="text-gray-500 fw-semibold fs-7">Active</span>
													</div>
												</div>    
											</div>        
										</div>
									</div>
								</div>    
							</div>
						</div> 
	                </div>
	                <div class="row g-5 g-xl-10">
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('advance_salaries.advance_salary.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #BBEFF3" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Advance-Salary</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('advance_salaries')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('advance_salaries')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>         
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('over_times.over_time.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #EBEDC6" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Over-Time</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('over_times')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('over_times')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('damages.damage.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #F5DDD9" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">damage & Loss</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('damages')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('damages')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                    <div class="col-md-3 mb-xl-5">              
	                        <div class="card card-flush h-xl-100">   
	                            <a href="{{ route('fines.fine.index') }}" class="card-header rounded bgi-no-repeat bgi-size-cover bgi-position-y-top bgi-position-x-center align-items-start h-175px" style="background-color: #E1EAF3" data-bs-theme="light">
	                                <h3 class="card-title align-items-start flex-column text-dark pt-5">
	                                    <span class="fw-bold fs-1x">Fine</span>
	                                    <div class="fs-4 text-dark mt-2">
	                                        <span class="opacity-75">Total :</span>
	                                        <span class="position-relative d-inline-block">
	                                            @php $count = DB::table('fines')->count(); @endphp
	                                            <span class="link-dark opacity-75-hover fw-bold d-block mb-1">{{$count}}</span>
	                                            <span class="position-absolute opacity-50 bottom-0 start-0 border-2 border-body border-bottom w-100"></span>
	                                        </span>
	                                    </div>    
	                                </h3>
	                            </a>
	                            <div class="card-body mt-n20">
	                                <div class="mt-n20 position-relative">
	                                    <div class="row g-3 g-lg-6">
	                                        <div class="col-6">
	                                            <div class="bg-gray-100 bg-opacity-70 rounded-4 px-4 py-5">
	                                                <div class="symbol symbol-30px me-5 mb-5">
														<img src="{{ asset('/images/active.png') }}" class="w-20px h-20px mb-2" alt="" />               
													</div>
	                                                <div class="m-0">
	                                                    @php $count = DB::table('fines')->count(); @endphp
	                                                    <span class="text-gray-700 fw-bolder d-block fs-5 lh-1 ls-n1 mb-1">{{$count}}</span>
	                                                    <span class="text-gray-500 fw-semibold fs-7">Active</span>
	                                                </div>
	                                            </div>    
	                                        </div>          
	                                    </div>
	                                </div>
	                            </div>    
	                        </div>
	                    </div>
	                </div>		
	            </div>
	        </div>
	    </div>
	</div>
</div>
@include('layouts.partials.footer')
