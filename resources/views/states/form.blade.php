
<div class="mb-0">
    <div class="card card-flush py-4">
        <div class="card-body pt-0">
            <div class="row gx-10 mb-5">
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('state_name') ? 'has-error' : '' }}">
                        <label for="state_name" class="required form-label fs-6 fw-bolder text-black-700 mb-3">State Name</label>
                        <div class="col-md-12">
                            <input class="form-control mb-2" name="state_name" type="text" id="state_name" value="{{ old('state_name', optional($state)->state_name) }}" required="true" placeholder="Enter state name here...">
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('country_id') ? 'has-error' : '' }}">
                        <label for="country_id" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Country</label>
                        <div class="col-md-12">
                            <select name="country_id" id="country_id" aria-label="Select a country" data-control="select2" data-placeholder="Select country" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('country_id', optional($state)->country_id ?: '') == '' ? 'selected' : '' }} disabled selected>Select country</option>
                            @foreach ($countries as $key => $country)
                                <option value="{{ $key }}" {{ old('country_id', optional($state)->country_id) == $key ? 'selected' : '' }}>
                                    {{ $country }}
                                </option>
                            @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="form-group {{ $errors->has('status') ? 'has-error' : '' }}">
                        <label for="status" class="required form-label fs-6 fw-bolder text-black-700 mb-3">Status</label>
                        <div class="col-md-12">
                            <select name="status" aria-label="Select a status" data-control="select2" data-placeholder="Select status" class="form-select mb-2">
                                <option value="" style="display: none;" {{ old('status', optional($state)->status ?: '') == '' ? 'selected' : '' }} disabled selected>Select Status</option>
                                @foreach (['active' => 'Active','inactive' => 'Inactive'] as $key => $text)
                                <option value="{{ $key }}" {{ old('status', optional($state)->status) == $key ? 'selected' : '' }}>
                                    {{ $text }}
                                </option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>